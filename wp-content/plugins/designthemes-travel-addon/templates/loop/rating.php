<?php
/**
 * Product loop thumbnail
 *
 * @author  designthemes
 * @package designthemes-travel-addon/templates
 * @version 1.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit();
}

global $post;
$rating = dt_travel_room_average_rating( $post->ID );
?>

<?php if ( comments_open( $post->ID ) ): ?>

	<div class="rating-wrapper">

        <div class="rating">
            <?php if ( $rating ) : ?>

                <?php if ( $rating ): ?>

                    <div class="star-rating" title="<?php echo sprintf( esc_attr__( 'Rated %0.2f out of 5', 'designthemes-travel' ), $rating ) ?>">
                        <span style="width:<?php echo ( $rating / 5 ) * 100; ?>%"></span>
                    </div>

                <?php endif; ?>

            <?php endif; ?>
        </div>
    
    	<div class="comment-count">
        	<?php echo dt_travel_get_review_count( $post->ID ).' '.esc_html__('Reviews', 'designthemes-travel'); ?>
        </div>

    </div>
<?php endif; ?>