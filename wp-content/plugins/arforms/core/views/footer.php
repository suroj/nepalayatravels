<script type="text/javascript" data-cfasync="false">

    var __ARFPAGELABELTEXT = "<?php echo addslashes(__('Page Label', 'ARForms')); ?>";
    var __ARFSECONDPAGELABEL = "<?php echo addslashes(__('Second Page Label', 'ARForms')); ?>";
    var __ARFPAGELABELARRAY = new Array("", "<?php echo addslashes(__('First', 'ARForms')); ?>", "<?php echo addslashes(__('Second', 'ARForms')); ?>",
            "<?php echo addslashes(__('Third', 'ARForms')); ?>", "<?php echo addslashes(__('Fourth', 'ARForms')); ?>",
            "<?php echo addslashes(__('Fifth', 'ARForms')); ?>", "<?php echo addslashes(__('Sixth', 'ARForms')); ?>",
            "<?php echo addslashes(__('Seventh', 'ARForms')); ?>", "<?php echo addslashes(__('Eighth', 'ARForms')); ?>",
            "<?php echo addslashes(__('Ninth', 'ARForms')); ?>", "<?php echo addslashes(__('Tenth', 'ARForms')); ?>",
            "<?php echo addslashes(__('Eleventh', 'ARForms')); ?>", "<?php echo addslashes(__('Twelfth', 'ARForms')); ?>",
            "<?php echo addslashes(__('Thirteenth', 'ARForms')); ?>", "<?php echo addslashes(__('Fourteenth', 'ARForms')); ?>",
            "<?php echo addslashes(__('Fifteenth', 'ARForms')); ?>", "<?php echo addslashes(__('Sixteenth', 'ARForms')); ?>",
            "<?php echo addslashes(__('Seventeenth', 'ARForms')); ?>", "<?php echo addslashes(__('Eighteenth', 'ARForms')); ?>",
            "<?php echo addslashes(__('Nineteenth', 'ARForms')); ?>", "<?php echo addslashes(__('Twentieth', 'ARForms')); ?>",
            "<?php echo addslashes(__('Twenty First', 'ARForms')); ?>", "<?php echo addslashes(__('Twenty Second', 'ARForms')); ?>",
            "<?php echo addslashes(__('Twenty Third', 'ARForms')); ?>", "<?php echo addslashes(__('Twenty Fourth', 'ARForms')); ?>",
            "<?php echo addslashes(__('Twenty Fifth', 'ARForms')); ?>", "<?php echo addslashes(__('Twenty Sixth', 'ARForms')); ?>",
            "<?php echo addslashes(__('Twenty Seventh', 'ARForms')); ?>", "<?php echo addslashes(__('Twenty Eighth', 'ARForms')); ?>",
            "<?php echo addslashes(__('Twenty Ninth', 'ARForms')); ?>", "<?php echo addslashes(__('Thirtieth', 'ARForms')); ?>",
            "<?php echo addslashes(__('Thirty First', 'ARForms')); ?>", "<?php echo addslashes(__('Thirty Second', 'ARForms')); ?>",
            "<?php echo addslashes(__('Thirty Third', 'ARForms')); ?>", "<?php echo addslashes(__('Thirty Fourth', 'ARForms')); ?>",
            "<?php echo addslashes(__('Thirty Fifth', 'ARForms')); ?>", "<?php echo addslashes(__('Thirty Sixth', 'ARForms')); ?>",
            "<?php echo addslashes(__('Thirty Seventh', 'ARForms')); ?>", "<?php echo addslashes(__('Thirty Eighth', 'ARForms')); ?>",
            "<?php echo addslashes(__('Thirty Ninth', 'ARForms')); ?>", "<?php echo addslashes(__('Fortieth', 'ARForms')); ?>",
            "<?php echo addslashes(__('Forty First', 'ARForms')); ?>", "<?php echo addslashes(__('Forty Second', 'ARForms')); ?>",
            "<?php echo addslashes(__('Forty Third', 'ARForms')); ?>", "<?php echo addslashes(__('Forty Fourth', 'ARForms')); ?>",
            "<?php echo addslashes(__('Forty Fifth', 'ARForms')); ?>", "<?php echo addslashes(__('Forty Sixth', 'ARForms')); ?>",
            "<?php echo addslashes(__('Forty Seventh', 'ARForms')); ?>", "<?php echo addslashes(__('Forty Eighth', 'ARForms')); ?>",
            "<?php echo addslashes(__('Forty Ninth', 'ARForms')); ?>", "<?php echo addslashes(__('Fiftieth', 'ARForms')); ?>"
            );

    __ARFMAINURL = '<?php echo $arfajaxurl ?>';

    __ARFDEFAULTTITLE = "<?php echo addslashes(__('(Click here to add text)', 'ARForms')); ?>";

    __ARFDEFAULTDESCRIPTION = "<?php echo addslashes(__('(Click here to add description or instructions)', 'ARForms')); ?>";

    __ARFDEFAULTSECTION = "<?php echo addslashes(__('(Blank Section)', 'ARForms')); ?>";

    __ARFDELETEURL = '<?php echo admin_url('admin.php?page=ARForms&err=1'); ?>';


    __ARFINVALID = '<?php global $arfsettings;
echo $arfsettings->blank_msg;
?>';


    __ARFEQUALS = '<?php echo addslashes(__('equals', 'ARForms')); ?>';
    __ARFNOTEQUALS = '<?php echo addslashes(__('not equals', 'ARForms')); ?>';
    __ARFGREATER = '<?php echo addslashes(__('greater than', 'ARForms')); ?>';
    __ARFLESS = '<?php echo addslashes(__('less than', 'ARForms')); ?>';
    __ARFCONTAIN = '<?php echo addslashes(__('contains', 'ARForms')); ?>';
    __ARFNOTCONTAIN = '<?php echo addslashes(__('not contains', 'ARForms')); ?>';
    __ARFADDRULE = '<?php echo addslashes(__('Please add one or more rules', 'ARForms')); ?>';

    __ARFSHOW = '<?php echo addslashes(__('Show','ARForms')); ?>';
    __ARFHIDE = '<?php echo addslashes(__('Hide','ARForms')); ?>';
    __ARFENBALE = '<?php echo addslashes(__('Enable','ARForms')); ?>';
    __ARFDISABLE = '<?php echo addslashes(__('Disable','ARForms')); ?>';
    __ARFSETVALUE = '<?php echo addslashes(__('Set Value of','ARForms')); ?>';

    _ARFRADIOCHKIMGMSG = '<?php echo addslashes(__('Are you sure you want to delete image', 'ARForms')); ?>';

    __ARF_BLANKMSG='<?php echo addslashes(__('This field cannot be blank', 'ARForms'));?>';
    __ARF_DELETE_IMAGE_TEXT = '<?php echo addslashes(__('Please, Select appropriate option','ARForms')).' <br/> '.addslashes(__('to change/delete image','ARForms')); ?>';
    __ARF_ADD_TEXT = '<?php echo addslashes(__('Add','ARForms')); ?>';
    __ARF_DELETE_TEXT = '<?php echo addslashes(__('Delete','ARForms')); ?>';

    __ARF_CL_IF_TEXT = '<?php echo addslashes(__('IF','ARForms')); ?>';
    __ARF_CL_ALL_TEXT = '<?php echo addslashes(__('All','ARForms')); ?>';
    __ARF_CL_ANY_TEXT = '<?php echo addslashes(__('Any','ARForms')); ?>';
    __ARF_CL_THAN_TEXT = '<?php echo addslashes(__('THAN','ARForms')); ?>';

    __ARF_CL_SELECT_FIELD_TEXT = '<?php echo addslashes(__('Select Field','ARForms')); ?>';

    __ARF_IS_TEXT = '<?php echo addslashes(__('is','ARForms')); ?>';

    __ARF_THEN_EMAIL_SEND_TO_TEXT = '<?php echo addslashes(__('Than Mail Send To','ARForms')); ?>';

    __ARF_THEN_REDIRECT_TO_TEXT = '<?php echo addslashes(__('Than Redirect to','ARForms') ); ?>';

    __ARF_UNTITLED_TEXT = '<?php echo addslashes(__('Untitled','ARForms')); ?>';

    __ARF_ADD_IMAGE_TEXT = '<?php echo addslashes(__('Add Image','ARForms')); ?>';

    __ARF_YES_TEXT = '<?php echo addslashes(__('Yes','ARForms')); ?>';

    __ARF_CANCEL_TEXT = '<?php echo addslashes(__('Cancel','ARForms')); ?>';

    __ARF_PAGE_BREAK_TEXT = '<?php echo addslashes(__('Page Break','ARForms')); ?>';

    __ARF_PAGE_ONLY_TEXT = '<?php echo addslashes(__('Page','ARForms')); ?>';

    __ARF_BEGIN_ONLY_TEXT = '<?php echo addslashes(__('begin','ARForms')); ?>';

    __ARF_ADD_ICON_TEXT = '<?php echo addslashes(__('Add Icon','ARForms')); ?>';
  
    var mycolsize = "1col";

jQuery(document).ready(function($){
});

    function arf_update_order(order) {

        jQuery.each(order, function (i) {
            var field_id = order[i].split('_');
            jQuery('#arf_field_order_' + field_id[1]).val(i);
        });
    }

</script>