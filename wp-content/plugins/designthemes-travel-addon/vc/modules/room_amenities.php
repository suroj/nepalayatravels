<?php add_action( 'vc_before_init', 'dt_sc_room_amenities_vc_map' );
function dt_sc_room_amenities_vc_map() {

	vc_map( array(
		"name" => esc_html__( "Room Amenities", 'designthemes-travel' ),
		"base" => "dt_sc_room_amenities",
		"icon" => "dt_sc_room_amenities",
		"category" => DTTRAVEL_VC_CATEGORY,
		"description" => esc_html__("Show list of selected amenities.",'designthemes-travel'),
		"params" => array(

			// Title
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Title', 'designthemes-travel' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter the title of section.', 'designthemes-travel' )
			),

			// Room ID
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Room Id', 'designthemes-travel' ),
				'param_name' => 'room_id',
				'description' => esc_html__( 'Enter the room id, not need in room single post.', 'designthemes-travel' )
			),

			// Count
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Post Counts', 'designthemes-travel' ),
				'param_name' => 'count',
				'description' => esc_html__( 'Enter post count', 'designthemes-travel' ),
				'default' => 6,
				'admin_label' => true
			),

			// Show Icons
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Show Icons','designthemes-travel'),
				'param_name' => 'show_icons',
				'value' => array(
					esc_html__('Yes','designthemes-travel') => 'true',
					esc_html__('No','designthemes-travel') => 'false'
				),
				'std' => 'true'
			)
		)
	) );
}?>