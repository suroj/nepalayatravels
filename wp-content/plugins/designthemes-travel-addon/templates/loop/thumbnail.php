<?php
/**
 * Room loop thumbnail
 *
 * @author  designthemes
 * @package designthemes-travel-addon/templates
 * @version 1.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
global $post;

$room_meta = get_post_meta($post->ID,'_room_settings',TRUE);
$room_meta = is_array($room_meta) ? $room_meta  : array();

// If it's galley
if( $room_meta['room-gallery'] != '' ) : ?>
	<ul class="entry-gallery-post-slider"><?php
		$items = explode(',', $room_meta["room-gallery"]);
		foreach ( $items as $item ) { ?>
			<li><?php echo wp_get_attachment_image( $item, 'full', array( 'class' => 'animate', 'data-animate' => 'fadeIn' ) ); ?></li><?php
		}?>
	</ul><?php
// Else if it's thumbnail	
elseif( has_post_thumbnail() ) : ?>
	<a href="<?php the_permalink();?>" title="<?php printf(esc_attr__('Permalink to %s','designthemes-travel'),the_title_attribute('echo=0'));?>"><?php the_post_thumbnail("full", array( 'class' => 'animate', 'data-animate' => 'fadeIn' ));?></a><?php
endif; ?>