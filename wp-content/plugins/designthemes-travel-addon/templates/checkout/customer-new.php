<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit();
}
?>
<div class="dt-order-new-customer" id="dt-order-new-customer">
    <div class="dt-col-padding dt-col-border">
        <h4><?php esc_html_e( 'New Customer', 'designthemes-travel' ); ?></h4>
        <ul class="dt-form-table dt-sc-one-half column first">
            <li class="dt-form-field">
                <label class="dt-form-field-label"><?php esc_html_e( 'First name', 'designthemes-travel' ); ?>
                    <span class="dt-required">*</span></label>

                <div class="dt-form-field-input">
                    <input type="text" name="first_name" value="<?php echo esc_attr( $customer->first_name ); ?>"
                           placeholder="<?php esc_html_e( 'First name', 'designthemes-travel' ); ?>" required/>
                </div>
            </li>
            <li class="dt-form-field">
                <label class="dt-form-field-label"><?php esc_html_e( 'Last name', 'designthemes-travel' ); ?>
                    <span class="dt-required">*</span></label>

                <div class="dt-form-field-input">
                    <input type="text" name="last_name" value="<?php echo esc_attr( $customer->last_name ); ?>"
                           placeholder="<?php esc_html_e( 'Last name', 'designthemes-travel' ); ?>" required/>
                </div>
            </li>
            <li class="dt-form-field">
                <label class="dt-form-field-label"><?php esc_html_e( 'Address', 'designthemes-travel' ); ?>
                    <span class="dt-required">*</span></label>

                <div class="dt-form-field-input">
                    <input type="text" name="address" value="<?php echo esc_attr( $customer->address ); ?>"
                           placeholder="<?php esc_html_e( 'Address', 'designthemes-travel' ); ?>" required/>
                </div>
            </li>
            <li class="dt-form-field">
                <label class="dt-form-field-label"><?php esc_html_e( 'City', 'designthemes-travel' ); ?>
                    <span class="dt-required">*</span></label>

                <div class="dt-form-field-input">
                    <input type="text" name="city" value="<?php echo esc_attr( $customer->city ); ?>"
                           placeholder="<?php esc_html_e( 'City', 'designthemes-travel' ); ?>" required/>
                </div>
            </li>
            <li class="dt-form-field">
                <label class="dt-form-field-label"><?php esc_html_e( 'State', 'designthemes-travel' ); ?>
                    <span class="dt-required">*</span></label>

                <div class="dt-form-field-input">
                    <input type="text" name="state" value="<?php echo esc_attr( $customer->state ); ?>"
                           placeholder="<?php esc_html_e( 'State', 'designthemes-travel' ); ?>" required/>
                </div>
            </li>
        </ul>
        <ul class="dt-form-table dt-sc-one-half column">
            <li class="dt-form-field">
                <label class="dt-form-field-label"><?php esc_html_e( 'Postal Code', 'designthemes-travel' ); ?>
                    <span class="dt-required">*</span></label>

                <div class="dt-form-field-input">
                    <input type="text" name="postal_code" value="<?php echo esc_attr( $customer->postal_code ); ?>"
                           placeholder="<?php esc_html_e( 'Postal code', 'designthemes-travel' ); ?>" required/>
                </div>
            </li>
            <li class="dt-form-field">
                <label class="dt-form-field-label"><?php esc_html_e( 'Country', 'designthemes-travel' ); ?>
                    <span class="dt-required">*</span></label>

                <div class="dt-form-field-input">
                	<select name="country_name" requird="required"><?php
                    	$countries = dt_travel_countries();
						array_shift($countries);
						echo '<option value="">'.esc_html__('Choose a Country', 'designthemes-travel').'</option>';
						foreach( $countries as $country ):
							echo '<option value="'.esc_attr($country).'" '.selected($country, $customer->country).'>'.esc_attr($country).'</option>';
						endforeach; ?>
                    </select>
                </div>
            </li>
            <li class="dt-form-field">
                <label class="dt-form-field-label"><?php esc_html_e( 'Phone', 'designthemes-travel' ); ?>
                    <span class="dt-required">*</span></label>

                <div class="dt-form-field-input">
                    <input type="text" name="phone_no" value="<?php echo esc_attr( $customer->phone ); ?>"
                           placeholder="<?php esc_html_e( 'Phone Number', 'designthemes-travel' ); ?>" required/>
                </div>
            </li>
            <li class="dt-form-field">
                <label class="dt-form-field-label"><?php esc_html_e( 'Email', 'designthemes-travel' ); ?>
                    <span class="dt-required">*</span></label>

                <div class="dt-form-field-input">
                    <input type="email" name="email_address" value="<?php echo esc_attr( $customer->email ); ?>"
                           placeholder="<?php esc_html_e( 'Email address', 'designthemes-travel' ); ?>" required/>
                </div>
            </li>
            <li class="dt-form-field">
                <label class="dt-form-field-label"><?php esc_html_e( 'Fax', 'designthemes-travel' ); ?></label>

                <div class="dt-form-field-input">
                    <input type="text" name="fax_no" value="<?php echo esc_attr( $customer->fax ); ?>"
                           placeholder="<?php esc_html_e( 'Fax', 'designthemes-travel' ); ?>"/>
                </div>
            </li>
        </ul>
        <input type="hidden" name="existing-customer-id" value=""/>
    </div>
</div>