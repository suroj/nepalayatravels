<?php
if (! class_exists ( 'DTTravelAddonVcModules' )) {

	class DTTravelAddonVcModules {

		function __construct() {

			add_action( 'after_setup_theme', array ( $this, 'dt_travel_map_shortcodes' ) , 1000 );
			add_action( 'admin_enqueue_scripts', array ( $this, 'dt_travel_vc_admin_scripts') );

			add_filter( 'vc_autocomplete_dt_sc_rooms_list_terms_callback', array ( $this, 'dt_travel_vc_autocomplete_terms_field_search' ), 10, 1 );
			add_filter( 'vc_autocomplete_dt_sc_rooms_list_terms_render', array ( $this, 'dt_travel_vc_autocomplete_terms_field_render' ), 10, 1 );

			add_filter( 'vc_autocomplete_dt_sc_packages_list_terms_callback', array ( $this, 'dt_travel_package_vc_autocomplete_terms_field_search' ), 10, 1 );
			add_filter( 'vc_autocomplete_dt_sc_packages_list_terms_render', array ( $this, 'dt_travel_package_vc_autocomplete_terms_field_render' ), 10, 1 );
		}

		function dt_travel_map_shortcodes() {

			define( 'DTTRAVEL_VC_CATEGORY', esc_html__('Travel Addon', 'designthemes-travel') );

			$path = plugin_dir_path ( __FILE__ ) . 'modules/';
			$modules = array(
				'dt_sc_rooms_search_form'		=>		$path . 'room_search_form.php',
				'dt_sc_rooms_cart'				=>		$path . 'rooms_cart.php',
				'dt_sc_rooms_checkout'			=>		$path . 'rooms_checkout.php',
				'dt_sc_user_account'			=>		$path . 'user_account.php',
				'dt_sc_rooms_list'				=>		$path . 'rooms_list.php',
				'dt_sc_room_amenities'			=>		$path . 'room_amenities.php',
				'dt_sc_packages_list'			=>		$path . 'packages_list.php',
				'dt_sc_room_item'				=>		$path . 'room_item.php'
			);

			$modules = apply_filters( 'vcex_builder_modules', $modules );
			if( !empty( $modules ) ){
				foreach ( $modules as $key => $val ) {
					require_once( $val );
				}
			}
		}

		function dt_travel_vc_admin_scripts( $hook ) {

			if( $hook == "post.php" || $hook == "post-new.php" ) {
				wp_enqueue_style( 'dt-travel-vc-admin', plugins_url ('designthemes-travel-addon') . '/vc/style.css', array(), false, 'all' );
			}
		}
		
		function dt_travel_vc_autocomplete_terms_field_search( $search_string ) {
			$data = array();
			$vc_filter_by = vc_post_param( 'vc_filter_by', '' );
			$vc_taxonomies = get_terms( array(
				'hide_empty' => false,
				'search' => $search_string,
				'taxonomy' => 'dt_room_types'
			) );
			if ( is_array( $vc_taxonomies ) && ! empty( $vc_taxonomies ) ) {
				foreach ( $vc_taxonomies as $t ) {
					if ( is_object( $t ) ) {
						$data[] = vc_get_term_object( $t );
					}
				}
			}
		
			return $data;
		}

		function dt_travel_vc_autocomplete_terms_field_render( $term ) {
			$terms = get_terms( array(
				'include' => array( $term['value'] ),
				'hide_empty' => false,
				'taxonomy' => 'dt_room_types'
			) );
			$data = false;
			if ( is_array( $terms ) && 1 === count( $terms ) ) {
				$term = $terms[0];
				$data = vc_get_term_object( $term );
			}
		
			return $data;
		}

		function dt_travel_package_vc_autocomplete_terms_field_search( $search_string ) {
			$data = array();
			$vc_filter_by = vc_post_param( 'vc_filter_by', '' );
			$vc_taxonomies = get_terms( array(
				'hide_empty' => false,
				'search' => $search_string,
				'taxonomy' => 'product_cat'
			) );
			if ( is_array( $vc_taxonomies ) && ! empty( $vc_taxonomies ) ) {
				foreach ( $vc_taxonomies as $t ) {
					if ( is_object( $t ) ) {
						$data[] = vc_get_term_object( $t );
					}
				}
			}
		
			return $data;
		}

		function dt_travel_package_vc_autocomplete_terms_field_render( $term ) {
			$terms = get_terms( array(
				'include' => array( $term['value'] ),
				'hide_empty' => false,
				'taxonomy' => 'product_cat'
			) );
			$data = false;
			if ( is_array( $terms ) && 1 === count( $terms ) ) {
				$term = $terms[0];
				$data = vc_get_term_object( $term );
			}
		
			return $data;
		}		
	}
}?>