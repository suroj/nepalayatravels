<?php
if ( !defined( 'ABSPATH' ) ) {
	exit();
}
?>

<?php
get_header();
$global_breadcrumb = cs_get_option( 'show-breadcrumb' );?>

<!-- ** Header Wrapper ** -->
<div id="header-wrapper">
    
	<!-- **Header** -->
	<header id="header">
		<div class="container"><?php
	        /**
	         * maharaj_header hook.
	         * 
	         * @hooked maharaj_vc_header_template - 10
	         *
	         */
	        do_action( 'maharaj_header' ); ?>
	    </div>
	</header><!-- **Header - End ** -->

	<!-- ** Breadcrumb ** -->
    <?php
        if( !empty( $global_breadcrumb ) ) {

            $breadcrumbs = array();
        	$bstyle = cs_get_option( 'breadcrumb-style', 'default' );
        	$style = maharaj_breadcrumb_css();

			if( $post->post_parent ) {
				$parent_id  = $post->post_parent;
				$parents = array();

				while( $parent_id ) {
					$page = get_page( $parent_id );
					$parents[] = '<a href="' . get_permalink( $page->ID ) . '">' . get_the_title( $page->ID ) . '</a>';
					$parent_id  = $page->post_parent;
				}

				$parents = array_reverse( $parents );
				$breadcrumbs = array_merge_recursive($breadcrumbs, $parents);
			}

			$breadcrumbs[] = the_title( '<span class="current">', '</span>', false );

            maharaj_breadcrumb_output ( the_title( '<h1>', '</h1>',false ), $breadcrumbs, $bstyle, $style );
        }
    ?><!-- ** Breadcrumb End ** -->
</div><!-- ** Header Wrapper - End ** -->

<!-- **Main** -->
<div id="main">
    <!-- ** Container ** -->
    <div class="container"><?php
    	$page_layout  = cs_get_option( 'post-archives-page-layout' );
    	$page_layout  = !empty( $page_layout ) ? $page_layout : "content-full-width";

    	$layout = maharaj_page_layout( $page_layout );
    	extract( $layout );

    	if ( $show_sidebar ) {
    		if ( $show_left_sidebar ) {?>
    		 	<!-- Secondary Left -->
    			<section id="secondary-left" class="secondary-sidebar <?php echo esc_attr( $sidebar_class );?>"><?php
    				get_sidebar('left');?>
    			</section><!-- Secondary Left End --><?php
    		}
    	}?>

    	<!-- Primary -->
        <section id="primary" class="<?php echo esc_attr( $page_layout );?>"><?php
			//Getting order id
			$order_id = '';
			$tot_amount = 0;
			if( isset( $_REQUEST['dt-transaction-method'] ) && $_REQUEST['dt-transaction-method'] == 'paypal-standard' && isset( $_REQUEST['amt'] ) && $_REQUEST['amt'] > 0  ):
				$custom = json_decode( stripslashes( $_REQUEST['cm'] )  );
				$order_id = $custom->order_id;
				if( isset( $custom->advance_payment ) ):
					$order_settings = get_post_meta ( $order_id, '_order_settings', TRUE );
					$order_settings = is_array ( $order_settings ) ? $order_settings : array ();
			
					$order_settings['advance_percent'] = $custom->advance_payment;
			
					update_post_meta( $order_id, '_order_settings', $order_settings );
				endif;
			else:
				$order_id = isset( $_GET['order'] ) ? $_GET['order'] : '';
			endif; ?>
			
			<?php if ( $order_id && get_post_type( $order_id ) == 'dt_orders' ) {
			
					$order_items = get_post_meta ( $order_id, '_order_items', TRUE );
					$order_items = is_array ( $order_items ) ? $order_items : array ();
			
					$order_settings = get_post_meta ( $order_id, '_order_settings', TRUE );
					$order_settings = is_array ( $order_settings ) ? $order_settings : array (); ?>
			
					<div class="dt-sc-success-box">
						<?php echo esc_html__( 'Thank you! Your booking has been placed. We will contact you to confirm about the booking soon.', 'designthemes-travel' ); ?>
					</div>
			
					<div id="booking-details">
						<div class="booking-data">
							<h3 class="booking-data-number"><?php echo sprintf( esc_attr__( 'Booking %s', 'designthemes-travel' ), ( '#'.$order_id ) ); ?></h3>
							<div class="booking-date">
								<?php echo sprintf( esc_html__( 'Date %s', 'designthemes-travel' ), get_the_date( '', $order_id ) ); ?>
							</div>
						</div>
					</div>
				
					<div id="booking-items">
				
						<h3><?php echo esc_html__( 'Booking Items', 'designthemes-travel' ); ?></h3>
				
						<table cellpadding="0" cellspacing="0" class="booking_item_table">
							<thead>
								<tr>
									<th><?php esc_html_e( 'Item', 'designthemes-travel' ); ?></th>
									<th><?php esc_html_e( 'Check in - Checkout', 'designthemes-travel' ) ?></th>
									<th><?php esc_html_e( 'Night', 'designthemes-travel' ); ?></th>
									<th><?php esc_html_e( 'Qty', 'designthemes-travel' ); ?></th>
									<th><?php esc_html_e( 'Total', 'designthemes-travel' ); ?></th>
								</tr>
							</thead>
							<tbody>
			
							<?php foreach ( $order_items as $key => $item ) {
			
								$room_id = $item['room_id'];	$checkin_date = $item['checkin_date'];	  $checkout_date = $item['checkout_date'];
								$nights  = $item['nights']; 	$qty = $item['qty'];					  $price = $qty * $item['price'];
								$tot_amount += $price; ?>
								<tr>
									<td>
										<?php printf( '<a href="%s">%s</a>', get_permalink( $room_id ), get_the_title($room_id) ) ?>
									</td>
									<td>
										<?php printf( '%s - %s', date('F d, Y', strtotime($checkin_date)), date('F d, Y', strtotime($checkout_date)) ) ?>
									</td>
									<td>
										<?php printf( '%d', $nights ) ?>
									</td>
									<td>
										<?php printf( '%s', $qty ) ?>
									</td>
									<td>
										<?php printf( '%s', dt_travel_get_formatted_price($price) ); ?>
									</td>
								</tr>
			
								<?php $service_items = array_key_exists('addon', $item) ? $item['addon'] : array(); ?>
								<?php if( count( $service_items ) > 0 ) { ?>
									<?php foreach( $service_items as $k => $s ) { ?>
										<?php $qty = $s['qty']; $price = $qty * $s['price']; $tot_amount += $price; ?>
										<tr>
											<td colspan="3">
												<?php $term = get_term( $s['term_id'], 'dt_services' ); echo $term->name;?>
											</td>
											<td>
												<?php printf( '%s', $qty ) ?>
											</td>
											<td>
												<?php printf( '%s', dt_travel_get_formatted_price($price) ); ?>
											</td>
										</tr>
									<?php } ?>
								<?php } ?>
							<?php } ?>
				
							<tr>
								<td colspan="4"><?php esc_html_e( 'Sub Total', 'designthemes-travel' ) ?></td>
								<td>
									<?php printf( '%s', dt_travel_get_formatted_price($tot_amount) ); ?>
								</td>
							</tr>
			
							<?php $tax_amount = ''; ?>
							<?php if ( $tax = $order_settings['tax_percent'] ) : ?>
								<tr>
									<td colspan="4"><?php esc_html_e( 'Tax', 'designthemes-travel' ) ?></td>
									<td>
										<?php $tax_amount = ($tot_amount) * ($tax) / 100; echo dt_travel_get_formatted_price( $tax_amount ); ?>
									</td>
								</tr>
							<?php endif; ?>
			
							<tr>
								<td colspan="4"><?php esc_html_e( 'Grand Total', 'designthemes-travel' ) ?></td>
								<td>
									<?php echo dt_travel_get_formatted_price( ($tot_amount) + ($tax_amount) ); ?>
								</td>
							</tr>
							</tbody>
						</table>
					</div>
				
					<div id="booking-customer">
				
						<div class="customer-details">
							<ul class="dt-form-table">
				
								<li>
									<label for="first_name"><?php echo esc_html__( 'First Name:', 'designthemes-travel' ); ?></label>
									<?php echo esc_html( $order_settings['first_name'] ); ?>
								</li>
				
								<li>
									<label for="last_name"><?php echo esc_html__( 'Last Name:', 'designthemes-travel' ); ?></label>
									<?php echo esc_html( $order_settings['last_name'] ); ?>
								</li>
				
								<li>
									<label for="address"><?php echo esc_html__( 'Address:', 'designthemes-travel' ); ?></label>
									<?php echo esc_html( $order_settings['address'] ); ?>
								</li>
				
								<li>
									<label for="city"><?php echo esc_html__( 'City:', 'designthemes-travel' ); ?></label>
									<?php echo esc_html( $order_settings['city'] ); ?>
								</li>
				
								<li>
									<label for="state"><?php echo esc_html__( 'State:', 'designthemes-travel' ); ?></label>
									<?php echo esc_html( $order_settings['state'] ); ?>
								</li>
				
								<li>
									<label for="postal_code"><?php echo esc_html__( 'Postal Code:', 'designthemes-travel' ); ?></label>
									<?php echo esc_html( $order_settings['postal_code'] ); ?>
								</li>
				
								<li>
									<label for="country"><?php echo esc_html__( 'Country:', 'designthemes-travel' ); ?></label>
									<?php echo esc_html( $order_settings['country_name'] ); ?>
								</li>
				
								<li>
									<label for="phone"><?php echo esc_html__( 'Phone:', 'designthemes-travel' ); ?></label>
									<?php echo esc_html( $order_settings['phone_no'] ); ?>
								</li>
				
								<li>
									<label for="email"><?php echo esc_html__( 'Email:', 'designthemes-travel' ); ?></label>
									<?php echo esc_html( $order_settings['email_address'] ); ?>
								</li>
				
								<li>
									<label for="fax"><?php echo esc_html__( 'Fax:', 'designthemes-travel' ); ?></label>
									<?php echo esc_html( $order_settings['fax_no'] ); ?>
								</li>
			
							</ul>
						</div>
				
						<div class="booking-notes">
							<label for="notes"><?php echo esc_html__( 'Booking Notes:', 'designthemes-travel' ); ?></label>
							<?php echo esc_html( $order_settings['notes'] ); ?>
						</div>
				
					</div><?php
				} else { ?>
					<div class="dt-sc-error-box"><?php echo esc_html__( 'Invalid Order Number.', 'designthemes-travel' ); ?></div>
                    <div class="dt-sc-hr-invisible-medium"></div>
			<?php } ?>
        </section><!-- Primary End --><?php

    	if ( $show_sidebar ) {
    		if ( $show_right_sidebar ) {?>
    		 	<!-- Secondary Right -->
    			<section id="secondary-right" class="secondary-sidebar <?php echo esc_attr( $sidebar_class );?>"><?php
    				get_sidebar('right');?>
    			</section><!-- Secondary Right End --><?php
    		}
    	}?>
    </div>
    <!-- ** Container End ** -->
</div><!-- **Main - End ** -->    

<?php get_footer(); ?>