<?php
if (! class_exists ( 'DTTravelAddonShortcodes' )) {

	class DTTravelAddonShortcodes {

		function __construct() {

			require_once plugin_dir_path ( __FILE__ ) . 'shortcodes.php';

			add_action( 'wp_enqueue_scripts', array ( $this, 'dt_travel_wp_enqueue_scripts' ) );
			add_action( 'init', array ( $this, 'dt_travel_start_session' ), 1 );
		}

		function dt_travel_wp_enqueue_scripts() {

			wp_enqueue_style ( 'dt-travel-datepicker', plugins_url ('designthemes-travel-addon') . '/custom-post-types/css/datepicker.css', array (), false, 'all' );
			wp_enqueue_style ( 'dt-travel-addon', plugins_url ('designthemes-travel-addon') . '/shortcodes/css/travel-addon.css', array (), false, 'all' );

			wp_enqueue_script ( 'dt-travel-validation', plugins_url ('designthemes-travel-addon') . '/shortcodes/js/jquery.validate.min.js', array (), false, true );
			wp_enqueue_script ( 'dt-travel-addon', plugins_url ('designthemes-travel-addon') . '/shortcodes/js/travel-addon.js', array ('jquery', 'jquery-ui-datepicker', 'jquery-effects-pulsate'), false, true );
			wp_localize_script( 'dt-travel-addon', 'dt_travel_msgs', array(
				'min_book_days' => maharaj_cs_get_option( 'min-book-day', 1 ),
				'addroom_success' => __('Success! Room Added.', 'designthemes-travel'),
				'payment_msg' => __('Please select any mode of payment.', 'designthemes-travel'),
				'policy_msg' => __('Please accept our policy.', 'designthemes-travel'),
				'thanks' => __('Thanks for Booking with Us.', 'designthemes-travel')
			));
		}

		function dt_travel_start_session() {
			if( !session_id() ) {
				session_start();
			}
		}
	}
} ?>