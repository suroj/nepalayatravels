<?php if ( ! defined( 'ABSPATH' ) ) { die; } // Cannot access pages directly.
// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK SETTINGS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$settings           = array(
  'menu_title'      => constant('MAHARAJ_THEME_NAME').' '.esc_html__('Options', 'maharaj'),
  'menu_type'       => 'theme', // menu, submenu, options, theme, etc.
  'menu_slug'       => 'cs-framework',
  'ajax_save'       => true,
  'show_reset_all'  => false,
  'framework_title' => __('Designthemes Framework <small>by Designthemes</small>', 'maharaj'),
);

// ===============================================================================================
// -----------------------------------------------------------------------------------------------
// FRAMEWORK OPTIONS
// -----------------------------------------------------------------------------------------------
// ===============================================================================================
$options        = array();

$options[]      = array(
  'name'        => 'general',
  'title'       => esc_html__('General', 'maharaj'),
  'icon'        => 'fa fa-gears',

  'fields'      => array(

	array(
	  'type'    => 'subheading',
	  'content' => esc_html__( 'General Options', 'maharaj' ),
	),
	
	array(
		'id'	=> 'header',
		'type'	=> 'select',
		'title'	=> esc_html__('Site Header', 'maharaj'),
		'class'	=> 'chosen',
		'options'	=> 'posts',
		'query_args'	=> array(
			'post_type'	=> 'dt_headers',
			'orderby'	=> 'title',
			'order'	=> 'ASC'
		),
		'default_option'	=> esc_attr__('Select Header', 'maharaj'),
		'attributes'	=> array ( 'style'	=> 'width:50%'),
		'info'	=> esc_html__('Select default header.','maharaj'),
	),
	
	array(
		'id'	=> 'footer',
		'type'	=> 'select',
		'title'	=> esc_html__('Site Footer', 'maharaj'),
		'class'	=> 'chosen',
		'options'	=> 'posts',
		'query_args'	=> array(
			'post_type'	=> 'dt_footers',
			'orderby'	=> 'title',
			'order'	=> 'ASC'
		),
		'default_option'	=> esc_attr__('Select Footer', 'maharaj'),
		'attributes'	=> array ( 'style'	=> 'width:50%'),
		'info'	=> esc_html__('Select defaultfooter.','maharaj'),
	),

	array(
	  'id'  	 => 'use-site-loader',
	  'type'  	 => 'switcher',
	  'title' 	 => esc_html__('Site Loader', 'maharaj'),
	  'info'	 => esc_html__('YES! to use site loader.', 'maharaj')
	),	

	array(
	  'id'  	 => 'enable-stylepicker',
	  'type'  	 => 'switcher',
	  'title' 	 => esc_html__('Style Picker', 'maharaj'),
	  'info'	 => esc_html__('YES! to show the style picker.', 'maharaj')
	),		

	array(
	  'id'  	 => 'show-pagecomments',
	  'type'  	 => 'switcher',
	  'title' 	 => esc_html__('Globally Show Page Comments', 'maharaj'),
	  'info'	 => esc_html__('YES! to show comments on all the pages. This will globally override your "Allow comments" option under your page "Discussion" settings.', 'maharaj'),
	  'default'  => true,
	),

	array(
	  'id'  	 => 'showall-pagination',
	  'type'  	 => 'switcher',
	  'title' 	 => esc_html__('Show all pages in Pagination', 'maharaj'),
	  'info'	 => esc_html__('YES! to show all the pages instead of dots near the current page.', 'maharaj')
	),



	array(
	  'id'      => 'google-map-key',
	  'type'    => 'text',
	  'title'   => esc_html__('Google Map API Key', 'maharaj'),
	  'after' 	=> '<p class="cs-text-info">'.esc_html__('Put a valid google account api key here', 'maharaj').'</p>',
	),

	array(
	  'id'      => 'mailchimp-key',
	  'type'    => 'text',
	  'title'   => esc_html__('Mailchimp API Key', 'maharaj'),
	  'after' 	=> '<p class="cs-text-info">'.esc_html__('Put a valid mailchimp account api key here', 'maharaj').'</p>',
	),

  ),
);

$options[]      = array(
  'name'        => 'layout_options',
  'title'       => esc_html__('Layout Options', 'maharaj'),
  'icon'        => 'dashicons dashicons-exerpt-view',
  'sections' => array(

	// -----------------------------------------
	// Header Options
	// -----------------------------------------
	array(
	  'name'      => 'breadcrumb_options',
	  'title'     => esc_html__('Breadcrumb Options', 'maharaj'),
	  'icon'      => 'fa fa-sitemap',

		'fields'      => array(

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Breadcrumb Options", 'maharaj' ),
		  ),

		  array(
			'id'  		 => 'show-breadcrumb',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Show Breadcrumb', 'maharaj'),
			'info'		 => esc_html__('YES! to display breadcrumb for all pages.', 'maharaj'),
			'default' 	 => true,
		  ),

		  array(
			'id'           => 'breadcrumb-delimiter',
			'type'         => 'icon',
			'title'        => esc_html__('Breadcrumb Delimiter', 'maharaj'),
			'info'         => esc_html__('Choose delimiter style to display on breadcrumb section.', 'maharaj'),
		  ),

		  array(
			'id'           => 'breadcrumb-style',
			'type'         => 'select',
			'title'        => esc_html__('Breadcrumb Style', 'maharaj'),
			'options'      => array(
			  'default' 							=> esc_html__('Default', 'maharaj'),
			  'aligncenter'    						=> esc_html__('Align Center', 'maharaj'),
			  'alignright'  						=> esc_html__('Align Right', 'maharaj'),
			  'breadcrumb-left'    					=> esc_html__('Left Side Breadcrumb', 'maharaj'),
			  'breadcrumb-right'     				=> esc_html__('Right Side Breadcrumb', 'maharaj'),
			  'breadcrumb-top-right-title-center'  	=> esc_html__('Top Right Title Center', 'maharaj'),
			  'breadcrumb-top-left-title-center'  	=> esc_html__('Top Left Title Center', 'maharaj'),
			),
			'class'        => 'chosen',
			'default'      => 'default',
			'info'         => esc_html__('Choose alignment style to display on breadcrumb section.', 'maharaj'),
		  ),

		  array(
			'id'    => 'breadcrumb_background',
			'type'  => 'background',
			'title' => esc_html__('Background', 'maharaj'),
			'desc'  => esc_html__('Choose background options for breadcrumb title section.', 'maharaj')
		  ),

		),
	),

  ),
);

$options[]      = array(
  'name'        => 'allpage_options',
  'title'       => esc_html__('All Page Options', 'maharaj'),
  'icon'        => 'fa fa-files-o',
  'sections' => array(

	// -----------------------------------------
	// Post Options
	// -----------------------------------------
	array(
	  'name'      => 'post_options',
	  'title'     => esc_html__('Post Options', 'maharaj'),
	  'icon'      => 'fa fa-file',

		'fields'      => array(

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Single Post Options", 'maharaj' ),
		  ),
		
		  array(
			'id'  		 => 'single-post-authorbox',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Single Author Box', 'maharaj'),
			'info'		 => esc_html__('YES! to display author box in single blog posts.', 'maharaj')
		  ),

		  array(
			'id'  		 => 'single-post-related',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Single Related Posts', 'maharaj'),
			'info'		 => esc_html__('YES! to display related blog posts in single posts.', 'maharaj')
		  ),

		  array(
			'id'  		 => 'single-post-navigation',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Single Post Navigation', 'maharaj'),
			'info'		 => esc_html__('YES! to display post navigation in single posts.', 'maharaj')
		  ),

		  array(
			'id'  		 => 'single-post-comments',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Posts Comments', 'maharaj'),
			'info'		 => esc_html__('YES! to display single blog post comments.', 'maharaj'),
			'default' 	 => true,
		  ),

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Post Archives Page Layout", 'maharaj' ),
		  ),

		  array(
			'id'      	 => 'post-archives-page-layout',
			'type'       => 'image_select',
			'title'      => esc_html__('Page Layout', 'maharaj'),
			'options'    => array(
			  'content-full-width'   => MAHARAJ_THEME_URI . '/cs-framework-override/images/without-sidebar.png',
			  'with-left-sidebar'    => MAHARAJ_THEME_URI . '/cs-framework-override/images/left-sidebar.png',
			  'with-right-sidebar'   => MAHARAJ_THEME_URI . '/cs-framework-override/images/right-sidebar.png',
			  'with-both-sidebar'    => MAHARAJ_THEME_URI . '/cs-framework-override/images/both-sidebar.png',
			),
			'default'      => 'content-full-width',
			'attributes'   => array(
			  'data-depend-id' => 'post-archives-page-layout',
			),
		  ),

		  array(
			'id'  		 => 'show-standard-left-sidebar-for-post-archives',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Show Standard Left Sidebar', 'maharaj'),
			'dependency' => array( 'post-archives-page-layout', 'any', 'with-left-sidebar,with-both-sidebar' ),
		  ),

		  array(
			'id'  		 => 'show-standard-right-sidebar-for-post-archives',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Show Standard Right Sidebar', 'maharaj'),
			'dependency' => array( 'post-archives-page-layout', 'any', 'with-right-sidebar,with-both-sidebar' ),
		  ),

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Post Archives Post Layout", 'maharaj' ),
		  ),

		  array(
			'id'      	   => 'post-archives-post-layout',
			'type'         => 'image_select',
			'title'        => esc_html__('Post Layout', 'maharaj'),
			'options'      => array(
			  'one-column' 		  => MAHARAJ_THEME_URI . '/cs-framework-override/images/one-column.png',
			  'one-half-column'   => MAHARAJ_THEME_URI . '/cs-framework-override/images/one-half-column.png',
			  'one-third-column'  => MAHARAJ_THEME_URI . '/cs-framework-override/images/one-third-column.png',
			  '1-2-2'			  => MAHARAJ_THEME_URI . '/cs-framework-override/images/1-2-2.png',
			  '1-2-2-1-2-2' 	  => MAHARAJ_THEME_URI . '/cs-framework-override/images/1-2-2-1-2-2.png',
			  '1-3-3-3'			  => MAHARAJ_THEME_URI . '/cs-framework-override/images/1-3-3-3.png',
			  '1-3-3-3-1' 		  => MAHARAJ_THEME_URI . '/cs-framework-override/images/1-3-3-3-1.png',
			),
			'default'      => 'one-half-column',
		  ),

		  array(
			'id'           => 'post-style',
			'type'         => 'select',
			'title'        => esc_html__('Post Style', 'maharaj'),
			'options'      => array(
			  'blog-default-style' 		=> esc_html__('Default', 'maharaj'),
			  'entry-date-left'      	=> esc_html__('Date Left', 'maharaj'),
			  'entry-date-author-left'  => esc_html__('Date and Author Left', 'maharaj'),
			  'blog-medium-style'       => esc_html__('Medium', 'maharaj'),
			  'blog-medium-style dt-blog-medium-highlight'     					 => esc_html__('Medium Hightlight', 'maharaj'),
			  'blog-medium-style dt-blog-medium-highlight dt-sc-skin-highlight'  => esc_html__('Medium Skin Highlight', 'maharaj'),
			),
			'class'        => 'chosen',
			'default'      => 'blog-default-style',
			'info'         => esc_html__('Choose post style to display post archives pages.', 'maharaj'),
		  ),

		  array(
			'id'  		 => 'post-archives-enable-excerpt',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Allow Excerpt', 'maharaj'),
			'info'		 => esc_html__('YES! to allow excerpt', 'maharaj'),
			'default'    => true,
		  ),

		  array(
			'id'  		 => 'post-archives-excerpt',
			'type'  	 => 'number',
			'title' 	 => esc_html__('Excerpt Length', 'maharaj'),
			'after'		 => '<span class="cs-text-desc">&nbsp;'.esc_html__('Put Excerpt Length', 'maharaj').'</span>',
			'default' 	 => 40,
		  ),

		  array(
			'id'  		 => 'post-archives-enable-readmore',
			'type'  	 => 'switcher',
			'title' 	 => esc_html__('Read More', 'maharaj'),
			'info'		 => esc_html__('YES! to enable read more button', 'maharaj'),
			'default'	 => true,
		  ),

		  array(
			'id'  		 => 'post-archives-readmore',
			'type'  	 => 'textarea',
			'title' 	 => esc_html__('Read More Shortcode', 'maharaj'),
			'info'		 => esc_html__('Paste any button shortcode here', 'maharaj'),
			'default'	 => '[dt_sc_button title="Read More" style="fully-rounded-border" size="medium" /]',
		  ),

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Single Post & Post Archive options", 'maharaj' ),
		  ),

		  array(
			'id'      => 'post-format-meta',
			'type'    => 'switcher',
			'title'   => esc_html__('Post Format Meta', 'maharaj' ),
			'info'	  => esc_html__('YES! to show post format meta information', 'maharaj'),
			'default' => true
		  ),

		  array(
			'id'      => 'post-author-meta',
			'type'    => 'switcher',
			'title'   => esc_html__('Author Meta', 'maharaj' ),
			'info'	  => esc_html__('YES! to show post author meta information', 'maharaj'),
			'default' => true
		  ),

		  array(
			'id'      => 'post-date-meta',
			'type'    => 'switcher',
			'title'   => esc_html__('Date Meta', 'maharaj' ),
			'info'	  => esc_html__('YES! to show post date meta information', 'maharaj'),
			'default' => true
		  ),

		  array(
			'id'      => 'post-comment-meta',
			'type'    => 'switcher',
			'title'   => esc_html__('Comment Meta', 'maharaj' ),
			'info'	  => esc_html__('YES! to show post comment meta information', 'maharaj'),
			'default' => true
		  ),

		  array(
			'id'      => 'post-category-meta',
			'type'    => 'switcher',
			'title'   => esc_html__('Category Meta', 'maharaj' ),
			'info'	  => esc_html__('YES! to show post category information', 'maharaj'),
			'default' => true
		  ),

		  array(
			'id'      => 'post-tag-meta',
			'type'    => 'switcher',
			'title'   => esc_html__('Tag Meta', 'maharaj' ),
			'info'	  => esc_html__('YES! to show post tag information', 'maharaj'),
			'default' => true
		  ),

		),
	),

	// -----------------------------------------
	// 404 Options
	// -----------------------------------------
	array(
	  'name'      => '404_options',
	  'title'     => esc_html__('404 Options', 'maharaj'),
	  'icon'      => 'fa fa-warning',

		'fields'      => array(

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "404 Message", 'maharaj' ),
		  ),
		  
		  array(
			'id'      => 'enable-404message',
			'type'    => 'switcher',
			'title'   => esc_html__('Enable Message', 'maharaj' ),
			'info'	  => esc_html__('YES! to enable not-found page message.', 'maharaj'),
			'default' => true
		  ),

		  array(
			'id'           => 'notfound-style',
			'type'         => 'select',
			'title'        => esc_html__('Template Style', 'maharaj'),
			'options'      => array(
			  'type1' 	   => esc_html__('Modern', 'maharaj'),
			  'type2'      => esc_html__('Classic', 'maharaj'),
			  'type4'  	   => esc_html__('Diamond', 'maharaj'),
			  'type5'      => esc_html__('Shadow', 'maharaj'),
			  'type6'      => esc_html__('Diamond Alt', 'maharaj'),
			  'type7'  	   => esc_html__('Stack', 'maharaj'),
			  'type8'  	   => esc_html__('Minimal', 'maharaj'),
			),
			'class'        => 'chosen',
			'default'      => 'type1',
			'info'         => esc_html__('Choose the style of not-found template page.', 'maharaj')
		  ),

		  array(
			'id'      => 'notfound-darkbg',
			'type'    => 'switcher',
			'title'   => esc_html__('404 Dark BG', 'maharaj' ),
			'info'	  => esc_html__('YES! to use dark bg notfound page for this site.', 'maharaj')
		  ),

		  array(
			'id'           => 'notfound-pageid',
			'type'         => 'select',
			'title'        => esc_html__('Custom Page', 'maharaj'),
			'options'      => 'pages',
			'class'        => 'chosen',
			'default_option' => esc_html__('Choose the page', 'maharaj'),
			'info'       	 => esc_html__('Choose the page for not-found content.', 'maharaj')
		  ),
		  
		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Background Options", 'maharaj' ),
		  ),

		  array(
			'id'    => 'notfound_background',
			'type'  => 'background',
			'title' => esc_html__('Background', 'maharaj')
		  ),

		  array(
			'id'  		 => 'notfound-bg-style',
			'type'  	 => 'textarea',
			'title' 	 => esc_html__('Custom Styles', 'maharaj'),
			'info'		 => esc_html__('Paste custom CSS styles for not found page.', 'maharaj')
		  ),

		),
	),

	// -----------------------------------------
	// Underconstruction Options
	// -----------------------------------------
	array(
	  'name'      => 'comingsoon_options',
	  'title'     => esc_html__('Under Construction Options', 'maharaj'),
	  'icon'      => 'fa fa-thumbs-down',

		'fields'      => array(

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Under Construction", 'maharaj' ),
		  ),
	
		  array(
			'id'      => 'enable-comingsoon',
			'type'    => 'switcher',
			'title'   => esc_html__('Enable Coming Soon', 'maharaj' ),
			'info'	  => esc_html__('YES! to check under construction page of your website.', 'maharaj')
		  ),
	
		  array(
			'id'           => 'comingsoon-style',
			'type'         => 'select',
			'title'        => esc_html__('Template Style', 'maharaj'),
			'options'      => array(
			  'type1' 	   => esc_html__('Diamond', 'maharaj'),
			  'type2'      => esc_html__('Teaser', 'maharaj'),
			  'type3'  	   => esc_html__('Minimal', 'maharaj'),
			  'type4'      => esc_html__('Counter Only', 'maharaj'),
			  'type5'      => esc_html__('Belt', 'maharaj'),
			  'type6'  	   => esc_html__('Classic', 'maharaj'),
			  'type7'  	   => esc_html__('Boxed', 'maharaj')
			),
			'class'        => 'chosen',
			'default'      => 'type1',
			'info'         => esc_html__('Choose the style of coming soon template.', 'maharaj'),
		  ),

		  array(
			'id'      => 'uc-darkbg',
			'type'    => 'switcher',
			'title'   => esc_html__('Coming Soon Dark BG', 'maharaj' ),
			'info'	  => esc_html__('YES! to use dark bg coming soon page for this site.', 'maharaj')
		  ),

		  array(
			'id'           => 'comingsoon-pageid',
			'type'         => 'select',
			'title'        => esc_html__('Custom Page', 'maharaj'),
			'options'      => 'pages',
			'class'        => 'chosen',
			'default_option' => esc_html__('Choose the page', 'maharaj'),
			'info'       	 => esc_html__('Choose the page for comingsoon content.', 'maharaj')
		  ),

		  array(
			'id'      => 'show-launchdate',
			'type'    => 'switcher',
			'title'   => esc_html__('Show Launch Date', 'maharaj' ),
			'info'	  => esc_html__('YES! to show launch date text.', 'maharaj'),
		  ),

		  array(
			'id'      => 'comingsoon-launchdate',
			'type'    => 'text',
			'title'   => esc_html__('Launch Date', 'maharaj'),
			'attributes' => array( 
			  'placeholder' => '10/30/2016 12:00:00'
			),
			'after' 	=> '<p class="cs-text-info">'.esc_html__('Put Format: 12/30/2016 12:00:00 month/day/year hour:minute:second', 'maharaj').'</p>',
		  ),

		  array(
			'id'           => 'comingsoon-timezone',
			'type'         => 'select',
			'title'        => esc_html__('UTC Timezone', 'maharaj'),
			'options'      => array(
			  '-12' => '-12', '-11' => '-11', '-10' => '-10', '-9' => '-9', '-8' => '-8', '-7' => '-7', '-6' => '-6', '-5' => '-5', 
			  '-4' => '-4', '-3' => '-3', '-2' => '-2', '-1' => '-1', '0' => '0', '+1' => '+1', '+2' => '+2', '+3' => '+3', '+4' => '+4',
			  '+5' => '+5', '+6' => '+6', '+7' => '+7', '+8' => '+8', '+9' => '+9', '+10' => '+10', '+11' => '+11', '+12' => '+12'
			),
			'class'        => 'chosen',
			'default'      => '0',
			'info'         => esc_html__('Choose utc timezone, by default UTC:00:00', 'maharaj'),
		  ),

		  array(
			'id'    => 'comingsoon_background',
			'type'  => 'background',
			'title' => esc_html__('Background', 'maharaj')
		  ),

		  array(
			'id'  		 => 'comingsoon-bg-style',
			'type'  	 => 'textarea',
			'title' 	 => esc_html__('Custom Styles', 'maharaj'),
			'info'		 => esc_html__('Paste custom CSS styles for under construction page.', 'maharaj'),
		  ),

		),
	),

  ),
);

// -----------------------------------------
// Widget area Options
// -----------------------------------------
$options[]      = array(
  'name'        => 'widgetarea_options',
  'title'       => esc_html__('Widget Area', 'maharaj'),
  'icon'        => 'fa fa-trello',

  'fields'      => array(

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Custom Widget Area for Sidebar", 'maharaj' ),
	  ),

	  array(
		'id'           => 'wtitle-style',
		'type'         => 'select',
		'title'        => esc_html__('Sidebar widget Title Style', 'maharaj'),
		'options'      => array(
		  'type1' 	   => esc_html__('Double Border', 'maharaj'),
		  'type2'      => esc_html__('Tooltip', 'maharaj'),
		  'type3'  	   => esc_html__('Title Top Border', 'maharaj'),
		  'type4'      => esc_html__('Left Border & Pattren', 'maharaj'),
		  'type5'      => esc_html__('Bottom Border', 'maharaj'),
		  'type6'  	   => esc_html__('Tooltip Border', 'maharaj'),
		  'type7'  	   => esc_html__('Boxed Modern', 'maharaj'),
		  'type8'  	   => esc_html__('Elegant Border', 'maharaj'),
		  'type9' 	   => esc_html__('Needle', 'maharaj'),
		  'type10' 	   => esc_html__('Ribbon', 'maharaj'),
		  'type11' 	   => esc_html__('Content Background', 'maharaj'),
		  'type12' 	   => esc_html__('Classic BG', 'maharaj'),
		  'type13' 	   => esc_html__('Tiny Boders', 'maharaj'),
		  'type14' 	   => esc_html__('BG & Border', 'maharaj'),
		  'type15' 	   => esc_html__('Classic BG Alt', 'maharaj'),
		  'type16' 	   => esc_html__('Left Border & BG', 'maharaj'),
		  'type17' 	   => esc_html__('Basic', 'maharaj'),
		  'type18' 	   => esc_html__('BG & Pattern', 'maharaj'),
		),
		'class'          => 'chosen',
		'default_option' => esc_html__('Choose any type', 'maharaj'),
		'info'           => esc_html__('Choose the style of sidebar widget title.', 'maharaj')
	  ),

	  array(
		'id'              => 'widgetarea-custom',
		'type'            => 'group',
		'title'           => esc_html__('Custom Widget Area', 'maharaj'),
		'button_title'    => esc_html__('Add New', 'maharaj'),
		'accordion_title' => esc_html__('Add New Widget Area', 'maharaj'),
		'fields'          => array(

		  array(
			'id'          => 'widgetarea-custom-name',
			'type'        => 'text',
			'title'       => esc_html__('Name', 'maharaj'),
		  ),

		)
	  ),

	),
);

// -----------------------------------------
// Woocommerce Options
// -----------------------------------------
if( function_exists( 'is_woocommerce' ) && ! class_exists ( 'DTWooPlugin' ) ){

	$options[]      = array(
	  'name'        => 'woocommerce_options',
	  'title'       => esc_html__('Woocommerce', 'maharaj'),
	  'icon'        => 'fa fa-shopping-cart',

	  'fields'      => array(

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Woocommerce Shop Page Options", 'maharaj' ),
		  ),

		  array(
			'id'  		 => 'shop-product-per-page',
			'type'  	 => 'number',
			'title' 	 => esc_html__('Products Per Page', 'maharaj'),
			'after'		 => '<span class="cs-text-desc">&nbsp;'.esc_html__('Number of products to show in catalog / shop page', 'maharaj').'</span>',
			'default' 	 => 12,
		  ),

		  array(
			'id'           => 'product-style',
			'type'         => 'select',
			'title'        => esc_html__('Product Style', 'maharaj'),
			'options'      => array(
			  'woo-type1' 	   => esc_html__('Thick Border', 'maharaj'),
			  'woo-type4'      => esc_html__('Diamond Icons', 'maharaj'),
			  'woo-type8' 	   => esc_html__('Modern', 'maharaj'),
			  'woo-type10' 	   => esc_html__('Easing', 'maharaj'),
			  'woo-type11' 	   => esc_html__('Boxed', 'maharaj'),
			  'woo-type12' 	   => esc_html__('Easing Alt', 'maharaj'),
			  'woo-type13' 	   => esc_html__('Parallel', 'maharaj'),
			  'woo-type14' 	   => esc_html__('Pointer', 'maharaj'),
			  'woo-type16' 	   => esc_html__('Stack', 'maharaj'),
			  'woo-type17' 	   => esc_html__('Bouncy', 'maharaj'),
			  'woo-type20' 	   => esc_html__('Masked Circle', 'maharaj'),
			  'woo-type21' 	   => esc_html__('Classic', 'maharaj')
			),
			'class'        => 'chosen',
			'default' 	   => 'woo-type1',
			'info'         => esc_html__('Choose products style to display shop & archive pages.', 'maharaj')
		  ),

		  array(
			'id'      	 => 'shop-page-product-layout',
			'type'       => 'image_select',
			'title'      => esc_html__('Product Layout', 'maharaj'),
			'options'    => array(
				  1   => MAHARAJ_THEME_URI . '/cs-framework-override/images/one-column.png',
				  2   => MAHARAJ_THEME_URI . '/cs-framework-override/images/one-half-column.png',
				  3   => MAHARAJ_THEME_URI . '/cs-framework-override/images/one-third-column.png',
				  4   => MAHARAJ_THEME_URI . '/cs-framework-override/images/one-fourth-column.png',
			),
			'default'      => 4,
			'attributes'   => array(
			  'data-depend-id' => 'shop-page-product-layout',
			),
		  ),

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Product Detail Page Options", 'maharaj' ),
		  ),

		  array(
			'id'      	   => 'product-layout',
			'type'         => 'image_select',
			'title'        => esc_html__('Layout', 'maharaj'),
			'options'      => array(
			  'content-full-width'   => MAHARAJ_THEME_URI . '/cs-framework-override/images/without-sidebar.png',
			  'with-left-sidebar'    => MAHARAJ_THEME_URI . '/cs-framework-override/images/left-sidebar.png',
			  'with-right-sidebar'   => MAHARAJ_THEME_URI . '/cs-framework-override/images/right-sidebar.png',
			  'with-both-sidebar'    => MAHARAJ_THEME_URI . '/cs-framework-override/images/both-sidebar.png',
			),
			'default'      => 'content-full-width',
			'attributes'   => array(
			  'data-depend-id' => 'product-layout',
			),
		  ),

		  array(
			'id'  		 	 => 'show-shop-standard-left-sidebar-for-product-layout',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Shop Standard Left Sidebar', 'maharaj'),
			'dependency'   	 => array( 'product-layout', 'any', 'with-left-sidebar,with-both-sidebar' ),
		  ),

		  array(
			'id'  			 => 'show-shop-standard-right-sidebar-for-product-layout',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Shop Standard Right Sidebar', 'maharaj'),
			'dependency' 	 => array( 'product-layout', 'any', 'with-right-sidebar,with-both-sidebar' ),
		  ),

		  array(
			'id'  		 	 => 'enable-related',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Related Products', 'maharaj'),
			'info'	  		 => esc_html__("YES! to display related products on single product's page.", 'maharaj')
		  ),

		  array(
			'id'              => 'product-custom-fields',
			'type'            => 'group',
			'title'           => esc_html__('Custom Fields', 'maharaj'),
			'info'            => esc_html__('Click button to add custom fields like name, url and date etc', 'maharaj'),
			'button_title'    => esc_html__('Add New Field', 'maharaj'),
			'accordion_title' => esc_html__('Adding New Custom Field', 'maharaj'),
			'fields'          => array(
			  array(
				'id'          => 'product-custom-fields-text',
				'type'        => 'text',
				'title'       => esc_html__('Enter Text', 'maharaj')
			  ),
			)
		  ),

		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Product Category Page Options", 'maharaj' ),
		  ),

		  array(
			'id'      	   => 'product-category-layout',
			'type'         => 'image_select',
			'title'        => esc_html__('Layout', 'maharaj'),
			'options'      => array(
			  'content-full-width'   => MAHARAJ_THEME_URI . '/cs-framework-override/images/without-sidebar.png',
			  'with-left-sidebar'    => MAHARAJ_THEME_URI . '/cs-framework-override/images/left-sidebar.png',
			  'with-right-sidebar'   => MAHARAJ_THEME_URI . '/cs-framework-override/images/right-sidebar.png',
			  'with-both-sidebar'    => MAHARAJ_THEME_URI . '/cs-framework-override/images/both-sidebar.png',
			),
			'default'      => 'content-full-width',
			'attributes'   => array(
			  'data-depend-id' => 'product-category-layout',
			),
		  ),

		  array(
			'id'  		 	 => 'show-shop-standard-left-sidebar-for-product-category-layout',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Shop Standard Left Sidebar', 'maharaj'),
			'dependency'   	 => array( 'product-category-layout', 'any', 'with-left-sidebar,with-both-sidebar' ),
		  ),

		  array(
			'id'  			 => 'show-shop-standard-right-sidebar-for-product-category-layout',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Shop Standard Right Sidebar', 'maharaj'),
			'dependency' 	 => array( 'product-category-layout', 'any', 'with-right-sidebar,with-both-sidebar' ),
		  ),
		  
		  array(
			'type'    => 'subheading',
			'content' => esc_html__( "Product Tag Page Options", 'maharaj' ),
		  ),

		  array(
			'id'      	   => 'product-tag-layout',
			'type'         => 'image_select',
			'title'        => esc_html__('Layout', 'maharaj'),
			'options'      => array(
			  'content-full-width'   => MAHARAJ_THEME_URI . '/cs-framework-override/images/without-sidebar.png',
			  'with-left-sidebar'    => MAHARAJ_THEME_URI . '/cs-framework-override/images/left-sidebar.png',
			  'with-right-sidebar'   => MAHARAJ_THEME_URI . '/cs-framework-override/images/right-sidebar.png',
			  'with-both-sidebar'    => MAHARAJ_THEME_URI . '/cs-framework-override/images/both-sidebar.png',
			),
			'default'      => 'content-full-width',
			'attributes'   => array(
			  'data-depend-id' => 'product-tag-layout',
			),
		  ),

		  array(
			'id'  		 	 => 'show-shop-standard-left-sidebar-for-product-tag-layout',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Shop Standard Left Sidebar', 'maharaj'),
			'dependency'   	 => array( 'product-tag-layout', 'any', 'with-left-sidebar,with-both-sidebar' ),
		  ),

		  array(
			'id'  			 => 'show-shop-standard-right-sidebar-for-product-tag-layout',
			'type'  		 => 'switcher',
			'title' 		 => esc_html__('Show Shop Standard Right Sidebar', 'maharaj'),
			'dependency' 	 => array( 'product-tag-layout', 'any', 'with-right-sidebar,with-both-sidebar' ),
		  ),

	  ),
	);
}

// -----------------------------------------
// Sociable Options
// -----------------------------------------
$options[]      = array(
  'name'        => 'sociable_options',
  'title'       => esc_html__('Sociable', 'maharaj'),
  'icon'        => 'fa fa-share-alt-square',

  'fields'      => array(

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Sociable", 'maharaj' ),
	  ),

	  array(
		'id'              => 'sociable_fields',
		'type'            => 'group',
		'title'           => esc_html__('Sociable', 'maharaj'),
		'info'            => esc_html__('Click button to add type of social & url.', 'maharaj'),
		'button_title'    => esc_html__('Add New Social', 'maharaj'),
		'accordion_title' => esc_html__('Adding New Social Field', 'maharaj'),
		'fields'          => array(
		  array(
			'id'          => 'sociable_fields_type',
			'type'        => 'select',
			'title'       => esc_html__('Select Social', 'maharaj'),
			'options'      => array(
			  'delicious' 	 => esc_html__('Delicious', 'maharaj'),
			  'deviantart' 	 => esc_html__('Deviantart', 'maharaj'),
			  'digg' 	  	 => esc_html__('Digg', 'maharaj'),
			  'dribbble' 	 => esc_html__('Dribbble', 'maharaj'),
			  'envelope' 	 => esc_html__('Envelope', 'maharaj'),
			  'facebook' 	 => esc_html__('Facebook', 'maharaj'),
			  'flickr' 		 => esc_html__('Flickr', 'maharaj'),
			  'google-plus'  => esc_html__('Google Plus', 'maharaj'),
			  'gtalk'  		 => esc_html__('GTalk', 'maharaj'),
			  'instagram'	 => esc_html__('Instagram', 'maharaj'),
			  'lastfm'	 	 => esc_html__('Lastfm', 'maharaj'),
			  'linkedin'	 => esc_html__('Linkedin', 'maharaj'),
			  'myspace'		 => esc_html__('Myspace', 'maharaj'),
			  'picasa'		 => esc_html__('Picasa', 'maharaj'),
			  'pinterest'	 => esc_html__('Pinterest', 'maharaj'),
			  'reddit'		 => esc_html__('Reddit', 'maharaj'),
			  'rss'		 	 => esc_html__('RSS', 'maharaj'),
			  'skype'		 => esc_html__('Skype', 'maharaj'),
			  'stumbleupon'	 => esc_html__('Stumbleupon', 'maharaj'),
			  'technorati'	 => esc_html__('Technorati', 'maharaj'),
			  'tumblr'		 => esc_html__('Tumblr', 'maharaj'),
			  'twitter'		 => esc_html__('Twitter', 'maharaj'),
			  'viadeo'		 => esc_html__('Viadeo', 'maharaj'),
			  'vimeo'		 => esc_html__('Vimeo', 'maharaj'),
			  'yahoo'		 => esc_html__('Yahoo', 'maharaj'),
			  'youtube'		 => esc_html__('Youtube', 'maharaj'),
			),
			'class'        => 'chosen',
			'default'      => 'delicious',
		  ),

		  array(
			'id'          => 'sociable_fields_url',
			'type'        => 'text',
			'title'       => esc_html__('Enter URL', 'maharaj')
		  ),
		)
	  ),

   ),
);

// -----------------------------------------
// Hook Options
// -----------------------------------------
$options[]      = array(
  'name'        => 'hook_options',
  'title'       => esc_html__('Hooks', 'maharaj'),
  'icon'        => 'fa fa-paperclip',

  'fields'      => array(

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Top Hook", 'maharaj' ),
	  ),

	  array(
		'id'  	=> 'enable-top-hook',
		'type'  => 'switcher',
		'title' => esc_html__('Enable Top Hook', 'maharaj'),
		'info'	=> esc_html__("YES! to enable top hook.", 'maharaj')
	  ),

	  array(
		'id'  		 => 'top-hook',
		'type'  	 => 'textarea',
		'title' 	 => esc_html__('Top Hook', 'maharaj'),
		'info'		 => esc_html__('Paste your top hook, Executes after the opening &lt;body&gt; tag.', 'maharaj')
	  ),

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Content Before Hook", 'maharaj' ),
	  ),

	  array(
		'id'  	=> 'enable-content-before-hook',
		'type'  => 'switcher',
		'title' => esc_html__('Enable Content Before Hook', 'maharaj'),
		'info'	=> esc_html__("YES! to enable content before hook.", 'maharaj')
	  ),

	  array(
		'id'  		 => 'content-before-hook',
		'type'  	 => 'textarea',
		'title' 	 => esc_html__('Content Before Hook', 'maharaj'),
		'info'		 => esc_html__('Paste your content before hook, Executes before the opening &lt;#primary&gt; tag.', 'maharaj')
	  ),

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Content After Hook", 'maharaj' ),
	  ),

	  array(
		'id'  	=> 'enable-content-after-hook',
		'type'  => 'switcher',
		'title' => esc_html__('Enable Content After Hook', 'maharaj'),
		'info'	=> esc_html__("YES! to enable content after hook.", 'maharaj')
	  ),

	  array(
		'id'  		 => 'content-after-hook',
		'type'  	 => 'textarea',
		'title' 	 => esc_html__('Content After Hook', 'maharaj'),
		'info'		 => esc_html__('Paste your content after hook, Executes after the closing &lt;/#main&gt; tag.', 'maharaj')
	  ),

	  array(
		'type'    => 'subheading',
		'content' => esc_html__( "Bottom Hook", 'maharaj' ),
	  ),

	  array(
		'id'  	=> 'enable-bottom-hook',
		'type'  => 'switcher',
		'title' => esc_html__('Enable Bottom Hook', 'maharaj'),
		'info'	=> esc_html__("YES! to enable bottom hook.", 'maharaj')
	  ),

	  array(
		'id'  		 => 'bottom-hook',
		'type'  	 => 'textarea',
		'title' 	 => esc_html__('Bottom Hook', 'maharaj'),
		'info'		 => esc_html__('Paste your bottom hook, Executes after the closing &lt;/body&gt; tag.', 'maharaj')
	  ),

   ),
);

// ------------------------------
// backup                       
// ------------------------------
$options[]   = array(
  'name'     => 'backup_section',
  'title'    => esc_html__('Backup', 'maharaj'),
  'icon'     => 'fa fa-shield',
  'fields'   => array(

    array(
      'type'    => 'notice',
      'class'   => 'warning',
      'content' => esc_html__('You can save your current options. Download a Backup and Import.', 'maharaj')
    ),

    array(
      'type'    => 'backup',
    ),

  )
);

// ------------------------------
// license
// ------------------------------
$options[]   = array(
  'name'     => 'theme_version',
  'title'    => constant('MAHARAJ_THEME_NAME').esc_html__(' Log', 'maharaj'),
  'icon'     => 'fa fa-info-circle',
  'fields'   => array(

    array(
      'type'    => 'heading',
      'content' => constant('MAHARAJ_THEME_NAME').esc_html__(' Theme Change Log', 'maharaj')
    ),
    array(
      'type'    => 'content',
      'content' => '<pre>
2017.12.02 - version 1.1
 * Optimized Dummy Content Updated

2017.11.29 - version 1.0
 * First release!  </pre>',
    ),

  )
);

// ------------------------------
// Seperator
// ------------------------------
$options[] = array(
  'name'   => 'seperator_1',
  'title'  => esc_html__('Plugin Options', 'maharaj'),
  'icon'   => 'fa fa-plug'
);


CSFramework::instance( $settings, $options );