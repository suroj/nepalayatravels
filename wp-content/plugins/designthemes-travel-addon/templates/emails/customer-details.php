<?php
/*
 * @Author : designthemes
 * @Date   : 10/30/2017
 * @Last Modified by: designthemes
 * @Last Modified time: 10/30/2017
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit();
}
?>
<h2 class="section-title"><?php _e( 'Customer details', 'designthemes-travel' ) ?></h2>
<ul>
    <li><strong><?php echo esc_html__( 'Customer Name:', 'designthemes-travel' ); ?></strong>
        <span><?php printf( '%s', $order_settings['first_name'] ) ?></span>
    </li>
    <li><strong><?php echo esc_html__( 'Email address:', 'designthemes-travel' ); ?></strong>
        <a href="mailto:<?php echo esc_attr( $order_settings['email_address'] ); ?>"><?php echo esc_html( $order_settings['email_address'] ); ?></a>
    </li>
    <li><strong><?php echo esc_html__( 'Phone:', 'designthemes-travel' ); ?></strong>
        <span class="text"><?php echo esc_html( $order_settings['phone_no'] ); ?></span>
    </li>
</ul>

<h2 class="section-title"><?php _e( 'Billing address', 'designthemes-travel' ) ?></h2>
<ul>
    <li><strong><?php echo esc_html__( 'Address:', 'designthemes-travel' ); ?></strong>
        <span>
			<?php printf( '%s', $order_settings['address'] ) ?><br>
			<?php printf( '%s', $order_settings['city'] ) ?><br>
			<?php printf( '%s', $order_settings['state'] ) ?><br>
			<?php printf( '%s', $order_settings['country_name'] ) ?><br>
        </span>
    </li>
    <li><strong><?php echo esc_html__( 'Postal Code:', 'designthemes-travel' ); ?></strong>
        <span><?php echo esc_html( $order_settings['postal_code'] ) ?></span>
    </li>
	<?php if ( $order_settings['notes'] != '' ) : ?>
        <li><strong><?php echo esc_html__( 'Addition Information:', 'designthemes-travel' ); ?></strong>
            <span><?php echo esc_html( $order_settings['notes'] ) ?></span>
        </li>
	<?php endif; ?>
</ul>