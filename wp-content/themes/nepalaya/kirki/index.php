<?php

require_once get_template_directory() . '/kirki/kirki-utils.php';
require_once get_template_directory() . '/kirki/include-kirki.php';
require_once get_template_directory() . '/kirki/kirki.php';

$config = maharaj_kirki_config();

add_action('customize_register', 'maharaj_customize_register');
function maharaj_customize_register( $wp_customize ) {

	$wp_customize->remove_section( 'colors' );
	$wp_customize->remove_section( 'header_image' );
	$wp_customize->remove_section( 'background_image' );
	$wp_customize->remove_section( 'static_front_page' );

	$wp_customize->remove_section('themes');
	$wp_customize->get_section('title_tagline')->priority = 10;
}

add_action( 'customize_controls_print_styles', 'maharaj_enqueue_customizer_stylesheet' );
function maharaj_enqueue_customizer_stylesheet() {
	wp_register_style( 'maharaj-customizer-css', MAHARAJ_THEME_URI.'/kirki/assets/css/customizer.css', NULL, NULL, 'all' );
	wp_enqueue_style( 'maharaj-customizer-css' );	
}

add_action( 'customize_controls_print_footer_scripts', 'maharaj_enqueue_customizer_script' );
function maharaj_enqueue_customizer_script() {
	wp_register_script( 'maharaj-customizer-js', MAHARAJ_THEME_URI.'/kirki/assets/js/customizer.js', array('jquery', 'customize-controls' ), false, true );
	wp_enqueue_script( 'maharaj-customizer-js' );
}

# Theme Customizer Begins
MAHARAJ_Kirki::add_config( $config , array(
	'capability'    => 'edit_theme_options',
	'option_type'   => 'theme_mod',
) );

	# Site Identity	
		# use-custom-logo
		MAHARAJ_Kirki::add_field( $config, array(
			'type'     => 'switch',
			'settings' => 'use-custom-logo',
			'label'    => __( 'Logo ?', 'maharaj' ),
			'section'  => 'title_tagline',
			'priority' => 1,
			'default'  => maharaj_defaults('use-custom-logo'),
			'description' => __('This is test description','maharaj'),
			'choices'  => array(
				'on'  => esc_attr__( 'Logo', 'maharaj' ),
				'off' => esc_attr__( 'Site Title', 'maharaj' )
			)			
		) );

		# custom-logo
		MAHARAJ_Kirki::add_field( $config, array(
			'type' => 'image',
			'settings' => 'custom-logo',
			'label'    => __( 'Logo', 'maharaj' ),
			'section'  => 'title_tagline',
			'priority' => 2,
			'default' => maharaj_defaults( 'custom-logo' ),
			'active_callback' => array(
				array( 'setting' => 'use-custom-logo', 'operator' => '==', 'value' => '1' )
			)
		));

		# custom-light-logo
		MAHARAJ_Kirki::add_field( $config, array(
			'type' => 'image',
			'settings' => 'custom-light-logo',
			'label'    => __( 'Light Logo', 'maharaj' ),
			'section'  => 'title_tagline',
			'priority' => 3,
			'default' => maharaj_defaults( 'custom-light-logo' ),
			'active_callback' => array(
				array( 'setting' => 'use-custom-logo', 'operator' => '==', 'value' => '1' )
			)
		));		

	# Site Layout
	require_once get_template_directory() . '/kirki/options/site-layout.php';

	# Site Skin
	require_once get_template_directory() . '/kirki/options/site-skin.php';

	# Additional JS
	require_once get_template_directory() . '/kirki/options/custom-js.php';

	# Typography
	MAHARAJ_Kirki::add_panel( 'dt_site_typography_panel', array(
		'title' => __( 'Typography', 'maharaj' ),
		'description' => __('Typography Settings','maharaj'),
		'priority' => 220
	) );	
	require_once get_template_directory() . '/kirki/options/site-typography.php';	
# Theme Customizer Ends