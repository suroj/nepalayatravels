<?php
class DTTravelAddonSCsDefinition {

	function __construct() {

		// Search Form
		add_shortcode ( "dt_sc_rooms_search_form", array (
			$this,
			"dt_sc_rooms_search_form"
		) );

		// Cart
		add_shortcode ( "dt_sc_rooms_cart", array (
			$this,
			"dt_sc_rooms_cart"
		) );

		// Checkout
		add_shortcode ( "dt_sc_rooms_checkout", array (
			$this,
			"dt_sc_rooms_checkout"
		) );

		// Account
		add_shortcode ( "dt_sc_user_account", array (
			$this,
			"dt_sc_user_account"
		) );

		// Rooms List
		add_shortcode ( "dt_sc_rooms_list", array (
			$this,
			"dt_sc_rooms_list"
		) );

		// Rooms Item
		add_shortcode ( "dt_sc_room_item", array (
			$this,
			"dt_sc_room_item"
		) );

		// Room Amenities
		add_shortcode ( "dt_sc_room_amenities", array (
			$this,
			"dt_sc_room_amenities"
		) );

		// Packages
		add_shortcode ( "dt_sc_packages_list", array (
			$this,
			"dt_sc_packages_list"
		) );
	}

	/**
	 * @param string $content
	 * @return string
	 */
	static function dtShortcodeHelper($content = null) {
		$content = do_shortcode ( shortcode_unautop ( $content ) );
		$content = preg_replace ( '#^<\/p>|^<br \/>|<p>$#', '', $content );
		$content = preg_replace ( '#<br \/>#', '', $content );
		return trim ( $content );
	}

	/**
	 * search form
	 * @return string
	 */
	function dt_sc_rooms_search_form($attrs, $content = null ){
		extract( shortcode_atts( array(
			'title' => '',
			'checkin' => esc_html__('Checkin Date', 'designthemes-travel'),
			'checkout' => esc_html__('Checkout Date', 'designthemes-travel'),
			'adults' => esc_html__('Adults', 'designthemes-travel'),
			'childs' => esc_html__('Children', 'designthemes-travel'),
			'btn_text' => esc_html__('Check Availability', 'designthemes-travel'),
			'search_page' => '',
			'class'	=> ''
		), $attrs ) );

		$out = '';
		$random = uniqid();

		// find the url for form action
		$search_permalink = '';
		if ( isset( $attrs['search_page'] ) && $search_page = $attrs['search_page'] ) {
			if ( is_numeric( $search_page ) ) {
				$search_permalink = get_the_permalink( $search_page );
			} else {
				$search_permalink = $search_page;
			}
		} else {
			$search_permalink = dt_travel_search_url();
		}
		$search_page = $search_permalink;

		$out .= '<div id="dt-travel-booking-search-'.$random.'" class="dt-travel-booking-search '.$class.'">';
			if( !empty( $title ) ):
				$out .= '<h3>'.$title.'</h3>';
			endif;
			$out .= '<form name="dt-travel-search-form" action="'.$search_page.'" class="dt-travel-search-form-'.$random.'">';
				$out .= '<ul class="dt-form-table">';
					$out .= '<li class="dt-form-field">';
						$out .= '<label>'.$checkin.'</label>';
						$out .= '<div class="dt-form-field-input dt_input_field">';
							$out .= '<input id="chkin_date_'.$random.'" name="checkin_date" class="dt_input_date_check" placeholder="'.$checkin.'" type="text" required="required">';
						$out .= '</div>';
					$out .= '</li>';

					$out .= '<li class="dt-form-field">';
						$out .= '<label>'.$checkout.'</label>';
						$out .= '<div class="dt-form-field-input dt_input_field">';
							$out .= '<input id="chkout_date_'.$random.'" name="checkout_date" class="dt_input_date_check" placeholder="'.$checkout.'" type="text" required="required">';
						$out .= '</div>';
					$out .= '</li>';

					$out .= '<li class="dt-form-field">';
						$out .= '<label>'.$adults.'</label>';
						$out .= '<div class="dt-form-field-input">';
							$out .= '<select name="adults_capacity" required="required">';
								$out .= '<option value="">'.$adults.'</option><option value="1">1</option><option value="2">2</option><option value="3">3</option><option value="4">4</option><option value="5">5</option><option value="6">6</option><option value="7">7</option><option value="8">8</option><option value="9">9</option><option value="10">10</option>';
							$out .= '</select>';
						$out .= '</div>';
					$out .= '</li>';

					$out .= '<li class="dt-form-field">';
						$out .= '<label>'.$childs.'</label>';
						$out .= '<div class="dt-form-field-input">';
							$out .= '<select name="max_child"><option value="0">'.$childs.'</option><option value="1">1</option><option value="2">2</option><option value="3">3</option></select>';
						$out .= '</div>';
					$out .= '</li>';
				$out .= '</ul>';
				$out .= '<input id="nonce" name="nonce" value="'.wp_create_nonce( 'dt_room_search' ).'" type="hidden">';
				$out .= '<input type="hidden" name="room-booking" value="results" />';
				$out .= '<p class="dt-form-submit">';
					$out .= '<button type="submit">'.$btn_text.'</button>';
				$out .= '</p>';
			$out .= '</form>';
		$out .= '</div>';

		$page 		   = dt_travel_get_request( 'room-booking' );
		$template      = '';
		$template_args = array();
		// Display the template based on current step
		switch ( $page ) {
			case 'results':
				if ( ! wp_verify_nonce( dt_travel_get_request( 'nonce' ), 'dt_room_search' ) ) {
					break;
				}

				$start_date = dt_travel_get_request( 'checkin_date' );
				$end_date = dt_travel_get_request( 'checkout_date' );

				$template                 = 'search/results.php';				
				$template_args['results'] = dt_travel_search_rooms(
					array(
						'check_in_date'  => $start_date,
						'check_out_date' => $end_date,
						'adults'         => dt_travel_get_request( 'adults_capacity', 1 ),
						'max_child'      => dt_travel_get_request( 'max_child', 0 )
					)
				);
				break;
			default:
				return $out;
				break;
		}
		$template = apply_filters( 'room_booking_search_form_template', $template );
		ob_start();
		dt_travel_get_template( $template, $template_args );

		return ob_get_clean();
	}

	/**
	 * rooms cart
	 * @return string
	 */
	function dt_sc_rooms_cart($attrs, $content = null ){
		extract( shortcode_atts( array(
			'title' => esc_html__('Cart', 'designthemes-travel')
		), $attrs ) );

        $template = apply_filters( 'room_booking_cart_template', 'cart/cart.php' );
		$template_args['title'] = $title;
        ob_start();
        dt_travel_get_template( $template, $template_args );

        return ob_get_clean();
	}

	/**
	 * rooms checkout
	 * @return string
	 */
	function dt_sc_rooms_checkout($attrs, $content = null ){

        $customer = new stdClass;
        $customer->first_name = '';
        $customer->last_name = '';
        $customer->email = '';
        $customer->address = '';
        $customer->state = '';
        $customer->city = '';
        $customer->postal_code = '';
        $customer->country = '';
        $customer->phone = '';
        $customer->fax = '';

        if ( is_user_logged_in() ) {
			$user_id = get_current_user_id();
			$user_info = wp_get_current_user();

            $user = get_user_meta( $user_id, '_dt_travel_user_fields', TRUE );

            $customer->first_name = isset( $user['first_name'] ) ? $user['first_name'] : $user_info->user_firstname;
            $customer->last_name = isset( $user['last_name'] ) ? $user['last_name'] : $user_info->user_lastname;
            $customer->email = isset( $user['email_address'] ) ? $user['email_address'] : $user_info->user_email;
            $customer->address = isset( $user['address'] ) ? $user['address'] : '';
            $customer->state = isset( $user['state'] ) ? $user['state'] : '';
            $customer->city = isset( $user['city'] ) ? $user['city'] : '';
            $customer->postal_code = isset( $user['postal_code'] ) ? $user['postal_code'] : '';
            $customer->country = isset( $user['country_name'] ) ? $user['country_name'] : '';
            $customer->phone = isset( $user['phone_no'] ) ? $user['phone_no'] : '';
            $customer->fax = isset( $user['fax_no'] ) ? $user['fax_no'] : '';
        }

        $template = apply_filters( 'room_booking_checkout_template', 'checkout/checkout.php' );
        $template_args = array( 'customer' => $customer );
        ob_start();
        dt_travel_get_template( $template, $template_args );

        return ob_get_clean();
	}

	/**
	 * user account
	 * @return string
	 */
	function dt_sc_user_account($attrs, $content = null ){
	
        $template = apply_filters( 'room_booking_account_template', 'account/account.php' );
        ob_start();
        dt_travel_get_template( $template, $attrs );

        return ob_get_clean();
    }

	/**
	 * rooms list
	 * @return string
	 */
	function dt_sc_rooms_list($attrs, $content = null ){
		extract( shortcode_atts( array(
			'count' => -1,
			'column' => 'one-third-column',
			'terms' => '',
			'style' => 'default',
		), $attrs ) );

		$out = '';

		switch($column):
			case 'one-fourth-column':
				$post_class = " dt-room column dt-sc-one-fourth";
				$columns = 4;
			break;

			case 'one-third-column':
				$post_class = " dt-room column dt-sc-one-third";
				$columns = 3;
			break;

			default:
			case 'one-half-column':
				$post_class = " dt-room column dt-sc-one-half";
				$columns = 2;
			break;
		endswitch;

		$categories = isset($terms) ? array_filter( explode(",",$terms) ) : array();

		$paged = 1;
		if ( get_query_var('paged') ) { 
			$paged = get_query_var('paged');
		} elseif ( get_query_var('page') ) {
			$paged = get_query_var('page');
		}

		$args = array();
		if( empty($categories) ):
			$args = array( 'paged' => $paged ,'posts_per_page' => $count,'post_type' => 'dt_rooms');
		else:
			$args = array(
				'paged' => $paged,
				'posts_per_page' => $count,
				'post_type' => 'dt_rooms',
				'orderby' => 'ID',
				'order' => 'ASC',
				'tax_query' => array( 
					array(
						'taxonomy' => 'dt_room_types',
						'field' => 'term_id',
						'operator' => 'IN',
						'terms' => $categories
					)
				)
			);
		endif;

		$the_query = new WP_Query($args);
		if ( $the_query->have_posts() ) :
			$i = 1;

			$out = '<div class="dt-rooms tp-room-booking">';

				while ( $the_query->have_posts() ) : $the_query->the_post();
					$temp_class = '';
					if($i == 1) $temp_class .= $post_class." first"; else $temp_class .= $post_class;
					if($i == $columns) $i = 1; else $i = $i + 1;

					$out .= '<div class="'.esc_attr( trim($temp_class)).'">';
						ob_start();
						dt_travel_get_template_part( 'content', 'room' );
						$out .= ob_get_clean();
					$out .= '</div>';

                endwhile;

			$out .= '</div>';

		endif;

		$out .= '<div class="pagination blog-pagination">';
			$out .= maharaj_pagination();
		$out .= '</div>';

		wp_reset_postdata();

        return $out;
	}

	/**
	 * rooms item
	 * @return string
	 */
	function dt_sc_room_item($attrs, $content = null ){
		extract( shortcode_atts( array(
			'id' => '',
			'style' => 'default',
		), $attrs ) );

		$out = '';

		if( !empty( $id ) ):

			$args = array( 'p' => $id, 'post_type' => 'dt_rooms' );
			$the_query = new WP_Query($args);
			if ( $the_query->have_posts() ) :

				while ( $the_query->have_posts() ) : $the_query->the_post();

					$out .= '<div class="dt-room column dt-sc-one-column">';
						ob_start();
						dt_travel_get_template_part( 'content', 'room' );
						$out .= ob_get_clean();
					$out .= '</div>';

                endwhile;

			endif;

			wp_reset_postdata();

		endif;

        return $out;
	}

	/**
	 * rooms amenities
	 * @return string
	 */
	function dt_sc_room_amenities($attrs, $content = null ){
		extract( shortcode_atts( array(
			'title' => '',
			'room_id' => '',
			'count' => 6,
			'show_icons' => 'true',
		), $attrs ) );

		$out = '';

		if( empty( $room_id ) ) {
			global $post;
			$room_id =  $post->ID;
		}

		$out = '<div class="dt-sc-room-amenities">';

			if( !empty( $title ) ) { $out .= '<h3>'.$title.'</h3>'; }

			$terms = get_the_terms( $room_id, 'dt_amenities' );
			if( count( $terms ) ):
				$out .= '<ul class="dt-amenities">';
					$i = 1;
					foreach( $terms as $term ):
						$out .= '<li>';
							$term_id = $term->term_id;
							$name = $term->name;

							$term_meta = get_term_meta( $term_id, '_room_amenities', false );
							$img_id = !empty($term_meta[0]['amenity_icon']) ? $term_meta[0]['amenity_icon'] : '';

							if( !empty ( $img_id ) && $show_icons != 'false' ):
								$image = wp_get_attachment_image_src( $img_id, 'full' );
								$out .= '<img src="'.$image[0].'" />';
							endif;

							$out .= '<span>'.$name.'</span>';
						$out .= '</li>';

						if( $i == $count ) break;
						$i++;
					endforeach;
				$out .= '</ul>';
			endif;

		$out .= '</div>';

        return $out;
	}

	/**
	 * packages list
	 * @return string
	 */
	function dt_sc_packages_list($attrs, $content = null ){
		extract( shortcode_atts( array(
			'count' => -1,
			'column' => 'one-half-column',
			'terms' => ''
		), $attrs ) );

		$out = '';

		if( function_exists( 'is_woocommerce' ) ):

			switch($column):
				case 'one-fourth-column':
					$post_class = " dt-package column dt-sc-one-fourth";
					$columns = 4;
				break;
	
				case 'one-third-column':
					$post_class = " dt-package column dt-sc-one-third";
					$columns = 3;
				break;
	
				default:
				case 'one-half-column':
					$post_class = " dt-package column dt-sc-one-half";
					$columns = 2;
				break;
			endswitch;
	
			$categories = isset($terms) ? array_filter( explode(",",$terms) ) : array();
	
			$paged = 1;
			if ( get_query_var('paged') ) { 
				$paged = get_query_var('paged');
			} elseif ( get_query_var('page') ) {
				$paged = get_query_var('page');
			}
	
			$args = array();
			if( empty($categories) ):
				$args = array( 'paged' => $paged ,'posts_per_page' => $count,'post_type' => 'product');
			else:
				$args = array(
					'paged' => $paged,
					'posts_per_page' => $count,
					'post_type' => 'product',
					'orderby' => 'ID',
					'order' => 'ASC',
					'tax_query' => array( 
						array(
							'taxonomy' => 'product_cat',
							'field' => 'term_id',
							'operator' => 'IN',
							'terms' => $categories
						)
					)
				);
			endif;
	
			$the_query = new WP_Query($args);
			if ( $the_query->have_posts() ) :
				$j = 1;
	
				$out = '<div class="dt-packages">';
	
					while ( $the_query->have_posts() ) : $the_query->the_post();
						$temp_class = '';
						if($j == 1) $temp_class .= $post_class." first"; else $temp_class .= $post_class;
						if($j == $columns) $j = 1; else $j = $j + 1;

						$out .= '<div class="'.esc_attr( trim($temp_class)).'">';
							ob_start();
	
							echo '<div class="dt-sc-package-thumb">';
								woocommerce_template_loop_product_link_open();
									woocommerce_template_loop_product_title();
									woocommerce_template_loop_product_thumbnail();
								woocommerce_template_loop_product_link_close();
							echo '</div>';

							echo '<div class="dt-sc-package-details">';
	
								$product_settings = get_post_meta ( get_the_ID(), '_product_settings', TRUE );
								$product_settings = is_array ( $product_settings ) ? $product_settings : array ();
	
								if( array_key_exists('product_opt_flds', $product_settings) ):
									echo '<div class="dt-sc-package-meta">';
										echo '<ul class="project-list">';
											for( $i = 1; $i <= sizeof($product_settings['product_opt_flds']) / 2; $i++ ):
	
												$label = $product_settings['product_opt_flds']["product_opt_flds_title_{$i}"];
												$value = $product_settings['product_opt_flds']["product_opt_flds_value_{$i}"]; ?>
	
												<li> <span><?php echo esc_html($label);?></span><?php echo ($value);?></li><?php
											endfor;
										echo '</ul>';
									echo '</div>';
								endif;

								echo '<div class="price-wrapper">';
									woocommerce_template_loop_price();
									echo '<span class="unit">'.esc_html__( 'per / member', 'designthemes-travel' ).'</span>';
								echo '</div>';
							echo '</div>';
	
							$out .= ob_get_clean();
						$out .= '</div>';
	
					endwhile;
	
				$out .= '</div>';
	
			endif;
	
			$out .= '<div class="pagination blog-pagination">';
				$out .= maharaj_pagination();
			$out .= '</div>';

			wp_reset_postdata();
		else:
			$out .= '<div class="dt-sc-warning-box">'.esc_html__( 'Please install & activate Woocommerce plugin to use this module.', 'designthemes-travel' ).'</div>';
		endif;

        return $out;
	}
}
new DTTravelAddonSCsDefinition();?>