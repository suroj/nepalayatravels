<?php
if (! class_exists ( 'DTTravelAddonWidgets' )) {

	class DTTravelAddonWidgets {

		function __construct() {

			add_action( 'admin_enqueue_scripts', array( $this, 'dt_travel_admin_scripts' ) );
			add_action( 'widgets_init', array ( $this, 'dt_travel_widgets_init' ) );

		}

		function dt_travel_admin_scripts() {

			wp_enqueue_script('dt-travel-media', plugins_url ('designthemes-travel-addon') . '/widgets/js/media.js', array ('media-upload'), false, true );
		}

		function dt_travel_widgets_init() { }
	}
} ?>