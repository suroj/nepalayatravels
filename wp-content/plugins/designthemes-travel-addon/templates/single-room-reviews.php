<?php
/**
 * Display single room reviews (comments)
 *
 * Override this template by copying it to yourtheme/tp-room-booking/single-room-reviews.php
 *
 * @author        designthemes
 * @package       designthemes-travel-addon/templates
 * @version       1.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;
if ( !comments_open() ) {
	return;
}
?>
<div id="reviews">
    <div id="comments">
        <h3>
			<?php
			if ( cs_get_option( 'single-room-comments' ) && ( $count = dt_travel_get_review_count($post->ID) ) )
				printf( _n( '%s review for %s', '%s reviews for %s', $count, 'designthemes-travel' ), $count, get_the_title() );
			else
				_e( 'Reviews', 'designthemes-travel' );
			?>
        </h3>

		<?php if ( have_comments() ) : ?>

            <ul class="commentlist">
				<?php wp_list_comments( apply_filters( 'dt_travel_room_review_list_args', array( 'callback' => 'dt_travel_room_comments' ) ) ); ?>
            </ul>

			<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
				echo '<nav class="dt-pagination">';
				paginate_comments_links( apply_filters( 'dt_travel_comment_pagination_args', array(
					'prev_text' => '&larr;',
					'next_text' => '&rarr;',
					'type'      => 'list',
				) ) );
				echo '</nav>';
			endif; ?>

		<?php else : ?>

            <p class="dt-noreviews"><?php _e( 'There are no reviews yet.', 'designthemes-travel' ); ?></p>

		<?php endif; ?>
    </div>

    <div id="review_form_wrapper">
        <div id="review_form">
            <?php
            $commenter    = wp_get_current_commenter();
            $comment_form = array(
                'title_reply'          => have_comments() ? __( 'Add a review', 'designthemes-travel' ) : __( 'Be the first to review', 'designthemes-travel' ) . ' &ldquo;' . get_the_title() . '&rdquo;',
                'title_reply_to'       => __( 'Leave a Reply to %s', 'designthemes-travel' ),
                'comment_notes_before' => '',
                'comment_notes_after'  => '',
                'fields'               => array(
                    'author' => '<div class="column dt-sc-one-half first"><p class="comment-form-author">' . '<label for="author">' . __( 'Name', 'designthemes-travel' ) . ' <span class="required">*</span></label> ' . '<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" aria-required="true" /></p></div>',
                    'email'  => '<div class="column dt-sc-one-half"><p class="comment-form-email"><label for="email">' . __( 'Email', 'designthemes-travel' ) . ' <span class="required">*</span></label> ' . '<input id="email" name="email" type="text" value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30" aria-required="true" /></p></div>',
                ),
                'label_submit'         => __( 'Submit', 'designthemes-travel' ),
                'logged_in_as'         => '',
                'comment_field'        => ''
            );

            $comment_form['comment_field'] = '<p class="comment-form-comment"><label for="comment">' . __( 'Your Review', 'designthemes-travel' ) . '</label><textarea id="comment" name="comment" cols="45" rows="8" aria-required="true"></textarea></p>';

            if ( cs_get_option( 'room-archives-rating' ) ) {
                $comment_form['comment_field'] .= '<p class="comment-form-rating"><label for="rating">' . __( 'Your Rating', 'designthemes-travel' ) . '</label>
                    </p><div class="dt-rating-input"></div>';
            }

            comment_form( apply_filters( 'dt_travel_room_comment_form_fields', $comment_form ) );
            ?>
        </div>
    </div>
    <div class="clear"></div>
</div>