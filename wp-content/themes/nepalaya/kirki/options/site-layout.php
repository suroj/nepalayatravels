<?php
$config = maharaj_kirki_config();

MAHARAJ_Kirki::add_section( 'dt_site_layout_section', array(
	'title' => __( 'Site Layout', 'maharaj' ),
	'priority' => 20
) );

	# site-layout
	MAHARAJ_Kirki::add_field( $config, array(
		'type'     => 'radio-image',
		'settings' => 'site-layout',
		'label'    => __( 'Site Layout', 'maharaj' ),
		'section'  => 'dt_site_layout_section',
		'default'  => maharaj_defaults('site-layout'),
		'choices' => array(
			'boxed' =>  MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/boxed.png',
			'wide' => MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/wide.png',
		)
	));

	# site-boxed-layout
	MAHARAJ_Kirki::add_field( $config, array(
		'type'     => 'switch',
		'settings' => 'site-boxed-layout',
		'label'    => __( 'Customize Boxed Layout?', 'maharaj' ),
		'section'  => 'dt_site_layout_section',
		'default'  => '1',
		'choices'  => array(
			'on'  => esc_attr__( 'Yes', 'maharaj' ),
			'off' => esc_attr__( 'No', 'maharaj' )
		),
		'active_callback' => array(
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
		)			
	));

	# body-bg-type
	MAHARAJ_Kirki::add_field( $config, array(
		'type' => 'select',
		'settings' => 'body-bg-type',
		'label'    => __( 'Background Type', 'maharaj' ),
		'section'  => 'dt_site_layout_section',
		'multiple' => 1,
		'default'  => 'none',
		'choices'  => array(
			'pattern' => esc_attr__( 'Predefined Patterns', 'maharaj' ),
			'upload' => esc_attr__( 'Set Pattern', 'maharaj' ),
			'none' => esc_attr__( 'None', 'maharaj' ),
		),
		'active_callback' => array(
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
			array( 'setting' => 'site-boxed-layout', 'operator' => '==', 'value' => '1' )
		)
	));

	# body-bg-pattern
	MAHARAJ_Kirki::add_field( $config, array(
		'type'     => 'radio-image',
		'settings' => 'body-bg-pattern',
		'label'    => __( 'Predefined Patterns', 'maharaj' ),
		'description'    => __( 'Add Background for body', 'maharaj' ),
		'section'  => 'dt_site_layout_section',
		'output' => array(
			array( 'element' => 'body' , 'property' => 'background-image' )
		),
		'choices' => array(
			MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern1.jpg'=> MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern1.jpg',
			MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern2.jpg'=> MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern2.jpg',
			MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern3.jpg'=> MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern3.jpg',
			MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern4.jpg'=> MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern4.jpg',
			MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern5.jpg'=> MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern5.jpg',
			MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern6.jpg'=> MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern6.jpg',
			MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern7.jpg'=> MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern7.jpg',
			MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern8.jpg'=> MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern8.jpg',
			MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern9.jpg'=> MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern9.jpg',
			MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern10.jpg'=> MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern10.jpg',
			MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern11.jpg'=> MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern11.jpg',
			MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern12.jpg'=> MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern12.jpg',
			MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern13.jpg'=> MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern13.jpg',
			MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern14.jpg'=> MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern14.jpg',
			MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern15.jpg'=> MAHARAJ_THEME_URI.'/kirki/assets/images/site-layout/pattern15.jpg',
		),
		'active_callback' => array(
			array( 'setting' => 'body-bg-type', 'operator' => '==', 'value' => 'pattern' ),
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
			array( 'setting' => 'site-boxed-layout', 'operator' => '==', 'value' => '1' )
		)						
	));

	# body-bg-image
	MAHARAJ_Kirki::add_field( $config, array(
		'type' => 'image',
		'settings' => 'body-bg-image',
		'label'    => __( 'Background Image', 'maharaj' ),
		'description'    => __( 'Add Background Image for body', 'maharaj' ),
		'section'  => 'dt_site_layout_section',
		'output' => array(
			array( 'element' => 'body' , 'property' => 'background-image' )
		),
		'active_callback' => array(
			array( 'setting' => 'body-bg-type', 'operator' => '==', 'value' => 'upload' ),
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
			array( 'setting' => 'site-boxed-layout', 'operator' => '==', 'value' => '1' )
		)
	));

	# body-bg-position
	MAHARAJ_Kirki::add_field( $config, array(
		'type' => 'select',
		'settings' => 'body-bg-position',
		'label'    => __( 'Background Position', 'maharaj' ),
		'section'  => 'dt_site_layout_section',
		'output' => array(
			array( 'element' => 'body' , 'property' => 'background-position' )
		),
		'default' => 'center',
		'multiple' => 1,
		'choices' => maharaj_image_positions(),
		'active_callback' => array(
			array( 'setting' => 'body-bg-type', 'operator' => 'contains', 'value' => array( 'pattern', 'upload') ),
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
			array( 'setting' => 'site-boxed-layout', 'operator' => '==', 'value' => '1' )
		)
	));

	# body-bg-repeat
	MAHARAJ_Kirki::add_field( $config, array(
		'type' => 'select',
		'settings' => 'body-bg-repeat',
		'label'    => __( 'Background Repeat', 'maharaj' ),
		'section'  => 'dt_site_layout_section',
		'output' => array(
			array( 'element' => 'body' , 'property' => 'background-repeat' )
		),
		'default' => 'repeat',
		'multiple' => 1,
		'choices' => maharaj_image_repeats(),
		'active_callback' => array(
			array( 'setting' => 'body-bg-type', 'operator' => 'contains', 'value' => array( 'pattern', 'upload' ) ),
			array( 'setting' => 'site-layout', 'operator' => '==', 'value' => 'boxed' ),
			array( 'setting' => 'site-boxed-layout', 'operator' => '==', 'value' => '1' )
		)
	));	