<?php
/**
 * Theme Functions
 *
 * @package DTtheme
 * @author DesignThemes
 * @link http://wedesignthemes.com
 */
define( 'MAHARAJ_THEME_DIR', get_template_directory() );
define( 'MAHARAJ_THEME_URI', get_template_directory_uri() );
define( 'MAHARAJ_CORE_PLUGIN', WP_PLUGIN_DIR.'/designthemes-core-features' );

if (function_exists ('wp_get_theme')) :
	$themeData = wp_get_theme();
	define( 'MAHARAJ_THEME_NAME', $themeData->get('Name'));
	define( 'MAHARAJ_THEME_VERSION', $themeData->get('Version'));
endif;

/* ---------------------------------------------------------------------------
 * Loads Kirki
 * ---------------------------------------------------------------------------*/
require_once( MAHARAJ_THEME_DIR .'/kirki/index.php' );

/* ---------------------------------------------------------------------------
 * Loads Codestar
 * ---------------------------------------------------------------------------*/
require_once MAHARAJ_THEME_DIR .'/cs-framework/cs-framework.php';

define( 'CS_ACTIVE_SHORTCODE',  false );
define( 'CS_ACTIVE_CUSTOMIZE',  false );

/* ---------------------------------------------------------------------------
 * Create function to get theme options
 * --------------------------------------------------------------------------- */
function maharaj_cs_get_option($key, $value = '') {

	$v = cs_get_option( $key );

	if ( !empty( $v ) ) {
		return $v;
	} else {
		return $value;
	}
}

/* ---------------------------------------------------------------------------
 * Loads Theme Textdomain
 * ---------------------------------------------------------------------------*/ 
define( 'MAHARAJ_LANG_DIR', MAHARAJ_THEME_DIR. '/languages' );
load_theme_textdomain( 'maharaj', MAHARAJ_LANG_DIR );

/* ---------------------------------------------------------------------------
 * Loads the Admin Panel Style
 * ---------------------------------------------------------------------------*/
function maharaj_admin_scripts() {
	wp_enqueue_style('maharaj-admin', MAHARAJ_THEME_URI .'/cs-framework-override/style.css');
}
add_action( 'admin_enqueue_scripts', 'maharaj_admin_scripts' );

/* ---------------------------------------------------------------------------
 * Loads Theme Functions
 * ---------------------------------------------------------------------------*/

// Functions --------------------------------------------------------------------
require_once( MAHARAJ_THEME_DIR .'/framework/register-functions.php' );

// Menu Walker ------------------------------------------------------------------
require_once( MAHARAJ_THEME_DIR .'/framework/register-menu-walker.php' );

// Header -----------------------------------------------------------------------
require_once( MAHARAJ_THEME_DIR .'/framework/register-head.php' );

// Hooks ------------------------------------------------------------------------
require_once( MAHARAJ_THEME_DIR .'/framework/register-hooks.php' );

// Post Functions ---------------------------------------------------------------
require_once( MAHARAJ_THEME_DIR .'/framework/register-post-functions.php' );
new maharaj_post_functions;

// Widgets ----------------------------------------------------------------------
add_action( 'widgets_init', 'maharaj_widgets_init' );
function maharaj_widgets_init() {
	require_once( MAHARAJ_THEME_DIR .'/framework/register-widgets.php' );

	if(class_exists('DTCorePlugin')) {
		register_widget('Maharaj_Twitter');
	}

	register_widget('Maharaj_Flickr');
	register_widget('Maharaj_Recent_Posts');
	register_widget('Maharaj_Portfolio_Widget');
}
if(class_exists('DTCorePlugin')) {
	require_once( MAHARAJ_THEME_DIR .'/framework/widgets/widget-twitter.php' );
}
require_once( MAHARAJ_THEME_DIR .'/framework/widgets/widget-flickr.php' );
require_once( MAHARAJ_THEME_DIR .'/framework/widgets/widget-recent-posts.php' );
require_once( MAHARAJ_THEME_DIR .'/framework/widgets/widget-recent-portfolios.php' );

// Plugins ---------------------------------------------------------------------- 
require_once( MAHARAJ_THEME_DIR .'/framework/register-plugins.php' );

// WooCommerce ------------------------------------------------------------------
if( function_exists( 'is_woocommerce' ) && ! class_exists ( 'DTWooPlugin' ) ){
	require_once( MAHARAJ_THEME_DIR .'/framework/register-woocommerce.php' );
}

// WP Store Locator -------------------------------------------------------------
if( maharaj_is_plugin_active( 'wp-store-locator/wp-store-locator.php' ) ){
	require_once( MAHARAJ_THEME_DIR .'/framework/register-storelocator.php' );
}?>