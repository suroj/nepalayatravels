<?php
if ( !defined( 'ABSPATH' ) ) {
	exit();
}
?>
<div class="dt-payment-form">
    <div class="dt-col-padding dt-col-border">
        <h4><?php esc_html_e( 'Payment Method', 'designthemes-travel' ); ?></h4>
        <ul class="dt-payment-methods">
			<?php $i = 0; ?>
			<?php if ( cs_get_option('offline-payment') ) : ?>
                <li>
                    <label>
                        <input type="radio" name="dt_payment_method" value="offline-payment"<?php echo ( $i === 0 ) ? ' checked' : '' ?>/>
						<?php esc_html_e( 'Offline Payment', 'designthemes-travel' ); ?>
                    </label>
                    <div class="dt-payment-method-form offline-payment">
                        <?php esc_html_e( 'Pay on arrival', 'designthemes-travel' ); ?>
                    </div>
                </li>
			<?php $i ++; endif; ?>
            
			<?php if ( cs_get_option('enable-paypal') ) : ?>
                <li>
                    <label>
                        <input type="radio" name="dt_payment_method" value="paypal"<?php echo ( $i === 0 ) ? ' checked' : '' ?>/>
						<?php esc_html_e( 'Paypal', 'designthemes-travel' ); ?>
                    </label>
                    <div class="dt-payment-method-form paypal">
                        <?php esc_html_e( 'Pay with Paypal', 'designthemes-travel' ); ?>
                    </div>
                </li>
			<?php endif; ?>
        </ul>
    </div>
</div>