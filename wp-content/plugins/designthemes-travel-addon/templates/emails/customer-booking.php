<?php
/*
 * @Author : designthemes
 * @Date   : 10/30/2017
 * @Last Modified by: designthemes
 * @Last Modified time: 10/30/2017
 */

if ( !defined( 'ABSPATH' ) ) {
	exit();
}

// email heading
dt_travel_get_template( 'emails/email-header.php',
	array(
		'email_heading'      => __( 'Thanks for your booking', 'designthemes-travel' ),
		'email_heading_desc' => __( 'Thank you for making reservation at our hotel. We will try our best to bring the best service. Good luck and see you soon!', 'designthemes-travel' )
	)
);

// order items
dt_travel_get_template( 'emails/order-details.php', array( 'order_settings' => $order_settings, 'order_items' => $order_items ) );

// customer details
dt_travel_get_template( 'emails/customer-details.php', array( 'order_settings' => $order_settings, 'order_items' => $order_items ) );

// email footer
dt_travel_get_template( 'emails/email-footer.php' );