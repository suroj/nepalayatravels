    <?php
        /**
         * maharaj_hook_content_after hook.
         * 
         */
        do_action( 'maharaj_hook_content_after' );
    ?>
    
        <!-- **Footer** -->
        <footer id="footer">
            <div class="container">
            <div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid vc_custom_1510823695267 vc_row-has-fill" style="background: #eee; padding-top: 20px;"><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner "><div class="wpb_wrapper"><div class='dt-sc-title with-two-border alignleft'><h2>Nepalaya Travels LLC</h2></div>
	<div class="wpb_text_column wpb_content_element  vc_custom_1511347340533" >
		<div class="wpb_wrapper">
			<p>Travel makes one modest. After all, you would slowly begin to see what a tiny place you occupy in the world. Therefore travel....as much as you can. As far as you can. As long as you can. Life is not meant to be lived in one place!</p>

		</div>
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-8"><div class="vc_column-inner "><div class="wpb_wrapper"><div class="vc_row wpb_row vc_inner vc_row-fluid"><div class="rs_col-sm-6 wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<ul class="list-none">
<li><a href="#">Hotel Bookings</a></li>
<li><a href="#">Flight Tickets</a></li>
<li><a href="#">Travel Insurance</a></li>
<li><a href="#">Holiday Packages</a></li>
<li><a href="#">Tour Packages</a></li>
</ul>

		</div>
	</div>
</div></div></div><div class="rs_col-sm-6 wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<ul class="list-none">
<li id="menu_2" class="footer_menu"><a href="#">Hotels in Nepal</a></li>
<li id="menu_2" class="footer_menu"><a href="#">Kathmandu Hotels</a></li>
<li id="menu_2" class="footer_menu"><a href="#">Pokhara Hotels</a></li>
<li id="menu_2" class="footer_menu"><a href="#">Vacations</a></li>
</ul>

		</div>
	</div>
</div></div></div><div class="rs_col-sm-6 wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element " >
		<div class="wpb_wrapper">
			<ul class="list-none">
<li><a href="#">Europe Holiday</a></li>
<li><a href="#">American Holiday</a></li>
<li><a href="#">Australia</a></li>
<li><a href="#">Singapore Holiday</a></li>
<li><a href="#">Dubai Holiday</a></li>
</ul>
<img class="img-responsive" src="https://aeroboleto.files.wordpress.com/2017/06/am_sbypp_mc_vs_dc_ae.jpg">

		</div>
	</div>
</div></div></div></div></div></div></div></div><div class="vc_row-full-width vc_clearfix"></div><div data-vc-full-width="true" data-vc-full-width-init="false" class="vc_row wpb_row vc_row-fluid dt-sc-dark-bg vc_custom_1520317718052 vc_row-has-fill" style="background: #2388D3; padding-top: 20px;"><div class="wpb_column vc_column_container vc_col-sm-8"><div class="vc_column-inner "><div class="wpb_wrapper">
	<div class="wpb_text_column wpb_content_element  vc_custom_1519817670607" >
		<div class="wpb_wrapper">
			<p>Copyright 2014-2018. Nepalaya Travels LLC. Ltd. All Rights Reserved.<br />
Design by <a href="#">Wpage Design</a></p>

		</div>
	</div>
</div></div></div><div class="wpb_column vc_column_container vc_col-sm-4"><div class="vc_column-inner "><div class="wpb_wrapper"><ul id='dt-1509945111338-979effdb-0f78' class='dt-sc-sociable small right'
                data-default-style = 'none'
                data-default-border-radius = 'no'
                data-default-shape = ''
                data-hover-style = 'none'
                data-hover-border-radius = 'no'
                data-hover-shape = ''
                ><li class="facebook">  <a href="#" title="" target="_self">      <span class="dt-icon-default"> <span></span> </span>      <i></i>      <span class="dt-icon-hover"> <span></span> </span>  </a></li><li class="twitter">  <a href="#" title="" target="_self">      <span class="dt-icon-default"> <span></span> </span>      <i></i>      <span class="dt-icon-hover"> <span></span> </span>  </a></li></ul></div></div></div></div><div class="vc_row-full-width vc_clearfix"></div>            </div>
        </footer><!-- **Footer - End** -->

    </div><!-- **Inner Wrapper - End** -->
        
</div><!-- **Wrapper - End** -->
<?php
    
    do_action( 'maharaj_hook_bottom' );

    wp_footer();
?>
</body>
</html>