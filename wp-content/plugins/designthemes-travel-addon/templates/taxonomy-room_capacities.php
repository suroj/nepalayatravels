<?php
/**
 * The Template for displaying archive room page
 *
 * Override this template by copying it to yourtheme/tp-room-booking/taxonomy-room_type.php
 *
 * @author        designthemes
 * @package       designthemes-travel-addon/templates
 * @version       1.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

dt_travel_get_template_part( 'archive', 'room' );