<?php
/**
 * Get Encoded Json date with price
 * @return json
 */
function dt_travel_print_pricing_json( $room_id = null, $date = null ) {
	$start = date( 'Y-m-01', strtotime( $date ) );
	$end = date( 'Y-m-t', strtotime( $date ) );

	$begin    = new DateTime($start);
    $stop     = (new DateTime($end))->modify('+1 day');
    $interval = new DateInterval('P1D');
    $period   = new DatePeriod($begin, $interval, $stop);

	$json = array();
	if ( !$room_id || !$date ) {
		return '';
	}

	$regular_arr = dt_travel_get_regular_price_dates($start, $end, $room_id );
	$ranging_arr = dt_travel_get_pricing_plan_dates($start, $end, $room_id );

	foreach ($ranging_arr as $key => $item)
		if ( !( strtotime($key) >= strtotime($start)  &&  strtotime($key) <= strtotime($end) ) )
			unset($ranging_arr[$key]);

	$diff_arr = array_diff_key($regular_arr, $ranging_arr);
	$final_arr = array_merge($diff_arr, $ranging_arr);
	ksort($final_arr);

	$symbol = dt_travel_get_currency_symbol();

	foreach( $final_arr as $k => $v ) {
		$json[] = array(
			'title' => $v ? html_entity_decode($symbol).floatval( $v ) : html_entity_decode($symbol).'0',
			'start' => $k,
			'end' => date( 'Y-m-d', strtotime( '+1 day', strtotime( $k ) ) )
		);
	}

	return json_encode( $json );
}

/**
 * Get between dates with regular price by two dates
 * @return array
 */
function dt_travel_get_regular_price_dates( $dateFrom = '',  $dateTo = '', $room_id = '' ) {

	if( $dateFrom == '' || $dateTo == '' || $room_id == '' )
		return false;

	$temp = array();
	$begin    = new DateTime($dateFrom);
	$stop     = (new DateTime($dateTo))->modify('+1 day');
	$interval = new DateInterval('P1D');
	$period   = new DatePeriod($begin, $interval, $stop);

	$room_meta = get_post_meta( $room_id, '_room_settings', TRUE );
	$room_meta = is_array( $room_meta ) ? $room_meta : array();

	foreach ($period as $dt) {
		$currentDate = $dt->format("Y-m-d");
		$wd = date('w', strtotime($currentDate)) + 1;

		$price = isset( $room_meta['regular_prices']["price_day_{$wd}"] ) ? $room_meta['regular_prices']["price_day_{$wd}"] : '0';
		$temp[$currentDate] = $price;
	}

	return $temp;
}

/**
 * Get between dates with pricing plan price by two dates
 * @return array
 */
function dt_travel_get_pricing_plan_dates( $dateFrom = '',  $dateTo = '', $room_id = '' ) {

	if( $dateFrom == '' || $dateTo == '' || $room_id == '' )
		return false;

	$temp = $datePrice = array();
	$begin    = new DateTime($dateFrom);
	$stop     = (new DateTime($dateTo))->modify('+1 day');
	$interval = new DateInterval('P1D');
	$period   = new DatePeriod($begin, $interval, $stop);

	$room_meta = get_post_meta( $room_id, '_room_settings', TRUE );
	$room_meta = is_array( $room_meta ) ? $room_meta : array();

	if( array_key_exists('pricing-range', $room_meta) ):
		foreach( $room_meta['pricing-range'] as $price_range ):
			$dfrom = $price_range['date-from'];
			$dto   = $price_range['date-to'];

			$temp = dt_travel_get_between_dates( $dfrom, $dto );

			foreach( $temp as $k => $v ):
				$datePrice[$k] = $price_range["price_day_{$v}"];
			endforeach;
		endforeach;
	endif;

	return $datePrice;
}

/**
 * Get between dates by two dates
 * @return array
 */
function dt_travel_get_between_dates( $dateFrom = '',  $dateTo = '' ) {

	if( $dateFrom == '' || $dateTo == '' )
		return false;

	$temp = array();
	$begin    = new DateTime($dateFrom);
	$stop     = (new DateTime($dateTo))->modify('+1 day');
	$interval = new DateInterval('P1D');
	$period   = new DatePeriod($begin, $interval, $stop);

	foreach ($period as $dt) {
		$currentDate = $dt->format("Y-m-d");
		$wd = date('w', strtotime($currentDate)) + 1;

		$temp[$currentDate] = $wd;
	}

	return $temp;
}

/**
 * Get no.of days between dates
 * @return number
 */
function dt_travel_get_days_between_dates( $dateFrom = '',  $dateTo = '' ) {

	if( $dateFrom == '' || $dateTo == '' )
		return false;

	$dateFrom = strtotime($dateFrom);
	$dateTo   = strtotime($dateTo);

	$datediff = $dateTo - $dateFrom;

	$n = floor($datediff / (60 * 60 * 24));

	if( $n != 0 )
		return $n;
	else
		return 1;
}

/**
 * Get sum of price between dates
 * @return float value
 */
function dt_travel_get_price_between_dates( $dateFrom = null,  $dateTo = null, $room_id = null ) {

	if ( !$room_id || !$dateFrom || !$dateTo ) {
		return '';
	}

	if( $dateFrom != $dateTo )
		$dateTo = date( 'Y-m-d', strtotime( '-1 day', strtotime( $dateTo ) ) );

	$regular_arr = dt_travel_get_regular_price_dates($dateFrom, $dateTo, $room_id );
	$ranging_arr = dt_travel_get_pricing_plan_dates($dateFrom, $dateTo, $room_id );

	foreach ($ranging_arr as $key => $item)
		if ( !( strtotime($key) >= strtotime($dateFrom)  &&  strtotime($key) <= strtotime($dateTo) ) )
			unset($ranging_arr[$key]);

	$diff_arr = array_diff_key($regular_arr, $ranging_arr);
	$final_arr = array_merge($diff_arr, $ranging_arr);
	ksort($final_arr);

	return array_sum($final_arr);
}

/**
 * Get Room Id's
 * @return array
 */
function dt_travel_get_room_ids( $post_id = 1 ) {
	global $wpdb;

	$dataIDs = array();
	$metas = $wpdb->get_results("SELECT * FROM $wpdb->postmeta WHERE meta_key = '_pricing_plan_settings'", ARRAY_A);

	foreach( $metas as $k => $v ):
		$temp = unserialize($v['meta_value']);
		$dataIDs[] = $temp['room-id'];
	endforeach;

	$curr_meta = get_post_meta( $post_id, '_pricing_plan_settings', TRUE );
	$curr_meta = is_array( $curr_meta ) ? $curr_meta : array();

	if( array_key_exists( 'room-id', $curr_meta ) ) :
		$rid = $curr_meta['room-id'];
		if(($key = array_search($rid, $dataIDs)) !== false) {
			unset($dataIDs[$key]);
		}
	endif;

	return $dataIDs;
}

/**
 * Get min checkin / max checkout date
 * @return string
 */
function dt_travel_get_order_checkin_date( $order_id = 1, $flag = 'checkin' ) {

	$order_items = get_post_meta ( $order_id, '_order_items', TRUE );
	$order_items = is_array ( $order_items ) ? $order_items : array ();

	$checkin_arr = $checkout_arr = array();

	foreach( $order_items as $key => $item ):
		$checkin_arr[] = $item['checkin_date'];
		$checkout_arr[] = $item['checkout_date'];
	endforeach;

	sort($checkin_arr);
	sort($checkout_arr);

	if( $flag == 'checkin' ){
		return $checkin_arr[0];
	} else {
		return end($checkout_arr);
	}
}

/**
 * Get order total amount
 * @return number
 */
function dt_travel_get_order_total( $order_id = 1 ) {

	$order_items = get_post_meta ( $order_id, '_order_items', TRUE );
	$order_items = is_array ( $order_items ) ? $order_items : array ();

	$total = $tax = 0;
	$order_settings = get_post_meta ( $order_id, '_order_settings', TRUE );
	$tax_percent = array_key_exists('tax_percent', $order_settings) ? $order_settings['tax_percent'] : '';

	foreach( $order_items as $key => $item ):
		$total += $item['qty'] * $item['price'];

		if( array_key_exists('addon', $item) ){
			foreach( $item['addon'] as $add ){
				$total += $add['qty'] * $add['price'];
			}
		}
	endforeach;

	$tax = $total * $tax_percent / 100;
	$total += $tax;

	return($total);
}

/**
 * Get Base Currency Code.
 *
 * @return string
 */
function dt_travel_get_currency() {
	return apply_filters( 'dt_travel_currency', cs_get_option( 'book-currency' ) );
}

/**
 * Get full list of currency codes.
 *
 * @return array
 */
function dt_travel_get_currencies() {
	return array_unique(
		apply_filters( 'dt_travel_currencies',
			array(
				'AED' => __( 'United Arab Emirates dirham', 'designthemes-travel' ),
				'AFN' => __( 'Afghan afghani', 'designthemes-travel' ),
				'ALL' => __( 'Albanian lek', 'designthemes-travel' ),
				'AMD' => __( 'Armenian dram', 'designthemes-travel' ),
				'ANG' => __( 'Netherlands Antillean guilder', 'designthemes-travel' ),
				'AOA' => __( 'Angolan kwanza', 'designthemes-travel' ),
				'ARS' => __( 'Argentine peso', 'designthemes-travel' ),
				'AUD' => __( 'Australian dollar', 'designthemes-travel' ),
				'AWG' => __( 'Aruban florin', 'designthemes-travel' ),
				'AZN' => __( 'Azerbaijani manat', 'designthemes-travel' ),
				'BAM' => __( 'Bosnia and Herzegovina convertible mark', 'designthemes-travel' ),
				'BBD' => __( 'Barbadian dollar', 'designthemes-travel' ),
				'BDT' => __( 'Bangladeshi taka', 'designthemes-travel' ),
				'BGN' => __( 'Bulgarian lev', 'designthemes-travel' ),
				'BHD' => __( 'Bahraini dinar', 'designthemes-travel' ),
				'BIF' => __( 'Burundian franc', 'designthemes-travel' ),
				'BMD' => __( 'Bermudian dollar', 'designthemes-travel' ),
				'BND' => __( 'Brunei dollar', 'designthemes-travel' ),
				'BOB' => __( 'Bolivian boliviano', 'designthemes-travel' ),
				'BRL' => __( 'Brazilian real', 'designthemes-travel' ),
				'BSD' => __( 'Bahamian dollar', 'designthemes-travel' ),
				'BTC' => __( 'Bitcoin', 'designthemes-travel' ),
				'BTN' => __( 'Bhutanese ngultrum', 'designthemes-travel' ),
				'BWP' => __( 'Botswana pula', 'designthemes-travel' ),
				'BYR' => __( 'Belarusian ruble', 'designthemes-travel' ),
				'BZD' => __( 'Belize dollar', 'designthemes-travel' ),
				'CAD' => __( 'Canadian dollar', 'designthemes-travel' ),
				'CDF' => __( 'Congolese franc', 'designthemes-travel' ),
				'CHF' => __( 'Swiss franc', 'designthemes-travel' ),
				'CLP' => __( 'Chilean peso', 'designthemes-travel' ),
				'CNY' => __( 'Chinese yuan', 'designthemes-travel' ),
				'COP' => __( 'Colombian peso', 'designthemes-travel' ),
				'CRC' => __( 'Costa Rican col&oacute;n', 'designthemes-travel' ),
				'CUC' => __( 'Cuban convertible peso', 'designthemes-travel' ),
				'CUP' => __( 'Cuban peso', 'designthemes-travel' ),
				'CVE' => __( 'Cape Verdean escudo', 'designthemes-travel' ),
				'CZK' => __( 'Czech koruna', 'designthemes-travel' ),
				'DJF' => __( 'Djiboutian franc', 'designthemes-travel' ),
				'DKK' => __( 'Danish krone', 'designthemes-travel' ),
				'DOP' => __( 'Dominican peso', 'designthemes-travel' ),
				'DZD' => __( 'Algerian dinar', 'designthemes-travel' ),
				'EGP' => __( 'Egyptian pound', 'designthemes-travel' ),
				'ERN' => __( 'Eritrean nakfa', 'designthemes-travel' ),
				'ETB' => __( 'Ethiopian birr', 'designthemes-travel' ),
				'EUR' => __( 'Euro', 'designthemes-travel' ),
				'FJD' => __( 'Fijian dollar', 'designthemes-travel' ),
				'FKP' => __( 'Falkland Islands pound', 'designthemes-travel' ),
				'GBP' => __( 'Pound sterling', 'designthemes-travel' ),
				'GEL' => __( 'Georgian lari', 'designthemes-travel' ),
				'GGP' => __( 'Guernsey pound', 'designthemes-travel' ),
				'GHS' => __( 'Ghana cedi', 'designthemes-travel' ),
				'GIP' => __( 'Gibraltar pound', 'designthemes-travel' ),
				'GMD' => __( 'Gambian dalasi', 'designthemes-travel' ),
				'GNF' => __( 'Guinean franc', 'designthemes-travel' ),
				'GTQ' => __( 'Guatemalan quetzal', 'designthemes-travel' ),
				'GYD' => __( 'Guyanese dollar', 'designthemes-travel' ),
				'HKD' => __( 'Hong Kong dollar', 'designthemes-travel' ),
				'HNL' => __( 'Honduran lempira', 'designthemes-travel' ),
				'HRK' => __( 'Croatian kuna', 'designthemes-travel' ),
				'HTG' => __( 'Haitian gourde', 'designthemes-travel' ),
				'HUF' => __( 'Hungarian forint', 'designthemes-travel' ),
				'IDR' => __( 'Indonesian rupiah', 'designthemes-travel' ),
				'ILS' => __( 'Israeli new shekel', 'designthemes-travel' ),
				'IMP' => __( 'Manx pound', 'designthemes-travel' ),
				'INR' => __( 'Indian rupee', 'designthemes-travel' ),
				'IQD' => __( 'Iraqi dinar', 'designthemes-travel' ),
				'IRR' => __( 'Iranian rial', 'designthemes-travel' ),
				'IRT' => __( 'Iranian toman', 'designthemes-travel' ),
				'ISK' => __( 'Icelandic kr&oacute;na', 'designthemes-travel' ),
				'JEP' => __( 'Jersey pound', 'designthemes-travel' ),
				'JMD' => __( 'Jamaican dollar', 'designthemes-travel' ),
				'JOD' => __( 'Jordanian dinar', 'designthemes-travel' ),
				'JPY' => __( 'Japanese yen', 'designthemes-travel' ),
				'KES' => __( 'Kenyan shilling', 'designthemes-travel' ),
				'KGS' => __( 'Kyrgyzstani som', 'designthemes-travel' ),
				'KHR' => __( 'Cambodian riel', 'designthemes-travel' ),
				'KMF' => __( 'Comorian franc', 'designthemes-travel' ),
				'KPW' => __( 'North Korean won', 'designthemes-travel' ),
				'KRW' => __( 'South Korean won', 'designthemes-travel' ),
				'KWD' => __( 'Kuwaiti dinar', 'designthemes-travel' ),
				'KYD' => __( 'Cayman Islands dollar', 'designthemes-travel' ),
				'KZT' => __( 'Kazakhstani tenge', 'designthemes-travel' ),
				'LAK' => __( 'Lao kip', 'designthemes-travel' ),
				'LBP' => __( 'Lebanese pound', 'designthemes-travel' ),
				'LKR' => __( 'Sri Lankan rupee', 'designthemes-travel' ),
				'LRD' => __( 'Liberian dollar', 'designthemes-travel' ),
				'LSL' => __( 'Lesotho loti', 'designthemes-travel' ),
				'LYD' => __( 'Libyan dinar', 'designthemes-travel' ),
				'MAD' => __( 'Moroccan dirham', 'designthemes-travel' ),
				'MDL' => __( 'Moldovan leu', 'designthemes-travel' ),
				'MGA' => __( 'Malagasy ariary', 'designthemes-travel' ),
				'MKD' => __( 'Macedonian denar', 'designthemes-travel' ),
				'MMK' => __( 'Burmese kyat', 'designthemes-travel' ),
				'MNT' => __( 'Mongolian t&ouml;gr&ouml;g', 'designthemes-travel' ),
				'MOP' => __( 'Macanese pataca', 'designthemes-travel' ),
				'MRO' => __( 'Mauritanian ouguiya', 'designthemes-travel' ),
				'MUR' => __( 'Mauritian rupee', 'designthemes-travel' ),
				'MVR' => __( 'Maldivian rufiyaa', 'designthemes-travel' ),
				'MWK' => __( 'Malawian kwacha', 'designthemes-travel' ),
				'MXN' => __( 'Mexican peso', 'designthemes-travel' ),
				'MYR' => __( 'Malaysian ringgit', 'designthemes-travel' ),
				'MZN' => __( 'Mozambican metical', 'designthemes-travel' ),
				'NAD' => __( 'Namibian dollar', 'designthemes-travel' ),
				'NGN' => __( 'Nigerian naira', 'designthemes-travel' ),
				'NIO' => __( 'Nicaraguan c&oacute;rdoba', 'designthemes-travel' ),
				'NOK' => __( 'Norwegian krone', 'designthemes-travel' ),
				'NPR' => __( 'Nepalese rupee', 'designthemes-travel' ),
				'NZD' => __( 'New Zealand dollar', 'designthemes-travel' ),
				'OMR' => __( 'Omani rial', 'designthemes-travel' ),
				'PAB' => __( 'Panamanian balboa', 'designthemes-travel' ),
				'PEN' => __( 'Peruvian nuevo sol', 'designthemes-travel' ),
				'PGK' => __( 'Papua New Guinean kina', 'designthemes-travel' ),
				'PHP' => __( 'Philippine peso', 'designthemes-travel' ),
				'PKR' => __( 'Pakistani rupee', 'designthemes-travel' ),
				'PLN' => __( 'Polish z&#x142;oty', 'designthemes-travel' ),
				'PRB' => __( 'Transnistrian ruble', 'designthemes-travel' ),
				'PYG' => __( 'Paraguayan guaran&iacute;', 'designthemes-travel' ),
				'QAR' => __( 'Qatari riyal', 'designthemes-travel' ),
				'RON' => __( 'Romanian leu', 'designthemes-travel' ),
				'RSD' => __( 'Serbian dinar', 'designthemes-travel' ),
				'RUB' => __( 'Russian ruble', 'designthemes-travel' ),
				'RWF' => __( 'Rwandan franc', 'designthemes-travel' ),
				'SAR' => __( 'Saudi riyal', 'designthemes-travel' ),
				'SBD' => __( 'Solomon Islands dollar', 'designthemes-travel' ),
				'SCR' => __( 'Seychellois rupee', 'designthemes-travel' ),
				'SDG' => __( 'Sudanese pound', 'designthemes-travel' ),
				'SEK' => __( 'Swedish krona', 'designthemes-travel' ),
				'SGD' => __( 'Singapore dollar', 'designthemes-travel' ),
				'SHP' => __( 'Saint Helena pound', 'designthemes-travel' ),
				'SLL' => __( 'Sierra Leonean leone', 'designthemes-travel' ),
				'SOS' => __( 'Somali shilling', 'designthemes-travel' ),
				'SRD' => __( 'Surinamese dollar', 'designthemes-travel' ),
				'SSP' => __( 'South Sudanese pound', 'designthemes-travel' ),
				'STD' => __( 'S&atilde;o Tom&eacute; and Pr&iacute;ncipe dobra', 'designthemes-travel' ),
				'SYP' => __( 'Syrian pound', 'designthemes-travel' ),
				'SZL' => __( 'Swazi lilangeni', 'designthemes-travel' ),
				'THB' => __( 'Thai baht', 'designthemes-travel' ),
				'TJS' => __( 'Tajikistani somoni', 'designthemes-travel' ),
				'TMT' => __( 'Turkmenistan manat', 'designthemes-travel' ),
				'TND' => __( 'Tunisian dinar', 'designthemes-travel' ),
				'TOP' => __( 'Tongan pa&#x2bb;anga', 'designthemes-travel' ),
				'TRY' => __( 'Turkish lira', 'designthemes-travel' ),
				'TTD' => __( 'Trinidad and Tobago dollar', 'designthemes-travel' ),
				'TWD' => __( 'New Taiwan dollar', 'designthemes-travel' ),
				'TZS' => __( 'Tanzanian shilling', 'designthemes-travel' ),
				'UAH' => __( 'Ukrainian hryvnia', 'designthemes-travel' ),
				'UGX' => __( 'Ugandan shilling', 'designthemes-travel' ),
				'USD' => __( 'United States dollar', 'designthemes-travel' ),
				'UYU' => __( 'Uruguayan peso', 'designthemes-travel' ),
				'UZS' => __( 'Uzbekistani som', 'designthemes-travel' ),
				'VEF' => __( 'Venezuelan bol&iacute;var', 'designthemes-travel' ),
				'VND' => __( 'Vietnamese &#x111;&#x1ed3;ng', 'designthemes-travel' ),
				'VUV' => __( 'Vanuatu vatu', 'designthemes-travel' ),
				'WST' => __( 'Samoan t&#x101;l&#x101;', 'designthemes-travel' ),
				'XAF' => __( 'Central African CFA franc', 'designthemes-travel' ),
				'XCD' => __( 'East Caribbean dollar', 'designthemes-travel' ),
				'XOF' => __( 'West African CFA franc', 'designthemes-travel' ),
				'XPF' => __( 'CFP franc', 'designthemes-travel' ),
				'YER' => __( 'Yemeni rial', 'designthemes-travel' ),
				'ZAR' => __( 'South African rand', 'designthemes-travel' ),
				'ZMW' => __( 'Zambian kwacha', 'designthemes-travel' ),
			)
		)
	);
}

/**
 * Get Currency symbol.
 *
 * @param string $currency (default: '')
 * @return string
 */
function dt_travel_get_currency_symbol( $currency = '' ) {
	if ( ! $currency ) {
		$currency = dt_travel_get_currency();
	}

	$symbols = apply_filters( 'dt_travel_currency_symbols', array(
		'AED' => '&#x62f;.&#x625;',
		'AFN' => '&#x60b;',
		'ALL' => 'L',
		'AMD' => 'AMD',
		'ANG' => '&fnof;',
		'AOA' => 'Kz',
		'ARS' => '&#36;',
		'AUD' => '&#36;',
		'AWG' => 'Afl.',
		'AZN' => 'AZN',
		'BAM' => 'KM',
		'BBD' => '&#36;',
		'BDT' => '&#2547;&nbsp;',
		'BGN' => '&#1083;&#1074;.',
		'BHD' => '.&#x62f;.&#x628;',
		'BIF' => 'Fr',
		'BMD' => '&#36;',
		'BND' => '&#36;',
		'BOB' => 'Bs.',
		'BRL' => '&#82;&#36;',
		'BSD' => '&#36;',
		'BTC' => '&#3647;',
		'BTN' => 'Nu.',
		'BWP' => 'P',
		'BYR' => 'Br',
		'BZD' => '&#36;',
		'CAD' => '&#36;',
		'CDF' => 'Fr',
		'CHF' => '&#67;&#72;&#70;',
		'CLP' => '&#36;',
		'CNY' => '&yen;',
		'COP' => '&#36;',
		'CRC' => '&#x20a1;',
		'CUC' => '&#36;',
		'CUP' => '&#36;',
		'CVE' => '&#36;',
		'CZK' => '&#75;&#269;',
		'DJF' => 'Fr',
		'DKK' => 'DKK',
		'DOP' => 'RD&#36;',
		'DZD' => '&#x62f;.&#x62c;',
		'EGP' => 'EGP',
		'ERN' => 'Nfk',
		'ETB' => 'Br',
		'EUR' => '&euro;',
		'FJD' => '&#36;',
		'FKP' => '&pound;',
		'GBP' => '&pound;',
		'GEL' => '&#x10da;',
		'GGP' => '&pound;',
		'GHS' => '&#x20b5;',
		'GIP' => '&pound;',
		'GMD' => 'D',
		'GNF' => 'Fr',
		'GTQ' => 'Q',
		'GYD' => '&#36;',
		'HKD' => '&#36;',
		'HNL' => 'L',
		'HRK' => 'Kn',
		'HTG' => 'G',
		'HUF' => '&#70;&#116;',
		'IDR' => 'Rp',
		'ILS' => '&#8362;',
		'IMP' => '&pound;',
		'INR' => '&#8377;',
		'IQD' => '&#x639;.&#x62f;',
		'IRR' => '&#xfdfc;',
		'IRT' => '&#x062A;&#x0648;&#x0645;&#x0627;&#x0646;',
		'ISK' => 'kr.',
		'JEP' => '&pound;',
		'JMD' => '&#36;',
		'JOD' => '&#x62f;.&#x627;',
		'JPY' => '&yen;',
		'KES' => 'KSh',
		'KGS' => '&#x441;&#x43e;&#x43c;',
		'KHR' => '&#x17db;',
		'KMF' => 'Fr',
		'KPW' => '&#x20a9;',
		'KRW' => '&#8361;',
		'KWD' => '&#x62f;.&#x643;',
		'KYD' => '&#36;',
		'KZT' => 'KZT',
		'LAK' => '&#8365;',
		'LBP' => '&#x644;.&#x644;',
		'LKR' => '&#xdbb;&#xdd4;',
		'LRD' => '&#36;',
		'LSL' => 'L',
		'LYD' => '&#x644;.&#x62f;',
		'MAD' => '&#x62f;.&#x645;.',
		'MDL' => 'MDL',
		'MGA' => 'Ar',
		'MKD' => '&#x434;&#x435;&#x43d;',
		'MMK' => 'Ks',
		'MNT' => '&#x20ae;',
		'MOP' => 'P',
		'MRO' => 'UM',
		'MUR' => '&#x20a8;',
		'MVR' => '.&#x783;',
		'MWK' => 'MK',
		'MXN' => '&#36;',
		'MYR' => '&#82;&#77;',
		'MZN' => 'MT',
		'NAD' => '&#36;',
		'NGN' => '&#8358;',
		'NIO' => 'C&#36;',
		'NOK' => '&#107;&#114;',
		'NPR' => '&#8360;',
		'NZD' => '&#36;',
		'OMR' => '&#x631;.&#x639;.',
		'PAB' => 'B/.',
		'PEN' => 'S/.',
		'PGK' => 'K',
		'PHP' => '&#8369;',
		'PKR' => '&#8360;',
		'PLN' => '&#122;&#322;',
		'PRB' => '&#x440;.',
		'PYG' => '&#8370;',
		'QAR' => '&#x631;.&#x642;',
		'RMB' => '&yen;',
		'RON' => 'lei',
		'RSD' => '&#x434;&#x438;&#x43d;.',
		'RUB' => '&#8381;',
		'RWF' => 'Fr',
		'SAR' => '&#x631;.&#x633;',
		'SBD' => '&#36;',
		'SCR' => '&#x20a8;',
		'SDG' => '&#x62c;.&#x633;.',
		'SEK' => '&#107;&#114;',
		'SGD' => '&#36;',
		'SHP' => '&pound;',
		'SLL' => 'Le',
		'SOS' => 'Sh',
		'SRD' => '&#36;',
		'SSP' => '&pound;',
		'STD' => 'Db',
		'SYP' => '&#x644;.&#x633;',
		'SZL' => 'L',
		'THB' => '&#3647;',
		'TJS' => '&#x405;&#x41c;',
		'TMT' => 'm',
		'TND' => '&#x62f;.&#x62a;',
		'TOP' => 'T&#36;',
		'TRY' => '&#8378;',
		'TTD' => '&#36;',
		'TWD' => '&#78;&#84;&#36;',
		'TZS' => 'Sh',
		'UAH' => '&#8372;',
		'UGX' => 'UGX',
		'USD' => '&#36;',
		'UYU' => '&#36;',
		'UZS' => 'UZS',
		'VEF' => 'Bs F',
		'VND' => '&#8363;',
		'VUV' => 'Vt',
		'WST' => 'T',
		'XAF' => 'Fr',
		'XCD' => '&#36;',
		'XOF' => 'Fr',
		'XPF' => 'Fr',
		'YER' => '&#xfdfc;',
		'ZAR' => '&#82;',
		'ZMW' => 'ZK',
	) );

	$currency_symbol = isset( $symbols[ $currency ] ) ? $symbols[ $currency ] : '';

	return apply_filters( 'dt_travel_currency_symbol', $currency_symbol, $currency );
}

/**
 * Get Users list
 * @return array
 */
function dt_travel_get_users() {

	$userslist = array( '' => esc_html__('Choose a Customer', 'designthemes-travel') );
	$args = array( 'blog_id' => $GLOBALS['blog_id'] );
	$users = get_users( $args );

	foreach( $users as $user ):
		$userslist[$user->ID] = $user->user_login.' (#'.$user->ID.' '.$user->user_email.')';
	endforeach;

	return $userslist;
}

/**
 * Get County list
 * @return array
 */
function dt_travel_countries() {
	return array(
		'' => esc_html__('Choose a Country', 'designthemes-travel'),
		'Afghanistan' => 'Afghanistan',
		'Albania' => 'Albania',
		'Algeria' => 'Algeria',
		'American Samoa' => 'American Samoa',
		'Andorra' => 'Andorra',
		'Angola' => 'Angola',
		'Anguilla' => 'Anguilla',
		'Antarctica' => 'Antarctica',
		'Antigua And Barbuda' => 'Antigua And Barbuda',
		'Argentina' => 'Argentina',
		'Armenia' => 'Armenia',
		'Aruba' => 'Aruba',
		'Australia' => 'Australia',
		'Austria' => 'Austria',
		'Azerbaijan' => 'Azerbaijan',
		'Bahamas The' => 'Bahamas The',
		'Bahrain' => 'Bahrain',
		'Bangladesh' => 'Bangladesh',
		'Barbados' => 'Barbados',
		'Belarus' => 'Belarus',
		'Belgium' => 'Belgium',
		'Belize' => 'Belize',
		'Benin' => 'Benin',
		'Bermuda' => 'Bermuda',
		'Bhutan' => 'Bhutan',
		'Bolivia' => 'Bolivia',
		'Bosnia and Herzegovina' => 'Bosnia and Herzegovina',
		'Botswana' => 'Botswana',
		'Bouvet Island' => 'Bouvet Island',
		'Brazil' => 'Brazil',
		'British Indian Ocean Territory' => 'British Indian Ocean Territory',
		'Brunei' => 'Brunei',
		'Bulgaria' => 'Bulgaria',
		'Burkina Faso' => 'Burkina Faso',
		'Burundi' => 'Burundi',
		'Cambodia' => 'Cambodia',
		'Cameroon' => 'Cameroon',
		'Canada' => 'Canada',
		'Cape Verde' => 'Cape Verde',
		'Cayman Islands' => 'Cayman Islands',
		'Central African Republic' => 'Central African Republic',
		'Chad' => 'Chad',
		'Chile' => 'Chile',
		'China' => 'China',
		'Christmas Island' => 'Christmas Island',
		'Cocos (Keeling) Islands' => 'Cocos (Keeling) Islands',
		'Colombia' => 'Colombia',
		'Comoros' => 'Comoros',
		'Congo' => 'Congo',
		'Congo The Democratic Republic Of The' => 'Congo The Democratic Republic Of The',
		'Cook Islands' => 'Cook Islands',
		'Costa Rica' => 'Costa Rica',
		'Cote D\'\'Ivoire (Ivory Coast)' => 'Cote D\'\'Ivoire (Ivory Coast)',
		'Croatia (Hrvatska)' => 'Croatia (Hrvatska)',
		'Cuba' => 'Cuba',
		'Cyprus' => 'Cyprus',
		'Czech Republic' => 'Czech Republic',
		'Denmark' => 'Denmark',
		'Djibouti' => 'Djibouti',
		'Dominica' => 'Dominica',
		'Dominican Republic' => 'Dominican Republic',
		'East Timor' => 'East Timor',
		'Ecuador' => 'Ecuador',
		'Egypt' => 'Egypt',
		'El Salvador' => 'El Salvador',
		'Equatorial Guinea' => 'Equatorial Guinea',
		'Eritrea' => 'Eritrea',
		'Estonia' => 'Estonia',
		'Ethiopia' => 'Ethiopia',
		'External Territories of Australia' => 'External Territories of Australia',
		'Falkland Islands' => 'Falkland Islands',
		'Faroe Islands' => 'Faroe Islands',
		'Fiji Islands' => 'Fiji Islands',
		'Finland' => 'Finland',
		'France' => 'France',
		'French Guiana' => 'French Guiana',
		'French Polynesia' => 'French Polynesia',
		'French Southern Territories' => 'French Southern Territories',
		'Gabon' => 'Gabon',
		'Gambia The' => 'Gambia The',
		'Georgia' => 'Georgia',
		'Germany' => 'Germany',
		'Ghana' => 'Ghana',
		'Gibraltar' => 'Gibraltar',
		'Greece' => 'Greece',
		'Greenland' => 'Greenland',
		'Grenada' => 'Grenada',
		'Guadeloupe' => 'Guadeloupe',
		'Guam' => 'Guam',
		'Guatemala' => 'Guatemala',
		'Guernsey and Alderney' => 'Guernsey and Alderney',
		'Guinea' => 'Guinea',
		'Guinea-Bissau' => 'Guinea-Bissau',
		'Guyana' => 'Guyana',
		'Haiti' => 'Haiti',
		'Heard and McDonald Islands' => 'Heard and McDonald Islands',
		'Honduras' => 'Honduras',
		'Hong Kong S.A.R.' => 'Hong Kong S.A.R.',
		'Hungary' => 'Hungary',
		'Iceland' => 'Iceland',
		'India' => 'India',
		'Indonesia' => 'Indonesia',
		'Iran' => 'Iran',
		'Iraq' => 'Iraq',
		'Ireland' => 'Ireland',
		'Israel' => 'Israel',
		'Italy' => 'Italy',
		'Jamaica' => 'Jamaica',
		'Japan' => 'Japan',
		'Jersey' => 'Jersey',
		'Jordan' => 'Jordan',
		'Kazakhstan' => 'Kazakhstan',
		'Kenya' => 'Kenya',
		'Kiribati' => 'Kiribati',
		'Korea North' => 'Korea North',
		'Korea South' => 'Korea South',
		'Kuwait' => 'Kuwait',
		'Kyrgyzstan' => 'Kyrgyzstan',
		'Laos' => 'Laos',
		'Latvia' => 'Latvia',
		'Lebanon' => 'Lebanon',
		'Lesotho' => 'Lesotho',
		'Liberia' => 'Liberia',
		'Libya' => 'Libya',
		'Liechtenstein' => 'Liechtenstein',
		'Lithuania' => 'Lithuania',
		'Luxembourg' => 'Luxembourg',
		'Macau S.A.R.' => 'Macau S.A.R.',
		'Macedonia' => 'Macedonia',
		'Madagascar' => 'Madagascar',
		'Malawi' => 'Malawi',
		'Malaysia' => 'Malaysia',
		'Maldives' => 'Maldives',
		'Mali' => 'Mali',
		'Malta' => 'Malta',
		'Man (Isle of)' => 'Man (Isle of)',
		'Marshall Islands' => 'Marshall Islands',
		'Martinique' => 'Martinique',
		'Mauritania' => 'Mauritania',
		'Mauritius' => 'Mauritius',
		'Mayotte' => 'Mayotte',
		'Mexico' => 'Mexico',
		'Micronesia' => 'Micronesia',
		'Moldova' => 'Moldova',
		'Monaco' => 'Monaco',
		'Mongolia' => 'Mongolia',
		'Montserrat' => 'Montserrat',
		'Morocco' => 'Morocco',
		'Mozambique' => 'Mozambique',
		'Myanmar' => 'Myanmar',
		'Namibia' => 'Namibia',
		'Nauru' => 'Nauru',
		'Nepal' => 'Nepal',
		'Netherlands Antilles' => 'Netherlands Antilles',
		'Netherlands The' => 'Netherlands The',
		'New Caledonia' => 'New Caledonia',
		'New Zealand' => 'New Zealand',
		'Nicaragua' => 'Nicaragua',
		'Niger' => 'Niger',
		'Nigeria' => 'Nigeria',
		'Niue' => 'Niue',
		'Norfolk Island' => 'Norfolk Island',
		'Northern Mariana Islands' => 'Northern Mariana Islands',
		'Norway' => 'Norway',
		'Oman' => 'Oman',
		'Pakistan' => 'Pakistan',
		'Palau' => 'Palau',
		'Palestinian Territory Occupied' => 'Palestinian Territory Occupied',
		'Panama' => 'Panama',
		'Papua new Guinea' => 'Papua new Guinea',
		'Paraguay' => 'Paraguay',
		'Peru' => 'Peru',
		'Philippines' => 'Philippines',
		'Pitcairn Island' => 'Pitcairn Island',
		'Poland' => 'Poland',
		'Portugal' => 'Portugal',
		'Puerto Rico' => 'Puerto Rico',
		'Qatar' => 'Qatar',
		'Reunion' => 'Reunion',
		'Romania' => 'Romania',
		'Russia' => 'Russia',
		'Rwanda' => 'Rwanda',
		'Saint Helena' => 'Saint Helena',
		'Saint Kitts And Nevis' => 'Saint Kitts And Nevis',
		'Saint Lucia' => 'Saint Lucia',
		'Saint Pierre and Miquelon' => 'Saint Pierre and Miquelon',
		'Saint Vincent And The Grenadines' => 'Saint Vincent And The Grenadines',
		'Samoa' => 'Samoa',
		'San Marino' => 'San Marino',
		'Sao Tome and Principe' => 'Sao Tome and Principe',
		'Saudi Arabia' => 'Saudi Arabia',
		'Senegal' => 'Senegal',
		'Serbia' => 'Serbia',
		'Seychelles' => 'Seychelles',
		'Sierra Leone' => 'Sierra Leone',
		'Singapore' => 'Singapore',
		'Slovakia' => 'Slovakia',
		'Slovenia' => 'Slovenia',
		'Smaller Territories of the UK' => 'Smaller Territories of the UK',
		'Solomon Islands' => 'Solomon Islands',
		'Somalia' => 'Somalia',
		'South Africa' => 'South Africa',
		'South Georgia' => 'South Georgia',
		'South Sudan' => 'South Sudan',
		'Spain' => 'Spain',
		'Sri Lanka' => 'Sri Lanka',
		'Sudan' => 'Sudan',
		'Suriname' => 'Suriname',
		'Svalbard And Jan Mayen Islands' => 'Svalbard And Jan Mayen Islands',
		'Swaziland' => 'Swaziland',
		'Sweden' => 'Sweden',
		'Switzerland' => 'Switzerland',
		'Syria' => 'Syria',
		'Taiwan' => 'Taiwan',
		'Tajikistan' => 'Tajikistan',
		'Tanzania' => 'Tanzania',
		'Thailand' => 'Thailand',
		'Togo' => 'Togo',
		'Tokelau' => 'Tokelau',
		'Tonga' => 'Tonga',
		'Trinidad And Tobago' => 'Trinidad And Tobago',
		'Tunisia' => 'Tunisia',
		'Turkey' => 'Turkey',
		'Turkmenistan' => 'Turkmenistan',
		'Turks And Caicos Islands' => 'Turks And Caicos Islands',
		'Tuvalu' => 'Tuvalu',
		'Uganda' => 'Uganda',
		'Ukraine' => 'Ukraine',
		'United Arab Emirates' => 'United Arab Emirates',
		'United Kingdom' => 'United Kingdom',
		'United States' => 'United States',
		'United States Minor Outlying Islands' => 'United States Minor Outlying Islands',
		'Uruguay' => 'Uruguay',
		'Uzbekistan' => 'Uzbekistan',
		'Vanuatu' => 'Vanuatu',
		'Vatican City State (Holy See)' => 'Vatican City State (Holy See)',
		'Venezuela' => 'Venezuela',
		'Vietnam' => 'Vietnam',
		'Virgin Islands (British)' => 'Virgin Islands (British)',
		'Virgin Islands (US)' => 'Virgin Islands (US)',
		'Wallis And Futuna Islands' => 'Wallis And Futuna Islands',
		'Western Sahara' => 'Western Sahara',
		'Yemen' => 'Yemen',
		'Yugoslavia' => 'Yugoslavia',
		'Zambia' => 'Zambia',
		'Zimbabwe' => 'Zimbabwe'
	);
}

/**
 * Get County list
 * @return array
 */
function dt_travel_get_rooms($count = -1) {
	$out = '';

	$args = array( 'post_type' => 'dt_rooms', 'posts_per_page' => $count );
	$the_query = new WP_Query($args);
	
	if($the_query->have_posts()):
	 while($the_query->have_posts()): $the_query->the_post();
	 	$out .= '<option value="'.esc_attr(get_the_ID()).'">'.esc_html(get_the_title()).'</option>';
	 endwhile;
	endif;

	wp_reset_postdata();
	
	return $out;
}

/**
 * Get Quantity dropdown
 * @return html select
 */
function dt_travel_qty_dropdown($qty = 1) {
	$out = '<select name="cmbqty" class="choosen">';
		$out .= '<option value="">'.esc_html__('No.of Quantity', 'designthemes-travel').'</option>';
		for($i = 1; $i <= $qty; $i++) {
			$out .= '<option value="'.esc_attr($i).'">'.esc_html($i).'</option>';
		}
	$out .= '</select>';
	
	return $out;
}

/**
 * Get Addon services dropdown
 * @return html select
 */
function dt_travel_addon_services_dropdown($room_id = 0) {

	$room_meta = get_post_meta( $room_id, '_room_settings', TRUE );
	$room_meta = is_array( $room_meta ) ? $room_meta : array();

	if( array_key_exists( 'room-services', $room_meta ) ):
		$s = $room_meta['room-services'];

		$out = '<h3>'.esc_html__('Additional Services', 'designthemes-travel').'</h3>';
		$out .= '<select name="cmbaddons" class="choosen" multiple="multiple" style="width:50%;">';
			foreach($s as $v):
				$term = get_term( $v, 'dt_services' );

				$term_meta = get_term_meta( $v, '_room_services', false );
				$cost = !empty($term_meta[0]['service_price']) ? $term_meta[0]['service_price'] : '';

				$symbol = dt_travel_get_currency_symbol();

				$cost = !empty($cost) ? " - {$symbol}{$cost}" : '';

				$out .= '<option value="'.esc_attr($v).'">'.esc_html($term->name).$cost.'</option>';
			endforeach;
		$out .= '</select>';
	else:
		$out = '<h4>'.esc_html__('No additional services found.', 'designthemes-travel').'</h4>';
	endif;

	return $out;
}

/**
 * Get Addon services dropdown
 * @return html select
 */
function dt_travel_number_format($n = 1) {

	$d = cs_get_option('price-decimal');
	
	return number_format($n, $d);
}

/**
 * Get Formatted price
 * @return html select
 */
function dt_travel_get_formatted_price($price = 30.55, $symbol = '$', $pos = 'left') {

	$symbol = dt_travel_get_currency_symbol();
	$pos 	= cs_get_option('currency-pos');

	switch($pos):
		case 'left':
		default:
			return $symbol.dt_travel_number_format($price);
			break;

		case 'left-with-space':
			return $symbol.' '.dt_travel_number_format($price);
			break;

		case 'right-with-space':
			return dt_travel_number_format($price).' '.$symbol;
			break;

		case 'right':
			return dt_travel_number_format($price).$symbol;
			break;
	endswitch;
}

/**
 * Get pricing breakdown
 * @return html
 */
function dt_travel_get_pricing_breakdown( $dateFrom = null,  $dateTo = null, $room_id = null ) {

	if ( !$room_id || !$dateFrom || !$dateTo ) {
		return '';
	}

	$chkout_date = $dateTo;
	if( $dateFrom != $dateTo )
		$dateTo = date( 'Y-m-d', strtotime( '-1 day', strtotime( $dateTo ) ) );

	$regular_arr = dt_travel_get_regular_price_dates($dateFrom, $dateTo, $room_id );
	$ranging_arr = dt_travel_get_pricing_plan_dates($dateFrom, $dateTo, $room_id );

	foreach ($ranging_arr as $key => $item)
		if ( !( strtotime($key) >= strtotime($dateFrom)  &&  strtotime($key) <= strtotime($dateTo) ) )
			unset($ranging_arr[$key]);

	$diff_arr = array_diff_key($regular_arr, $ranging_arr);
	$final_arr = array_merge($diff_arr, $ranging_arr);
	ksort($final_arr);

	$out  = '<span class="dt_room_item_detail_price_close"> <i class="fa fa-times"></i> </span>';
	$out .= '<table class="dt_room_item_pricing_price">';
		$out .= '<tbody>';
			foreach( $final_arr as $k => $v ):
				$out .= '<tr>';
					$out .= '<td class="dt_room_item_day">'.date('D', strtotime($k)).'</td>';
					$out .= '<td class="dt_room_item_total_description">x1 '.esc_html__('Night', 'designthemes-travel').'</td>';
					$out .= '<td class="dt_room_item_bprice">'.dt_travel_get_formatted_price($v).'</td>';
				$out .= '</tr>';
			endforeach;
		$out .= '</tbody>';
		$out .= '<tfoot>';
			$out .= '<tr>';
				$out .= '<td class="dt_room_item_total" colspan="2">'.esc_html__('Total:', 'designthemes-travel').' </td>';
				$price = dt_travel_get_price_between_dates($dateFrom, $chkout_date, $room_id);
				$out .= '<td class="dt_room_item_tprice">'.dt_travel_get_formatted_price($price).'</td>';
			$out .= '</tr>';
			$out .= '<tr>';
				$out .= '<td class="vat" colspan="3">* '.esc_html__('tax is not included', 'designthemes-travel').'</td>';
			$out .= '</tr>';
		$out .= '</tfoot>';
	$out .= '</table>';
	
	echo ($out);
}

/**
 * Get Addon services checkbox
 * @return html select
 */
function dt_travel_addon_services_checkboxes($room_id = 0) {

	$room_meta = get_post_meta( $room_id, '_room_settings', TRUE );
	$room_meta = is_array( $room_meta ) ? $room_meta : array();

	if( array_key_exists( 'room-services', $room_meta ) ):
		$s = $room_meta['room-services'];

		$out = '<div class="dt_addon_service_title">';
			$out .= '<h5 class="dt_addon_service_title_toggle">';
				$out .= '<a href="javascript:void(0)" class="dt_addon_service_toggle">'.esc_html__('Additional Services', 'designthemes-travel').'</a>';
			$out .= '</h5>';
		$out .= '</div>';
		$out .= '<div class="dt_addon_services">';
			$out .= '<ul class="dt_addon_services_ul">';
				foreach($s as $v):
					$term = get_term( $v, 'dt_services' );
	
					$term_meta = get_term_meta( $v, '_room_services', false );
					$cost = !empty($term_meta[0]['service_price']) ? $term_meta[0]['service_price'] : '';
			
					$out .= '<li>';
						$out .= '<div class="dt_addon_optional_right">';
							$out .= '<input type="checkbox" name="dt_optional_quantity_selected['.$v.']" class="dt_optional_quantity_selected" id="dt-service-'.$room_id.$v.'">';
						$out .= '</div>';
						$out .= '<div class="dt_addon_optional_left">';
							$out .= '<div class="dt_addon_title">';
								$out .= '<div class="dt_service_title">';
									$out .= '<label for="dt-service-'.$room_id.$v.'">'.$term->name.'</label>';
								$out .= '</div>';
								$out .= '<p>'.$term->description.'</p>';
							$out .= '</div>';
							$out .= '<div class="dt_addon_detail_price">';
								$out .= '<input type="number" step="1" min="1" name="dt_optional_quantity['.$v.']" class="dt_optional_quantity">';
								$out .= '<label> <strong>'.dt_travel_get_formatted_price($cost).'</strong> <small>/ '.esc_html__('Room', 'designthemes-travel').' / '.esc_html__('Night', 'designthemes-travel').'</small> </label>';
							$out .= '</div>';
						$out .= '</div>';
					$out .= '</li>';
				endforeach;
			$out .= '</ul>';
		$out .= '</div>';
	else:
		$out = '<h4>'.esc_html__('No additional services found.', 'designthemes-travel').'</h4>';
	endif;

	return $out;
}