<?php
/*
 * @Author : designthemes
 * @Date   : 10/30/2017
 * @Last Modified by: designthemes
 * @Last Modified time: 10/30/2017
 */

if ( !defined( 'ABSPATH' ) ) {
	exit();
}

$user_id  = get_current_user_id();
$orders = get_posts( array(
    'post_type'   => 'dt_orders',
    'post_status' => 'publish',
    'posts_per_page' => -1,
    'fields' => 'ids'
    )
);
$allorders = array();
foreach( $orders as $order ):
	$order_settings = get_post_meta ( $order, '_order_settings', TRUE );
	$order_settings = is_array ( $order_settings ) ? $order_settings : array ();
	
	if( in_array( $user_id, $order_settings ) ):
		$temp = array();
		$temp['order_id'] = $order;
		$temp['order_date'] = date( 'F d, Y', strtotime( get_post_field( 'post_date', $order ) ) );
		$temp['total_price'] = dt_travel_get_formatted_price( dt_travel_get_order_total( $order ) );
		$temp['order_starus'] = $order_settings['booking_status'];

		$allorders[] = $temp;
	endif;
endforeach;

if ( empty($allorders) ) {
	echo '<div class="dt-sc-warning-box">'.esc_html__( 'You have no order booking system.', 'designthemes-travel' ).'</div>';
	return;
}

?>

<div class="dt_booking_wrapper">

    <h2><?php _e( 'Bookings', 'designthemes-travel' ) ?></h2>

    <table class="dt_booking_table">

        <thead>
        <tr>
            <th><?php esc_html_e( 'ID', 'designthemes-travel' ); ?></th>
            <th><?php esc_html_e( 'Booking Date', 'designthemes-travel' ); ?></th>
            <th><?php esc_html_e( 'Total', 'designthemes-travel' ); ?></th>
            <th><?php esc_html_e( 'Status', 'designthemes-travel' ); ?></th>
        </tr>
        </thead>

        <tbody>
		<?php foreach ( $allorders as $k => $order ) : ?>

            <tr>
                <td><?php printf( '%s', $order['order_id'] ) ?></td>
                <td><?php printf( '%s', $order['order_date'] ) ?></td>
                <td><?php printf( '%s', $order['total_price'] ) ?></td>
                <td><?php printf( '%s', $order['order_starus'] ) ?></td>
            </tr>

		<?php endforeach; ?>
        </tbody>

    </table>

</div>
<div class="dt-sc-hr-invisible-medium"></div>