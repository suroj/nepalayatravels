(function ($) {

	var $info = $("#dt-travel-add-room-item");
	$info.dialog({
		'title'			: $('#add_room_item').attr('title'),
		'dialogClass'   : 'wp-dialog',
		'modal'         : true,
		'autoOpen'      : false,
		'width'			: 400,
		'closeOnEscape' : true,
		'buttons'       : [
			{
				text: "Check Available",
				click: function() {
					var _this = $(this);
					var _self = $(this).next('.ui-dialog-buttonpane').find('.ui-button:first');

					$.ajax({
						url       : ajaxurl,
						type      : 'POST',
						data      : {
							action : 'dt_travel_check_room_availability',
							room_id: $(this).find('select[name="cmbrooms"]').val(),
							chkin  : $(this).find('input[name="txtcheckindate"]').val(),
							chkout : $(this).find('input[name="txtcheckoutdate"]').val()
						},
						beforeSend: function () {
							_self.append('<i class="fa fa-spinner fa-spin"></i>');
						}
					}).done(function (res) {
						_self.find('.fa').remove();
						if (res.status === true) {

							try {
								_this.find('p.ajax-quantity').html(res.result + res.addons);
							} catch (error) {
								console.debug(error);
							}

						} else if (res.status === false) {
							confirm(res.message);
						}

					}).fail(function () {
						_self.find('.fa').remove();
					})
	            }
			},
			{
				text: "Add",
				"class": 'button-primary',
				click: function() {
					var _model = $(this);
					var _self  = $(this).next('.ui-dialog-buttonpane').find('.ui-button.button-primary');

					$.ajax({
						url       : ajaxurl,
						type      : 'POST',
						data      : {
							action   : 'dt_travel_add_available_room_item',
							order_id : $(this).find('input[name="order_id"]').val(),
							room_id  : $(this).find('select[name="cmbrooms"]').val(),
							chkin    : $(this).find('input[name="txtcheckindate"]').val(),
							chkout   : $(this).find('input[name="txtcheckoutdate"]').val(),
							quantity : $(this).find('select[name="cmbqty"]').val(),
							addons   : $(this).find('select[name="cmbaddons"]').val()
						},
						beforeSend: function () {
							_self.append('<i class="fa fa-spinner fa-spin"></i>');
						}
					}).done(function (res) {
						_self.find('.fa').remove();
						if (res.status === true) {

							try {
								confirm('Success! Room(s) added.');
								_model.dialog('close');
								document.post.submit();
							} catch (error) {
								console.debug(error);
							}

						} else if (res.status === false) {
							confirm(res.message);
						}

					}).fail(function () {
						_self.find('.fa').remove();
					})
	            }
			},
			{
				text: "Close",
				click: function() {
                	$(this).dialog('close');
	            }
			}
		]
	});

	$('#add_room_item').click(function(event) {
		event.preventDefault();
		$info.dialog('open');
	});

	// Individual Delete
	$('.dt_travel_tbl tbody a.remove').click(function(event) {
		var _self = $(this);

		if(confirm('Are you sure wants to delete?')) {
			$.ajax({
				url       : ajaxurl,
				type      : 'POST',
				data      : {
					action   	: 'dt_travel_delete_room_item',
					order_id 	: _self.attr('data-order-id'),
					item_type	: _self.attr('data-order-item-type'),
					item_id  	: _self.attr('data-order-item-id'),
					item_parent : _self.attr('data-order-item-parent')
				},
				beforeSend: function () {
					_self.append('<i class="fa fa-spinner fa-spin"></i>');
				}
			}).done(function (res) {
				_self.find('.fa-spinner').remove();
				if (res.status === true) {

					try {
						document.post.submit();
					} catch (error) {
						console.debug(error);
					}

				} else if (res.status === false) {
					confirm(res.message);
				}

			}).fail(function () {
				_self.find('.fa-spinner').remove();
			});
		}

		event.preventDefault();
		return false;
	});

	// Bulk Delete
	$('.dt_travel_tbl tfoot a#bulk_delete').click(function(event) {

		var _btn = $(this);
		var _flag = false;

		$('.dt_travel_tbl tbody td input[name="chkitem"]').each(function(index, element) {
            var _self = $(this);

			if( _self.is(':checked') ){

				if( confirm('Are you sure wants to delete?') ) {
					$.ajax({
						url       : ajaxurl,
						type      : 'POST',
						data      : {
							action   	: 'dt_travel_delete_room_item',
							order_id 	: _btn.attr('data-order-id'),
							item_type	: _self.attr('data-type'),
							item_id  	: _self.val(),
							item_parent : _self.attr('data-parent')
						},
						beforeSend: function () {
							_btn.append('<i class="fa fa-spinner fa-spin"></i>');
						}
					}).done(function (res) {
						_btn.find('.fa-spinner').remove();
						if (res.status === true) {

							try {
							} catch (error) {
								console.debug(error);
							}

						} else if (res.status === false) {
							confirm(res.message);
						}

					}).fail(function () {
						_btn.find('.fa-spinner').remove();
					});
				}
			}
        });

		document.post.submit();

		event.preventDefault();
		return false;
	});

	$("body").delegate(".dt_order_datepicker","focus",function(event){

		var $This = $(this);
		var $a = $This.attr('name');

		if( $a == 'txtcheckoutdate' ){
			$This.datepicker({
				dateFormat     : 'yy-mm-dd',
				numberOfMonths : 1,
				onSelect       : function () {
					var date = $This.datepicker('getDate');
					$This.parents('.dt-travel-add-room').find('input[name="txtcheckindate"]').datepicker('option', 'maxDate', date)
				}
			});
		} else {
			$This.datepicker({
				dateFormat     : 'yy-mm-dd',
				numberOfMonths: 1,
				onSelect       : function () {
					var date = $This.datepicker('getDate');
					$This.parents('.dt-travel-add-room').find('input[name="txtcheckoutdate"]').datepicker('option', 'minDate', date)
				}
			});
		}
	});

	$("body").delegate(".dt_order_datepicker, .dt-travel-add-room select[name='cmbrooms']","click",function(event){
		$(this).parents('.dt-travel-add-room').find('select[name="cmbqty"], h3, select[name="cmbaddons"]').remove();
	});

})(jQuery);