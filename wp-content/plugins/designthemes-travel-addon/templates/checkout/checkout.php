<?php
if ( !defined( 'ABSPATH' ) ) {
	exit();
}
?>
<?php if ( array_key_exists('dt_room_cart_items', $_SESSION) && !empty( $_SESSION['dt_room_cart_items'] ) ) : ?>

    <div id="room-booking-payment">

        <form name="dt-payment-form" id="dt-payment-form" method="post" action="<?php echo isset( $search_page ) ? $search_page : ''; ?>">
            <h3><?php esc_html_e( 'Booking Rooms', 'designthemes-travel' ); ?></h3>
            <?php $subtotal = 0; ?>
            <table class="dt_checkout_table">
                <thead>
                <th class="dt_room_name"><?php esc_html_e( 'Room', 'designthemes-travel' ); ?></th>
                <th class="dt_capacity"><?php esc_html_e( 'Capacity', 'designthemes-travel' ); ?></th>
                <th class="dt_check_in"><?php esc_html_e( 'Check - in', 'designthemes-travel' ); ?></th>
                <th class="dt_check_out"><?php esc_html_e( 'Check - out', 'designthemes-travel' ); ?></th>
                <th class="dt_night"><?php esc_html_e( 'Night', 'designthemes-travel' ); ?></th>
                <th class="dt_quantity"><?php esc_html_e( 'Quantity', 'designthemes-travel' ); ?></th>
                <th class="dt_gross_total"><?php esc_html_e( 'Gross Total', 'designthemes-travel' ); ?></th>
                </thead>
				<?php if ( $rooms = $_SESSION['dt_room_cart_items'] ): ?>

					<?php foreach ( $rooms as $cart_id => $room ): ?>
						<?php
							$room_meta = get_post_meta( $cart_id, '_room_settings', TRUE );
							$room_meta = is_array( $room_meta ) ? $room_meta : array();
			
							$term_meta = get_term_meta( $room_meta['room-adults'], '_room_capacity_options', false );
							$capacity = !empty($term_meta[0]['room_capacity']) ? $term_meta[0]['room_capacity'] : '';

							$cart_addon = array_key_exists('addon', $room) ? count($room['addon']) : '';
						?>
                        <tr class="dt_checkout_item" data-cart-id="<?php echo esc_attr( $cart_id ); ?>">
                            <td class="dt_room_name"<?php echo $cart_addon ? ' rowspan="' . ( count( $room['addon'] ) + 1 ) . '"' : '' ?>>
                                <a href="<?php echo get_permalink( $cart_id ); ?>"><?php echo get_the_title( $cart_id ); ?></a>
                            </td>
                            <td class="dt_capacity"><?php echo sprintf( _n( '%d adult', '%d adults', $capacity, 'designthemes-travel' ), $capacity ); ?> </td>
                            <td class="dt_check_in"><?php echo date( 'F d, Y', strtotime( $room['checkin_date'] ) ); ?></td>
                            <td class="dt_check_out"><?php echo date( 'F d, Y', strtotime( $room['checkout_date'] ) ); ?></td>
                            <td class="dt_night"><?php echo $room['nights']; ?></td>
                            <td class="dt_quantity"><?php echo esc_attr( $room['qty'] ); ?></td>
                            <td class="dt_gross_total">
								<?php $total = $room['price'] * $room['qty']; $subtotal += $total; echo dt_travel_get_formatted_price($total); ?>
                            </td>
                        </tr><?php
						if( $cart_addon != '' ):
							foreach( $room['addon'] as $k => $v ): ?>
								<tr>
									<td class="dt_room_name" colspan="4">
										<?php $term = get_term( $v['term_id'], 'dt_services' ); echo ($term->name); ?>
									</td>
									<td class="dt_quantity"><?php echo esc_attr( $v['qty'] ); ?></td>
									<td class="dt_gross_total">
										<?php $total = $v['price'] * $v['qty']; $subtotal += $total; echo dt_travel_get_formatted_price($total); ?>
									</td>
								</tr><?php
							endforeach;
						endif; ?>
					<?php endforeach; ?>
				<?php endif; ?>

                <tr class="dt_sub_total">
                    <td colspan="6"><?php esc_html_e( 'Sub Total', 'designthemes-travel' ); ?>
                    <td><span class="dt-align-right dt_sub_total_value"><?php echo dt_travel_get_formatted_price( $subtotal ); ?></span></td>
                </tr>

				<?php $tax_amount = ''; ?>
				<?php if ( cs_get_option('include-tax') && $tax = cs_get_option('tax-percentage') ) : ?>
                    <tr class="dt_advance_tax">
                        <td colspan="6">
							<?php _e( 'Tax', 'designthemes-travel' ); ?>
							<?php if ( $tax < 0 ) { ?>
                                <span><?php printf( esc_html__( '(price including tax)', 'designthemes-travel' ) ); ?></span>
							<?php } else { ?>
								<span>(<?php echo ($tax); ?> %)</span>
							<?php } ?>
                        </td>
                        <td>
                            <span class="dt-align-right"><?php $tax_amount = ($subtotal) * ($tax) / 100; echo dt_travel_get_formatted_price( $tax_amount ); ?></span>
                        </td>
                    </tr>
				<?php endif; ?>

                <tr class="dt_advance_grand_total">
                    <td colspan="6">
						<?php esc_html_e( 'Grand Total', 'designthemes-travel' ); ?>
                    </td>
                    <td>
                        <span class="dt-align-right dt_grand_total_value"><?php echo dt_travel_get_formatted_price( ($subtotal) + ($tax_amount) ); ?></span>
                    </td>
                </tr>

				<?php $advance_payment = ''; ?>
				<?php if ( cs_get_option('enable-advance') && $advance_percent = cs_get_option('advance-percentage') ) : ?>
                    <tr class="dt_advance_payment">
                        <td colspan="6">
							<?php printf( esc_html__( 'Advance Payment (%s%% of Grand Total)', 'designthemes-travel' ), $advance_percent ); ?>
                        </td>
                        <td>
                            <span class="dt-align-right"><?php $advance_payment = (($subtotal) + ($tax_amount)) * ($advance_percent) / 100; echo dt_travel_get_formatted_price( $advance_payment ); ?></span>
                        </td>
                    </tr>
                    <tr class="dt_payment_all">
                        <td colspan="7" class="dt-align-right">
                            <label class="dt-align-right">
                                <input type="checkbox" name="pay_all" />
                                <?php esc_html_e( 'I want to pay all', 'designthemes-travel' ); ?>
                            </label>
                        </td>
                    </tr>
				<?php endif; ?>

            </table>

			<?php if ( !is_user_logged_in() && !cs_get_option('enable-booking') && get_option( 'users_can_register' ) ) : ?>

				<?php printf( __( 'You have to <strong><a href="%s">login</a></strong> or <strong><a href="%s">register</a></strong> to checkout.', 'designthemes-travel' ), wp_login_url( dt_travel_checkout_url() ), wp_registration_url() ) ?>

			<?php else : ?>

				<?php dt_travel_get_template( 'checkout/customer.php', array( 'customer' => $customer ) ); ?>
				<?php dt_travel_get_template( 'checkout/payment-method.php', array( 'customer' => $customer ) ); ?>
				<?php dt_travel_get_template( 'checkout/addition-information.php' ); ?>

                <input type="hidden" name="action" value="dt_travel_checkout_place_order" />
                <input type="hidden" name="total_advance" value="<?php echo esc_attr( $advance_payment ); ?>" />
                <input type="hidden" name="total_price" value="<?php echo esc_attr( ($subtotal) + ($tax_amount) ); ?>" />
				<?php if ( $tos_page_id = cs_get_option('terms-pageid') ) { ?>
                    <p>
                        <label>
                            <input type="checkbox" name="tos" value="1" />
							<?php printf( __( 'I agree with ', 'designthemes-travel' ) . '<a href="%s" target="_blank">%s</a>', get_permalink( $tos_page_id ), get_the_title( $tos_page_id ) ); ?>
                        </label>
                    </p>
				<?php } ?>
                <p>
                    <button type="submit" class="dt-sc-button filled medium checkout"><?php esc_html_e( 'Check out', 'designthemes-travel' ); ?></button>
                </p>

			<?php endif; ?>
        </form>
    </div>
    <div class="dt-sc-hr-invisible-medium"></div>

<?php endif; ?>