<?php
/**
 * Get Template
 * @return  string
 */
if ( !function_exists( 'dt_travel_get_template_part' ) ) {

	function dt_travel_get_template_part( $slug, $name = '' ) {
		$template = '';

		if ( $name ) {
			$template = locate_template( array( "{$slug}-{$name}.php", DTTRAVELADDON_PATH . "/{$slug}-{$name}.php" ) );
		}

		// Get default slug-name.php
		if ( !$template && $name && file_exists( DTTRAVELADDON_PATH . "/templates/{$slug}-{$name}.php" ) ) {
			$template = DTTRAVELADDON_PATH . "/templates/{$slug}-{$name}.php";
		}

		if ( !$template ) {
			$template = locate_template( array( "{$slug}.php", DTTRAVELADDON_PATH . "{$slug}.php" ) );
		}

		// Allow 3rd party plugin filter template file from their plugin
		if ( $template ) {
			$template = apply_filters( 'dt_travel_get_template_part', $template, $slug, $name );
		}
		if ( $template && file_exists( $template ) ) {
			load_template( $template, false );
		}

		return $template;
	}
}

/**
 * Include Template
 * @return nothing
 */
if ( !function_exists( 'dt_travel_get_template' ) ) {

	function dt_travel_get_template( $template_name, $args = array(), $template_path = '', $default_path = '' ) {
		if ( $args && is_array( $args ) ) {
			extract( $args );
		}

		$located = dt_travel_locate_template( $template_name, $template_path, $default_path );

		if ( !file_exists( $located ) ) {
			_doing_it_wrong( __FUNCTION__, sprintf( '<code>%s</code> does not exist.', $located ), '2.1' );
			return;
		}
		// Allow 3rd party plugin filter template file from their plugin
		$located = apply_filters( 'dt_travel_get_template', $located, $template_name, $args, $template_path, $default_path );

		do_action( 'dt_travel_before_template_part', $template_name, $template_path, $located, $args );

		if ( $located && file_exists( $located ) ) {
			include( $located );
		}

		do_action( 'dt_travel_after_template_part', $template_name, $template_path, $located, $args );
	}
}

/**
 * Lccate Template
 * @return template
 */
if ( !function_exists( 'dt_travel_locate_template' ) ) {

	function dt_travel_locate_template( $template_name, $template_path = '', $default_path = '' ) {

		if ( !$template_path ) {
			$template_path = 'designthemes-travel-addon';
		}

		if ( !$default_path ) {
			$default_path = DTTRAVELADDON_PATH . '/templates/';
		}

		$template = null;
		// Look within passed path within the theme - this is priority
		$template = locate_template(
			array(
				trailingslashit( $template_path ) . $template_name,
				$template_name
			)
		);
		// Get default template
		if ( !$template ) {
			$template = $default_path . $template_name;
		}

		// Return what we found
		return apply_filters( 'dt_travel_locate_template', $template, $template_name, $template_path );
	}
}

/**
 * Returns true when viewing a room taxonomy archive.
 * @return bool
 */
if ( ! function_exists( 'is_room_taxonomy' ) ) {

	function is_room_taxonomy() {
		return is_tax( get_object_taxonomies( 'dt_rooms' ) );
	}
}

/**
 * Getting room thumbnail.
 * @return html
 */
add_action( 'dt_travel_booking_loop_room_thumbnail', 'dt_travel_booking_room_thumbnail' );
add_action( 'dt_travel_booking_single_room_gallery', 'dt_travel_booking_room_thumbnail' );
if ( !function_exists( 'dt_travel_booking_room_thumbnail' ) ) {

	function dt_travel_booking_room_thumbnail() {
		dt_travel_get_template( 'loop/thumbnail.php' );
	}
}

/**
 * Getting room title.
 * @return html
 */
add_action( 'dt_travel_booking_loop_room_title', 'dt_travel_booking_room_title' );
add_action( 'dt_travel_booking_single_room_title', 'dt_travel_booking_room_title' );
if ( !function_exists( 'dt_travel_booking_room_title' ) ) {

	function dt_travel_booking_room_title() {
		dt_travel_get_template( 'loop/title.php' );
	}
}

/**
 * Getting room excerpt.
 * @return html
 */
add_action( 'dt_travel_booking_loop_room_excerpt', 'dt_travel_booking_room_excerpt' );
if ( !function_exists( 'dt_travel_booking_room_excerpt' ) ) {

	function dt_travel_booking_room_excerpt() {
		dt_travel_get_template( 'loop/excerpt.php' );
	}
}

/**
 * Getting room price.
 * @return html
 */
add_action( 'dt_travel_booking_loop_room_price', 'dt_travel_booking_room_price' );
add_action( 'dt_travel_booking_single_room_price', 'dt_travel_booking_room_price' );
if ( !function_exists( 'dt_travel_booking_room_price' ) ) {

	function dt_travel_booking_room_price() {
		dt_travel_get_template( 'loop/price.php' );
	}
}

/**
 * Getting room rating.
 * @return html
 */
add_action( 'dt_travel_booking_loop_room_rating', 'dt_travel_booking_room_rating' );
if ( !function_exists( 'dt_travel_booking_room_rating' ) ) {

	function dt_travel_booking_room_rating() {
		if ( cs_get_option( 'room-archives-rating' ) ) {
			dt_travel_get_template( 'loop/rating.php' );
		}
	}
}

/**
 * Getting room information.
 * @return html
 */
add_action( 'dt_travel_booking_single_room_infomation', 'dt_travel_booking_single_room_infomation' );
if ( !function_exists( 'dt_travel_booking_single_room_infomation' ) ) {

	function dt_travel_booking_single_room_infomation() {
		dt_travel_get_template( 'single-room/details.php' );
	}
}

/*
 * Getting related room
 * @return html
 */
add_action( 'dt_travel_booking_after_single_product', 'dt_travel_booking_single_room_related' );
if ( !function_exists( 'dt_travel_booking_single_room_related' ) ) {
	function dt_travel_booking_single_room_related() {
		dt_travel_get_template( 'single-room/related-room.php' );
	}
}

/*
 * Getting room additional info
 * @return html
 */
function maharaj_room_info($room_id = NULL, $limit = NULL) {
	$room_id = !empty($room_id) ? $room_id : 0;
	$limit = !empty($limit) ? $limit : 10;

	$room_meta = get_post_meta($room_id,'_room_settings',TRUE);
	$room_meta = is_array($room_meta) ? $room_meta  : array();
	
	$excerpt = array();
	if( isset( $room_meta['additional-info'] ) && $room_meta['additional-info'] != '' ):
		$excerpt = explode(' ', $room_meta['additional-info'], $limit);
	else:
		$excerpt = explode(' ', get_the_excerpt(), $limit);
	endif;

	$excerpt = array_filter($excerpt);

	if (!empty($excerpt)) {
		if (count($excerpt) >= $limit) {
			array_pop($excerpt);
			$excerpt = implode(" ", $excerpt).'...';
		} else {
			$excerpt = implode(" ", $excerpt);
		}
		$excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
		$excerpt = str_replace('&nbsp;', '', $excerpt);
		if(!empty ($excerpt))
			return "<p>{$excerpt}</p>";
	}
}

/**
 * Load comment template filter.
 * @return html
 */
add_filter( 'comments_template', 'dt_travel_load_comments_template' );
function dt_travel_load_comments_template( $template ) {
	if ( get_post_type() !== 'dt_rooms' ) {
		return $template;
	}

	$check_dirs = array(
		trailingslashit( get_stylesheet_directory() ) . 'designthemes-travel-addon',
		trailingslashit( get_template_directory() ) . 'designthemes-travel-addon',
		trailingslashit( get_stylesheet_directory() ),
		trailingslashit( get_template_directory() ),
		trailingslashit( DTTRAVELADDON_PATH . '/templates/' )
	);

	foreach ( $check_dirs as $dir ) {
		if ( file_exists( trailingslashit( $dir ) . 'single-room-reviews.php' ) ) {
			return trailingslashit( $dir ) . 'single-room-reviews.php';
		}
	}
}

/**
 * Add comment rating
 * @param int $comment_id
 */
add_action( 'comment_post', 'dt_travel_add_comment_rating', 10, 2 );
if ( !function_exists( 'dt_travel_add_comment_rating' ) ) {
	function dt_travel_add_comment_rating( $comment_id, $approved ) {
		if ( isset( $_POST['rating'] ) && 'dt_rooms' === get_post_type( $_POST['comment_post_ID'] ) ) {
			$rating = absint( sanitize_text_field( $_POST['rating'] ) );
			if ( $rating && $rating <= 5 && $rating > 0 ) {
				// save comment rating
				add_comment_meta( $comment_id, 'rating', $rating, true );

				if ( $approved === 1 ) {
					// save post meta arveger_rating
					$comment = get_comment( $comment_id );

					$postID = absint( $comment->comment_post_ID );

					$averger_rating = dt_travel_room_average_rating($postID);

					$old_rating = get_post_meta( $postID, 'arveger_rating', true );
					$old_modify = get_post_meta( $postID, 'arveger_rating_last_modify', true );
					if ( $old_rating ) {
						update_post_meta( $postID, 'arveger_rating', $averger_rating );
						update_post_meta( $postID, 'arveger_rating_last_modify', time() );
					} else {
						add_post_meta( $postID, 'arveger_rating', $averger_rating );
						add_post_meta( $postID, 'arveger_rating_last_modify', time() );
					}
				}
			}
		}
	}
}

/**
 * Move comment field to bottom
 * @param int $fields
 */
add_filter( 'comment_form_fields', 'dt_travel_room_comment_field_to_bottom' );
function dt_travel_room_comment_field_to_bottom( $fields ) {
	$comment_field = $fields['comment'];
	unset( $fields['comment'] );
	$fields['author']  = $fields['author'];
	$fields['email']  = $fields['email'];
	$fields['url']  = isset( $fields['url'] ) ? $fields['url'] : '';
	$fields['comment'] = $comment_field;

	return $fields;
}

/**
 * Output the Review comments template.
 *
 * @param WP_Comment object
 * @param mixed
 * @param int
 */
if ( !function_exists( 'dt_travel_room_comments' ) ) {

	function dt_travel_room_comments( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment;
		dt_travel_get_template( 'single-room/review.php', array( 'comment' => $comment, 'args' => $args, 'depth' => $depth ) );
	}
}

/**
 * Getting room average rating.
 * @return html
 */
if ( !function_exists( 'dt_travel_room_average_rating' ) ) {

	function dt_travel_room_average_rating( $room_id ) {
		$comments = get_comments( array( 'post_id' => $room_id, 'status' => 'approve' ) );
		$total    = 0;
		$i        = 0;
		foreach ( $comments as $key => $comment ) {
			$rating = get_comment_meta( $comment->comment_ID, 'rating', true );
			if ( $rating ) {
				$total = $total + $rating;
				$i ++;
			}
		}
		if ( $comments && $i )
			return $total / $i;

		return null;
	}
}

/**
 * Getting room review count.
 * @return number
 */
function dt_travel_get_review_count( $room_id ) {
	$count = count( get_comments( array( 'post_id' => $room_id, 'status' => 'approve' ) ) );
	return $count;
}

/**
 * Get Template Content
 * @return template
 */
if ( !function_exists( 'dt_travel_get_template_content' ) ) {

	function dt_travel_get_template_content( $template_name, $args = array(), $template_path = '', $default_path = '' ) {
		ob_start();
		dt_travel_get_template( $template_name, $args, $template_path, $default_path );
		return ob_get_clean();
	}
}

/**
 * Search Page
 * @return url
 */
if ( ! function_exists( 'dt_travel_search_url' ) ) {
	function dt_travel_search_url( $params = array() ) {
		return apply_filters( 'dt_travel_search_url', dt_travel_get_page_permalink( 'search' ), dt_travel_get_page_id( 'search' ), $params );
	}
}

/**
 * Cart Page
 * @return url
 */
if ( ! function_exists( 'dt_travel_cart_url' ) ) {
	function dt_travel_cart_url( $params = array() ) {
		return apply_filters( 'dt_travel_cart_url', dt_travel_get_page_permalink( 'cart' ), dt_travel_get_page_id( 'cart' ), $params );
	}
}

/**
 * Checkout Page
 * @return url
 */
if ( ! function_exists( 'dt_travel_checkout_url' ) ) {
	function dt_travel_checkout_url( $params = array() ) {
		return apply_filters( 'dt_travel_checkout_url', dt_travel_get_page_permalink( 'checkout' ), dt_travel_get_page_id( 'checkout' ), $params );
	}
}

/**
 * Account Page
 * @return url
 */
if ( ! function_exists( 'dt_travel_account_url' ) ) {
	function dt_travel_account_url() {
		$id = dt_travel_get_page_id( 'account' );

		$url = home_url();
		if ( $id ) {
			$url = get_the_permalink( $id );
		}

		return apply_filters( 'dt_travel_account_url', $url );
	}
}

/**
 * Get check out return URL
 * @return mixed
 */
if ( !function_exists( 'dt_travel_return_url' ) ) {
	function dt_travel_return_url() {
		$url = dt_travel_checkout_url();
		return apply_filters( 'dt_travel_return_url', $url );
	}
}

/**
 * Get Permalink
 * @return url
 */
if ( ! function_exists( 'dt_travel_get_page_permalink' ) ) {
	function dt_travel_get_page_permalink( $name ) {
		return get_the_permalink( dt_travel_get_page_id( $name ) );
	}
}

/**
 * Get Page ID
 * @return ID
 */
if ( ! function_exists( 'dt_travel_get_page_id' ) ) {
	function dt_travel_get_page_id( $name ) {

		switch($name):
			case 'search':
				$id = cs_get_option( 'booking-search-pageid' );
				break;

			case 'checkout'	:
				$id = cs_get_option( 'booking-checkout-pageid' );
				break;
				
			case 'cart'	:
				$id = cs_get_option( 'booking-cart-pageid' );
				break;
				
			case 'account'	:
				$id = cs_get_option( 'user-account-pageid' );
				break;
		endswitch;

		return apply_filters( 'dt_travel_get_page_id', $id );
	}
}

/**
 * Get URL Request
 * @return value
 */
if ( ! function_exists( 'dt_travel_get_request' ) ) {
	function dt_travel_get_request( $name, $default = null, $var = '' ) {
		$return = $default;
		switch ( strtolower( $var ) ) {
			case 'post':
				$var = $_POST;
				break;
			case 'get':
				$var = $_GET;
				break;
			default:
				$var = $_REQUEST;
		}
		if ( ! empty( $var[ $name ] ) ) {
			$return = $var[ $name ];
		}
		if ( is_string( $return ) ) {
			$return = sanitize_text_field( $return );
		}

		return $return;
	}
}

add_filter( 'the_content', 'dt_travel_setup_shortcode_page_content' );
function dt_travel_setup_shortcode_page_content( $content ) {
	global $post;

	$page_id = $post->ID;

	if ( !$page_id ) {
		return $content;
	}

	if ( cs_get_option( 'booking-cart-pageid' ) == $page_id ) {
		$content = '[dt_sc_rooms_cart]';
	} else if ( cs_get_option( 'booking-checkout-pageid' ) == $page_id ) {
		$content = '[dt_sc_rooms_checkout]';
	} else if ( cs_get_option( 'booking-search-pageid' ) == $page_id ) {
		$content = '[dt_sc_rooms_search_form]';
	} else if ( cs_get_option( 'user-account-pageid' ) == $page_id ) {
		$content = '[dt_sc_user_account]';
	}
	return do_shortcode( $content );
}


/**
 * Get Available Rooms
 * @return rooms
 */
if ( ! function_exists( 'dt_travel_search_rooms' ) ) {
	function dt_travel_search_rooms( $args = array() ) {

		$chkin_date = $args['check_in_date'];
		$chkout_date = $args['check_out_date'];
		$adults = $args['adults'];
		$child = $args['max_child'];

		if( $chkin_date == '' || $chkout_date == '' || $adults == '' ) {
			return '<p>'.esc_html__('Invalid input data.').'</p>';
		}

		$mata_query = array(
			array(
				'key'     => '_order_settings',
				'value'   => 'cancelled',
				'compare' => 'NOT LIKE',
			),
		);

		$all_rooms = dt_travel_get_rooms_quantity();

		$args = array( 'post_type' => 'dt_orders', 'meta_key' => '_order_settings', 'posts_per_page' => -1, 'meta_query' => $mata_query );
		$the_query = new WP_Query($args);

		if($the_query->have_posts()):
			// dates between search criteria...
			$chkinchkout = dt_travel_get_between_dates( date( 'Y-m-d', strtotime($chkin_date) ), date( 'Y-m-d', strtotime($chkout_date) ) );
			while($the_query->have_posts()): $the_query->the_post();

				$order_meta = get_post_meta( get_the_ID(), '_order_items', TRUE );
				$order_meta = is_array( $order_meta ) ? $order_meta : array();

				foreach( $order_meta as $v ):
					$temp = dt_travel_get_between_dates( $v['checkin_date'], $v['checkout_date'] );

					$containsSearch = count(array_intersect_key($chkinchkout, $temp));
					if( $containsSearch > 0 ) {
						$qty = $all_rooms[$v['room_id']]['qty'] - $v['qty'];
						$all_rooms[$v['room_id']]['qty'] = $qty;
					}
				endforeach;
			endwhile;
		endif;
		wp_reset_postdata();

		$temp = $result_arr = array();
		foreach( $all_rooms as $k => $r ):
			if( $r['qty'] <= 0 || $r['adults'] > $adults )
				$temp[$k] = $r;
		endforeach;

		$result_arr = array_diff_key($all_rooms, $temp);

		return ($result_arr);
	}
}

/**
 * Get Rooms with Quantity
 * @return rooms array
 */
if ( ! function_exists( 'dt_travel_get_rooms_quantity' ) ) {
	function dt_travel_get_rooms_quantity( $count = -1 ) {

		$args = array( 'post_type' => 'dt_rooms', 'posts_per_page' => $count );
		$the_query = new WP_Query($args);

		$rooms_arr = array();
		if($the_query->have_posts()):
			while($the_query->have_posts()): $the_query->the_post();

				$room_id = get_the_ID();

				$room_meta = get_post_meta( $room_id, '_room_settings', TRUE );
				$room_meta = is_array( $room_meta ) ? $room_meta : array();

				$term_meta = get_term_meta( $room_meta['room-adults'], '_room_capacity_options', false );
				$capacity = !empty($term_meta[0]['room_capacity']) ? $term_meta[0]['room_capacity'] : '';
				
				$t = array();

				$t['qty'] = $room_meta['room-qty'];
				$t['adults'] = $capacity;
				$t['childs'] = $room_meta['room-childs'];

				$rooms_arr[$room_id] = $t;
			endwhile;
		endif;
		wp_reset_postdata();

		return ($rooms_arr);
	}
}

/**
 * Get cart description
 * @return string
 */
if ( !function_exists( 'dt_travel_get_cart_description' ) ) {
	function dt_travel_get_cart_description( $order_id ) {

		$order_items = get_post_meta ( $order_id, '_order_items', TRUE );
		$order_items = is_array ( $order_items ) ? $order_items : array ();

		$description = array();
		foreach ( $order_items as $item ) {
			$description[] = sprintf( '%s (x %d)', get_the_title( $item['room_id'] ), $item['qty'] );
		}
		return join( ', ', $description );
	}
}