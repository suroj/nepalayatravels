<?php
/**
 * Room loop title
 *
 * @author  designthemes
 * @package designthemes-travel-addon/templates
 * @version 1.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>
<div class="title">
    <h4>
        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
    </h4>
</div>