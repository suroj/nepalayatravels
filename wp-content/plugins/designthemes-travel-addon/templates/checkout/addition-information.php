<?php
if ( !defined( 'ABSPATH' ) ) {
	exit();
}
?>
<div class="dt-addition-information">
    <div class="dt-col-padding dt-col-border">
        <h4><?php esc_html_e( 'Addition Information', 'designthemes-travel' ); ?></h4>
        <textarea name="addition_information"></textarea>
    </div>
</div>