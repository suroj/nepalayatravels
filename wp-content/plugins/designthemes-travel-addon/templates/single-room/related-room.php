<?php
if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;

$related_rooms = cs_get_option('single-room-related');
$terms = wp_get_object_terms( $post->ID, 'dt_room_types', array('fields' => 'ids') );
?>
<?php if( $related_rooms && $terms ) : ?>
    <div class="dt-sc-hr-invisible"></div>
    <div class="dt-sc-clear"></div>

    <div class="related-rooms">
    	<div class="dt-sc-title with-two-border aligncenter"><h2><?php esc_html_e('Related', 'designthemes-travel');?> <strong> <?php esc_html_e('Rooms', 'designthemes-travel');?></strong></h2></div><?php

		$args = array(
			'post_type'				=> 'dt_rooms',
			'posts_per_page'		=> 3,
			'post__not_in'			=> array( $post->ID ),
			'post_status'			=> 'publish',
			'ignore_sticky_posts'	=> true,
			'no_found_rows'			=> true,
			'tax_query'				=> array()
		);

		$args['tax_query'][] = array( 'taxonomy' => 'dt_room_types',
			'field' => 'term_id',
			'terms' => $terms ,
			'operator' => 'IN');

		$post_class = " dt-room column dt-sc-one-third";
		$columns = 3;

		$the_query = new WP_Query( $args );
		if( $the_query->have_posts() ) :
			$i = 1;

			while ( $the_query->have_posts() ) : $the_query->the_post();
			
				$temp_class = '';
				if($i == 1) $temp_class .= $post_class." first"; else $temp_class .= $post_class;
				if($i == $columns) $i = 1; else $i = $i + 1;
				?>
                <div class="<?php echo esc_attr( trim($temp_class));?>"><?php
					dt_travel_get_template_part( 'content', 'room' ); ?>
				</div><?php

			endwhile;
		endif; ?>
    </div>
<?php endif; ?>