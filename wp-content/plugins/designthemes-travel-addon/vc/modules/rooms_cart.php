<?php add_action( 'vc_before_init', 'dt_sc_rooms_cart_vc_map' );
function dt_sc_rooms_cart_vc_map() {

	vc_map( array(
		"name" => esc_html__( "Cart", 'designthemes-travel' ),
		"base" => "dt_sc_rooms_cart",
		"icon" => "dt_sc_rooms_cart",
		"category" => DTTRAVEL_VC_CATEGORY,
		"description" => esc_html__("Show rooms cart.",'designthemes-travel'),
		"params" => array(

			// Title
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Title', 'designthemes-travel' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter the title of cart form. ex: Cart', 'designthemes-travel' )
			),
		)
	) );
}?>