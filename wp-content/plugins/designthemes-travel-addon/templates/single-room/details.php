<?php
if ( !defined( 'ABSPATH' ) ) {
	exit();
}
?>

<div class="dt_single_room_details">
	<?php the_content();

	$room_comment = cs_get_option('single-room-comments');
	if( $room_comment ): ?>
        <div class="dt-sc-hr"></div>
        <div class="dt-sc-clear"></div>
    
        <!-- ** Comment Entries ** -->
        <section class="commententries">
			<?php comments_template('', true); ?>
        </section><?php
    endif; ?>
</div>