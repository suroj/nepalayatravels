<?php
vc_map( array(
	"name" => esc_html__( "Sociable", 'dt-core' ),
	"base" => "dt_sc_sociable_new",
	"icon" => "dt_sc_sociable_new",
	"category" => THEME_NAME,
    'params'    => array(
        array(
            'type' => 'el_id',
            'param_name' => 'el_id',
            'edit_field_class' => 'hidden',
            'settings' => array(
                'auto_generate' => true,
            )
        ),

        # Social List
        array(
        	'heading' => esc_html__( 'Socials', 'dt-core' ),
  			'param_name' => 'social_list',
  			'type'	=> 'param_group',
  			'save_always' => true,  			
  			'params' => array(
  				array(
  					'type' => 'dropdown',
  					'heading' => esc_html__( 'Social', 'dt-core' ),
  					'param_name' => 'social',
  					'value' => array(
						esc_html__('Delicious', 'maharaj')	 	 => 'delicious',
						esc_html__('Deviantart', 'maharaj')	 => 'deviantart',
						esc_html__('Digg', 'maharaj')	 		 => 'digg',
						esc_html__('Dribbble', 'maharaj')	 	 => 'dribbble',
						esc_html__('Envelope', 'maharaj')	 	 => 'envelope',
						esc_html__('Facebook', 'maharaj')	 	 => 'facebook',
						esc_html__('Flickr', 'maharaj')	 	 => 'flickr',
						esc_html__('Google Plus', 'maharaj') 	 => 'google-plus',
						esc_html__('Instagram', 'maharaj')	 	 => 'instagram',
						esc_html__('Lastfm', 'maharaj')	 	 => 'lastfm',
						esc_html__('Linkedin', 'maharaj')	 	 => 'linkedin',
						esc_html__('Myspace', 'maharaj')	 	 => 'myspace',
						esc_html__('Picasa', 'maharaj')	 	 => 'picasa',
						esc_html__('Pinterest', 'maharaj')	 	 => 'pinterest',
						esc_html__('Reddit', 'maharaj')	 	 => 'reddit',
						esc_html__('RSS', 'maharaj')	 		 => 'rss',
						esc_html__('Skype', 'maharaj')	 		 => 'skype',
						esc_html__('Stumbleupon', 'maharaj')	 => 'stumbleupon',
						esc_html__('Tumblr', 'maharaj')	 	 => 'tumblr',
						esc_html__('Twitter', 'maharaj')	 	 => 'twitter',
						esc_html__('Viadeo', 'maharaj')	 	 => 'viadeo',
						esc_html__('Vimeo', 'maharaj')	 	 	 => 'vimeo',
						esc_html__('Yahoo', 'maharaj')	 		 => 'yahoo',
						esc_html__('Youtube', 'maharaj')	 	 => 'youtube',
  					),
  					'edit_field_class' => 'vc_column vc_col-sm-6',
  					'save_always' => true,  			
  					'admin_label' => true,
  					'std' => 'facebook'
  				),
  				array(
  					'type' => 'vc_link',
  					'heading' => esc_html__( 'Link', 'dt-core' ),
  					'param_name' => 'link',
  					'edit_field_class' => 'vc_column vc_col-sm-6',
  					'save_always' => true,
  				),
  			),
        ),

  		# Size
  		array(
  			'type' => 'dropdown',
  			'heading' => esc_html__( 'Size', 'dt-core' ),
  			'param_name' => 'size',
  			'value' => array( 
  				esc_html__('Small','dt-core') => 'small',
				esc_html__('Medium','dt-core') => 'medium',
				esc_html__('Large','dt-core') => 'large',
				esc_html__('Extra Large','dt-core') => 'extra-large',
  			),
  			'description' => esc_html__( 'Select size of sociable.', 'dt-core' ),
  			'std' => 'small',
  			'edit_field_class' => 'vc_column padding-top-16px vc_col-sm-6',
  			'save_always' => true,  			
  			'admin_label' => true
  		),

  		# Align
  		array(
  			'type' => 'dropdown',
  			'heading' => esc_html__('Alignment', 'dt-core' ),
  			'param_name' => 'align',
  			'value' => array(
  				esc_html__('Left', 'dt-core' ) => 'left',
				esc_html__('Right', 'dt-core' ) => 'right',
				esc_html__('Center', 'dt-core' ) => 'center',
			),
  			'std' => 'center',
  			'edit_field_class' => 'vc_column vc_col-sm-6',
  			'save_always' => true,
		),

		# Class
  		array(
  			"type" => "textfield",
  			"heading" => esc_html__( "Extra class name", 'dt-core' ),
  			"param_name" => "class",
  			'description' => esc_html__('Style particular element differently - add a class name and refer to it in custom CSS','dt-core')
  		),

  		# Default State Tab
      		
      		# Style
      		array(
      			'type' => 'dropdown',
      			'heading' => esc_html__( 'Style', 'dt-core' ),
      			'param_name' => 'default-style',
      			'group'	=> esc_html__( 'Default State', 'dt-core' ),
      			'value' => array(
      				esc_html__('Bordered','dt-core') => 'bordered',
      				esc_html__('Filled','dt-core') => 'filled',
      				esc_html__('None','dt-core') => 'none',
      			),
      			'description' => esc_html__( 'Select style of sociable in default state.', 'dt-core' ),
      			'std' => 'filled',
      			'edit_field_class' => 'vc_column padding-top-0px vc_col-sm-6',
      			'save_always' => true,
      		),

      		# Shape  		
      		array(
      			'type' => 'dropdown',
      			'heading' => esc_html__( 'Shape', 'dt-core' ),
      			'param_name' => 'default-shape',
      			'group'	=> esc_html__( 'Default State', 'dt-core' ),
      			'value' => array(
                    esc_html__('Square','dt-core') => 'square',
                    esc_html__('Circle','dt-core') => 'circle',
                    esc_html__('Hexagon','dt-core') => 'hexagon',
                    esc_html__('Hexagon Alt','dt-core') => 'hexagon-alt',
                    esc_html__('Diamond Square','dt-core') => 'diamond-square',
                    esc_html__('Diamond Narrow','dt-core') => 'diamond-narrow',
                    esc_html__('Diamond Wide','dt-core') => 'diamond-wide',
     			),
      			'description' => esc_html__( 'Select shape of sociable in default state.', 'dt-core' ),
      			'std' => 'square',
      			'save_always' => true,
      			'edit_field_class' => 'vc_column padding-top-0px vc_col-sm-6',
      			'dependency'  => array( 'element' => 'default-style', 'value' => array('bordered', 'filled') )
      		),

      		# Apply Rounded Corner?
            array(
            	'type' => 'checkbox',
                'heading' => __( 'Apply Rounded Corner?', 'dt-core' ),
                'param_name' => 'default-border-radius', 
                'save_always' => true,
                'group' => esc_html__( 'Default State', 'dt-core' ),
                'dependency'  => array( 'element' => 'default-shape', 'value' => array('square', 'diamond-square') )
            ),

            array(
            	'type' => 'dt_sc_vc_hr',
                'param_name' => 'hr_for_color_section_end',
                'group' => esc_html__( 'Default State', 'dt-core' ),
            ),

            # Icon Color
            array(
            	'type' => 'dropdown',
            	'heading' => esc_html__( 'Color', 'dt-core' ),
            	'param_name' => 'default-icon-color',
            	'group' => esc_html__( 'Default State', 'dt-core' ),
            	'value' => array(
            		esc_html__('Theme Primary','dt-core') => 'primary-color',
            		esc_html__('Theme Secondary','dt-core') => 'secondary-color',
            		esc_html__('Theme Tertiary','dt-core') => 'tertiary-color',
            		esc_html__('Custom Color','dt-core') => 'custom',
                ),
                'std' => 'primary-color',
                'save_always' => true,
                'edit_field_class' => 'vc_column vc_col-sm-4', 
            ),

            # BG Color
            array(
            	'type' => 'dropdown',
            	'heading' => esc_html__( 'BG Color', 'dt-core' ),
            	'param_name' => 'default-bg-color',
            	'group' => esc_html__( 'Default State', 'dt-core' ),
            	'value' => array(
            		esc_html__('Theme Primary','dt-core') => 'primary-color',
            		esc_html__('Theme Secondary','dt-core') => 'secondary-color',
            		esc_html__('Theme Tertiary','dt-core') => 'tertiary-color',
            		esc_html__('Custom Color','dt-core') => 'custom',
                ),
                'std' => 'secondary-color',
                'save_always' => true,
                'edit_field_class' => 'vc_column vc_col-sm-4',
                'dependency' => array( 'element' => 'default-style', 'value' => array( 'filled' ) ),
            ),

            # Border Color
            array(
            	'type' => 'dropdown',
            	'heading' => esc_html__( 'Border Color', 'dt-core' ),
            	'param_name' => 'default-border-color',
            	'group' => esc_html__( 'Default State', 'dt-core' ),
            	'value' => array(
            		esc_html__('Theme Primary','dt-core') => 'primary-color',
            		esc_html__('Theme Secondary','dt-core') => 'secondary-color',
            		esc_html__('Theme Tertiary','dt-core') => 'tertiary-color',
            		esc_html__('Custom Color','dt-core') => 'custom',
                ),
                'std' => 'tertiary-color',
                'save_always' => true,
                'edit_field_class' => 'vc_column vc_col-sm-4', 
                'dependency' => array( 'element' => 'default-style', 'value' => array( 'filled', 'bordered' ) ), 
            ),

            # Custom Icon Color                        
            array(
            	'type' => 'colorpicker',
            	'heading' => esc_html__( 'Custom Icon Color', 'dt-core' ),
            	'param_name' => 'default-icon-custom-color',
            	'group' => esc_html__( 'Default State', 'dt-core' ),
            	'save_always' => true,
            	'value' => '#da0000',                
            	'edit_field_class' => 'vc_column vc_col-sm-4',
            	'dependency' => array( 'element' => 'default-icon-color', 'value' => array( 'custom' ) ),                 
            ),

            # Custom BG Color                        
            array(
            	'type' => 'colorpicker',
            	'heading' => esc_html__( 'Custom BG Color', 'dt-core' ),
            	'param_name' => 'default-bg-custom-color',
            	'group' => esc_html__( 'Default State', 'dt-core' ),
            	'save_always' => true,
            	'value' => '#da0000',                
            	'edit_field_class' => 'vc_column vc_col-sm-4',
            	'dependency' => array( 'element' => 'default-bg-color', 'value' => array( 'custom' ) ),                 
            ),

            # Custom Border Color                        
            array(
            	'type' => 'colorpicker',
            	'heading' => esc_html__( 'Custom Border Color', 'dt-core' ),
            	'param_name' => 'default-border-custom-color',
            	'group' => esc_html__( 'Default State', 'dt-core' ),
            	'save_always' => true,
            	'value' => '#da0000',                
            	'edit_field_class' => 'vc_column vc_col-sm-4',
            	'dependency' => array( 'element' => 'default-border-color', 'value' => array( 'custom' ) ),                 
            ),                                                                                            		
  		# Default State Tab

  		# Hover State Tab
      		# Style
      		array(
      			'type' => 'dropdown',
      			'heading' => esc_html__( 'Style', 'dt-core' ),
      			'param_name' => 'hover-style',
      			'group'	=> esc_html__( 'Hover State', 'dt-core' ),
      			'value' => array(
      				esc_html__('Bordered','dt-core') => 'bordered',
      				esc_html__('Filled','dt-core') => 'filled',
      				esc_html__('None','dt-core') => 'none',
      			),
      			'description' => esc_html__( 'Select style of sociable in hover state.', 'dt-core' ),
      			'std' => 'filled',
      			'edit_field_class' => 'vc_column padding-top-0px vc_col-sm-6',
      			'save_always' => true,
      		),

      		# Shape  		
      		array(
      			'type' => 'dropdown',
      			'heading' => esc_html__( 'Shape', 'dt-core' ),
      			'param_name' => 'hover-shape',
      			'group'	=> esc_html__( 'Hover State', 'dt-core' ),
      			'value' => array(
                    esc_html__('Square','dt-core') => 'square',
                    esc_html__('Circle','dt-core') => 'circle',
                    esc_html__('Hexagon','dt-core') => 'hexagon',
                    esc_html__('Hexagon Alt','dt-core') => 'hexagon-alt',
                    esc_html__('Diamond Square','dt-core') => 'diamond-square',
                    esc_html__('Diamond Narrow','dt-core') => 'diamond-narrow',
                    esc_html__('Diamond Wide','dt-core') => 'diamond-wide',
     			),
      			'description' => esc_html__( 'Select shape of sociable in hover state.', 'dt-core' ),
      			'std' => 'square',
      			'save_always' => true,
      			'edit_field_class' => 'vc_column padding-top-0px vc_col-sm-6',
      			'dependency'  => array( 'element' => 'hover-style', 'value' => array('bordered', 'filled') )
      		),

      		# Apply Rounded Corner?
            array(
            	'type' => 'checkbox',
                'heading' => __( 'Apply Rounded Corner?', 'dt-core' ),
                'param_name' => 'hover-border-radius', 
                'save_always' => true,
                'group' => esc_html__( 'Hover State', 'dt-core' ),
                'dependency'  => array( 'element' => 'hover-shape', 'value' => array('square', 'diamond-square') )
            ),

            array(
            	'type' => 'dt_sc_vc_hr',
                'param_name' => 'hr_for_hover_color_section_end',
                'group' => esc_html__( 'Hover State', 'dt-core' ),
            ),

            # Icon Color
            array(
            	'type' => 'dropdown',
            	'heading' => esc_html__( 'Color', 'dt-core' ),
            	'param_name' => 'hover-icon-color',
            	'group' => esc_html__( 'Hover State', 'dt-core' ),
            	'value' => array(
            		esc_html__('Theme Primary','dt-core') => 'primary-color',
            		esc_html__('Theme Secondary','dt-core') => 'secondary-color',
            		esc_html__('Theme Tertiary','dt-core') => 'tertiary-color',
            		esc_html__('Custom Color','dt-core') => 'custom',
                ),
                'std' => 'primary-color',
                'save_always' => true,
                'edit_field_class' => 'vc_column vc_col-sm-4', 
            ),

            # BG Color
            array(
            	'type' => 'dropdown',
            	'heading' => esc_html__( 'BG Color', 'dt-core' ),
            	'param_name' => 'hover-bg-color',
            	'group' => esc_html__( 'Hover State', 'dt-core' ),
            	'value' => array(
            		esc_html__('Theme Primary','dt-core') => 'primary-color',
            		esc_html__('Theme Secondary','dt-core') => 'secondary-color',
            		esc_html__('Theme Tertiary','dt-core') => 'tertiary-color',
            		esc_html__('Custom Color','dt-core') => 'custom',
                ),
                'std' => 'secondary-color',
                'save_always' => true,
                'edit_field_class' => 'vc_column vc_col-sm-4',
                'dependency' => array( 'element' => 'hover-style', 'value' => array( 'filled' ) ),
            ),

            # Border Color
            array(
            	'type' => 'dropdown',
            	'heading' => esc_html__( 'Border Color', 'dt-core' ),
            	'param_name' => 'hover-border-color',
            	'group' => esc_html__( 'Hover State', 'dt-core' ),
            	'value' => array(
            		esc_html__('Theme Primary','dt-core') => 'primary-color',
            		esc_html__('Theme Secondary','dt-core') => 'secondary-color',
            		esc_html__('Theme Tertiary','dt-core') => 'tertiary-color',
            		esc_html__('Custom Color','dt-core') => 'custom',
                ),
                'std' => 'tertiary-color',
                'save_always' => true,
                'edit_field_class' => 'vc_column vc_col-sm-4', 
                'dependency' => array( 'element' => 'hover-style', 'value' => array( 'filled', 'bordered' ) ), 
            ),

            # Custom Icon Color                        
            array(
            	'type' => 'colorpicker',
            	'heading' => esc_html__( 'Custom Icon Color', 'dt-core' ),
            	'param_name' => 'hover-icon-custom-color',
            	'group' => esc_html__( 'Hover State', 'dt-core' ),
            	'save_always' => true,
            	'value' => '#da0000',                
            	'edit_field_class' => 'vc_column vc_col-sm-4',
            	'dependency' => array( 'element' => 'hover-icon-color', 'value' => array( 'custom' ) ),                 
            ),

            # Custom BG Color                        
            array(
            	'type' => 'colorpicker',
            	'heading' => esc_html__( 'Custom BG Color', 'dt-core' ),
            	'param_name' => 'hover-bg-custom-color',
            	'group' => esc_html__( 'Hover State', 'dt-core' ),
            	'save_always' => true,
            	'value' => '#da0000',                
            	'edit_field_class' => 'vc_column vc_col-sm-4',
            	'dependency' => array( 'element' => 'hover-bg-color', 'value' => array( 'custom' ) ),                 
            ),

            # Custom Border Color                        
            array(
            	'type' => 'colorpicker',
            	'heading' => esc_html__( 'Custom Border Color', 'dt-core' ),
            	'param_name' => 'hover-border-custom-color',
            	'group' => esc_html__( 'Hover State', 'dt-core' ),
            	'save_always' => true,
            	'value' => '#da0000',                
            	'edit_field_class' => 'vc_column vc_col-sm-4',
            	'dependency' => array( 'element' => 'hover-border-color', 'value' => array( 'custom' ) ),                 
            ),
  		# Hover State Tab
  		
        # Settings
        	array(
            	'type' => 'dt_sc_vc_title',
            	'group' => __('Settings','dt-core'),
            	'heading'    => esc_html__( 'Hide On', 'dt-core' ),
            	'param_name' => 'title_for_sociable_settings',
            	'save_always' => true,
        	),

        	array(
	            'type' => 'checkbox',
    	        'edit_field_class' => 'vc_column vc_col-sm-6',
            	'param_name' => 'hide_on_lg',
            	'value' => array( __( 'Large Devices', 'dt-core' ) => 'yes' ),
            	'group' => __('Settings','dt-core'),
            	'save_always' => true,
        	),

        	array(
	            'type' => 'checkbox',
    	        'edit_field_class' => 'vc_column vc_col-sm-6',
            	'param_name' => 'hide_on_md',
            	'value' => array( __( 'Medium Devices', 'dt-core' ) => 'yes' ),
            	'group' => __('Settings','dt-core'),
            	'save_always' => true,
        	),

        	array(
	            'type' => 'checkbox',
    	        'param_name' => 'hide_on_sm',
        	    'edit_field_class' => 'vc_column vc_col-sm-6 no-heading',
            	'value' => array( __( 'Small Devices', 'dt-core' ) => 'yes' ),
            	'group' => __('Settings','dt-core'),
            	'save_always' => true,
        	),

        	array(
            	'type' => 'checkbox',
            	'edit_field_class' => 'vc_column vc_col-sm-6 no-heading',
            	'param_name' => 'hide_on_xs',
            	'value' => array( __( 'Very Small Devices', 'dt-core' ) => 'yes' ),
            	'group' => __('Settings','dt-core'),
            	'save_always' => true,
        	)        
        # Settings
    )
) );