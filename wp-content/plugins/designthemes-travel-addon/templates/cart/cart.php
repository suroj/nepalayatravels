<?php
if ( !defined( 'ABSPATH' ) ) {
	exit();
}
?>
<?php if ( array_key_exists('dt_room_cart_items', $_SESSION) && !empty( $_SESSION['dt_room_cart_items'] ) ) : ?>

	<?php
	// Update Quantity...
	if( isset($_REQUEST['btndupdate'] ) && $_REQUEST['btndupdate'] == 'update' ) :

		if( isset($_REQUEST['room_addon_qty_cart']) ):
			$addon_ids = $_REQUEST['room_addon_qty_cart'];
			foreach( $addon_ids as $k => $v ):
				foreach( $v as $key => $qty  ):
					if( $qty == 0 )
						unset($_SESSION['dt_room_cart_items'][$k]['addon'][$key]);
					else
						$_SESSION['dt_room_cart_items'][$k]['addon'][$key]['qty'] = $qty;
				endforeach;
			endforeach;
		endif;

		$room_ids = $_REQUEST['room_qty_cart'];
		foreach( $room_ids as $k => $v ):
			if( $v == 0 )
				unset($_SESSION['dt_room_cart_items'][$k]);
			else
				$_SESSION['dt_room_cart_items'][$k]['qty'] = $v;
		endforeach;

		array_filter($_SESSION['dt_room_cart_items']);

		echo '<div class="dt-sc-success-box">'.esc_html__('Cart updated successfully.', 'designthemes-travel').'</div>';
	endif; ?>

    <div id="room-booking-cart">

        <form name="dt-cart-form" id="dt-cart-form" method="post">
            <h3><?php echo ($title); ?></h3>
            <?php $subtotal = 0; ?>
            <table class="dt_cart_table">
                <thead>
                <th>&nbsp;</th>
                <th class="dt_room_name"><?php esc_html_e( 'Room', 'designthemes-travel' ); ?></th>
                <th class="dt_capacity"><?php esc_html_e( 'Capacity', 'designthemes-travel' ); ?></th>
                <th class="dt_check_in"><?php esc_html_e( 'Check - in', 'designthemes-travel' ); ?></th>
                <th class="dt_check_out"><?php esc_html_e( 'Check - out', 'designthemes-travel' ); ?></th>
                <th class="dt_night"><?php esc_html_e( 'Night', 'designthemes-travel' ); ?></th>                
                <th class="dt_quantity"><?php esc_html_e( 'Quantity', 'designthemes-travel' ); ?></th>
                <th class="dt_gross_total"><?php esc_html_e( 'Gross Total', 'designthemes-travel' ); ?></th>
                </thead>
				<?php if ( $rooms = $_SESSION['dt_room_cart_items'] ): ?>

					<?php foreach ( $rooms as $cart_id => $room ): ?>
						<?php
							$room_meta = get_post_meta( $cart_id, '_room_settings', TRUE );
							$room_meta = is_array( $room_meta ) ? $room_meta : array();
			
							$term_meta = get_term_meta( $room_meta['room-adults'], '_room_capacity_options', false );
							$capacity = !empty($term_meta[0]['room_capacity']) ? $term_meta[0]['room_capacity'] : '';

							$cart_addon = array_key_exists('addon', $room) ? count($room['addon']) : '';
						?>
                        <tr class="dt_checkout_item" data-cart-id="<?php echo esc_attr( $cart_id ); ?>">
                            <td<?php echo $cart_addon ? ' rowspan="' . ( count( $room['addon'] ) + 1 ) . '"' : '' ?>>
                                <a href="javascript:void(0)" class="dt_remove_cart_item" data-type="line_item" data-cart-id="<?php echo esc_attr( $cart_id ); ?>"><i class="fa fa-times"></i></a>
                            </td>
                            <td class="dt_room_name">
                                <a href="<?php echo get_permalink( $cart_id ); ?>"><?php echo get_the_title( $cart_id ); ?></a>
                            </td>
                            <td class="dt_capacity"><?php echo sprintf( _n( '%d adult', '%d adults', $capacity, 'designthemes-travel' ), $capacity ); ?> </td>
                            <td class="dt_check_in"><?php echo date( 'F d, Y', strtotime( $room['checkin_date'] ) ); ?></td>
                            <td class="dt_check_out"><?php echo date( 'F d, Y', strtotime( $room['checkout_date'] ) ); ?></td>
                            <td class="dt_night"><?php echo $room['nights']; ?></td>
                            <td class="dt_quantity">
                                <input type="number" min="0" class="dt_room_qty_edit" name="room_qty_cart[<?php echo esc_attr( $cart_id ) ?>]" value="<?php echo esc_attr( $room['qty'] ); ?>" />
                            </td>
                            <td class="dt_gross_total">
								<?php $total = $room['price'] * $room['qty']; $subtotal += $total; echo dt_travel_get_formatted_price($total); ?>
                            </td>
                        </tr><?php
						if( $cart_addon != '' ):
							foreach( $room['addon'] as $k => $v ): ?>
								<tr>
									<td>
										<a href="javascript:void(0)" class="dt_remove_cart_item" data-type="sub_item" data-cart-id="<?php echo esc_attr( $cart_id ); ?>" data-item-id="<?php echo esc_attr( $k ); ?>"><i class="fa fa-times"></i></a>
									</td>
									<td class="dt_room_name" colspan="4">
										<?php $term = get_term( $v['term_id'], 'dt_services' ); echo ($term->name); ?>
									</td>
									<td class="dt_quantity">
										<input type="number" min="0" class="dt_room_addon_qty_edit" name="room_addon_qty_cart[<?php echo esc_attr( $cart_id ) ?>][<?php echo esc_attr( $k ) ?>]" value="<?php echo esc_attr( $v['qty'] ); ?>" />
									</td>
									<td class="dt_gross_total">
										<?php $total = $v['price'] * $v['qty']; $subtotal += $total; echo dt_travel_get_formatted_price($total); ?>
									</td>
								</tr><?php
							endforeach;
						endif; ?>
					<?php endforeach; ?>

				<?php endif; ?>

                <tr class="dt_sub_total">
                    <td colspan="7"><?php esc_html_e( 'Sub Total', 'designthemes-travel' ); ?></td>
                    <td><span class="dt-align-right dt_sub_total_value"><?php echo dt_travel_get_formatted_price( $subtotal ); ?></span></td>
                </tr>

				<?php $tax_amount = ''; ?>
				<?php if ( cs_get_option('include-tax') && $tax = cs_get_option('tax-percentage') ) : ?>
                    <tr class="dt_advance_tax">
                        <td colspan="7">
							<?php _e( 'Tax', 'designthemes-travel' ); ?>
							<?php if ( $tax < 0 ) { ?>
                                <span><?php printf( esc_html__( '(price including tax)', 'designthemes-travel' ) ); ?></span>
							<?php } else { ?>
								<span>(<?php echo ($tax); ?> %)</span>
							<?php } ?>
                        </td>
                        <td>
                            <span class="dt-align-right"><?php $tax_amount = ($subtotal) * ($tax) / 100; echo dt_travel_get_formatted_price( $tax_amount ); ?></span>
                        </td>
                    </tr>
				<?php endif; ?>

                <tr class="dt_advance_grand_total">
                    <td colspan="7">
						<?php esc_html_e( 'Grand Total', 'designthemes-travel' ); ?>
                    </td>
                    <td>
                        <span class="dt-align-right dt_grand_total_value"><?php echo dt_travel_get_formatted_price( ($subtotal) + ($tax_amount) ); ?></span>
                    </td>
                </tr>

				<?php $advance_payment = ''; ?>
				<?php if ( cs_get_option('enable-advance') && $advance_percent = cs_get_option('advance-percentage') ) : ?>
                    <tr class="dt_advance_payment">
                        <td colspan="7">
							<?php printf( esc_html__( 'Advance Payment (%s%% of Grand Total)', 'designthemes-travel' ), $advance_percent ); ?>
                        </td>
                        <td>
                            <span class="dt-align-right"><?php $advance_payment = (($subtotal) + ($tax_amount)) * ($advance_percent) / 100; echo dt_travel_get_formatted_price( $advance_payment ); ?></span>
                        </td>
                    </tr>
				<?php endif; ?>

            </table>
            <p>
                <a href="<?php echo dt_travel_checkout_url() ?>" class="dt-sc-button rounded-corner filled medium"><?php esc_html_e( 'Check Out', 'designthemes-travel' ); ?></a>
                <button name="btndupdate" type="submit" value="<?php esc_attr_e('update', 'designthemes-travel'); ?>" class="dt-sc-button filled medium update"><?php esc_html_e( 'Update', 'designthemes-travel' ); ?></button>
            </p>
        </form>
        <div class="dt-sc-hr-invisible-medium"></div>
    </div>

<?php else: ?>
    <!--Empty cart-->
    <div class="dt-sc-warning-box"><?php esc_html_e( 'Your cart is empty!', 'designthemes-travel' ) ?></div>
    <div class="dt-sc-hr-invisible-medium"></div>
<?php endif; ?>