<?php add_action( 'vc_before_init', 'dt_sc_rooms_checkout_vc_map' );
function dt_sc_rooms_checkout_vc_map() {

	vc_map( array(
		"name" => esc_html__( "Checkout", 'designthemes-travel' ),
		"base" => "dt_sc_rooms_checkout",
		"icon" => "dt_sc_rooms_checkout",
		"category" => DTTRAVEL_VC_CATEGORY,
		"description" => esc_html__("Show rooms checkout.",'designthemes-travel')
	) );
}?>