<?php add_action( 'vc_before_init', 'dt_sc_rooms_list_vc_map' );
function dt_sc_rooms_list_vc_map() {

	vc_map( array(
		"name" => esc_html__( "Rooms", 'designthemes-travel' ),
		"base" => "dt_sc_rooms_list",
		"icon" => "dt_sc_rooms_list",
		"category" => DTTRAVEL_VC_CATEGORY,
		"description" => esc_html__("Show list of rooms.",'designthemes-travel'),
		"params" => array(

			// Post Count
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Post Counts', 'designthemes-travel' ),
				'param_name' => 'count',
				'description' => esc_html__( 'Enter post count', 'designthemes-travel' ),
				'admin_label' => true
			),

			// Post column
			array(
				'type' => 'dropdown',
				'heading' => esc_html__('Columns','designthemes-travel'),
				'param_name' => 'column',
				'value' => array(
					esc_html__('II Columns','designthemes-travel') => 'one-half-column' ,
					esc_html__('III Columns','designthemes-travel') => 'one-third-column',
					esc_html__('IV Columns','designthemes-travel') => 'one-fourth-column'
				),
				'std' => 'one-third-column'
			),

			// Taxonomy Room Type
			array(
				'type' => 'autocomplete',
				'heading' => __( 'Terms', 'designthemes-travel' ),
				'param_name' => 'terms',
				'settings' => array(
					'multiple' => true,
					'min_length' => 1,
					'groups' => true,
					'unique_values' => true,
					'display_inline' => true,
					'delay' => 500,
					'auto_focus' => true,
				),
				'param_holder_class' => 'vc_not-for-custom',
				'description' => __( 'Enter room type & pick.', 'designthemes-travel' )
			)
		)
	) );
}?>