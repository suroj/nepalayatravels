<?php
/**
 * Room loop content
 *
 * @author  designthemes
 * @package designthemes-travel-addon/templates
 * @version 1.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post; ?>
<div class="content"><?php echo maharaj_room_info($post->ID, 12); ?></div>