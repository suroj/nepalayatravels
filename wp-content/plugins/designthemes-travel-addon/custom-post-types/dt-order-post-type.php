<?php
if (! class_exists ( 'DTOrderPostType' )) {
	class DTOrderPostType {

		function __construct() {
			// Add Hook into the 'init()' action
			add_action ( 'init', array (
					$this,
					'dt_init'
			) );

			// Add Hook into the 'admin_init()' action
			add_action ( 'admin_init', array (
					$this,
					'dt_admin_init'
			) );

			// Add Hook into the 'admin_enqueue_scripts' filter
			add_action( 'admin_enqueue_scripts', array (
					$this,
					'dt_travel_admin_scripts'
			) );

			// Add Hook into the 'cs_metabox_options' filter
			add_filter ( 'cs_metabox_options', array (
					$this,
					'dt_orders_cs_metabox_options'
			) );
		}

		/**
		 * A function hook that the WordPress core launches at 'init' points
		 */
		function dt_init() {
			$this->createPostType ();

			add_action ( 'save_post', array (
					$this,
					'dt_orders_save_post_meta'
			) );
		}

		/**
		 * A function hook that the WordPress core launches at 'admin_init' points
		 */
		function dt_admin_init() {
			add_filter ( "manage_edit-dt_orders_columns", array (
					$this,
					"dt_orders_edit_columns" 
			) );

			add_action ( "manage_posts_custom_column", array (
					$this,
					"dt_orders_columns_display" 
			), 10, 2 );

			add_filter( 'gettext', array (
					$this,
					'dt_travel_publish_button'
			), 10, 2 );

			add_action ( 'add_meta_boxes', array (
					$this,
					'dt_travel_add_order_meta_box'
			) );

			add_action( 'admin_head-edit.php', array (
					$this,
					'dt_travel_edit_post_change_title_in_list'
			) );
		}

		/**
		 * UI dialog admin scripts
		 */
		function dt_travel_admin_scripts( $hook ) {

			if( $hook == "post.php" || $hook == 'post-new.php' ) {

				wp_enqueue_script( 'jquery-ui-dialog' );
				wp_enqueue_style( 'wp-jquery-ui-dialog' );

				wp_enqueue_script('dt-travel-order-scripts', plugins_url('designthemes-travel-addon') . '/custom-post-types/js/order_scripts.js', array(), false, true );
			}

			if( $hook == "edit.php" ) {
				wp_enqueue_style ( 'dt-travel-admin', plugins_url ('designthemes-travel-addon') . '/custom-post-types/css/admin_styles.css', array (), false, 'all' );
			}
		}

		/**
		 * Creating a post type
		 */
		function createPostType() {

			$labels = array (
				'name' 				 => esc_html__( 'Orders', 'designthemes-travel' ),
				'all_items' 		 => esc_html__( 'All Orders', 'designthemes-travel' ),
				'singular_name' 	 => esc_html__( 'Order', 'designthemes-travel' ),
				'add_new' 			 => esc_html__( 'Add New', 'designthemes-travel' ),
				'add_new_item' 		 => esc_html__( 'Add New Order', 'designthemes-travel' ),
				'edit_item' 		 => esc_html__( 'Edit Order', 'designthemes-travel' ),
				'new_item' 			 => esc_html__( 'New Order', 'designthemes-travel' ),
				'view_item' 		 => esc_html__( 'View Order', 'designthemes-travel' ),
				'search_items' 		 => esc_html__( 'Search Orders', 'designthemes-travel' ),
				'not_found' 		 => esc_html__( 'No Orders found', 'designthemes-travel' ),
				'not_found_in_trash' => esc_html__( 'No Orders found in Trash', 'designthemes-travel' ),
				'parent_item_colon'  => esc_html__( 'Parent Order:', 'designthemes-travel' ),
				'menu_name' 		 => esc_html__( 'Orders', 'designthemes-travel' ) ,
			);

			$args = array (
				'labels' 				=> $labels,
				'hierarchical' 			=> false,
				'supports' 				=> array(''),
				'public' 				=> false,
				'show_ui' 				=> true,
				'show_in_menu' 			=> 'edit.php?post_type=dt_rooms',
				'show_in_admin_bar'  	=> true,
				'map_meta_cap'       	=> true,
				'menu_position' 		=> 5,
				'menu_icon' 			=> 'dashicons-admin-home',
				'show_in_nav_menus' 	=> false,
				'publicly_queryable' 	=> false,
				'query_var' 			=> true,
				'capability_type' 		=> 'post'
			);

			register_post_type ( 'dt_orders', $args );
		}

		/**
		 * Change publish, update button text
		 */
		function dt_travel_publish_button( $translation, $text ) {

			if ( 'dt_orders' == get_post_type() )
				if ( $text == 'Publish' )
					return esc_html__('Save Order', 'designthemes-travel');
				elseif ( $text == 'Update' )
					return esc_html__('Update Order', 'designthemes-travel');

			return $translation;
		}

		/**
		 * Add order items meta box
		 */
		function dt_travel_add_order_meta_box() {
			add_meta_box ( 'dt-travel-order-items-metabox', esc_html__( 'Item Details', 'designthemes-travel' ), array (
			    $this,
			   'dt_travel_order_items_metabox'
			), 'dt_orders', 'normal', 'low' );
		}

		/**
		 * Include order item meta box
		 */
		function dt_travel_order_items_metabox() {
			include_once plugin_dir_path ( __FILE__ ) . 'metaboxes/dt_travel_order_items_metabox.php';
		}

		/**
		 *
		 * @param unknown $columns
		 * @return multitype:
		 */
		function dt_orders_edit_columns($columns) {
			unset( $columns['author'] );
			unset( $columns['date'] );
			$columns['customer']       = esc_html__( 'Customer', 'designthemes-travel' );
			$columns['booking_date']   = esc_html__( 'Date', 'designthemes-travel' );
			$columns['check_in_date']  = esc_html__( 'Check in', 'designthemes-travel' );
			$columns['check_out_date'] = esc_html__( 'Check out', 'designthemes-travel' );
			$columns['total']          = esc_html__( 'Total', 'designthemes-travel' );
			$columns['title']          = esc_html__( 'Booking Order', 'designthemes-travel' );		
			$columns['status']         = esc_html__( 'Status', 'designthemes-travel' );

			return $columns;
		}

		/**
		 *
		 * @param unknown $columns
		 * @param unknown $id
		 */
		function dt_orders_columns_display($columns, $post_id) {

			$order_meta = get_post_meta ( $post_id, '_order_settings', TRUE );
			$order_meta = is_array ( $order_meta ) ? $order_meta : array ();

			switch ($columns) {

				case 'order_id' :
					echo '#'.$post_id;
				break;

				case 'customer' :
					if( array_key_exists('first_name', $order_meta))
						echo $order_meta['first_name'];

					if( array_key_exists('last_name', $order_meta))
						echo ' '.$order_meta['last_name'];

					if( array_key_exists('customer', $order_meta) && $order_meta['customer'] != ''):
						$user_info = get_userdata($order_meta['customer']);

						echo '<br><a href="'.get_edit_user_link($order_meta['customer']).'" title="'.$user_info->user_login.'" target="_blank">'.$user_info->user_login.'</a>';
					endif;
				break;

				case 'booking_date' :
					echo date( 'F d, Y', strtotime( get_post_field( 'post_date', $post_id ) ) );
				break;

				case 'check_in_date' :
					echo date( 'F d, Y', strtotime( dt_travel_get_order_checkin_date( $post_id, 'checkin' ) ) );
				break;

				case 'check_out_date' :
					echo date( 'F d, Y', strtotime( dt_travel_get_order_checkin_date( $post_id, 'checkout' ) ) );
				break;

				case 'total' :
					$symbol	   = dt_travel_get_currency_symbol();
					$totamount = dt_travel_get_order_total( $post_id );
					echo $symbol.$totamount;

					if( array_key_exists('payment_method', $order_meta) && $order_meta['payment_method'] == 'offline-payment' ):
						echo '<br>'.esc_html__('(Pay on arrival)', 'designthemes-travel');
					else:
						echo '<br>'.esc_html__('(Pay with paypal)', 'designthemes-travel');
					endif;

					if( array_key_exists('advance_percent', $order_meta) && $order_meta['advance_percent'] != '' ){
						$adper = $order_meta['advance_percent'];
						$adpay = $totamount * $adper / 100;
						echo '<br><span style="color:#dd6464">('.esc_html__('Charged ', 'designthemes-travel').$adper.'% = '.$symbol.$adpay.')</span>';
					}
				break;

				case 'status' :
					if( array_key_exists('booking_status', $order_meta)) :
						$class = $order_meta['booking_status'];
						echo '<span class="'.$class.'">'.ucfirst($class).'</span>';
					endif;
				break;
			}
		}

		function dt_travel_edit_post_change_title_in_list() {
			add_filter( 'the_title', array(
				$this,
				'dt_travel_edit_post_new_title_in_list'), 100, 2 );
		}

		function dt_travel_edit_post_new_title_in_list( $title, $post_id ) {
			global $post_type;
			if ( $post_type == 'dt_orders' ) {
				$title = '#'.$post_id;

			}

			return $title;
		}

		/**
		 * Orders metabox options
		 */
		function dt_orders_cs_metabox_options( $options ) {

			$symbol = dt_travel_get_currency_symbol();
			$order_no = isset( $_REQUEST['post'] ) ? $_REQUEST['post'] : '';

			$order_date = '';

			if( !empty( $order_no ) ) {
			 	$order_date = get_the_date( 'Y-m-d H:i:s', $order_no );
			} else {
				$order_date = date('Y-m-d H:i:s');
			}

			$options[]    = array(
			  'id'        => '_order_settings',
			  'title'     => esc_html__('Order Options', 'designthemes-travel'),
			  'post_type' => 'dt_orders',
			  'context'   => 'normal',
			  'priority'  => 'default',
			  'sections'  => array(

				array(
				  'name'  => 'general_section',
				  'title' => esc_html__('General Options', 'designthemes-travel'),
				  'icon'  => 'fa fa-cogs',
				  'fields' => array(

					array(
					  'type'    => 'notice',
					  'class'   => 'warning',
					  'content' => esc_html__('Book ID #', 'designthemes-travel').$order_no
					),

					array(
					  'type'    => 'subheading',
					  'content' => esc_html__('Booked on ', 'designthemes-travel').$order_date
					),
					
					array(
					  'id'                => 'payment_method',
					  'type'              => 'select',
					  'title'             => esc_html('Payment Method', 'designthemes-travel'),
					  'options'           => array(
						'offline-payment' => esc_html__('Offline Payment (Pay on Arrival)', 'designthemes-travel'),
						'paypal'          => esc_html__('PayPal (Pay with PayPal)', 'designthemes-travel')
					  ),
					  'class'             => 'chosen',
					  'default'    		  => 'offline-payment',
					),

					array(
					  'id'           => 'booking_status',
					  'type'         => 'select',
					  'title'        => esc_html__('Booking Status', 'designthemes-travel'),
					  'options'      => array(
						'processing' => esc_html__('Processing', 'designthemes-travel'),
						'completed'  => esc_html__('Completed', 'designthemes-travel'),
						'cancelled'  => esc_html__('Cancelled', 'designthemes-travel'),
						'pending'    => esc_html__('Pending', 'designthemes-travel')
					  ),
					  'class'        => 'chosen',
					  'default'    	 => 'processing',
					),

					array(
					  'id'      => 'tax_percent',
					  'type'    => 'text',
					  'title'   => esc_html__('Tax Percentage', 'designthemes-travel'),
					  'default'	=> cs_get_option('tax-percentage'),
					  'after'	=> '<span class="cs-text-desc">&nbsp;'.esc_html__('%', 'designthemes-travel').'</span>',
					),

					array(
					  'id'                => 'customer',
					  'type'              => 'select',
					  'title'             => esc_html__('Customer', 'designthemes-travel'),
					  'options'           => dt_travel_get_users(),
					  'class'         => 'chosen'
					),

				  ), // end: fields
				), // end: a section

				array(
				  'name'  => 'customer_section',
				  'title' => esc_html__("Customer's Details", 'designthemes-travel'),
				  'icon'  => 'fa fa-user',
				  'fields' => array(

					array(
					  'id'      => 'first_name',
					  'type'    => 'text',
					  'title'   => esc_html__('First Name', 'designthemes-travel'),
					  'attributes'    => array(
						'placeholder' => esc_html('First Name', 'designthemes-travel')
					  )
					),

					array(
					  'id'      => 'last_name',
					  'type'    => 'text',
					  'title'   => esc_html__('Last Name', 'designthemes-travel'),
					  'attributes'    => array(
						'placeholder' => esc_html('Last Name', 'designthemes-travel')
					  )
					),

					array(
					  'id'      => 'address',
					  'type'    => 'text',
					  'title'   => esc_html__('Address', 'designthemes-travel'),
					  'attributes'    => array(
						'placeholder' => esc_html('Address', 'designthemes-travel')
					  )
					),

					array(
					  'id'      => 'city',
					  'type'    => 'text',
					  'title'   => esc_html__('City', 'designthemes-travel'),
					  'attributes'    => array(
						'placeholder' => esc_html('City', 'designthemes-travel')
					  )
					),

					array(
					  'id'      => 'state',
					  'type'    => 'text',
					  'title'   => esc_html__('State', 'designthemes-travel'),
					  'attributes'    => array(
						'placeholder' => esc_html('State', 'designthemes-travel')
					  )
					),

					array(
					  'id'      => 'postal_code',
					  'type'    => 'text',
					  'title'   => esc_html__('Postal Code', 'designthemes-travel'),
					  'attributes'    => array(
						'placeholder' => esc_html('Postal Code', 'designthemes-travel')
					  )
					),

					array(
					  'id'      => 'email_address',
					  'type'    => 'text',
					  'title'   => esc_html__('Email Address', 'designthemes-travel'),
					  'attributes'    => array(
						'placeholder' => esc_html('Email Address', 'designthemes-travel')
					  )
					),

					array(
					  'id'      => 'fax_no',
					  'type'    => 'text',
					  'title'   => esc_html__('Fax', 'designthemes-travel'),
					  'attributes'    => array(
						'placeholder' => esc_html('Fax', 'designthemes-travel')
					  )
					),

					array(
					  'id'      => 'phone_no',
					  'type'    => 'text',
					  'title'   => esc_html__('Phone', 'designthemes-travel'),
					  'attributes'    => array(
						'placeholder' => esc_html('Phone', 'designthemes-travel')
					  )
					),

					array(
					  'id'      => 'country_name',
					  'type'    => 'select',
					  'title'   => esc_html__('Country', 'designthemes-travel'),
					  'options' => dt_travel_countries(),
					  'class'   => 'chosen'
					),

					array(
					  'id'      => 'notes',
					  'type'    => 'textarea',
					  'title'   => esc_html__("Customer's Notes", 'designthemes-travel')
					),

				  ), // end: fields
				), // end: a section
			  ),
		   );
		   
		   return $options;
		}

		function dt_orders_save_post_meta($post_id) {

			if( key_exists ( '_inline_edit',$_POST )) :
				if ( wp_verify_nonce($_POST['_inline_edit'], 'inlineeditnonce')) return;
			endif;

			if (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) return;

			if (!current_user_can('edit_post', $post_id)) :
				return;
			endif;

			if ( (key_exists('post_type', $_POST)) && ('dt_orders' == $_POST['post_type']) ) :

				$settings = array ();
				$user_id = isset( $_POST['_order_settings']['customer'] ) ? $_POST['_order_settings']['customer'] : '';

				if( $user_id != '' ):
					$settings ['first_name'] = $_POST['_order_settings']['first_name'];
					$settings ['last_name'] = $_POST['_order_settings']['last_name'];
					$settings ['address'] = $_POST['_order_settings']['address'];
					$settings ['city'] = $_POST['_order_settings']['city'];
					$settings ['state'] = $_POST['_order_settings']['state'];
					$settings ['postal_code'] = $_POST['_order_settings']['postal_code'];
					$settings ['email_address'] = $_POST['_order_settings']['email_address'];
					$settings ['fax_no'] = $_POST['_order_settings']['fax_no'];
					$settings ['phone_no'] = $_POST['_order_settings']['phone_no'];
					$settings ['country_name'] = $_POST['_order_settings']['country_name'];

					update_user_meta ( $user_id, '_dt_travel_user_fields', array_filter ( $settings ) );
				endif;

				$meta = get_post_meta( $post_id, '_order_settings', TRUE );

				$new_status = $_POST['_order_settings']['booking_status'];
				$old_status = $meta['booking_status'];

				if ( $new_status !== $old_status ) :
					do_action( 'dt_travel_order_status_changed', $post_id, $old_status, $new_status );
				endif;

			endif;
		}
	}
}
?>