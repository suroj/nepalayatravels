(function ($) {

    function isInteger(a) {
        return Number(a) || ( a % 1 === 0 );
    }

    function isEmail(email) {
		return new RegExp('^[-!#$%&\'*+\\./0-9=?A-Z^_`a-z{|}~]+@[-!#$%&\'*+\\/0-9=?A-Z^_`a-z{|}~]+\.[-!#$%&\'*+\\./0-9=?A-Z^_`a-z{|}~]+$').test(email);
    }
	
	// Room search checkin & checkout starts...
	$("body").delegate(".dt_input_date_check","focus",function(event){

		var today = new Date();
        var tomorrow = new Date();

        var start_plus = $(document).triggerHandler('dt_travel_min_check_in_date', [1, today, tomorrow]);
        start_plus = parseInt(start_plus);
        if (!isInteger(start_plus)) {
            start_plus = 1;
        }

        tomorrow.setDate(today.getDate() + start_plus);

		var $This = $(this);
		var $a = $This.attr('name');

		if( $a == 'checkout_date' ){
			$This.datepicker({
				dateFormat     : 'yy-mm-dd',
				minDate		   : tomorrow,
				maxDate		   : '+365D',
				numberOfMonths : 1,
				onSelect       : function () {
					var unique = $(this).attr('id');
					unique = unique.replace('chkout_date_', '');
					var check_in = $('#chkin_date_' + unique),
						selected = $(this).datepicker('getDate');

					var check_in_range_check_out = dt_travel_msgs.min_book_days;
					if (!isInteger(check_in_range_check_out)) {
						check_in_range_check_out = 1;
					}

					selected.setDate(selected.getDate() - check_in_range_check_out);

					check_in.datepicker('option', 'maxDate', selected);
				}
			});
		} else {
			$This.datepicker({
				dateFormat     : 'yy-mm-dd',
				minDate		   : today,
				maxDate		   : '+365D',
				numberOfMonths : 1,
				onSelect       : function () {
					var unique = $This.attr('id');
					unique = unique.replace('chkin_date_', '');
					var date = $This.datepicker('getDate');

					var check_in_range_check_out = dt_travel_msgs.min_book_days;
					if (!isInteger(check_in_range_check_out)) {
						check_in_range_check_out = 1;
					}

					if (date) {
						date.setDate(date.getDate() + check_in_range_check_out);
					}

					var checkout = $('#chkout_date_' + unique);
					checkout.datepicker('option', 'minDate', date);
				}
			});
		}
	}); // Room search checkin & checkout ends...

	// Price breakdown starts...
	$("body").delegate(".dt-view-booking-room-details","click",function(event){

		var $This = $(this);
		$This.next('.dt-booking-room-details').toggleClass('active');

		event.preventDefault();
		return false;
	});

	$("body").delegate(".dt_room_item_detail_price_close","click",function(event){

		var $This = $(this);
		$This.parent('.dt-booking-room-details').toggleClass('active');

		event.preventDefault();
		return false;
	}); // Price breakdown ends...

	// Addon services starts
	$(document).on('change', '.dt_room_quantity select[name="cmbqty"]', function (e) {
		e.preventDefault();

		var _self = $(this),
			_form = _self.parents('.dt-select-room-item'),
			_exta_area = _form.find('.dt_addon_package_extra'),
			_toggle = _exta_area.find('.dt_addon_services'),
			_val = _self.val();

		if (_val !== '') {
			_form.parent().siblings().find('.dt_addon_services').removeClass('active').slideUp();
			_toggle.removeAttr('style').addClass('active');
			_exta_area.removeAttr('style').slideDown();
		}
		else {
			_exta_area.slideUp();
			_val = 1;
		}

		_form.find('.dt_optional_quantity').val(_val);

	});

	$(document).on('click', '.dt_addon_service_toggle', function (e) {
		e.preventDefault();

		var _self = $(this),
			parent = _self.parents('.dt_addon_package_extra');
			toggle = parent.find('.dt_addon_services');

		_self.toggleClass('active');
		toggle.toggleClass('active');

		if (toggle.hasClass('active'))
			toggle.slideDown();
		else
			toggle.slideUp();

	}); // Addon services ends

	// Room add to cart starts
	$(document).on('click', '.dt_room_add_to_cart .dt_add_to_cart', function (e) {
		e.preventDefault();

		var _self = $(this),
			_form = _self.parents('.dt-select-room-item'),
			_qty = _form.find('select[name="cmbqty"]').val();

		if( _qty != '' ) {

			var _room_id = _form.find('input[name="room-id"]').val(),
				_checkin = _form.find('input[name="check_in_date"]').val(),
				_checkout = _form.find('input[name="check_out_date"]').val();
			var _addons = [];

			_form.find('input.dt_optional_quantity_selected').each(function(index, element) {
				var _chk = $(this);
                if (_chk.is(':checked')) {
					var ele_name = _chk.attr('name');
					var _t_key = ele_name.substring(30, ele_name.length - 1);
					var _t_qty = _form.find('input[name="'+ele_name.substr(0, 20)+'['+_t_key+']"]').val();

					_addons.push({id:_t_key, qty:_t_qty});
				}
            });

			var _addonstr = '';
			$.each(_addons,function(i, value){
				_addonstr += value.id + ':' + value.qty + ',';
			});
			// console.log(_addonstr);

			$.ajax({
				url       : dttheme_urls.ajaxurl,
				type      : 'POST',
				data      : {
					action   : 'dt_travel_add_cart_room_item',
					room_id  : _room_id,
					chkin    : _checkin,
					chkout   : _checkout,
					quantity : _qty,
					addons   : _addonstr
				},
				beforeSend: function () {
					_self.append('<i class="fa fa-spinner fa-spin"></i>');
				}
			}).done(function (res) {
				_self.find('.fa').remove();
				if (res.status === true) {
	
					try {
						_form.find('select[name="cmbqty"]').val('');
						_form.find('.dt_addon_services').removeAttr('style').removeClass('active');
						_form.find('.dt_addon_package_extra').slideUp();

						_self.after('<div class="dt-sc-success-box">'+dt_travel_msgs.addroom_success+'</div>');
						var timeOut = setTimeout(function () {
							_self.next('.dt-sc-success-box').fadeOut('slow');
						}, 3000);

					} catch (error) {
						console.debug(error);
					}

				} else if (res.status === false) {
					confirm(res.message);
				}
	
			}).fail(function () {
				_self.find('.fa').remove();
			});
		} else {
			_form.find('select[name="cmbqty"]').effect('pulsate', { times:3 }, 500);
		}
	}); // Room add to cart ends

	// Cart item delete starts
	$('#dt-cart-form tbody a.dt_remove_cart_item').click(function(event) {
		var _self = $(this);

		if(confirm('Are you sure wants to delete?')) {
			$.ajax({
				url       : dttheme_urls.ajaxurl,
				type      : 'POST',
				data      : {
					action   	: 'dt_travel_delete_cart_item',
					cart_id 	: _self.attr('data-cart-id'),
					item_type	: _self.attr('data-type'),
					item_id  	: _self.attr('data-item-id')
				},
				beforeSend: function () {
					_self.append('<i class="fa fa-spinner fa-spin"></i>');
				}
			}).done(function (res) {
				_self.find('.fa-spinner').remove();
				if (res.status === true) {

					try {
						window.location.reload(true);
					} catch (error) {
						console.debug(error);
					}

				} else if (res.status === false) {
					confirm(res.message);
				}

			}).fail(function () {
				_self.find('.fa-spinner').remove();
			});
		}

		event.preventDefault();
		return false;
	}); // Cart item delete ends

	$("#dt-payment-form").validate({
		rules: {
			first_name: "required",
			last_name: "required",
			address: {
				required: true,
				minlength: 2
			},
			city: "required",
			state: "required",
			postal_code: "required",
			country_name: "required",
			phone_no: "required",
			email_address: {
				required: true,
				email: true
			},
			dt_payment_method: "required",
			tos: "required"
		},
		messages: {
			first_name: '',
			last_name: '',
			address: '',
			city: '',
			state: '',
			postal_code: '',
			country_name: '',
			phone_no: '',
			email_address: '',
			dt_payment_method: dt_travel_msgs.payment_msg,
			tos: dt_travel_msgs.policy_msg
		},
	});

	// Checkout payment starts...
	$('#dt-payment-form').submit(function () {

		var _This = $(this);
		var _self = _This.find('button.checkout');

		if(_This.valid()) {
			var _action = _This.find('input[name="action"]').val();

			$.ajax({
				url       : dttheme_urls.ajaxurl,
				type      : 'POST',
				data      : {
					action   	: _action,
					data		: _This.serialize()
				},
				beforeSend: function () {
					_self.append('<i class="fa fa-spinner fa-spin"></i>');
				}
			}).done(function (res) {
				_self.find('.fa-spinner').remove();
				if (res.status === true) {

					try {
                        if (res.result == 'success') {
                            if (res.redirect != undefined) {
                                window.location.href = res.redirect;
                            }
                        } else if (typeof res.message !== 'undefined') {
                            alert(res.message);
                        }
					} catch (error) {
						console.debug(error);
					}

				} else if (res.status === false) {
					confirm(res.message);
				}

			}).fail(function () {
				_self.find('.fa-spinner').remove();
			});
        }
        return false;
    }); // Checkout payment ends...

	// Get existing use info starts...
	$('#dt-payment-form button#fetch-customer-info').click(function(event) {

        var $button = $(this),
            $email = $('input[name="existing-customer-email"]');
        if (!isEmail($email.val())) {
            $email.addClass('error');
            $email.focus();
            return;
        }
        $button.attr('disabled', true);
        $email.attr('disabled', true);
        var customer_table = $('.dt-col-padding.dt-col-border');
        $.ajax({
            url: dttheme_urls.ajaxurl,
            dataType: 'html',
            type: 'post',
            data: {
                action: 'dt_travel_fetch_customer_info',
                email: $email.val()
            },
            beforeSend: function () {
                customer_table.dt_overlay_ajax_start();
            },
            success: function (res) {
                customer_table.dt_overlay_ajax_stop();
                res = $.parseJSON(res);

                if (res.data && res.data.ID) {
                    var $container = $('#dt-order-new-customer');
                    for (var key in res.data) {
                        var $field = $container.find('input[name="' + key + '"], select[name="' + key + '"], textarea[name="' + key + '"]');
                        $field.val(res.data[key]);
                    }
                    $container.find('input[name="existing-customer-id"]').val(res.data.ID);
                    $('.dt-order-existing-customer').fadeOut();
                } else {
					customer_table.find('h4.dt-existing-title').after(res.message);
					var timeOut = setTimeout(function () {
						customer_table.find('.dt-sc-error-box').fadeOut('slow');
					}, 2000);
                }
                $button.removeAttr('disabled');
                $email.removeAttr('disabled');
            },
            error: function () {
                customer_table.dt_overlay_ajax_stop();
				alert('Error on loading ajax...');
                $button.removeAttr('disabled');
                $email.removeAttr('disabled');
            }
        });
	}); // Get existing use info ends

    // overlay before ajax
    $.fn.dt_overlay_ajax_start = function () {
        var _self = this;
        _self.css({
            'position': 'relative',
            'overflow': 'hidden'
        });
        var overlay = '<div class="dt_overlay_ajax">';
        overlay += '</div>';

        _self.append(overlay);
    }

    $.fn.dt_overlay_ajax_stop = function () {
        var _self = this;
        var overlay = _self.find('.dt_overlay_ajax');

        overlay.addClass('hide');
        var timeOut = setTimeout(function () {
            overlay.remove();
            clearTimeout(timeOut);
        }, 400);
    }
	
    // Animate Post Images
    $(".dt_rooms .entry-summary img.animate").each(function(){
      $(this).one('inview', function (event, visible) {
          if(visible) {
              $(this).addClass($(this).attr('data-animate'));
          }
      });
    });

    // rating single room
    $.fn.rating = function () {
        var ratings = this,
            legnth = this.length;

        for (var i = 0; i < legnth; i++) {
            var rating = $(ratings[i]),
                html = [];

            html.push('<span class="rating-input" data-rating="1"></span>');
            html.push('<span class="rating-input" data-rating="2"></span>');
            html.push('<span class="rating-input" data-rating="3"></span>');
            html.push('<span class="rating-input" data-rating="4"></span>');
            html.push('<span class="rating-input" data-rating="5"></span>');
            html.push('<input name="rating" id="rating" type="hidden" value="" />');
            rating.html(html.join(''));

            rating.mousemove(function (e) {
                e.preventDefault();
                var parentOffset = ratings.offset(),
                    relX = e.pageX - parentOffset.left,
                    star = $(this).find('.rating-input'),
                    star_width = star.width(),
                    rate = Math.ceil(relX / star_width);

                for (var y = 0; y < star.length; y++) {
                    var st = $(star[y]),
                        _data_star = parseInt(st.attr('data-rating'));
                    if (_data_star <= rate) {
                        st.addClass('high-light');
                    }
                }
            }).mouseout(function (e) {
                var parentOffset = ratings.offset(),
                    relX = e.pageX - parentOffset.left,
                    star = $(this).find('.rating-input'),
                    star_width = star.width(),
                    rate = $(this).find('.rating-input.selected');

                if (rate.length === 0) {
                    star.removeClass('high-light');
                } else {
                    for (var y = 0; y < star.length; y++) {
                        var st = $(star[y]),
                            _data_star = parseInt(st.attr('data-rating'));

                        if (_data_star <= parseInt(rate.attr('data-rating'))) {
                            st.addClass('high-light');
                        } else {
                            st.removeClass('high-light');
                        }
                    }
                }
            }).mousedown(function (e) {
                var parentOffset = ratings.offset(),
                    relX = e.pageX - parentOffset.left,
                    star = $(this).find('.rating-input'),
                    star_width = star.width(),
                    rate = Math.ceil(relX / star_width);
                star.removeClass('selected').removeClass('high-light');
                for (var y = 0; y < star.length; y++) {
                    var st = $(star[y]),
                        _data_star = parseInt(st.attr('data-rating'));
                    if (_data_star === rate) {
                        st.addClass('selected').addClass('high-light');
                        break;
                    } else {
                        st.addClass('high-light');
                    }
                }
                rating.find('input[name="rating"]').val(rate);
            });

        }
    }

	$('.dt-rating-input').rating();

})(jQuery);