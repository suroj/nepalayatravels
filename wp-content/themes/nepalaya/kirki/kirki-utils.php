<?php
function maharaj_kirki_config() {
	return 'maharaj_kirki_config';
}

function maharaj_defaults( $key = '' ) {
	$defaults = array();

	# site identify
	$defaults['use-custom-logo'] = '1';
	$defaults['custom-logo'] = MAHARAJ_THEME_URI.'/images/logo.png';
	$defaults['custom-light-logo'] = MAHARAJ_THEME_URI.'/images/light-logo.png';
	$defaults['site_icon'] = MAHARAJ_THEME_URI.'/images/favicon.ico';

	# site layout
	$defaults['site-layout'] = 'wide';

	# site skin
	$defaults['use-predefined-skin'] = '0';
	$defaults['predefined-skin'] = 'blue';
	$defaults['primary-color'] = '#00b9ff';
	$defaults['secondary-color'] = '#8659d3';
	$defaults['tertiary-color'] = '#47189a';

	# site breadcrumb
	$defaults['customize-breadcrumb-title-typo'] = '0';
	$defaults['breadcrumb-title-typo'] = array( 'font-family' => 'Montserrat',
		'variant' => '700',
		'subsets' => array( 'latin-ext' ),
		'font-size' => '30px',
		'line-height' => '',
		'letter-spacing' => '0px',
		'color' => '#ffffff',
		'text-align' => 'unset',
		'text-transform' => 'none' );
	$defaults['customize-breadcrumb-typo'] = '0';
	$defaults['breadcrumb-typo'] = array( 'font-family' => 'Poppins',
		'variant' => '500',
		'subsets' => array( 'latin-ext' ),
		'font-size' => '13px',
		'line-height' => '',
		'letter-spacing' => '0',
		'color' => '#ffffff',
		'text-align' => 'unset',
		'text-transform' => 'none' );

	# site footer
	$defaults['customize-footer-title-typo'] = '1';
	$defaults['footer-title-typo'] = array( 'font-family' => 'Open Sans',
		'variant' => '700',
		'subsets' => array( 'latin-ext' ),
		'font-size' => '20px',
		'line-height' => '36px',
		'letter-spacing' => '0',
		'color' => '#2B2B2B',
		'text-align' => 'left',
		'text-transform' => 'none' );
	$defaults['customize-footer-content-typo'] = '1';
	$defaults['footer-content-typo'] = array( 'font-family' => 'Open Sans',
		'variant' => 'regular',
		'subsets' => array( 'latin-ext' ),
		'font-size' => '14px',
		'line-height' => '24px',
		'letter-spacing' => '0',
		'color' => '#333333',
		'text-align' => 'left',
		'text-transform' => 'none' );

	# site typography
	$defaults['customize-body-h1-typo'] = '1';
	$defaults['h1'] = array(
		'font-family' => 'Montserrat',
		'variant' => '700',
		'font-size' => '38px',
		'line-height' => '',
		'letter-spacing' => '0px',
		'color' => '#000000',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-h2-typo'] = '1';
	$defaults['h2'] = array(
		'font-family' => 'Montserrat',
		'variant' => '700',
		'font-size' => '34px',
		'line-height' => '',
		'letter-spacing' => '0px',
		'color' => '#000000',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-h3-typo'] = '1';
	$defaults['h3'] = array(
		'font-family' => 'Montserrat',
		'variant' => '700',
		'font-size' => '26px',
		'line-height' => '',
		'letter-spacing' => '0px',
		'color' => '#000000',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-h4-typo'] = '1';
	$defaults['h4'] = array(
		'font-family' => 'Montserrat',
		'variant' => '700',
		'font-size' => '20px',
		'line-height' => '',
		'letter-spacing' => '0px',
		'color' => '#000000',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-h5-typo'] = '1';
	$defaults['h5'] = array(
		'font-family' => 'Montserrat',
		'variant' => '700',
		'font-size' => '18px',
		'line-height' => '',
		'letter-spacing' => '0px',
		'color' => '#000000',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-h6-typo'] = '1';
	$defaults['h6'] = array(
		'font-family' => 'Montserrat',
		'variant' => '700',
		'font-size' => '16px',
		'line-height' => '',
		'letter-spacing' => '0px',
		'color' => '#000000',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);
	$defaults['customize-body-content-typo'] = '1';
	$defaults['body-content-typo'] = array(
		'font-family' => 'Poppins',
		'variant' => 'normal',
		'font-size' => '16px',
		'line-height' => '28px',
		'letter-spacing' => '',
		'color' => '#797979',
		'text-align' => 'unset',
		'text-transform' => 'none'
	);

	if( !empty( $key ) && array_key_exists( $key, $defaults) ) {
		return $defaults[$key];
	}

	return '';
}

function maharaj_image_positions() {

	$positions = array( "top left" => esc_attr__('Top Left','maharaj'),
		"top center"    => esc_attr__('Top Center','maharaj'),
		"top right"     => esc_attr__('Top Right','maharaj'),
		"center left"   => esc_attr__('Center Left','maharaj'),
		"center center" => esc_attr__('Center Center','maharaj'),
		"center right"  => esc_attr__('Center Right','maharaj'),
		"bottom left"   => esc_attr__('Bottom Left','maharaj'),
		"bottom center" => esc_attr__('Bottom Center','maharaj'),
		"bottom right"  => esc_attr__('Bottom Right','maharaj'),
	);

	return $positions;
}

function maharaj_image_repeats() {

	$image_repeats = array( "repeat" => esc_attr__('Repeat','maharaj'),
		"repeat-x"  => esc_attr__('Repeat in X-axis','maharaj'),
		"repeat-y"  => esc_attr__('Repeat in Y-axis','maharaj'),
		"no-repeat" => esc_attr__('No Repeat','maharaj')
	);

	return $image_repeats;
}

function maharaj_border_styles() {

	$image_repeats = array(
		"none"	 => esc_attr__('None','maharaj'),
		"dotted" => esc_attr__('Dotted','maharaj'),
		"dashed" => esc_attr__('Dashed','maharaj'),
		"solid"	 => esc_attr__('Solid','maharaj'),
		"double" => esc_attr__('Double','maharaj'),
		"groove" => esc_attr__('Groove','maharaj'),
		"ridge"	 => esc_attr__('Ridge','maharaj'),
	);

	return $image_repeats;
}

function maharaj_animations() {

	$animations = array(
		'' 					 => esc_html__('Default','maharaj'),	
		"bigEntrance"        =>  esc_attr__("bigEntrance",'maharaj'),
        "bounce"             =>  esc_attr__("bounce",'maharaj'),
        "bounceIn"           =>  esc_attr__("bounceIn",'maharaj'),
        "bounceInDown"       =>  esc_attr__("bounceInDown",'maharaj'),
        "bounceInLeft"       =>  esc_attr__("bounceInLeft",'maharaj'),
        "bounceInRight"      =>  esc_attr__("bounceInRight",'maharaj'),
        "bounceInUp"         =>  esc_attr__("bounceInUp",'maharaj'),
        "bounceOut"          =>  esc_attr__("bounceOut",'maharaj'),
        "bounceOutDown"      =>  esc_attr__("bounceOutDown",'maharaj'),
        "bounceOutLeft"      =>  esc_attr__("bounceOutLeft",'maharaj'),
        "bounceOutRight"     =>  esc_attr__("bounceOutRight",'maharaj'),
        "bounceOutUp"        =>  esc_attr__("bounceOutUp",'maharaj'),
        "expandOpen"         =>  esc_attr__("expandOpen",'maharaj'),
        "expandUp"           =>  esc_attr__("expandUp",'maharaj'),
        "fadeIn"             =>  esc_attr__("fadeIn",'maharaj'),
        "fadeInDown"         =>  esc_attr__("fadeInDown",'maharaj'),
        "fadeInDownBig"      =>  esc_attr__("fadeInDownBig",'maharaj'),
        "fadeInLeft"         =>  esc_attr__("fadeInLeft",'maharaj'),
        "fadeInLeftBig"      =>  esc_attr__("fadeInLeftBig",'maharaj'),
        "fadeInRight"        =>  esc_attr__("fadeInRight",'maharaj'),
        "fadeInRightBig"     =>  esc_attr__("fadeInRightBig",'maharaj'),
        "fadeInUp"           =>  esc_attr__("fadeInUp",'maharaj'),
        "fadeInUpBig"        =>  esc_attr__("fadeInUpBig",'maharaj'),
        "fadeOut"            =>  esc_attr__("fadeOut",'maharaj'),
        "fadeOutDownBig"     =>  esc_attr__("fadeOutDownBig",'maharaj'),
        "fadeOutLeft"        =>  esc_attr__("fadeOutLeft",'maharaj'),
        "fadeOutLeftBig"     =>  esc_attr__("fadeOutLeftBig",'maharaj'),
        "fadeOutRight"       =>  esc_attr__("fadeOutRight",'maharaj'),
        "fadeOutUp"          =>  esc_attr__("fadeOutUp",'maharaj'),
        "fadeOutUpBig"       =>  esc_attr__("fadeOutUpBig",'maharaj'),
        "flash"              =>  esc_attr__("flash",'maharaj'),
        "flip"               =>  esc_attr__("flip",'maharaj'),
        "flipInX"            =>  esc_attr__("flipInX",'maharaj'),
        "flipInY"            =>  esc_attr__("flipInY",'maharaj'),
        "flipOutX"           =>  esc_attr__("flipOutX",'maharaj'),
        "flipOutY"           =>  esc_attr__("flipOutY",'maharaj'),
        "floating"           =>  esc_attr__("floating",'maharaj'),
        "hatch"              =>  esc_attr__("hatch",'maharaj'),
        "hinge"              =>  esc_attr__("hinge",'maharaj'),
        "lightSpeedIn"       =>  esc_attr__("lightSpeedIn",'maharaj'),
        "lightSpeedOut"      =>  esc_attr__("lightSpeedOut",'maharaj'),
        "pullDown"           =>  esc_attr__("pullDown",'maharaj'),
        "pullUp"             =>  esc_attr__("pullUp",'maharaj'),
        "pulse"              =>  esc_attr__("pulse",'maharaj'),
        "rollIn"             =>  esc_attr__("rollIn",'maharaj'),
        "rollOut"            =>  esc_attr__("rollOut",'maharaj'),
        "rotateIn"           =>  esc_attr__("rotateIn",'maharaj'),
        "rotateInDownLeft"   =>  esc_attr__("rotateInDownLeft",'maharaj'),
        "rotateInDownRight"  =>  esc_attr__("rotateInDownRight",'maharaj'),
        "rotateInUpLeft"     =>  esc_attr__("rotateInUpLeft",'maharaj'),
        "rotateInUpRight"    =>  esc_attr__("rotateInUpRight",'maharaj'),
        "rotateOut"          =>  esc_attr__("rotateOut",'maharaj'),
        "rotateOutDownRight" =>  esc_attr__("rotateOutDownRight",'maharaj'),
        "rotateOutUpLeft"    =>  esc_attr__("rotateOutUpLeft",'maharaj'),
        "rotateOutUpRight"   =>  esc_attr__("rotateOutUpRight",'maharaj'),
        "shake"              =>  esc_attr__("shake",'maharaj'),
        "slideDown"          =>  esc_attr__("slideDown",'maharaj'),
        "slideExpandUp"      =>  esc_attr__("slideExpandUp",'maharaj'),
        "slideLeft"          =>  esc_attr__("slideLeft",'maharaj'),
        "slideRight"         =>  esc_attr__("slideRight",'maharaj'),
        "slideUp"            =>  esc_attr__("slideUp",'maharaj'),
        "stretchLeft"        =>  esc_attr__("stretchLeft",'maharaj'),
        "stretchRight"       =>  esc_attr__("stretchRight",'maharaj'),
        "swing"              =>  esc_attr__("swing",'maharaj'),
        "tada"               =>  esc_attr__("tada",'maharaj'),
        "tossing"            =>  esc_attr__("tossing",'maharaj'),
        "wobble"             =>  esc_attr__("wobble",'maharaj'),
        "fadeOutDown"        =>  esc_attr__("fadeOutDown",'maharaj'),
        "fadeOutRightBig"    =>  esc_attr__("fadeOutRightBig",'maharaj'),
        "rotateOutDownLeft"  =>  esc_attr__("rotateOutDownLeft",'maharaj')
    );

	return $animations;
}