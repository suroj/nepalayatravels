<?php add_action( 'vc_before_init', 'dt_sc_rooms_search_form_vc_map' );
function dt_sc_rooms_search_form_vc_map() {

	vc_map( array(
		"name" => esc_html__( "Rooms Search Form", 'designthemes-travel' ),
		"base" => "dt_sc_rooms_search_form",
		"icon" => "dt_sc_rooms_search_form",
		"category" => DTTRAVEL_VC_CATEGORY,
		"description" => esc_html__("Show rooms search form.",'designthemes-travel'),
		"params" => array(

			// Title
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Title', 'designthemes-travel' ),
				'param_name' => 'title',
				'description' => esc_html__( 'Enter the title of search form. ex: Search your room', 'designthemes-travel' )
			),

			// Checkin
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Checkin Field Text', 'designthemes-travel' ),
				'param_name' => 'checkin',
				'description' => esc_html__( 'Enter the text of checkin field. ex: Checkin Date', 'designthemes-travel' )
			),

			// Checkout
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Checkout Field Text', 'designthemes-travel' ),
				'param_name' => 'checkout',
				'description' => esc_html__( 'Enter the text of checkout field. ex: Checkout Date', 'designthemes-travel' )
			),

			// Adults
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Adults Field Text', 'designthemes-travel' ),
				'param_name' => 'adults',
				'description' => esc_html__( 'Enter the text of adults field. ex: Adults', 'designthemes-travel' )
			),

			// Children
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Childs Field Text', 'designthemes-travel' ),
				'param_name' => 'childs',
				'description' => esc_html__( 'Enter the text of childs field. ex: Children', 'designthemes-travel' )
			),

			// Submit Button
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Submit Button Text', 'designthemes-travel' ),
				'param_name' => 'btn_text',
				'description' => esc_html__( 'Enter the text of submit button field. ex: Check Availability', 'designthemes-travel' )
			),
			
			// Search Page URL
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Search Page URL', 'designthemes-travel' ),
				'param_name' => 'search_page',
				'description' => esc_html__( 'Enter the url of search page.', 'designthemes-travel' )
			),

			// Class
      		array(
      			"type" => "textfield",
      			"heading" => esc_html__( "Extra class name", 'designthemes-travel' ),
      			"param_name" => "class",
      			'description' => esc_html__('Style particular icon box element differently - add a class name and refer to it in custom CSS','designthemes-travel')
      		)
		)
	) );
}?>