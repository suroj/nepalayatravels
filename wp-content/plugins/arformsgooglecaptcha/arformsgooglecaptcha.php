<?php
/*
    Plugin Name: Google reCaptcha Add-On For ARForms
    Description: Extension for ARForms plugin to provide google captcha facility. ( This plugin will work with ARForms plugin only. )
    Version: 1.0
    Plugin URI: http://www.arformsplugin.com/
    Author: Repute InfoSystems
    Author URI: http://reputeinfosystems.com/
    Text Domain: ARForms-Google-Captcha
 */

if(!defined('ARF_GGL_CAPT_DIR')){
    define('ARF_GGL_CAPT_DIR', WP_PLUGIN_DIR . '/arformsgooglecaptcha');
}

global $arfsiteurl;
$arfsiteurl = home_url();
if (is_ssl() and ( !preg_match('/^https:\/\/.*\..*$/', $arfsiteurl) or ! preg_match('/^https:\/\/.*\..*$/', WP_PLUGIN_URL))) {
    $arfsiteurl = str_replace('http://', 'https://', $arfsiteurl);
    define('ARF_GGL_CAPT_URL', str_replace('http://', 'https://', WP_PLUGIN_URL . '/arformsgooglecaptcha'));
} else{
    define('ARF_GGL_CAPT_URL', WP_PLUGIN_URL . '/arformsgooglecaptcha');
}

if(!defined('ARF_GC_TEXT_DOMAIN')){
    define('ARF_GC_TEXT_DOMAIN', 'ARF_Google_Captcha');
}

if(!defined('ARF_GC_PATH')){
    define('ARF_GC_PATH', WP_PLUGIN_DIR . '/arformsgooglecaptcha');
}

if(!defined('ARF_GC_CORE')){
    define('ARF_GC_CORE', ARF_GC_PATH . '/core');
}

if(!defined('ARF_GC_SLUG')){
    define('ARF_GC_SLUG', 'captcha');
}

load_plugin_textdomain(ARF_GC_TEXT_DOMAIN, false, 'arformsgooglecaptcha/languages/');

global $arf_google_captcha, $arf_new_captcha_field_data, $arf_captcha_field_class_name, $arf_captcha_field_image_path;
$arf_google_captcha = new ARF_Google_Captcha();

$arfgc_memory_limit = 256;
$memory_limit = ini_get("memory_limit");
if (isset($memory_limit)) {
    $memory_limit = str_replace('M', '', $memory_limit);
}

global $arf_ggl_capt_version;
$arf_ggl_capt_version = '1.0';

global $arformsgooglecaptchashortname;
$arformsgooglecaptchashortname = 'ARFGGLCAPT';

$arfajaxurl = admin_url('admin-ajax.php');
$arf_captcha_field_image_path = array(ARF_GC_SLUG => ARF_GGL_CAPT_URL . '/images/signature-icon.png');
$arf_captcha_field_class_name = array(ARF_GC_SLUG => 'purple');
$arf_new_captcha_field_data = array(ARF_GC_SLUG => __('CAPTCHA', ARF_GC_TEXT_DOMAIN));

class ARF_Google_Captcha {

    function __construct() {

        global $gc_arforms_version_compatible;
        $gc_arforms_version_compatible = $this->gc_arforms_version_compatible();

        register_activation_hook(__FILE__, array('ARF_Google_Captcha', 'install'));

        register_uninstall_hook(__FILE__, array('ARF_Google_Captcha', 'uninstall'));

        add_action('admin_notices', array(&$this, 'arf_ggl_capt_admin_notices'));

        if($gc_arforms_version_compatible) {
            add_action('arfafterbasicfieldlisting', array(&$this, 'arf_add_google_captcha_field'), 10, 2);
            
            add_filter('form_fields', array(&$this, 'add_captcha_field_to_frontend'), 11, 11);
        }

        add_filter('arf_new_field_array_filter_outside', array(&$this, 'arf_add_captcha_field_array_filter_outside'), 10, 4);

        add_filter('arf_new_field_array_materialize_filter_outside', array(&$this, 'arf_add_captcha_field_array_materialize_filter_outside'), 10, 4);

        add_filter('arf_change_json_default_data_ouside', array(&$this, 'arf_new_field_json_filter_outside'), 10, 1);

        add_filter('arfavailablefieldsbasicoptions', array(&$this, 'add_availablefieldsbasicoptions'), 10, 3);

    }

    function add_captcha_field_to_frontend($return_string, $form, $field_name, $arf_data_uniq_id, $field, $field_tootip, $field_description,$res_data,$inputStyle,$arf_main_label,$get_onchage_func_data){

        if ($field['type'] != ARF_GC_SLUG) {
            return $return_string;
        }

        global $arfsettings, $arfversion, $arf_form_all_footer_js;

        if ($field['type'] == ARF_GC_SLUG) {

            if($arfsettings->pubkey !='' && $arfsettings->privkey !=''){

                if (apply_filters('arf_check_for_draw_outside', false, $field)) {

                    /* arf_dev_flag => action to filter conversion */
                    $return_string = apply_filters('arf_drawthisfieldfromoutside', $return_string, $field,$arf_on_change_function,$arf_data_uniq_id);
                } else {

                    $error_msg = null;

                    if (!empty($errors)) {

                        foreach ($errors as $error_key => $error) {

                            if (preg_match('/^captcha-/', $error_key))
                                $error_msg = preg_replace('/^captcha-/', '', $error_key);
                        }
                    }

                    if (!empty($arfsettings->pubkey)) {

                        $lang = apply_filters('arfrecaptchalang', $arfsettings->re_lang, $field);

                        if (defined('DOING_AJAX')) {


                            $arf_form_all_footer_js .= "Recaptcha.create('" . $arfsettings->pubkey . "','field_" . $field['field_key'] . "',{theme:'" . $arfsettings->re_theme . "',lang:'" . $lang . "'" . apply_filters('arfrecaptchacustom', '', $field) . "});";

                            $return_string .= '<div id="field_' . $field['field_key'] . '"></div>';
                        } else {
                            $data_size = "data-size='normal'";
                            $dsize = "normal";
                            if ($field['classes'] == 'arf_2' || $field['classes'] == 'arf_3') {
                                $data_size = "data-size='compact'";
                                $dsize = "compact";
                            }
                            if (wp_is_mobile()) {
                                $data_size = "data-size='compact'";
                                $dsize = "compact";
                            }

                            wp_enqueue_script('arf-google-recaptcha', 'https://www.google.com/recaptcha/api.js?hl=' . $lang . '&onload=render_arf_captcha&render=explicit', array(), $arfversion);

                            $arf_form_all_footer_js .='if( !window["arf_recaptcha"] ){
                                window["arf_recaptcha"] = {};
                            }
                            window["arf_recaptcha"]["arf_recaptcha_' . $field['field_key'] . '_'.$arf_data_uniq_id.'"] = {
                                size : "' . $dsize . '"
                            };';



                            $return_string .= '<div id="recaptcha_style">';
                                $return_string .= '<div id="arf_recaptcha_' . $field['field_key'] . '_'.$arf_data_uniq_id.'" class="arf_captcha_wrapper controls"></div>';
                                $return_string .= '<div class="help-block"></div>';
                            $return_string .= '</div>';
                        }
                    }

                    $return_string .= "<input type='hidden' id='arf_google_recaptcha_response' class='arf_required' data-validation-required-message='" . esc_attr($field['invalid']) . "' value='' /> ";

                    $return_string .= '<div><input type="hidden" name="field_captcha" data-type="' . $field['is_recaptcha'] . '" id="field_captcha" value="' . $field['id'] . '"></div>';
                }
            }
        }
        return $return_string;
    }

    function arf_add_captcha_field_array_filter_outside($fields, $field_icons, $json_data,$positioned_field_icons) {

        global $arfsettings;

        $captcha_theme = (isset($arfsettings->re_theme) && $arfsettings->re_theme != '') ? $arfsettings->re_theme : 'light';

        $notice_msg_recaptcha = '';
        if(empty($arfsettings->pubkey)){
            $notice_msg_recaptcha = '<div class="howto" id="setup_captcha_message" style="font-weight:bold;color:red;line-height:1;font-size:11px;">'.__("Please setup site key and private key in Global Settings otherwise recaptcha will not appear.",ARF_GC_TEXT_DOMAIN).'</div>';
        }

        $gcJsonFile = file_get_contents(ARF_GC_CORE . '/arf_google_captcha_field_data.json');
        $field_data_array = json_decode(stripslashes($gcJsonFile));

        $field_opt_arr = array(
            ARF_GC_SLUG => array(
                'invalidmessage' => 1,
                'labelname' => 2,
            ),
        );

        $field_order_captcha = isset($field_opt_arr[ARF_GC_SLUG]) ? $field_opt_arr[ARF_GC_SLUG] : '';
        $field_data_obj_captcha = $field_data_array->field_data->captcha;

        $captcha_html = array(
            "captcha" => "<div class='arf_inner_wrapper_sortable arfmainformfield edit_form_item arffieldbox ui-state-default 1  arf1columns single_column_wrapper' data-id='arf_editor_main_row_{arf_editor_index_row}'><div class='arf_multiiconbox'><div class='arf_field_option_multicolumn' id='arf_multicolumn_wrapper'><input type='hidden' name='multicolumn' />{$field_icons['multicolumn_one']} {$field_icons['multicolumn_two']} {$field_icons['multicolumn_three']} {$field_icons['multicolumn_four']} {$field_icons['multicolumn_five']} {$field_icons['multicolumn_six']}</div>{$field_icons['multicolumn_expand_icon']}</div><div class='sortable_inner_wrapper edit_field_type_captcha' inner_class='arf_1col' id='arfmainfieldid_{arf_field_id}'><div id='arf_field_{arf_field_id}' class='arfformfield control-group arfmainformfield top_container  arfformfield  arf_field_{arf_field_id}'><div class='fieldname-row' style='display : block;'><div class='fieldname'><label class='arf_main_label' id='field_{arf_field_id}'><span class='arfeditorfieldopt_label arf_edit_in_place'><input type='text' class='arf_edit_in_place_input inplace_field' data-ajax='false' data-field-opt-change='true' data-field-opt-key='name' value='Captcha' data-field-id='{arf_field_id}' /></span></label></div></div><div class='arf_fieldiconbox' data-field_id='{arf_field_id}'>{$positioned_field_icons[ARF_GC_SLUG]}</div><div class='controls'><img alt='' id='recaptcha_{arf_field_id}' src='".ARF_GGL_CAPT_URL."/images/".$captcha_theme."-captcha.png' class='captcha_class' style='max-width: 100%'/><div id='custom-captcha_{arf_field_id}' class='alignleft custom_captcha_div captcha_class' style='display:none;'></div><div style='clear:both'><div class='howto' style='clear:both;font-weight:bold;color:red;line-height:1;font-size:11px;'>".__('We are providing very powerful inbuilt captcha mechanism with ARForms. So you dont need to place captcha field. If you place captcha field in the form then it will be validate from both captcha mechanism.', ARF_GC_TEXT_DOMAIN)."</div></div>".$notice_msg_recaptcha."<div class='howto' id='setup_general_message' style='font-weight:bold;color:red;line-height:1;font-size:11px;margin-top: 5px;'></div><input type='hidden' name='item_meta[{arf_field_id}]' value='1' style='float: left;' id='field_{arf_unique_key}'><div class='arf_field_description' id='field_description_{arf_field_id}'></div><div class='help-block'></div></div><input type='hidden' class='arf_field_data_hidden' name='arf_field_data_{arf_field_id}' id='arf_field_data_{arf_field_id}' value='". htmlspecialchars(json_encode($field_data_obj_captcha))."' data-field_options='".json_encode($field_order_captcha)."' /><div class='arf_field_option_model arf_field_option_model_cloned' data-field_id='{arf_field_id}'><div class='arf_field_option_model_header'>Field Options</div><div class='arf_field_option_model_container'><div class='arf_field_option_content_row'></div></div><div class='arf_field_option_model_footer'><button type='button' class='arf_field_option_close_button' onClick='arf_close_field_option_popup({arf_field_id});'>Cancel</button><button type='button' class='arf_field_option_submit_button' data-field_id='{arf_field_id}'>OK</button></div></div></div></div></div>",
        );

        if ($this->gc_check_arforms_support()) {
            return $fields;
        }

        return array_merge($fields, $captcha_html);

    }

    function add_availablefieldsbasicoptions($basic_option) {

        $recaptcha_field_option = array(
            ARF_GC_SLUG => array(
                'invalidmessage' => 1,
                'labelname' => 2,
            )
        );

        if ($this->gc_check_arforms_support()) {
            return $basic_option;
        }
        return array_merge($basic_option, $recaptcha_field_option);
    }

    function arf_new_field_json_filter_outside($field_json) {
        $field_data = file_get_contents(ARF_GC_CORE . '/arf_google_captcha_field_data.json');
        $field_data_array = json_decode($field_data,true);
        $field_data_obj_capt = $field_data_array['field_data']['captcha'];

        if ($this->gc_check_arforms_support()) {
            return $field_json;
        }
        $field_json['field_data']['captcha'] = $field_data_obj_capt;
        return $field_json;
    }

    function arf_add_captcha_field_array_materialize_filter_outside($fields, $field_icons, $json_data, $positioned_field_icons){

        global $arfsettings;

        $captcha_theme = (isset($arfsettings->re_theme) && $arfsettings->re_theme != '') ? $arfsettings->re_theme : 'light';

        $notice_msg_recaptcha = '';
        if(empty($arfsettings->pubkey)){
            $notice_msg_recaptcha = '<div class="howto" id="setup_captcha_message" style="font-weight:bold;color:red;line-height:1;font-size:11px;">'.__("Please setup site key and private key in Global Settings otherwise recaptcha will not appear.",ARF_GC_TEXT_DOMAIN).'</div>';
        }

        $gcJsonFile = file_get_contents(ARF_GC_CORE . '/arf_google_captcha_field_data.json');
        $field_data_array = json_decode(stripslashes($gcJsonFile));

        $field_opt_arr = array(
            ARF_GC_SLUG => array(
                'invalidmessage' => 1,
                'labelname' => 2,
            ),
        );

        $field_order_captcha = isset($field_opt_arr[ARF_GC_SLUG]) ? $field_opt_arr[ARF_GC_SLUG] : '';
        $field_data_obj_captcha = $field_data_array->field_data->captcha;

        $captcha_html = array(
            "captcha" => "<div class='arf_inner_wrapper_sortable single_column_wrapper arfmainformfield edit_form_item arffieldbox ui-state-default 1  arf1columns' data-id='arf_editor_main_row_{arf_editor_index_row}'><div class='arf_multiiconbox'><div class='arf_field_option_multicolumn' id='arf_multicolumn_wrapper'><input type='hidden' name='multicolumn' />{$field_icons['multicolumn_one']} {$field_icons['multicolumn_two']} {$field_icons['multicolumn_three']} {$field_icons['multicolumn_four']} {$field_icons['multicolumn_five']} {$field_icons['multicolumn_six']}</div>{$field_icons['multicolumn_expand_icon']}</div><div class='sortable_inner_wrapper edit_field_type_captcha' inner_class='arf_1col' id='arfmainfieldid_{arf_field_id}' ><div id='arf_field_{arf_field_id}' class='arfformfield control-group arfmainformfield top_container  arfformfield  arf_field_{arf_field_id}'><div class='fieldname-row' style='display : block;'><div class='fieldname'><label class='arf_main_label' id='field_{arf_field_id}'><span class='arfeditorfieldopt_label arf_edit_in_place'><input type='text' class='arf_edit_in_place_input inplace_field' data-ajax='false' data-field-opt-change='true' data-field-opt-key='name' value='Captcha' data-field-id='{arf_field_id}' /></span></label></div></div><div class='arf_fieldiconbox' data-field_id='{arf_field_id}'>{$positioned_field_icons[ARF_GC_SLUG]}</div><div class='controls'><img alt='' id='recaptcha_{arf_field_id}' src='".ARF_GGL_CAPT_URL."/images/".$captcha_theme."-captcha.png' class='captcha_class'/><div id='custom-captcha_{arf_field_id}' class='alignleft custom_captcha_div captcha_class' style='display:none;'></div><div style='clear:both'><div class='howto' style='clear:both;font-weight:bold;color:red;line-height:1;font-size:11px;'>".__('We are providing very powerful inbuilt captcha mechanism with ARForms. So you dont need to place captcha field. If you place captcha field in the form then it will be validate from both captcha mechanism.', ARF_GC_TEXT_DOMAIN)."</div></div>".$notice_msg_recaptcha."<div class='howto' id='setup_general_message' style='font-weight:bold;color:red;line-height:1;font-size:11px;margin-top: 5px;'></div><input type='hidden' name='item_meta[{arf_field_id}]' value='1' style='float: left;' id='field_{arf_unique_key}'><div class='arf_field_description' id='field_description_{arf_field_id}'></div><div class='help-block'></div></div><input type='hidden' class='arf_field_data_hidden' name='arf_field_data_{arf_field_id}' id='arf_field_data_{arf_field_id}' value='". htmlspecialchars(json_encode($field_data_obj_captcha))."' data-field_options='".json_encode($field_order_captcha)."' /><div class='arf_field_option_model arf_field_option_model_cloned' data-field_id='{arf_field_id}'><div class='arf_field_option_model_header'>Field Options</div><div class='arf_field_option_model_container'><div class='arf_field_option_content_row'></div></div><div class='arf_field_option_model_footer'><button type='button' class='arf_field_option_close_button' onClick='arf_close_field_option_popup({arf_field_id});'>Cancel</button><button type='button' class='arf_field_option_submit_button' data-field_id='{arf_field_id}'>OK</button></div></div></div></div>",
        );

        if ($this->gc_check_arforms_support()) {
            return $fields;
        }

        return array_merge($fields, $captcha_html);

    }
    function arf_add_google_captcha_field($id = '', $is_ref_form = '', $values = ''){

        if ($this->gc_check_arforms_support()) {
            return;
        }

        global $arf_captcha_field_class_name, $arf_new_captcha_field_data, $arf_captcha_field_image_path;

        if (is_rtl()) {
            $floating_style = 'float:right;';
        } else {
            $floating_style = 'float:left;';
        }

        foreach ($arf_new_captcha_field_data as $field_key => $field_type) {
        ?>
            <li class="arf_form_element_item frmbutton frm_t<?php echo $field_key ?>" id="<?php echo $field_key; ?>" data-field-id="<?php echo $id; ?>" data-type="<?php echo $field_key; ?>">
                <div class="arf_form_element_item_inner_container">
                    <span class="arf_form_element_item_icon">
                        <svg viewBox="2 0 30 30"><g id="captcha"><path fill="#4E5462" fill-rule="evenodd" clip-rule="evenodd" d="M17.058,20.833c-9.73,0-15.569-10.069-15.569-10.069S7.328,0.85,17.058,0.85c9.731,0,15.569,9.975,15.569,9.975S26.79,20.833,17.058,20.833z M17.058,2.796c-8.06,0-12.768,8.028-12.768,8.028s4.708,7.979,12.768,7.979s12.935-7.979,12.935-7.979S25.118,2.796,17.058,2.796z M17.058,16.664c-3.224,0-5.839-2.614-5.839-5.839s2.615-5.838,5.839-5.838s5.839,2.613,5.839,5.838S20.282,16.664,17.058,16.664z M17.059,6.829c-2.207,0-3.997,1.79-3.997,3.997s1.79,3.996,3.997,3.996s3.996-1.789,3.996-3.996S19.266,6.829,17.059,6.829z"/></g></svg>
                    </span>
                    <label class="arf_form_element_item_text"><?php echo $field_type; ?></label>
                </div>
            </li>
        <?php
        }
    }

    function arf_ggl_capt_admin_notices() {

        if (!$this->is_arforms_support()){
            echo "<div class='updated'><p>" . __('Google reCaptcha add-on for ARForms requires ARForms plugin installed and active.', ARF_GC_TEXT_DOMAIN) . "</p></div>";
        } else if (!version_compare($this->get_arforms_version(), '3.0', '>=')){
            echo "<div class='updated'><p>" . __('Google reCaptcha add-on for ARForms requires ARForms installed with version 3.0 or higher.', ARF_GC_TEXT_DOMAIN) . "</p></div>";
        }
    }

    function is_arforms_support() {
        if (file_exists( ABSPATH . 'wp-admin/includes/plugin.php' )) {
            include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
        }
        return is_plugin_active('arforms/arforms.php');
    }

    public static function install() {
        $arf_db_google_captcha_version = get_option('arf_ggl_capt_version');
        if (!isset($arf_db_google_captcha_version) || $arf_db_google_captcha_version == '') {
            global $arf_ggl_capt_version;
            update_option('arf_ggl_capt_version', $arf_ggl_capt_version);
        }
    }

    public static function uninstall() {
        global $wpdb;
        if (is_multisite()) {
            $blogs = $wpdb->get_results("SELECT blog_id FROM {$wpdb->blogs}", ARRAY_A);
            if ($blogs) {
                foreach ($blogs as $blog) {
                    switch_to_blog($blog['blog_id']);
                    delete_option('arf_ggl_capt_version');
                }
                restore_current_blog();
            }
        } else {
            delete_option('arf_ggl_capt_version');
        }
    }

    function get_arforms_version() {

        $arf_db_version = get_option('arf_db_version');

        return (isset($arf_db_version)) ? $arf_db_version : 0;
    }

    function gc_check_arforms_support() {

        if (!version_compare($this->get_arforms_version(), '3.0', '>=')) {
            return true;
        } else {
            return false;
        }
    }

    function gc_arforms_version_compatible() {
        if (version_compare($this->get_arforms_version(), '3.0', '>='))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}