<?php
/*
 * @Author : designthemes
 * @Date   : 10/30/2017
 * @Last Modified by: designthemes
 * @Last Modified time: 10/30/2017
 */

if ( !defined( 'ABSPATH' ) ) {
	exit();
}

if ( !is_user_logged_in() ) {
	printf( __( 'You must <strong><a href="%s">Login<a/></strong>.', 'designthemes-travel' ), wp_login_url( dt_travel_account_url() ) );
	echo '<div class="dt-sc-hr-invisible-medium"></div>';
	return;
}

// list orders
dt_travel_get_template( 'account/bookings.php' );

// user info
dt_travel_get_template( 'account/user-info.php' );
