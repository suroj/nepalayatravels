<?php
vc_map( array(
    "name" => esc_html__( "Search Form", 'dt-core' ),
    "base" => "dt_sc_header_search_form",
    "icon" => "dt_sc_header_search_form",
    "category" => esc_html__( 'Header', 'dt-core' ),
    "params"    => array(

        # ID
        array(
            'type' => 'el_id',
            'param_name' => 'el_id',
            'edit_field_class' => 'hidden',
            'settings' => array (
                'auto_generate' => true
            )
        ),

        # Display Style
        array(
            'param_name' => 'style',
            'heading'    => esc_html__( 'Display Style', 'dt-core' ),
            'type'       => 'dropdown',
            'save_always' => true,
            'std' => 'simple',
            'value'      => array(
                __( 'Simple', 'dt-core' )  => 'simple',
                __( 'Full width', 'dt-core' )  => 'overlay',
                __( 'Slide Down', 'dt-core' )  => 'slide-down',
            ),
        ),
	  
	  array(
	  	'type' => 'textfield',
		'heading' => __( 'Extra class name', 'dt-core' ),
		'param_name' => 'el_class',
		'description' => __( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'dt-core' ),
	 ),	  
    )
) );