<?php
/**
 * Get web hooks
 * @return array
 */
if ( !function_exists( 'dt_travel_get_web_hooks' ) ) {
	function dt_travel_get_web_hooks() {
		$temp['paypal-standard'] = 'room-order-paypal-standard';
		$GLOBALS['dt-room-booking']['web_hooks'] = $temp;

		$web_hooks = empty( $GLOBALS['dt-room-booking']['web_hooks'] ) ? array() : (array) $GLOBALS['dt-room-booking']['web_hooks'];
		return ( $web_hooks );
	}
}

/**
 * Get web hook
 * @return string
 */
if ( !function_exists( 'dt_travel_get_web_hook' ) ) {
	function dt_travel_get_web_hook( $key ) {
		$web_hooks = dt_travel_get_web_hooks();
		$web_hook  = empty( $web_hooks[$key] ) ? false : $web_hooks[$key];
		return ( $web_hook );
	}
}

/**
 * Process web hooks
 * @calling method
 */
if ( !function_exists( 'dt_travel_process_web_hooks' ) ) {
	function dt_travel_process_web_hooks() {
		// Grab registered web_hooks
		$web_hooks           = dt_travel_get_web_hooks();
		$web_hooks_processed = false;
		// Loop through them and init callbacks

		foreach ( $web_hooks as $key => $param ) {
			if ( !empty( $_REQUEST[$param] ) ) {
				$web_hooks_processed           = true;
				$request_scheme                = is_ssl() ? 'https://' : 'http://';
				$requested_web_hook_url        = untrailingslashit( $request_scheme . $_SERVER['HTTP_HOST'] ) . $_SERVER['REQUEST_URI']; //REQUEST_URI includes the slash
				$parsed_requested_web_hook_url = parse_url( $requested_web_hook_url );
				$required_web_hook_url         = add_query_arg( $param, '1', trailingslashit( get_site_url() ) ); //add the slash to make sure we match
				$parsed_required_web_hook_url  = parse_url( $required_web_hook_url );
				$web_hook_diff                 = array_diff_assoc( $parsed_requested_web_hook_url, $parsed_required_web_hook_url );

				if ( empty( $web_hook_diff ) ) { //No differences in the requested webhook and the required webhook
					do_action( 'dt_travel_web_hook_' . $param, $_REQUEST );
				} else {

				}
				break; //we can stop processing here... no need to continue the foreach since we can only handle one webhook at a time
			}
		}
		if ( $web_hooks_processed ) {
			do_action( 'dt_travel_web_hooks_processed' );
			wp_die( __( 'Room Booking webhook process Complete', 'designthemes-travel' ), __( 'Room Booking webhook process Complete', 'designthemes-travel' ), array('response' => 200));
		}
	}
}
add_action( 'wp', 'dt_travel_process_web_hooks' );

/**
 * Get paypal checkout url
 * @param $order_id
 * @return string
 */
add_filter( 'dt_travel_paypal_basic_checkout_url', 'dt_travel_get_paypal_basic_checkout_url', 10, 2 );
function dt_travel_get_paypal_basic_checkout_url(  $order_id, $formdata ) {

	$paypal = array();
	$paypal['sandbox'] = cs_get_option( 'enable-sandbox' ) ? 'on' : 'off';
	$paypal['sandbox_account'] = cs_get_option( 'paypal-sandbox-account' ) ? cs_get_option( 'paypal-sandbox-account' ) : '';
	$paypal['live'] = cs_get_option( 'enable-paypal' ) ? 'on' : 'off';
	$paypal['paypal_account'] = cs_get_option( 'paypal-live-account' ) ? cs_get_option( 'paypal-live-account' ) : '';
	$paypal['paypal_url'] = 'https://www.paypal.com/cgi-bin/webscr';
	$paypal['sandbox_url'] = 'https://www.sandbox.paypal.com/cgi-bin/webscr';

	$paypal_args = array (
		'cmd'      => '_xclick',
		'amount'   => $formdata['pay_all'] ? $formdata['total_price'] : $formdata['total_advance'],
		'quantity' => '1',
	);

	$pay_all = $formdata['pay_all'];

	$paypal_email = $paypal['sandbox'] == 'on' ? $paypal['sandbox_account'] : $paypal['paypal_account'];

	$custom = array( 'order_id' => $order_id );

	if( cs_get_option('enable-advance') && cs_get_option('advance-percentage') && ! $pay_all ){
		$custom['advance_payment'] = cs_get_option('advance-percentage');
	}
	$query = array(
		'business'      => $paypal_email,
		'item_name'     => dt_travel_get_cart_description( $order_id ),
		'return'        => add_query_arg( array( 'dt-transaction-method' => 'paypal-standard' ), dt_travel_cart_url() . 'thank-you/' ),
		'currency_code' => cs_get_option( 'book-currency' ),
		'notify_url'    => get_site_url() . '/?' . dt_travel_get_web_hook( 'paypal-standard' ) . '=1',
		'no_note'       => '1',
		'shipping'      => '0',
		'email'         => $formdata['email_address'],
		'rm'            => '2',
		'cancel_return' => dt_travel_return_url(),
		'custom'        => json_encode( $custom ),
		'no_shipping'   => '1'
	);

	$query = array_merge( $paypal_args, $query );

	$paypal_payment_url = ( $paypal['sandbox'] == 'on' ? $paypal['sandbox_url'] : $paypal['paypal_url'] ) . '?' .  http_build_query( $query );

	return $paypal_payment_url;
}

/**
 * Web hook to process order with paypal IPN
 * @param $request
 */
add_action( 'dt_travel_web_hook_room-order-paypal-standard', 'dt_travel_web_hook_process_paypal_standard' );
function dt_travel_web_hook_process_paypal_standard( $request ){
	$payload = array_merge_recursive( array( 'cmd' => '_notify-validate' ), wp_unslash( $_POST ) );
	$paypal_api_url = ! empty( $_REQUEST['test_ipn'] ) ? 'https://www.sandbox.paypal.com/cgi-bin/webscr' : 'https://www.paypal.com/cgi-bin/webscr';

	$params = array(
		'body'        => $payload,
		'timeout'     => 60,
		'httpversion' => '1.1',
		'compress'    => false,
		'decompress'  => false,
		'user-agent'  => 'RoomBooking'
	);
	$response = wp_safe_remote_post( $paypal_api_url, $params );
	$body = wp_remote_retrieve_body( $response );

	if ( 'VERIFIED' === $body ) {
		if ( ! empty( $request['txn_type'] ) ) {

			switch ( $request['txn_type'] ) {
				case 'web_accept':
					if ( ! empty( $request['custom'] ) && ( $order_id = dt_travel_get_custom_request( $request['custom'] ) ) ) {
						$request['payment_status'] = strtolower( $request['payment_status'] );

						if ( isset( $request['test_ipn'] ) && 1 == $request['test_ipn'] && 'pending' == $request['payment_status'] ) {
							$request['payment_status'] = 'completed';
						}
						if ( method_exists( 'dt_travel_payment_status_' . $request['payment_status'] ) ) {
							call_user_func( 'dt_travel_payment_status_' . $request['payment_status'], $order_id, $request );
						}
					}
					break;

			}
		}
	}
}

/**
 * Get order id
 * @param $request['custom']
 */
function dt_travel_get_custom_request( $raw_custom ) {
	$raw_custom = stripslashes( $raw_custom );
	if ( ( $custom = json_decode( $raw_custom ) ) && is_object( $custom ) ) {
		$order_id  = $custom->order_id;
		
		return $order_id;
	}

	return false;
}

function dt_travel_payment_status_completed( $order_id, $request ) {
	// Order status is already completed
	$order_settings = get_post_meta ( $order_id, '_order_settings', TRUE );
	$order_settings = is_array ( $order_settings ) ? $order_settings : array ();
	if ( $order_settings['booking_status'] == 'completed' ) {
		exit;
	}

	if ( 'completed' === $request['payment_status'] ) {
		if( (float)dt_travel_get_order_total( $order_id ) === (float)$request['payment_gross'] ) {
			$order_settings['booking_status'] = 'completed';
			if( ! empty( $request['txn_id'] ) )
				$order_settings['txn_id'] = $request['txn_id'];

			update_post_meta( $order_id, '_order_settings', $order_settings );
		} else {
			$order_settings['booking_status'] = 'processing';
			update_post_meta( $order_id, '_order_settings', $order_settings );
		}
		// save paypal fee
		if ( ! empty( $request['mc_fee'] ) ) {
			$order_settings['txn_fee'] = $request['mc_fee'];
			update_post_meta( $order_id, '_order_settings', $order_settings );
		}

	} else {

	}

}

function dt_travel_payment_status_pending( $order_id, $request ) {
	dt_travel_payment_status_completed( $order_id, $request );
}