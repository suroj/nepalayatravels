<?php
/**
 * The Template for displaying all archive rooms
 *
 * Override this template by copying it to yourtheme/tp-room-booking/archive-room.php
 *
 * @author        designthemes
 * @package       designthemes-travel-addon/templates
 * @version       1.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header();
$global_breadcrumb = cs_get_option( 'show-breadcrumb' );
$wtstyle = cs_get_option( 'wtitle-style' ); ?>

<!-- ** Header Wrapper ** -->
<div id="header-wrapper">
    
	<!-- **Header** -->
	<header id="header">
		<div class="container"><?php

	        /**
	         * maharaj_header hook.
	         * 
	         * @hooked maharaj_vc_header_template - 10
	         *
	         */
	        do_action( 'maharaj_header' ); ?>
	    </div>
	</header><!-- **Header - End ** -->

	<!-- ** Breadcrumb ** -->
    <?php
        if( !empty( $global_breadcrumb ) ) {

        	$bstyle = cs_get_option( 'breadcrumb-style', 'default' );
        	$style = maharaj_breadcrumb_css();

            $title = get_the_archive_title();
            $breadcrumbs[] = __('Rooms','designthemes-travel');

			maharaj_breadcrumb_output ( '<h1>'.$title.'</h1>', 'dt-breadcrumb-for-archive '.$breadcrumbs, $bstyle, $style );
        }
    ?><!-- ** Breadcrumb End ** -->                
</div><!-- ** Header Wrapper - End ** -->

<!-- **Main** -->
<div id="main">
    <!-- ** Container ** -->
    <div class="container"><?php
    	$page_layout = cs_get_option( 'room-archives-page-layout' );
    	$page_layout = !empty( $page_layout ) ? $page_layout : "content-full-width";

    	$layout = maharaj_page_layout( $page_layout );
    	extract( $layout );

		$post_layout = cs_get_option( 'room-archives-post-layout' );
		switch($post_layout):
			case 'one-fourth-column':
				$post_class = " dt-room column dt-sc-one-fourth";
				$columns = 4;
			break;

			case 'one-third-column':
				$post_class = " dt-room column dt-sc-one-third";
				$columns = 3;
			break;

			default:
			case 'one-half-column':
				$post_class = " dt-room column dt-sc-one-half";
				$columns = 2;
			break;
		endswitch;

    	if ( $show_sidebar ) {
    		if ( $show_left_sidebar ) {?>
    		 	<!-- Secondary Left -->
                <section id="secondary-left" class="secondary-sidebar <?php echo esc_attr( $sidebar_class );?>"><?php
                    echo !empty( $wtstyle ) ? "<div class='{$wtstyle}'>" : '';

                    if( is_active_sidebar('custom-post-portfolio-archives-sidebar-left') ):
                        dynamic_sidebar('custom-post-portfolio-archives-sidebar-left');
                    endif;

                    $enable = cs_get_option( 'show-standard-left-sidebar-for-room-archives' );
                    if( $enable ):
                        if( is_active_sidebar('standard-sidebar-left') ):
                            dynamic_sidebar('standard-sidebar-left');
                        endif;
                    endif;

                    echo !empty( $wtstyle ) ? '</div>' : ''; ?>
    			</section><!-- Secondary Left End --><?php
    		}
    	}?>
    	<!-- Primary -->
        <section id="primary" class="<?php echo esc_attr( $page_layout );?>">
			<?php if ( have_posts() ) : ?>

            	<?php $i = 1; ?>

            	<div class="dt-rooms tp-room-booking">

					<?php while ( have_posts() ) : the_post(); ?>

                    	<?php
						$temp_class = '';
                        if($i == 1) $temp_class .= $post_class." first"; else $temp_class .= $post_class;
                        if($i == $columns) $i = 1; else $i = $i + 1;
						?>

                    	<div class="<?php echo esc_attr( trim($temp_class));?>">
	                        <?php dt_travel_get_template_part( 'content', 'room' ); ?>
                        </div>

                    <?php endwhile; ?>

                </div>

            <?php endif; ?>

			<!-- **Pagination** -->
            <div class="pagination blog-pagination">
            	<?php echo maharaj_pagination(); ?>
            </div><!-- **Pagination** -->

        </section><!-- Primary End --><?php

    	if ( $show_sidebar ) {
    		if ( $show_right_sidebar ) {?>
    		 	<!-- Secondary Right -->
                <section id="secondary-right" class="secondary-sidebar <?php echo esc_attr( $sidebar_class );?>"><?php
                    echo !empty( $wtstyle ) ? "<div class='{$wtstyle}'>" : '';

                    if( is_active_sidebar('custom-post-portfolio-archives-sidebar-right') ):
                        dynamic_sidebar('custom-post-portfolio-archives-sidebar-right');
                    endif;

                    $enable = cs_get_option( 'show-standard-right-sidebar-for-room-archives' );
                    if( $enable ):
                        if( is_active_sidebar('standard-sidebar-right') ):
                            dynamic_sidebar('standard-sidebar-right');
                        endif;
                    endif;

                    echo !empty( $wtstyle ) ? '</div>' : ''; ?>
    			</section><!-- Secondary Right End --><?php
    		}
    	}?>
    </div>
    <!-- ** Container End ** -->
</div><!-- **Main - End ** -->    

<?php get_footer(); ?>