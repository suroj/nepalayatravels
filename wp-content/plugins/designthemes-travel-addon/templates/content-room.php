<?php
/**
 * The template for displaying room content in the archive-room.php template
 *
 * Override this template by copying it to yourtheme/tp-room-booking/content-room.php
 *
 * @author        designthemes
 * @package       designthemes-travel-addon/templates
 * @version       1.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<div id="dt-room-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="summary entry-summary">

		<?php
		/**
		 * dt_travel_booking_loop_room_thumbnail hook
		 */
		do_action( 'dt_travel_booking_loop_room_thumbnail' );
		?>

		<div class="entry-left">
			<?php
            /**
             * dt_travel_booking_room_title hook
             */
            do_action( 'dt_travel_booking_loop_room_title' );
    
            /**
             * dt_travel_booking_loop_room_rating hook
             */
            do_action( 'dt_travel_booking_loop_room_rating' );
            ?>
        </div>
        
        <div class="entry-right">
			<?php
            /**
             * dt_travel_booking_loop_room_price hook
             */
            do_action( 'dt_travel_booking_loop_room_price' );
			?>
        </div>

    </div><!-- .summary -->

</div>