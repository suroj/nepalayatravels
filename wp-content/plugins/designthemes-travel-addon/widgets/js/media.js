jQuery.noConflict();
var dtblogAdmin = {
  init : function(){

    "use strict";
    dtblogAdmin.mediaUpload();

  },// init() End

  mediaUpload: function(){
    "use strict";

	jQuery( ".upload_image_button" ).live( "click", function(event) {
		event.preventDefault();

		var custom_file_frame = null;
		var item_clicked = jQuery(this);

		// Create the media frame.
		custom_file_frame = wp.media.frames.customHeader = wp.media({
			title: jQuery(this).data( "choose" ),
			button: {
				text: jQuery(this).data( "update" )
			}
		});

		custom_file_frame.on( "select", function() {
			var attachment = custom_file_frame.state().get( "selection" ).first();
			item_clicked.parent().find('.uploadfield').val(attachment.attributes.url).trigger('change');
			item_clicked.parent().find('img:last').attr('src', attachment.attributes.url);
		});

		custom_file_frame.open();
	});

	jQuery( ".upload_image_reset" ).click( function( event ) {
		event.preventDefault();
		
		var item_clicked = jQuery(this);
		item_clicked.parent().find('.uploadfield').val('');
		var $temp = item_clicked.parent().find('img:last').attr('data-default');
		item_clicked.parent().find('img:last').attr('src', $temp);
	});
  },//mediaUpload

}; // dtblogAdmin End

jQuery(document).ready(function($){

  dtblogAdmin.init();

});