<?php
/*
 * @Author : designthemes
 * @Date   : 10/30/2017
 * @Last Modified by: designthemes
 * @Last Modified time: 10/30/2017
 */
if ( ! defined( 'ABSPATH' ) ) {
	exit();
}
?>
<h2 class="section-title"><?php echo __( 'Booking #', 'designthemes-travel' ) . $order_settings['order_id']; ?></h2>
<table class="width-100 booking_details" cellspacing="0" cellpadding="0">
    <tr>
        <th><?php _e( 'Room', 'designthemes-travel' ) ?></th>
        <th><?php _e( 'Check in', 'designthemes-travel' ) ?></th>
        <th><?php _e( 'Check out', 'designthemes-travel' ) ?></th>
        <th><?php _e( '#', 'designthemes-travel' ) ?></th>
        <th><?php _e( 'Price', 'designthemes-travel' ) ?></th>
    </tr>
	<?php $tot_amount = 0;
	$items = $order_items;
	foreach ( $items as $k => $item ) :

		$qty = $item['qty'];	$price = $qty * $item['price'];	$tot_amount += $price; ?>
        <tr>
            <td><?php printf( '%s', get_the_title( $item['room_id'] ) ) ?></td>
            <td><?php printf( '%s', date_i18n( 'F d, Y', strtotime( $item['checkin_date'] ) ) ) ?></td>
            <td><?php printf( '%s', date_i18n( 'F d, Y', strtotime( $item['checkout_date'] ) ) ) ?></td>
            <td><?php printf( '%s', $qty ) ?></td>
            <td><?php printf( '%s', dt_travel_get_formatted_price( $price ) ) ?></td>
        </tr>

		<?php $service_items = array_key_exists('addon', $item) ? $item['addon'] : array(); ?>
        <?php if( count( $service_items ) > 0 ) { ?>
            <?php foreach( $service_items as $k => $s ) { ?>
                <?php $qty = $s['qty']; $price = $qty * $s['price']; $tot_amount += $price; ?>
                <tr>
                    <td colspan="3">
                        <?php $term = get_term( $s['term_id'], 'dt_services' ); echo $term->name;?>
                    </td>
                    <td>
                        <?php printf( '%s', $qty ) ?>
                    </td>
                    <td>
                        <?php printf( '%s', dt_travel_get_formatted_price($price) ); ?>
                    </td>
                </tr>
            <?php } ?>
        <?php } ?>

	<?php endforeach; ?>
    <tr>
        <td colspan="4"><b><?php _e( 'Subtotal', 'designthemes-travel' ) ?></b></td>
        <td><?php printf( '%s', dt_travel_get_formatted_price( $tot_amount ) ); ?></td>
    </tr>
    <tr>
        <td colspan="4"><b><?php _e( 'Payment method', 'designthemes-travel' ) ?></b></td>
        <td><?php echo esc_html( str_replace( '-', ' ', ucfirst( $order_settings['payment_method'] ) ) ) ?></td>
    </tr>
    <tr>
        <td colspan="4"><b><?php _e( 'Total', 'designthemes-travel' ) ?></b></td>
        <td><?php 
			$tax_amount = 0;
			if ( $tax = $order_settings['tax_percent'] )
				$tax_amount = ($tot_amount) * ($tax) / 100;

			echo dt_travel_get_formatted_price( ($tot_amount) + ($tax_amount) );
		 ?></td>
    </tr>
</table>

<?php if ( $order_settings['notes'] != '' ) : ?>
    <h2><?php _e( 'Addition Information', 'designthemes-travel' ); ?></h2>
    <p><?php printf( '%s', $order_settings['notes'] ) ?></p>
<?php endif; ?>