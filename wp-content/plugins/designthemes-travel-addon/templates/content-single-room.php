<?php
/**
 * The template for displaying room content in the single-room.php template
 *
 * Override this template by copying it to yourtheme/tp-room-booking/content-single-room.php
 *
 * @author        designthemes
 * @package       designthemes-travel-addon/templates
 * @version       1.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
?>

<?php
if ( post_password_required() ) {
	echo get_the_password_form();
	return;
}
?>

<div id="dt-room-<?php the_ID(); ?>" <?php post_class( 'dt-single-room' ); ?>>

    <div class="summary entry-summary">

		<?php
		/**
		 * dt_travel_booking_single_room_gallery hook
		 */
		do_action( 'dt_travel_booking_single_room_gallery' );
		?>
        <div class="title-wrapper"><?php
			/**
			 * dt_travel_booking_single_room_title hook
			 */
			do_action( 'dt_travel_booking_single_room_title' );

			/**
			 * dt_travel_booking_single_room_price
			 */
			do_action( 'dt_travel_booking_single_room_price' );
			?>

        	<a href="<?php echo dt_travel_search_url(); ?>" class="dt-sc-button medium fully-rounded-border white" title="<?php esc_attr_e('Book Now', 'designthemes-travel'); ?>"><?php esc_html_e('Book Now', 'designthemes-travel'); ?></a>
        </div>

    </div><!-- .summary -->

    <?php
	/**
	 * dt_travel_booking_single_room_infomation hook
	 */
	do_action( 'dt_travel_booking_single_room_infomation' );
	?>

</div><!-- #room-<?php the_ID(); ?> -->

<?php do_action( 'dt_travel_booking_after_single_product' ); ?>