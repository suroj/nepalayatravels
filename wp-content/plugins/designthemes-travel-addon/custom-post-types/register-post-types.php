<?php
if (! class_exists ( 'DTTravelAddonCustomPostTypes' )) {

	/**
	 * Custom Post Types
	 */
	class DTTravelAddonCustomPostTypes {

		function __construct() {

			// Room custom post type
			require_once plugin_dir_path ( __FILE__ ) . '/dt-room-post-type.php';
			if (class_exists ( 'DTRoomPostType' )) {
				new DTRoomPostType();
			}
			
			// Order custom post type
			require_once plugin_dir_path ( __FILE__ ) . '/dt-order-post-type.php';
			if (class_exists ( 'DTOrderPostType' )) {
				new DTOrderPostType();
			}
		}
	}
}
?>