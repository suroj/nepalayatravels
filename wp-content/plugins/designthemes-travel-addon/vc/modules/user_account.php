<?php add_action( 'vc_before_init', 'dt_sc_user_account_vc_map' );
function dt_sc_user_account_vc_map() {

	vc_map( array(
		"name" => esc_html__( "User Account", 'designthemes-travel' ),
		"base" => "dt_sc_user_account",
		"icon" => "dt_sc_user_account",
		"category" => DTTRAVEL_VC_CATEGORY,
		"description" => esc_html__("Show user account.",'designthemes-travel')
	) );
}?>