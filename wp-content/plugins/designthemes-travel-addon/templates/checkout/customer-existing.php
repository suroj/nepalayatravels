<?php
if ( !defined( 'ABSPATH' ) ) {
	exit();
}
?>
<?php if ( !is_user_logged_in() ) : ?>

    <div class="dt-order-existing-customer" data-label="<?php esc_attr_e( '-Or-', 'designthemes-travel' ); ?>">
        <div class="dt-col-padding dt-col-border">
            <h4 class="dt-existing-title"><?php esc_html_e( 'Existing customer?', 'designthemes-travel' ); ?></h4>
            <ul class="dt-form-table">
                <li class="dt-form-field">
                    <label class="dt-form-field-label"><?php esc_html_e( 'Email', 'designthemes-travel' ); ?></label>
                    <div class="dt-form-field-input">
                        <input type="email" name="existing-customer-email" placeholder="<?php esc_html_e( 'Your email here', 'designthemes-travel' ); ?>" />
                    </div>
                </li>
                <li>
                    <button type="button" id="fetch-customer-info"><?php esc_html_e( 'Apply', 'designthemes-travel' ); ?></button>
                </li>
            </ul>
        </div>
    </div>

<?php endif; ?>