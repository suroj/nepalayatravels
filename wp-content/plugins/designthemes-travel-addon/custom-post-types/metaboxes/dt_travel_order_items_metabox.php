<?php
global $post;
$order_items = get_post_meta ( $post->ID, '_order_items', TRUE );
$order_items = is_array ( $order_items ) ? $order_items : array ();

$order_settings = get_post_meta ( $post->ID, '_order_settings', TRUE );
$order_settings = is_array ( $order_settings ) ? $order_settings : array ();

$orderID = $post->ID; ?>

<style type="text/css">
table.dt_travel_tbl { background-color: #FFFFFF; width: 100%; text-align: left; border-collapse: collapse; }
table.dt_travel_tbl td, table.dt_travel_tbl th { border: 1px solid #AAAAAA; padding: 5px 2px; }
table.dt_travel_tbl tbody td { font-size: 13px; }
table.dt_travel_tbl tr:nth-child(even) { background: #EDEDED; }
table.dt_travel_tbl thead { background: #F8F8F8; border-bottom: 2px solid #444444; }
table.dt_travel_tbl thead th { font-size: 14px; font-weight: bold; color: #000000; border-left: 1px solid #AAAAAA; }
table.dt_travel_tbl tfoot { font-size: 14px; font-weight: bold; color: #FFFFFF; background: #D0E4F5; background: -moz-linear-gradient(top, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%); background: -webkit-linear-gradient(top, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%); background: linear-gradient(to bottom, #dcebf7 0%, #d4e6f6 66%, #D0E4F5 100%); border-top: 2px solid #444444; }
table.dt_travel_tbl tfoot td { font-size: 14px; }
table.dt_travel_tbl th.center, table.dt_travel_tbl td.center { text-align:center; }
table.dt_travel_tbl th.right, table.dt_travel_tbl td.right { text-align:right; }
</style>

<div id="dt_travel_booking_items">
	<table cellpadding="0" cellspacing="0" class="dt_travel_tbl">
		<thead>
			<tr>
				<th class="center"><input type="checkbox" id="booking-item-checkall"></th>
				<th class="left"><?php esc_html_e('Item', 'designthemes-travel');?></th>
				<th class="center"><?php esc_html_e('Checkin - Checkout', 'designthemes-travel');?></th>
				<th class="center"><?php esc_html_e('Night', 'designthemes-travel');?></th>
				<th class="center"><?php esc_html_e('Qty', 'designthemes-travel');?></th>
				<th class="center"><?php esc_html_e('Total', 'designthemes-travel');?></th>
				<th class="center"><?php esc_html_e('Actions', 'designthemes-travel');?></th>
			</tr>
		</thead><?php
		if( count( $order_items ) > 0 ):

			echo '<tbody>';
				$symbol = dt_travel_get_currency_symbol();
				$tot_amount = 0;

				foreach( $order_items as $key => $item ):
					$room_id = $item['room_id'];	$checkin_date = $item['checkin_date'];	  $checkout_date = $item['checkout_date'];
					$nights  = $item['nights']; 	$qty = $item['qty'];					  $price = $qty * $item['price'];
					$tot_amount += $price; ?>
                    <tr>
                        <td class="center"><input type="checkbox" name="chkitem" value="<?php echo ($key);?>" data-type="line_item" /></td>
                        <td class="left"><a target="_blank" href="<?php echo get_edit_post_link($room_id);?>"><?php echo get_the_title($room_id);?></a></td>
                        <td class="center"><?php echo date('F d, Y', strtotime($checkin_date)).' - '.date('F d, Y', strtotime($checkout_date));?></td>
                        <td class="center"><?php echo ($nights);?></td>
                        <td class="center"><?php echo ($qty);?></td>
                        <td class="center"><?php echo ($symbol.dt_travel_number_format($price));?></td>
                        <td class="actions">
                            <a href="#" class="edit" data-order-id="<?php echo ($orderID);?>" data-order-item-id="<?php echo ($key);?>" data-order-item-type="line_item">
                            	<i class="fa fa-pencil"></i>
                            </a>
                            <a href="#" class="remove" data-order-id="<?php echo ($orderID);?>" data-order-item-id="<?php echo ($key);?>" data-order-item-type="line_item">
                            	<i class="fa fa-times-circle"></i>
                            </a>
                        </td>
                    </tr><?php
					$service_items = array_key_exists('addon', $item) ? $item['addon'] : array();
					if( count( $service_items ) > 0 ):
						foreach( $service_items as $k => $s ):
							$qty = $s['qty']; $price = $qty * $s['price'];
							$tot_amount += $price; ?>
                            <tr>
                                <td class="center"><input type="checkbox" name="chkitem" value="<?php echo ($k);?>" data-type="sub_item" data-parent="<?php echo ($key);?>" /></td>
                                <td class="center" colspan="3"><?php $term = get_term( $s['term_id'], 'dt_services' ); echo $term->name;?></td>
                                <td class="center"><?php echo ($qty);?></td>
                                <td class="center"><?php echo ($symbol.dt_travel_number_format($price));?></td>
                                <td class="actions">
                                    <a href="#" class="remove" data-order-id="<?php echo ($orderID);?>" data-order-item-id="<?php echo ($k);?>" data-order-item-type="sub_item" data-order-item-parent="<?php echo ($key);?>">
                                        <i class="fa fa-times-circle"></i>
                                    </a>
                                </td>
                            </tr><?php
						endforeach;
					endif;
				endforeach;

			echo '</tbody>';
		else:?>
        	<tbody>
            	<tr>
                	<td class="center" colspan="7"><?php esc_html_e('No items Found', 'designthemes-travel');?></td>
                </tr>
            </tbody><?php
		endif;?>

		<tfoot>
			<tr>
				<th class="left" colspan="3"><select id="actions"><option><?php esc_html_e('Delete select item(s)', 'designthemes-travel');?></option></select><a href="#" class="button button-primary" id="bulk_delete" data-order-id="<?php echo ($orderID);?>"><?php esc_html_e('Apply', 'designthemes-travel');?></a></th>
				<th class="right" colspan="4">
                    <a href="#" class="button button-primary" title="<?php esc_attr_e('Add Room Item', 'designthemes-travel');?>" id="add_room_item"><?php esc_html_e('Add Room Item', 'designthemes-travel');?></a>
				</th>
			</tr>
		</tfoot>
	</table>

	<?php if( isset($symbol) && isset($tot_amount) ):?>
        <table class="dt_travel_tbl">
            <tbody>
                <tr>
                    <td class="center"><?php esc_html_e('Sub Total', 'designthemes-travel');?></td>
                    <td class="center"><?php echo $symbol.dt_travel_number_format($tot_amount);?></td>
                </tr><?php
				if( cs_get_option('include-tax')):
					$tax = array_key_exists('tax_percent', $order_settings) ? $order_settings['tax_percent'] : cs_get_option('tax-percentage');?>
					<tr>
						<td class="center"><?php echo esc_html__('Tax', 'designthemes-travel').' ('.$tax.' %)';?></td>
						<td class="center"><?php $tax_amount = ($tot_amount) * ($tax) / 100; echo $symbol.dt_travel_number_format($tax_amount);?></td>
					</tr><?php
				endif;?>
                <tr>
                    <td class="center"><?php esc_html_e('Grand Total', 'designthemes-travel');?></td>
                    <td class="center"><?php $net_amount = ($tot_amount) + ($tax_amount); echo $symbol.dt_travel_number_format($net_amount);?></td>
                </tr>
            </tbody>
        </table>
    <?php endif;?>
</div>

<div id="dt-travel-add-room-item" class="hidden">
	<div class="dt-travel-add-room">
    	<form method="post" name="frmaddroomitem">
        	<p><select name="cmbrooms"><option value=""><?php esc_html_e('Choose any Room', 'designthemes-travel');?></option><?php echo dt_travel_get_rooms();?></select></p>
            <p><input type="text" name="txtcheckindate" class="dt_order_datepicker" placeholder="<?php esc_attr_e('Check In', 'designthemes-travel');?>" /></p>
            <p><input type="text" name="txtcheckoutdate" class="dt_order_datepicker" placeholder="<?php esc_attr_e('Check Out', 'designthemes-travel');?>" /></p>
            <p><input type="hidden" name="order_id" value="<?php echo ($orderID);?>" /></p>
            <p class="ajax-quantity"> </p>
        </form>
    </div>
</div>