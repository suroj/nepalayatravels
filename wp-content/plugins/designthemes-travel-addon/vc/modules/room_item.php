<?php add_action( 'vc_before_init', 'dt_sc_room_item_vc_map' );
function dt_sc_room_item_vc_map() {

	vc_map( array(
		"name" => esc_html__( "Room Item", 'designthemes-travel' ),
		"base" => "dt_sc_room_item",
		"icon" => "dt_sc_room_item",
		"category" => DTTRAVEL_VC_CATEGORY,
		"description" => esc_html__("Show a room item.",'designthemes-travel'),
		"params" => array(

			// Room Id
			array(
				'type' => 'textfield',
				'heading' => esc_html__( 'Room ID', 'designthemes-travel' ),
				'param_name' => 'id',
				'description' => esc_html__( 'Enter the room id.', 'designthemes-travel' ),
				'admin_label' => true
			)
		)
	) );
}?>