<?php
/*
 * Plugin Name:	DesignThemes Travel Addon
 * URI: 		http://wedesignthemes.com/plugins/designthemes-travel-addon
 * Description: A simple wordpress plugin designed to implements <strong>travel features of DesignThemes</strong>
 * Version: 	1.0
 * Author: 		DesignThemes
 * Text Domain: designthemes-travel
 * Author URI:	http://themeforest.net/user/designthemes
 */
if (! class_exists ( 'DTTravelAddon' )) {

	class DTTravelAddon {

		function __construct() {

			add_action ( 'init', array($this, 'dtTravelAddonTextDomain') );

			define( 'DTTRAVELADDON_PATH', dirname( __FILE__ ) );

			register_activation_hook( __FILE__ , array( $this , 'dtTravelAddonActivate' ) );
			register_deactivation_hook( __FILE__ , array( $this , 'dtTravelAddonDeactivate' ) );

			// Include Functions
			require_once plugin_dir_path ( __FILE__ ) . '/functions/core-functions.php';
			require_once plugin_dir_path ( __FILE__ ) . '/functions/paypal-webhooks.php';
			require_once plugin_dir_path ( __FILE__ ) . '/functions/ajax-functions.php';
			require_once plugin_dir_path ( __FILE__ ) . '/functions/utils-functions.php';
			require_once plugin_dir_path ( __FILE__ ) . '/functions/email-functions.php';

			// Register Custom Post Types
			require_once plugin_dir_path ( __FILE__ ) . '/custom-post-types/register-post-types.php';
			if(class_exists('DTTravelAddonCustomPostTypes')){
				new DTTravelAddonCustomPostTypes();
			}

			// Register Shortcodes
			require_once plugin_dir_path ( __FILE__ ) . '/shortcodes/register-shortcodes.php';
			if(class_exists('DTTravelAddonShortcodes')){
				new DTTravelAddonShortcodes();
			}

			// Register Widgets
			require_once plugin_dir_path ( __FILE__ ) . '/widgets/register-widgets.php';
			if(class_exists('DTTravelAddonWidgets')){
				new DTTravelAddonWidgets();
			}

			// Register Templates
			require_once plugin_dir_path ( __FILE__ ) . '/templates/register-templates.php';
			if(class_exists('DTTravelAddonTemplates')){
				new DTTravelAddonTemplates();
			}

			// Register Visual Composer
			if( $this->dtTravelAddonIsPluginActive( 'js_composer/js_composer.php' ) ) {
				require_once plugin_dir_path ( __FILE__ ) . '/vc/register-vc.php';
				if(class_exists('DTTravelAddonVcModules')){
					new DTTravelAddonVcModules();
				}
			}
		}

		/**
		 * Load Text Domain
		 */
		function dtTravelAddonTextDomain() {
			load_plugin_textdomain ( 'designthemes-travel', false, dirname ( plugin_basename ( __FILE__ ) ) . '/languages/' );
		}

		/**
		 * Check Plugin Is Active
		 */
		function dtTravelAddonIsPluginActive( $plugin ) {
			include_once( ABSPATH . 'wp-admin/includes/plugin.php' );

			if( is_plugin_active( $plugin ) || is_plugin_active_for_network( $plugin ) ) return true;
			else return false;
		}

		/**
		 * Travel Addon Activate
		 */
		public static function dtTravelAddonActivate() {
		}

		/**
		 * Travel Addon Deactivate
		 */
		public static function dtTravelAddonDeactivate() {
		}

	}

	new DTTravelAddon();
}?>