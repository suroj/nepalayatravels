<?php
/**
 * Get json via ajax
 * @return json
 */
add_action('wp_ajax_dt_travel_load_other_full_calendar', 'dt_travel_load_other_full_calendar');
function dt_travel_load_other_full_calendar() {
	check_ajax_referer( 'dt_booking_nonce', 'nonce' );

	if ( ! isset( $_POST['room_id'] ) ) {
		wp_send_json( array(
			'status'  => false,
			'message' => __( 'Room is not exists.', 'designthemes-travel' )
		) );
	}

	$room_id = absint( $_POST['room_id'] );
	if ( ! isset( $_POST['date'] ) ) {
		wp_send_json( array(
			'status'  => false,
			'message' => __( 'Date is not exists.', 'designthemes-travel' )
		) );
	}
	$date = sanitize_text_field( $_POST['date'] );

	wp_send_json( array(
		'status'     => true,
		'events'     => dt_travel_print_pricing_json( $room_id, date( 'm/d/Y', strtotime( $date ) ) ),
		'next'       => date( 'm/d/Y', strtotime( '+1 month', strtotime( $date ) ) ),
		'prev'       => date( 'm/d/Y', strtotime( '-1 month', strtotime( $date ) ) ),
		'month_name' => date_i18n( 'F, Y', strtotime( $date ) )
	) );
}

/**
 * Get quantity via ajax
 * @return quantities
 */
add_action('wp_ajax_dt_travel_check_room_availability', 'dt_travel_check_room_availability');
function dt_travel_check_room_availability() {

	if ( empty( $_POST['room_id'] ) ) {
		wp_send_json( array(
			'status'  => false,
			'message' => __( 'Room is not exists.', 'designthemes-travel' )
		) );
	}

	if ( empty( $_POST['chkin'] ) || empty( $_POST['chkout'] ) ) {
		wp_send_json( array(
			'status'  => false,
			'message' => __( 'Check In & Check Out dates are not exists.', 'designthemes-travel' )
		) );
	}

	$room_id = $_POST['room_id'];
	$checkin  = strtotime( $_POST['chkin'] );
	$checkout = strtotime( $_POST['chkout'] );
	if( $checkin > $checkout ) {
		wp_send_json( array(
			'status'  => false,
			'message' => __( 'Check Out must be greater than Check In date.', 'designthemes-travel' )
		) );
	}

	$room_meta = get_post_meta( $room_id, '_room_settings', TRUE );
	$room_meta = is_array( $room_meta ) ? $room_meta : array();

	$mata_query = array(
		array(
			'key'     => '_order_settings',
			'value'   => 'cancelled',
			'compare' => 'NOT LIKE',
		),
	);

	$allorder_items = array();
	$args = array( 'post_type' => 'dt_orders', 'meta_key' => '_order_settings', 'posts_per_page' => -1, 'meta_query' => $mata_query );
	$the_query = new WP_Query($args);

	if($the_query->have_posts()):
		while($the_query->have_posts()): $the_query->the_post();

			$order_meta = get_post_meta( get_the_ID(), '_order_items', TRUE );
			$order_meta = is_array( $order_meta ) ? $order_meta : array();

			foreach( $order_meta as $key => $v ) {
				if( array_key_exists( 'room_id', $v ) && ( $room_id == $v['room_id'] ) ){
					$allorder_items[] = $v;
				}
			}
			
		endwhile;
	endif;
	wp_reset_postdata();

	$totqty = $netqty = '';
	$chkinchkout = dt_travel_get_between_dates( date( 'Y-m-d', $checkin ), date( 'Y-m-d', $checkout ) );

	foreach($allorder_items as $item) {
		$temp = dt_travel_get_between_dates( $item['checkin_date'], $item['checkout_date'] );

		$containsSearch = count(array_intersect_key($chkinchkout, $temp));
		if( $containsSearch > 0 ) {
			$totqty = $totqty + $item['qty'];
		}
	}

	$netqty = ($room_meta['room-qty']) - ($totqty);

	if( $netqty == 0 ) {
		wp_send_json( array(
			'status'  => false,
			'message' => __( 'No Rooms available in your search criteria. Please try with different dates.', 'designthemes-travel' )
		) );
	} elseif( $netqty > 0 ) {
		wp_send_json( array(
			'status'  => true,
			'result'  => dt_travel_qty_dropdown($netqty),
			'addons'  => dt_travel_addon_services_dropdown($room_id)
		) );
	}
}

/**
 * Add available room via ajax
 * @return success message & table row
 */
add_action('wp_ajax_dt_travel_add_available_room_item', 'dt_travel_add_available_room_item');
function dt_travel_add_available_room_item() {

	$order_id = $_POST['order_id'];

	if ( empty( $_POST['room_id'] ) ) {
		wp_send_json( array(
			'status'  => false,
			'message' => __( 'Room is not exists.', 'designthemes-travel' )
		) );
	}

	if ( empty( $_POST['chkin'] ) || empty( $_POST['chkout'] ) ) {
		wp_send_json( array(
			'status'  => false,
			'message' => __( 'Check In & Check Out dates are not exists.', 'designthemes-travel' )
		) );
	}

	if ( empty( $_POST['quantity'] ) ) {
		wp_send_json( array(
			'status'  => false,
			'message' => __( 'Choose no.of rooms, if not try with different dates.', 'designthemes-travel' )
		) );
	}

	$order_meta = get_post_meta( $order_id, '_order_items', true );
	$order_meta = is_array( $order_meta ) ? $order_meta : array();

	$temp = $a = array();
	$temp['room_id'] = $_POST['room_id'];
	$temp['checkin_date'] = $_POST['chkin'];
	$temp['checkout_date'] = $_POST['chkout'];
	$temp['nights'] = dt_travel_get_days_between_dates( $_POST['chkin'], $_POST['chkout'] );
	$temp['qty'] = $_POST['quantity'];
	$temp['price'] = dt_travel_get_price_between_dates( $_POST['chkin'], $_POST['chkout'], $_POST['room_id'] );

	if( !empty($_POST['addons']) ):
		foreach( $_POST['addons'] as $s ):
			$term_meta = get_term_meta( $s, '_room_services', false );
			$cost = !empty($term_meta[0]['service_price']) ? $term_meta[0]['service_price'] : '';

			$a[] = array( 'term_id' => $s, 'qty' => 1, 'price' => $cost );
		endforeach;

		$temp['addon'] = $a;
	endif;

	$order_meta[] = $temp;

	update_post_meta ( $order_id, '_order_items', array_filter ( $order_meta ) );

	wp_send_json( array(
		'status'  => true
	) );
}

/**
 * Delete item via ajax
 * @return success message
 */
add_action('wp_ajax_dt_travel_delete_room_item', 'dt_travel_delete_room_item');
function dt_travel_delete_room_item() {

	if ( empty( $_POST['order_id'] ) || empty( $_POST['item_type'] ) || !isset( $_POST['item_id'] ) ) {
		wp_send_json( array(
			'status'  => false,
			'message' => __( 'Error on deleting, values missing.', 'designthemes-travel' )
		) );
	}

	$order_meta = get_post_meta( $_POST['order_id'], '_order_items', true );
	$order_meta = is_array( $order_meta ) ? $order_meta : array();

	if( $_POST['item_type'] == 'line_item' ):
		unset( $order_meta[$_POST['item_id']] );
	elseif( $_POST['item_type'] == 'sub_item' ):
		unset( $order_meta[$_POST['item_parent']]['addon'][$_POST['item_id']] );
	endif;

	update_post_meta ( $_POST['order_id'], '_order_items', array_filter ( $order_meta ) );
	
	wp_send_json( array(
		'status'  => true
	) );
}

/**
 * Add to cart room via ajax
 * @return success message
 */
add_action('wp_ajax_dt_travel_add_cart_room_item', 'dt_travel_add_cart_room_item');
add_action( 'wp_ajax_nopriv_dt_travel_add_cart_room_item', 'dt_travel_add_cart_room_item' );
function dt_travel_add_cart_room_item() {

	if ( empty( $_POST['room_id'] ) ) {
		wp_send_json( array(
			'status'  => false,
			'message' => __( 'Room is not exists.', 'designthemes-travel' )
		) );
	}

	if ( empty( $_POST['chkin'] ) || empty( $_POST['chkout'] ) ) {
		wp_send_json( array(
			'status'  => false,
			'message' => __( 'Check In & Check Out dates are not exists.', 'designthemes-travel' )
		) );
	}

	if ( empty( $_POST['quantity'] ) ) {
		wp_send_json( array(
			'status'  => false,
			'message' => __( 'Choose no.of rooms, if not try with different dates.', 'designthemes-travel' )
		) );
	}

	$temp = $a = $temp_order = array();
	$temp['room_id'] = $_POST['room_id'];
	$temp['checkin_date'] = $_POST['chkin'];
	$temp['checkout_date'] = $_POST['chkout'];
	$temp['nights'] = dt_travel_get_days_between_dates( $_POST['chkin'], $_POST['chkout'] );
	$temp['qty'] = $_POST['quantity'];
	$temp['price'] = dt_travel_get_price_between_dates( $_POST['chkin'], $_POST['chkout'], $_POST['room_id'] );

	if( !empty($_POST['addons']) ):
		$addon_arr = array_filter( explode(',', $_POST['addons']) );

		foreach( $addon_arr as $s ):
			$p = strpos($s, ':');
			$t = substr($s, 0, $p);
			$q = substr($s, $p+1);

			$term_meta = get_term_meta( $t, '_room_services', false );
			$cost = !empty($term_meta[0]['service_price']) ? $term_meta[0]['service_price'] : '';

			$a[] = array( 'term_id' => $t, 'qty' => $q, 'price' => $cost );
		endforeach;

		$temp['addon'] = $a;
	endif;

	if( $_SESSION['dt_room_cart_items'] == '' ) {

		$temp_order[$_POST['room_id']] = $temp;
		$_SESSION['dt_room_cart_items'] = $temp_order;

	} else {

		$temp_order   = $_SESSION['dt_room_cart_items'];
		$temp_order[$_POST['room_id']] =  $temp;

		$_SESSION['dt_room_cart_items'] = $temp_order;
	}

	wp_send_json( array(
		'status'  => true
	) );
}

/**
 * Delete cart item via ajax
 * @return success message
 */
add_action('wp_ajax_dt_travel_delete_cart_item', 'dt_travel_delete_cart_item');
add_action( 'wp_ajax_nopriv_dt_travel_delete_cart_item', 'dt_travel_delete_cart_item' );
function dt_travel_delete_cart_item() {

	if ( empty( $_POST['cart_id'] ) || empty( $_POST['item_type'] ) ) {
		wp_send_json( array(
			'status'  => false,
			'message' => __( 'Error on deleting, values missing.', 'designthemes-travel' )
		) );
	}

	if( $_POST['item_type'] == 'line_item' ):
		unset( $_SESSION['dt_room_cart_items'][$_POST['cart_id']] );
	elseif( $_POST['item_type'] == 'sub_item' ):
		unset( $_SESSION['dt_room_cart_items'][$_POST['cart_id']]['addon'][$_POST['item_id']] );
	endif;

	array_filter ( $_SESSION['dt_room_cart_items'] );

	wp_send_json( array(
		'status'  => true
	) );
}

/**
 * Get custom info via ajax
 * @return custom details array
 */
add_action('wp_ajax_dt_travel_fetch_customer_info', 'dt_travel_fetch_customer_info');
add_action( 'wp_ajax_nopriv_dt_travel_fetch_customer_info', 'dt_travel_fetch_customer_info' );
function dt_travel_fetch_customer_info() {

	if ( empty( $_POST['email'] ) ) {
		wp_send_json( array(
			'status'  => false,
			'message' => __( '<div class="dt-sc-error-box">Error on fetching, values missing.</div>', 'designthemes-travel' )
		) );
	}

	$userID = '';
	$args = array( 'meta_key' => '_dt_travel_user_fields', 'meta_value' => $_POST['email'], 'meta_compare' => 'LIKE' );
	$user_query = new WP_User_Query( $args );
	// User Loop
	if ( ! empty( $user_query->results ) ) {
		foreach ( $user_query->results as $user ) {
			if( $user->ID > 0 ) {
				$userID = $user->ID;
				break;
			}
		}
	} else {
		wp_send_json( array(
			'status'  => false,
			'message' => __( '<div class="dt-sc-error-box">No users found, for this email. Please enter correct user email.</div>', 'designthemes-travel' )
		) );
	}

	$customer->data = array();
	$data           = get_user_meta ( $userID, '_dt_travel_user_fields', TRUE );
	foreach ( $data as $k => $v ) {
		$customer->data[ $k ] = $v;
	}
	$customer->data['ID'] = $userID;

	wp_send_json( $customer );
}

/**
 * Place order via ajax
 * @return return url / cancel url
 */
add_action('wp_ajax_dt_travel_checkout_place_order', 'dt_travel_checkout_place_order');
add_action( 'wp_ajax_nopriv_dt_travel_checkout_place_order', 'dt_travel_checkout_place_order' );
function dt_travel_checkout_place_order() {

	if ( !array_key_exists('dt_room_cart_items', $_SESSION) || empty( $_SESSION['dt_room_cart_items'] ) ) :
		wp_send_json( array(
			'status'  => false,
			'message' => __( 'Your cart is empty.', 'designthemes-travel' )
		) );
	endif;

	if ( !is_user_logged_in() && !cs_get_option('enable-booking') ):
		wp_send_json( array(
			'status'  => false,
			'message' => __( 'You have to Login to process checkout.', 'designthemes-travel' )
		) );
	endif;

	$formdata = '';
	parse_str($_POST['data'], $formdata);

	if( $formdata['first_name'] == '' || $formdata['dt_payment_method'] == '' || $formdata['tos'] == '' ):
		wp_send_json( array(
			'status'  => false,
			'message' => __( 'Missing required fields.', 'designthemes-travel' )
		) );
	endif;

	$meta_settings['_order_settings'] = array( 
		'payment_method' => $formdata['dt_payment_method'],
		'booking_status' => 'processing',
		'tax_percent'	 => cs_get_option('include-tax') ? cs_get_option('tax-percentage') : '',
		'customer'		 => $formdata['existing-customer-id'],
		'first_name'	 => $formdata['first_name'],
		'last_name'		 => $formdata['last_name'],
		'address'		 => $formdata['address'],
		'city'			 => $formdata['city'],
		'state'			 => $formdata['state'],
		'postal_code'	 => $formdata['postal_code'],
		'email_address'	 => $formdata['email_address'],
		'fax_no'		 => $formdata['fax_no'],
		'phone_no'	 	 => $formdata['phone_no'],
		'country_name'	 => $formdata['country_name'],
		'notes'			 => $formdata['addition_information'],
		'advance_percent'=> ''
	);

	$meta_items['_order_items'] = $_SESSION['dt_room_cart_items'];

	$order_info = array(
	  'post_status'   => 'publish',
	  'post_type' 	  => 'dt_orders',
	  'meta_input'	  => array_merge($meta_settings, $meta_items)
	);

	// Creating order...
	$order_id = wp_insert_post( $order_info );
	if( is_user_logged_in() || $formdata['existing-customer-id'] != '' ):

		$user_id = ( is_user_logged_in() ) ? get_current_user_id() : $formdata['existing-customer-id'];

		unset( $meta_settings['_order_settings']['payment_method'] );
		unset( $meta_settings['_order_settings']['booking_status'] );
		unset( $meta_settings['_order_settings']['tax_percent'] );
		unset( $meta_settings['_order_settings']['customer'] );
		unset( $meta_settings['_order_settings']['notes'] );
		unset( $meta_settings['_order_settings']['advance_percent'] );

		update_user_meta ( $user_id, '_dt_travel_user_fields', array_filter ( $meta_settings['_order_settings'] ) );
	endif;

	if( $formdata['dt_payment_method'] == 'offline-payment' ):
		if( !is_wp_error($order_id) ):
			// Removing cart values...
			unset($_SESSION['dt_room_cart_items']);

			// Order mails...
			do_action( 'dt_travel_place_order', $order_id );

			// Response to thank you page...
			wp_send_json( array(
				'status'  => true,
				'result'  => 'success',
				'redirect' => add_query_arg( array( 'order' => $order_id ), dt_travel_cart_url() . 'thank-you/' )
			) );
		endif;
	elseif( $formdata['dt_payment_method'] == 'paypal' ):
		if( !is_wp_error($order_id) ):
			// Removing cart values...
			unset($_SESSION['dt_room_cart_items']);

			// Order Mails...
			do_action( 'dt_travel_place_order', $order_id );

			$paypal_url = apply_filters( 'dt_travel_paypal_basic_checkout_url', $order_id, $formdata );

			// Response to paypal page...
			wp_send_json( array(
				'status'  => true,
				'result'  => 'success',
				'redirect' => $paypal_url
			) );
		endif;
	endif;
}