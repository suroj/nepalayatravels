<?php
/*
 * @Author : designthemes
 * @Date   : 10/30/2017
 * @Last Modified by: designthemes
 * @Last Modified time: 10/30/2017
 */

/**
 * Prevent loading this file directly
 */
defined( 'ABSPATH' ) || exit;

// email heading
dt_travel_get_template( 'emails/email-header.php',
	array(
		'email_heading'      => $email_heading,
		'email_heading_desc' => $email_heading_desc
	)
);

// order details
dt_travel_get_template( 'emails/order-details.php', array( 'order_settings' => $order_settings, 'order_items' => $order_items ) );

// customer details
dt_travel_get_template( 'emails/customer-details.php', array( 'order_settings' => $order_settings, 'order_items' => $order_items ) );

// email footer
dt_travel_get_template( 'emails/email-footer.php' );