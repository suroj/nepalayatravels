(function ($) {

	$("body").delegate(".dt_datepicker","focus",function(event){

		var $This = $(this);
		var $a = $This.attr('data-sub-depend-id');

		if( $a == 'date-to' ){
			$This.datepicker({
				dateFormat     : 'yy-mm-dd',
				numberOfMonths : 1,
				onSelect       : function () {
					var date = $This.datepicker('getDate');
					$This.parents('.cs-group-content').find('input[data-sub-depend-id="date-from"]').datepicker('option', 'maxDate', date)
				}
			});
		} else {
			$This.datepicker({
				dateFormat     : 'yy-mm-dd',
				numberOfMonths: 1,
				onSelect       : function () {
					var date = $This.datepicker('getDate');
					$This.parents('.cs-group-content').find('input[data-sub-depend-id="date-to"]').datepicker('option', 'minDate', date)
				}
			});
		}
	});

	$("body").delegate(".cs-nav ul li a[data-section='pricing_plan_section']","click",function(event){

		$('#dt-fullcalendar').fullCalendar('render');

	});

	var dt_fullcalendar = $('#dt-fullcalendar');
	for (var i = 0; i < dt_fullcalendar.length; i++) {
		var _fullcalendar = $(dt_fullcalendar[i]),
			_data_events = _fullcalendar.attr('data-events');

		if (typeof _data_events === 'undefined') {
			_data_events = [];
		}

		_fullcalendar.fullCalendar({
			header              : {
				left : '',
				right: '',
			},
			ignoreTimezone      : false,
			handleWindowResize  : true,
			showNonCurrentDates : false,
			editable            : false,
			events              : function (start, end, timezone, callback) {
				callback(JSON.parse(_data_events));
			}
		});
	}

	var date = new Date();
	var fullcalendar_initdates = [];
	fullcalendar_initdates.push(date.getYear() + '-' + date.getMonth());
	$(document).on('click', '.dt-fullcalendar-toolbar .fc-button', function (event) {
		event.preventDefault();
		var _self = $(this),
			_calendar = $('#dt-fullcalendar'),
			_room_id = _self.attr('data-room'),
			_date = _self.attr('data-month');

		$.ajax({
			url       : ajaxurl,
			type      : 'POST',
			data      : {
				action : 'dt_travel_load_other_full_calendar',
				nonce  : dtBookingCustom.nonce,
				room_id: _room_id,
				date   : _date
			},
			beforeSend: function () {
				_self.append('<i class="fa fa-spinner fa-spin"></i>');
			}
		}).done(function (res) {
			_self.find('.fa').remove();
			if (res.status === true) {
				var events = JSON.parse(res.events);

				try {
					var date = new Date(events[0].start),
						month_string = date.getYear() + '-' + date.getMonth();
					if (fullcalendar_initdates.indexOf(month_string) == -1) {

						fullcalendar_initdates.push(month_string);
						for (var i = 0; i < events.length; i++) {
							var event = events[i];
							_calendar.fullCalendar('renderEvent', event, true);
						}
					}
				} catch (error) {
					console.debug(error);
				}
				$('#dt-fullcalendar').fullCalendar('refetchEvents');

				if (_self.hasClass('fc-next-button')) {
					_calendar.fullCalendar('next');
				} else {
					_calendar.fullCalendar('prev');
				}

				$('h2.dt-fullcalendar-month').text(res.month_name);
				$('.dt-fullcalendar-toolbar .fc-next-button').attr('data-month', res.next);
				$('.dt-fullcalendar-toolbar .fc-prev-button').attr('data-month', res.prev);
			}

		}).fail(function () {
			_self.find('.fa').remove();
		});

		return false;
	});

})(jQuery);