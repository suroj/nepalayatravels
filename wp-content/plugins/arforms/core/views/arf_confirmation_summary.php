<?php
global $arf_confirmation_summary;
$arf_confirmation_summary = new arf_submit_confirmation_summary();

class arf_submit_confirmation_summary{

	function __construct(){

		add_action('arf_option_before_submit_conditional_logic', array(&$this,'arf_submit_confirmation_summary_options'), 12,2 );

		add_filter('arf_save_form_options_outside', array(&$this,'arf_save_confirmation_summary'), 10,2);

		add_filter('getsubmitbutton', array(&$this,'arf_display_summary_button'),10,2);

		add_filter('arf_add_submit_btn_attributes_outside',array(&$this,'arf_add_submit_btn_attributes_function'),10,2);

		add_filter('arf_additional_form_content_outside',array(&$this,'arf_add_confirmation_summary_box_outside'),10,5);

		add_filter('arf_check_for_running_total_field',array(&$this,'arf_add_confirmation_action_from_outside'),12,5);

		add_filter('arf_additional_form_content_outside',array(&$this,'arf_add_confirmation_script_from_outside'),100,5);

	}

	function arf_submit_confirmation_summary_options($id,$values){
		
		global $armainhelper, $arformcontroller;
		?>
		<div class="arf_confirmation_summary_container">
			<div class="arf_confirmation_summary_inner_container">
				
				<div class="arf_confirmation_summary_enable">
					<div class="arf_popup_checkbox_wrapper" style="margin-top:5px;">
						<div class="arf_custom_checkbox_div" style="margin-top: 4px;">
							<div class="arf_custom_checkbox_wrapper">
								<input type="checkbox" class="arf_enable_confirmation_summary" name="options[arf_confirmation_summary]" id="arf_confirmation_summary" value="1" <?php isset($values['arf_confirmation_summary']) ? checked($values['arf_confirmation_summary'],1) : ''; ?> />
								<svg width="18px" height="18px">
		                        	<?php echo ARF_CUSTOM_UNCHECKED_ICON; ?>
		                        	<?php echo ARF_CUSTOM_CHECKED_ICON; ?>
		                        </svg>
							</div>
							<span>
								<label for="arf_confirmation_summary" style="margin-left:4px;"><?php echo addslashes(__('Show confirmation (Summary) before submitting form','ARForms')); ?></label>
							</span>
						</div>
					</div>
				</div>

				<?php
					$arf_enable_confirmation_summary = (isset($values['arf_confirmation_summary']) && $values['arf_confirmation_summary'] == 1) ? '' : 'display:none;';
				?>

				<div class="arf_confirmation_summary_inner_block arfmarginl15" style="<?php echo $arf_enable_confirmation_summary; ?>">
					<div class="arf_confirmation_summary_input_wrapper">
						<label for="arf_confirmation_summary_button_text" class="arf_dropdown_autoresponder_label"><?php echo addslashes(__('Confirmation Button Label','ARForms')); ?>:</label>
						<input type="text" id="arf_confirmation_summary_button_text" class="arf_large_input_box arf_confirmation_summary_input_box" name="options[arf_confirmation_summary_button_text]" value="<?php echo isset($values['arf_confirmation_summary_button_text']) ? $values['arf_confirmation_summary_button_text'] : addslashes(__('Confirm','ARForms')); ?>" />
						<span class="arferrmessage" id="arf_confirmation_summary_button_text_error"><?php echo addslashes(__('This field cannot be blank','ARForms')); ?></span>
					</div>
					<div class="arf_confirmation_summary_input_wrapper">
						<label for="arf_confirmation_summary_edit_button_text" class="arf_dropdown_autoresponder_label"><?php echo addslashes(__('Edit Button Label','ARForms')); ?>:</label>
						<input type="text" id="arf_confirmation_summary_edit_button_text" class="arf_large_input_box arf_confirmation_summary_input_box" name="options[arf_confirmation_summary_edit_button_text]" value="<?php echo isset($values['arf_confirmation_summary_edit_button_text']) ? $values['arf_confirmation_summary_edit_button_text'] : addslashes(__('Edit','ARForms')); ?>" />
						<span class="arferrmessage" id="arf_confirmation_summary_edit_button_text_error"><?php echo addslashes(__('This field cannot be blank','ARForms')); ?></span>
					</div>
				</div>
			</div>
		</div>
		<?php		

	}

	function arf_save_confirmation_summary($options,$values){

		$options['arf_confirmation_summary'] = isset($values['options']['arf_confirmation_summary']) ?  $values['options']['arf_confirmation_summary'] : '';

		$options['arf_confirmation_summary_button_text'] = isset($values['options']['arf_confirmation_summary_button_text']) ? $values['options']['arf_confirmation_summary_button_text'] : '';

		$options['arf_confirmation_summary_edit_button_text'] = isset($values['options']['arf_confirmation_summary_edit_button_text']) ? $values['options']['arf_confirmation_summary_edit_button_text'] : '';

		return $options;
	}

	function arf_display_summary_button($submit,$form){
		
		if( !isset($form) || empty($form) ){
			return $submit;
		}

		$display_summary_buttons = ( isset($form->options['arf_confirmation_summary']) && $form->options['arf_confirmation_summary'] == 1 ) ? true : false;

		if( !$display_summary_buttons ){
			return $submit;
		}

		$submit = isset($form->options['arf_confirmation_summary_button_text']) ? $form->options['arf_confirmation_summary_button_text'] : __('Confirm','ARForms');

		return $submit;
	}

	function arf_add_submit_btn_attributes_function( $submit_content, $form){

		if( !isset($form) || empty($form) ){
			return $submit_content;
		}

		$display_summary_buttons = ( isset($form->options['arf_confirmation_summary']) && $form->options['arf_confirmation_summary'] == 1 ) ? true : false;

		if( !$display_summary_buttons ){
			return $submit_content;
		}

		$submit_content .= ' data-arf-confirm="true" ';

		return $submit_content;		

	}

	function arf_add_confirmation_summary_box_outside($arf_form, $form, $form_data_id,$arfbrowser_name,$browser_info){


		if( !isset($form) || empty($form) ){
			return $arf_form;
		}

		$display_summary_buttons = ( isset($form->options['arf_confirmation_summary']) && $form->options['arf_confirmation_summary'] == 1 ) ? true : false;

		if( !$display_summary_buttons ){
			return $arf_form;
		}

		$submit = $form->options['submit_value'];

		$inputStyle = $form->form_css['arfinputstyle'];

		$wrapper_class = 'arf_materialize_form';
		if( $inputStyle == 'standard' ){
			$wrapper_class = 'arf_standard_form';
		} else if( $inputStyle == 'rounded' ){
			$wrapper_class = 'arf_rounded_form';
		}

		$arf_form .= "<div class='arf_confirmation_summary_wrapper {$wrapper_class}' id='arf_confirmation_summary_wrapper_{$form_data_id}' style='display:none;' >";

		$arf_form .= "<input type='hidden' id='arf_submit_form_after_confirm_{$form_data_id}'  value='false' />";

		$arf_form .= "<div class='arftitlecontainer'>";

		$arf_form .= "<div class='formtitle_style'>";

			$arf_form .= html_entity_decode(stripslashes($form->name));

		$arf_form .= "</div>";

		$arf_form .= "</div>";

		$arf_form .= "<div class='arf_confirmation_summary_inner_wrapper'>";

		$arf_form .= "</div>";

		$submit_height = ($form->form_css['arfsubmitbuttonheightsetting'] == '') ? '35' : $form->form_css['arfsubmitbuttonheightsetting'];
        $padding_loading_tmp = $submit_height - 24;
        $padding_loading = $padding_loading_tmp / 2;

		$submitbtnclass = '';

		$sbmt_class = "";
        if( $inputStyle == 'material' ){
            $sbmt_class = "btn btn-flat";
        }

        $submit_btn_content = "<div class='arfsubmitbutton arf_confirmation_summary_submit_wrapper'>";

        $arf_modify_button_content = isset($form->options['arf_confirmation_summary_edit_button_text']) ? $form->options['arf_confirmation_summary_edit_button_text'] : addslashes(__('Modify','ARForms'));

        $submit_btn_content .= "<input type='button' class='previous_btn arf_modify_button' data-form-id='{$form->id}' data-form-unique-id='{$form_data_id}' value='{$arf_modify_button_content}' />";

		$submit_btn_content .= '<button class="arf_submit_btn '.$sbmt_class.' btn-info arf_submit_after_confirm arfstyle-button ' . $submitbtnclass .' '.$arfbrowser_name.'"  id="arf_submit_btn_' . $form_data_id . '_confirm" name="arf_submit_btn_' . $form_data_id . '" data-style="zoom-in" >';

		$submit_btn_content .= '<span class="arfsubmitloader"></span><span class="arfstyle-label">' . esc_attr($submit) . '</span>';

		if (( $browser_info['name'] == 'Internet Explorer' and $browser_info['version'] <= '9' ) || $browser_info['name'] == 'Opera') {
            $padding_loading = isset($padding_loading) ? $padding_loading : '';
            $submit_btn_content .= '<span class="arf_ie_image" style="display:none;">';
            $submit_btn_content .= '<img src="' . ARFURL . '/images/submit_btn_image.gif" style="width:24px; box-shadow:none;-webkit-box-shadow:none;-o-box-shadow:none;-moz-box-shadow:none; vertical-align:middle; height:24px; padding-top:' . $padding_loading . 'px;"/>';
            $submit_btn_content .= '</span>';
        }
        
        $submit_btn_content .= '</button>';

        $submit_btn_content .= '</div>';

        $arf_form .= $submit_btn_content;

		$arf_form .= "</div>";

		return $arf_form;
	}

	function arf_add_confirmation_action_from_outside($arf_on_change_function,$field,$data_unique_id,$form,$res_data){
		global $arf_form_all_footer_js,$trigger_fields_on_load;
		if( !isset($form) || empty($form) ){
			return $arf_on_change_function;
		}

		$form_options = maybe_unserialize($form->options);

		$display_summary_buttons = ( isset($form_options['arf_confirmation_summary']) && $form_options['arf_confirmation_summary'] == 1 ) ? true : false;

		if( !$display_summary_buttons ){
			return $arf_on_change_function;
		}

		$exclude_for_summary = array('hidden','break','file', 'arf_product','arf_signature','imagecontrol','captcha','confirm_email','password','confirm_password');

		$exclude_for_summary = apply_filters('arf_exclude_field_for_confirmation_summary',$exclude_for_summary,$field);

		$onchange_fields = array('checkbox', 'radio', 'scale', 'select', 'arfslider', 'arf_smiley','like', 'date','time','colorpicker');
		if( !isset($trigger_fields_on_load) ){
			$trigger_fields_on_load = array();
			$trigger_fields_on_load[$form->id] = array();
		}
		$arf_on_change_function = trim($arf_on_change_function);
		if( $arf_on_change_function == '' ){
			if( !in_array($field['type'],$exclude_for_summary) ){
				if( in_array($field['type'],$onchange_fields) ){
					$arf_on_change_function .= " onchange='clearTimeout(__arf_confirm_handle); __arf_confirm_handle = setTimeout(function(){arf_add_field_to_summary(\"".$data_unique_id."\",\"{$field['id']}\",\"{$field['type']}\");},100);'";
				} else {
					$arf_on_change_function .= " onkeyup='clearTimeout(__arf_confirm_handle); __arf_confirm_handle = setTimeout(function(){arf_add_field_to_summary(\"".$data_unique_id."\",\"{$field['id']}\",\"{$field['type']}\");},100);'";
				}
			}
		} else {
			$arf_on_change_function = substr($arf_on_change_function,0,-1);
			
			if( !in_array($field['type'],$exclude_for_summary) ){
				$arf_on_change_function .= " clearTimeout(__arf_confirm_handle); __arf_confirm_handle = setTimeout(function(){arf_add_field_to_summary(\"".$data_unique_id."\",\"{$field['id']}\",\"{$field['type']}\");},100);'";
			}

		}
		if( !in_array($field['type'],$exclude_for_summary) ){
			$trigger_fields_on_load[$form->id][] = "arf_add_field_to_summary(\"".$data_unique_id."\",\"{$field['id']}\",\"{$field['type']}\");";
		}

		return $arf_on_change_function;
	}

	function arf_add_confirmation_script_from_outside($arf_form, $form, $form_data_id,$arfbrowser_name,$browser_info){
		global $trigger_fields_on_load;

		if( isset($trigger_fields_on_load[$form->id]) && count($trigger_fields_on_load[$form->id]) > 0 ){
			$arf_form .= "<script type='text/javascript' data-cfasync='false'>jQuery(document).ready(function(){";
			
			foreach( $trigger_fields_on_load[$form->id] as $k => $val ){
				$arf_form .= "setTimeout(function(){
					".$val."
				},1000);";
			}

			$arf_form .= "});</script>";
		}

		return $arf_form;
	}

}