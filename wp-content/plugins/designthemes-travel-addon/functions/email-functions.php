<?php
/**
 * filter email from
 */
if ( ! function_exists( 'dt_travel_wp_mail_from' ) ) {
	function dt_travel_wp_mail_from( $email ) {

		$email = '';
		if( cs_get_option('sender-email') != '' )
			$email = cs_get_option('sender-email');
		else
			$email = get_option( 'admin_email' );

		if ( $email ) {
			if ( filter_var( $email, FILTER_VALIDATE_EMAIL ) ) {
				return $email;
			}
		}

		return $email;
	}
}

/**
 * email from name
 */
if ( ! function_exists( 'dt_travel_wp_mail_from_name' ) ) {
	function dt_travel_wp_mail_from_name( $name ) {

		$name = '';
		if( cs_get_option('sender-name') != '' )
			$name = cs_get_option('sender-name');
		else
			$name = get_option( 'blogname' );

		if ( $name ) {
			return $name;
		}

		return $name;
	}
}

/**
 * Filter content type to text/html for email
 */
if ( ! function_exists( 'dt_travel_set_html_content_type' ) ) {

	function dt_travel_set_html_content_type() {
		return 'text/html';
	}
}

/**
 * Place order process send email
 * admin and cusomer
 */
add_action( 'dt_travel_place_order', 'dt_travel_customer_place_order_email', 10, 1 );
if ( ! function_exists( 'dt_travel_customer_place_order_email' ) ) {
	/**
	 * dt_travel_customer_place_order_email
	 *
	 * @param null $order_id
	 *
	 * @return bool|void
	 */
	function dt_travel_customer_place_order_email( $order_id = null ) {
		if ( ! $order_id ) {
			return;
		}

		$settings = array();
		$settings['new-order-recipients'] = get_option( 'admin_email' );
		if( cs_get_option('new-order-email') != '' )
			$settings['new-order-recipients'] = cs_get_option('new-order-email');

		$settings['new-order-email-format'] = 'html';
		if( cs_get_option('new-order-email-format') != '' )
			$settings['new-order-email-format'] = cs_get_option('new-order-email-format');

		$order_items = get_post_meta ( $order_id, '_order_items', TRUE );
		$order_items = is_array ( $order_items ) ? $order_items : array ();

		$order_settings = get_post_meta ( $order_id, '_order_settings', TRUE );
		$order_settings = is_array ( $order_settings ) ? $order_settings : array ();
		$order_settings['order_id'] = $order_id;

		$format  = $settings['new-order-email-format'];
		$headers = "Content-Type: " . ( $format == 'html' ? 'text/html' : 'text/plain' ) . "\r\n";
		// set mail from email
		add_filter( 'wp_mail_from', 'dt_travel_wp_mail_from' );
		// set mail from name
		add_filter( 'wp_mail_from_name', 'dt_travel_wp_mail_from_name' );
		add_filter( 'wp_mail_content_type', 'dt_travel_set_html_content_type' );

		// customer place order email
		$customer_email_subject      = __( 'Order pending', 'designthemes-travel' );
		$customer_email_heading      = __( 'Your order is pending', 'designthemes-travel' );
		$customer_email_heading_desc = __( 'Your order is pending until the payment is completed', 'designthemes-travel' );

		$customer_body = dt_travel_get_template_content( 'emails/order-accepted.php', array(
			'order_settings'	 => $order_settings,
			'order_items'		 => $order_items,
			'email_heading'      => $customer_email_heading,
			'email_heading_desc' => $customer_email_heading_desc
		) );

		if ( ! $customer_body ) {
			return;
		}

		$customer_send = wp_mail( $order_settings['email_address'], $customer_email_subject, $customer_body, $headers );

		// admin place order email
		$admin_email              = $settings['new-order-recipients'];
		$admin_subject            = '[{site_title}] New customer order ({order_number}) - {order_date}';
		$admin_email_heading      = __( 'New customer order', 'designthemes-travel' );
		$admin_email_heading_desc = __( 'You have a new order room', 'designthemes-travel' );

		$find = array(
			'order-date'   => '{order_date}',
			'order-number' => '{order_number}',
			'site-title'   => '{site_title}'
		);

		$replace = array(
			'order-date'   => date_i18n( 'd.m.Y', strtotime( date( 'd.m.Y' ) ) ),
			'order-number' => $order_id,
			'site-title'   => wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES )
		);

		$subject = str_replace( $find, $replace, $admin_subject );

		$body = dt_travel_get_template_content( 'emails/admin/admin-new-order.php', array(
			'order_settings'	 => $order_settings,
			'order_items'		 => $order_items,
			'email_heading'      => $admin_email_heading,
			'email_heading_desc' => $admin_email_heading_desc
		) );

		if ( ! $body ) {
			return;
		}

		$admin_send = wp_mail( $admin_email, $subject, $body, $headers );

		return ( $customer_send && $admin_send );

	}
}

add_action( 'dt_travel_order_status_changed', 'dt_travel_customer_email_order_changes_status', 10, 3 );
if ( ! function_exists( 'dt_travel_customer_email_order_changes_status' ) ) {
	// Send customer when completed
	function dt_travel_customer_email_order_changes_status( $order_id = null, $old_status = null, $new_status = null ) {
		if ( ! ( $order_id || $new_status ) ) {
			return;
		}

		if ( $new_status == 'completed' ) {
			// send customer email
			dt_travel_new_customer_order_email( $order_id );

			// send admin uer
			$enable = cs_get_option('enable-new-order-email');
			if ( $enable ) {
				dt_travel_new_order_email( $order_id );
			}
		} else if ( $new_status == 'cancelled' ) {
			// send customer email
			dt_travel_cancel_customer_order_email( $order_id );

			// send admin uer
			$enable = cs_get_option('enable-cancel-order-email');
			if ( $enable ) {
				dt_travel_cancel_order_email( $order_id );
			}
		}
	}
}

/**
 * Send email to admin after customer ordered room
 *
 * @param int $order_id
 */
if ( ! function_exists( 'dt_travel_new_order_email' ) ) {

	function dt_travel_new_order_email( $order_id = null ) {
		if ( ! $order_id ) {
			return;
		}
		$settings = array();
		$settings['new-order-recipients'] 	= get_option( 'admin_email' );
		if( cs_get_option('new-order-email') != '' )
			$settings['new-order-recipients'] 	= cs_get_option('new-order-email');

		$settings['new-order-subject']	  	= '[{site_title}] Reservation completed ({order_number}) - {order_date}';
		if( cs_get_option('new-order-subject') != '' )
			$settings['new-order-subject']	  	= cs_get_option('new-order-subject');

		$settings['new-order-heading']	 	= __( 'New Order Payment', 'designthemes-travel' );
		if( cs_get_option('new-order-heading') != '' )
			$settings['new-order-heading']	 	= cs_get_option('new-order-heading');

		$settings['new-order-heading-desc'] = __( 'The customer has completed the transaction', 'designthemes-travel' );
		if( cs_get_option('new-order-heading-desc') != '' )
			$settings['new-order-heading-desc'] = cs_get_option('new-order-heading-desc');

		$settings['new-order-email-format'] = 'html';
		if( cs_get_option('new-order-email-format') != '' )
			$settings['new-order-email-format'] = cs_get_option('new-order-email-format');

		$order_items = get_post_meta ( $order_id, '_order_items', TRUE );
		$order_items = is_array ( $order_items ) ? $order_items : array ();

		$order_settings = get_post_meta ( $order_id, '_order_settings', TRUE );
		$order_settings = is_array ( $order_settings ) ? $order_settings : array ();
		$order_settings['order_id'] = $order_id;

		$to                 = $settings['new-order-recipients'];
		$subject            = $settings['new-order-subject'];
		$email_heading      = $settings['new-order-heading'];
		$email_heading_desc = $settings['new-order-heading-desc'];
		$format             = $settings['new-order-email-format'];

		$find = array(
			'order-date'   => '{order_date}',
			'order-number' => '{order_number}',
			'site-title'   => '{site_title}'
		);

		$replace = array(
			'order-date'   => date_i18n( 'd.m.Y', strtotime( date( 'd.m.Y' ) ) ),
			'order-number' => $order_settings['order_id'],
			'site-title'   => wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES )
		);

		$subject = str_replace( $find, $replace, $subject );

		$body = dt_travel_get_template_content( 'emails/admin/admin-new-order.php', array(
			'order_settings'	 => $order_settings,
			'order_items'		 => $order_items,
			'email_heading'      => $email_heading,
			'email_heading_desc' => $email_heading_desc
		) );

		if ( ! $body ) {
			return;
		}

		$headers = "Content-Type: " . ( $format == 'html' ? 'text/html' : 'text/plain' ) . "\r\n";
		$send    = wp_mail( $to, $subject, $body, $headers );

		return $send;
	}
}

// send mail to customer when have new order
if ( ! function_exists( 'dt_travel_new_customer_order_email' ) ) {
	function dt_travel_new_customer_order_email( $order_id = null ) {
		if ( ! $order_id ) {
			return;
		}

		$order_items = get_post_meta ( $order_id, '_order_items', TRUE );
		$order_items = is_array ( $order_items ) ? $order_items : array ();

		$order_settings = get_post_meta ( $order_id, '_order_settings', TRUE );
		$order_settings = is_array ( $order_settings ) ? $order_settings : array ();
		$order_settings['order_id'] = $order_id;

		$email_subject = __( 'Reservation', 'designthemes-travel' );
		if( cs_get_option('email-subject') != '' )
			$email_subject = cs_get_option('email-subject');

		$headers[] = 'Content-Type: text/html; charset=UTF-8';
		// set mail from email
		add_filter( 'wp_mail_from', 'dt_travel_wp_mail_from' );
		// set mail from name
		add_filter( 'wp_mail_from_name', 'dt_travel_wp_mail_from_name' );
		add_filter( 'wp_mail_content_type', 'dt_travel_set_html_content_type' );

		$email_content = dt_travel_get_template_content( 'emails/customer-booking.php', array(
			'order_settings'	 => $order_settings,
			'order_items'		 => $order_items
		) );

		wp_mail( $order_settings['email_address'], $email_subject, stripslashes( $email_content ), $headers );

		remove_filter( 'wp_mail_content_type', 'dt_travel_set_html_content_type' );
	}
}

/**
 * Send email to admin after order has been marked cancelled.
 *
 * @param int $order_id
 */
if ( ! function_exists( 'dt_travel_cancel_order_email' ) ) {

	function dt_travel_cancel_order_email( $order_id = null ) {
		if ( ! $order_id ) {
			return;
		}
		$settings = array();
		$settings['cancel-order-recipients'] 	= get_option( 'admin_email' );
		if( cs_get_option('cancel-order-email') != '' )
			$settings['cancel-order-recipients'] 	= cs_get_option('cancel-order-email');

		$settings['cancel-order-subject']	  	= '[{site_title}] Cancelled Reservation  ({order_number}) - {order_date}';
		if( cs_get_option('cancel-order-subject') != '' )
			$settings['cancel-order-subject']	  	= cs_get_option('cancel-order-subject');

		$settings['cancel-order-heading']	 	= __( 'Cancelled order', 'designthemes-travel' );
		if( cs_get_option('cancel-order-heading') != '' )
			$settings['cancel-order-heading']	 	= cs_get_option('cancel-order-heading');

		$settings['cancel-order-heading-desc']  = __( 'Order has been marked cancelled', 'designthemes-travel' );
		if( cs_get_option('cancel-order-heading-desc') != '' )
			$settings['cancel-order-heading-desc']  = cs_get_option('cancel-order-heading-desc');

		$settings['cancel-order-email-format']  = 'html';
		if( cs_get_option('cancel-order-email-format') != '' )
			$settings['cancel-order-email-format']  = cs_get_option('cancel-order-email-format');

		$order_items = get_post_meta ( $order_id, '_order_items', TRUE );
		$order_items = is_array ( $order_items ) ? $order_items : array ();

		$order_settings = get_post_meta ( $order_id, '_order_settings', TRUE );
		$order_settings = is_array ( $order_settings ) ? $order_settings : array ();
		$order_settings['order_id'] = $order_id;

		$to                 = $settings['cancel-order-recipients'];
		$subject            = $settings['cancel-order-subject'];
		$email_heading      = $settings['cancel-order-heading'];
		$email_heading_desc = $settings['cancel-order-heading-desc'];
		$format             = $settings['cancel-order-email-format'];

		$find = array(
			'order-date'   => '{order_date}',
			'order-number' => '{order_number}',
			'site-title'   => '{site_title}'
		);

		$replace = array(
			'order-date'   => date_i18n( 'd.m.Y', strtotime( date( 'd.m.Y' ) ) ),
			'order-number' => $order_settings['order_id'],
			'site-title'   => wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES )
		);

		$subject = str_replace( $find, $replace, $subject );

		$body = dt_travel_get_template_content( 'emails/admin/admin-cancel-booking.php', array(
			'order_settings'	 => $order_settings,
			'order_items'		 => $order_items,
			'email_heading'      => $email_heading,
			'email_heading_desc' => $email_heading_desc
		) );

		if ( ! $body ) {
			return;
		}

		$headers = "Content-Type: " . ( $format == 'html' ? 'text/html' : 'text/plain' ) . "\r\n";
		$send    = wp_mail( $to, $subject, $body, $headers );

		return $send;
	}
}

// send mail to customer when order has been marked cancelled
if ( ! function_exists( 'dt_travel_cancel_customer_order_email' ) ) {
	function dt_travel_cancel_customer_order_email( $order_id = null ) {
		if ( ! $order_id ) {
			return;
		}

		$order_items = get_post_meta ( $order_id, '_order_items', TRUE );
		$order_items = is_array ( $order_items ) ? $order_items : array ();

		$order_settings = get_post_meta ( $order_id, '_order_settings', TRUE );
		$order_settings = is_array ( $order_settings ) ? $order_settings : array ();
		$order_settings['order_id'] = $order_id;

		$email_subject = __('Cancelled Reservation', 'designthemes-travel');
		if( cs_get_option('cancel-order-subject') != '' )
			$email_subject = cs_get_option('cancel-order-subject');

		$headers[] = 'Content-Type: text/html; charset=UTF-8';
		// set mail from email
		add_filter( 'wp_mail_from', 'dt_travel_wp_mail_from' );
		// set mail from name
		add_filter( 'wp_mail_from_name', 'dt_travel_wp_mail_from_name' );
		add_filter( 'wp_mail_content_type', 'dt_travel_set_html_content_type' );

		$email_content = dt_travel_get_template_content( 'emails/customer-cancelled.php', array(
			'order_settings'	 => $order_settings,
			'order_items'		 => $order_items
		) );

		wp_mail( $order_settings['email_address'], $email_subject, stripslashes( $email_content ), $headers );

		remove_filter( 'wp_mail_content_type', 'dt_travel_set_html_content_type' );
	}
}