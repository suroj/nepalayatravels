<?php
if (! class_exists ( 'DTRoomPostType' )) {
	class DTRoomPostType {

		function __construct() {
			// Add Hook into the 'init()' action
			add_action ( 'init', array (
					$this,
					'dt_init'
			) );

			// Add Hook into the 'admin_init()' action
			add_action ( 'admin_init', array (
					$this,
					'dt_admin_init'
			) );

			// Add Hook into the 'admin_enqueue_scripts' filter
			add_action( 'admin_enqueue_scripts', array (
					$this,
					'dt_travel_admin_scripts'
			) );

			// Add Hook into the 'cs_metabox_options' filter
			add_filter ( 'cs_metabox_options', array (
					$this,
					'dt_rooms_cs_metabox_options'
			) );

			// Add Hook into the 'cs_taxonomy_options' filter
			add_filter ( 'cs_taxonomy_options', array (
					$this,
					'dt_rooms_cs_taxonomy_options'
			) );

			// Add Hook into the 'taxonomy_columns' filter
			add_filter( 'manage_edit-dt_room_capacities_columns', array (
					$this,
					'dt_rooms_custom_room_capacities_taxonomy_columns'
			) );
			
			// Add Hook into the 'taxonomy_columns' filter
			add_filter( 'manage_edit-dt_services_columns', array (
					$this,
					'dt_rooms_custom_services_taxonomy_columns'
			) );

			// Add Hook into the 'taxonomy_custom_columns' filter
			add_filter( 'manage_dt_room_capacities_custom_column', array (
					$this,
					'dt_rooms_custom_room_capacities_taxonomy_columns_content'
			), 10, 3 );

			// Add Hook into the 'taxonomy_custom_columns' filter
			add_filter( 'manage_dt_services_custom_column', array (
					$this,
					'dt_rooms_custom_services_taxonomy_columns_content'
			), 10, 3 );

			// Add Hook into the 'cs_framework_options' filter
			add_filter ( 'cs_framework_options', array (
					$this,
					'dt_rooms_cs_framework_options'
			) );

			// Add Hook into the 'cs_framework_settings' filter
			add_filter ( 'cs_framework_settings', array (
					$this,
					'dt_rooms_cs_framework_settings'
			) );
			
		}

		/**
		 * A function hook that the WordPress core launches at 'init' points
		 */
		function dt_init() {
			$this->createPostType ();
		}

		/**
		 * A function hook that the WordPress core launches at 'admin_init' points
		 */
		function dt_admin_init() {
			add_filter ( "manage_edit-dt_rooms_columns", array (
					$this,
					"dt_rooms_edit_columns" 
			) );
			
			add_action ( "manage_posts_custom_column", array (
					$this,
					"dt_rooms_columns_display" 
			), 10, 2 );
		}

		/**
		 * Date picker admin scripts
		 */
		function dt_travel_admin_scripts( $hook ) {

			if( $hook == "post.php" || $hook == 'post-new.php' ) {

				wp_enqueue_style ( 'dt-travel-datepicker', plugins_url ('designthemes-travel-addon') . '/custom-post-types/css/datepicker.css', array (), false, 'all' );
				wp_enqueue_style ( 'dt-travel-fullcalendar', plugins_url ('designthemes-travel-addon') . '/custom-post-types/css/fullcalendar.min.css', array (), false, 'all' );

				wp_enqueue_script('dt-travel-moment', plugins_url('designthemes-travel-addon') . '/custom-post-types/js/moment.min.js', array ('jquery-ui-datepicker'), false, true );
				wp_enqueue_script('dt-travel-fullcalendar-min', plugins_url('designthemes-travel-addon') . '/custom-post-types/js/fullcalendar.min.js', array (), false, true );

				wp_enqueue_script('dt-travel-admin-scripts', plugins_url('designthemes-travel-addon') . '/custom-post-types/js/admin_scripts.js', array (), false, true );
				wp_localize_script('dt-travel-admin-scripts', 'dtBookingCustom', array(
					'ajaxurl'  => admin_url( 'admin-ajax.php' ),
					'nonce'    => wp_create_nonce( 'dt_booking_nonce' ),
				));
			}
		}

		/**
		 * Creating a post type
		 */
		function createPostType() {

			$roomslug 			= maharaj_cs_get_option( 'single-room-slug', 'dt_rooms' );
			$roomtypeslug  		= maharaj_cs_get_option( 'room-type-slug', 'dt_room_types' );
			$roomcapacityslug   = maharaj_cs_get_option( 'room-capacity-slug', 'dt_room_capacities' );
			$roomserviceslug    = maharaj_cs_get_option( 'room-service-slug', 'dt_services' );
			$roomamenityslug    = maharaj_cs_get_option( 'room-amenity-slug', 'dt_amenities' );

			$labels = array (
				'name' 				 => esc_html__( 'Rooms', 'designthemes-travel' ),
				'all_items' 		 => esc_html__( 'All Rooms', 'designthemes-travel' ),
				'singular_name' 	 => esc_html__( 'Room', 'designthemes-travel' ),
				'add_new' 			 => esc_html__( 'Add New', 'designthemes-travel' ),
				'add_new_item' 		 => esc_html__( 'Add New Room', 'designthemes-travel' ),
				'edit_item' 		 => esc_html__( 'Edit Room', 'designthemes-travel' ),
				'new_item' 			 => esc_html__( 'New Room', 'designthemes-travel' ),
				'view_item' 		 => esc_html__( 'View Room', 'designthemes-travel' ),
				'search_items' 		 => esc_html__( 'Search Rooms', 'designthemes-travel' ),
				'not_found' 		 => esc_html__( 'No Rooms found', 'designthemes-travel' ),
				'not_found_in_trash' => esc_html__( 'No Rooms found in Trash', 'designthemes-travel' ),
				'parent_item_colon'  => esc_html__( 'Parent Room:', 'designthemes-travel' ),
				'menu_name' 		 => esc_html__( 'Rooms', 'designthemes-travel' ) ,
			);

			$args = array (
				'labels' 				=> $labels,
				'hierarchical' 			=> false,
				'description' 			=> esc_html__( 'This is custom post type rooms', 'designthemes-travel' ),
				'supports' 				=> array (
											'title',
											'editor',
											'comments',
											'thumbnail'
										),
				'public' 				=> true,
				'show_ui' 				=> true,
				'show_in_menu' 			=> true,
				'menu_position' 		=> 5,
				'menu_icon' 			=> 'dashicons-admin-home',
				
				'show_in_nav_menus' 	=> true,
				'publicly_queryable' 	=> true,
				'exclude_from_search' 	=> false,
				'has_archive' 			=> true,
				'query_var' 			=> true,
				'can_export' 			=> true,
				'rewrite' 				=> array( 'slug' => $roomslug ),
				'capability_type' 		=> 'post'
			);

			register_post_type ( 'dt_rooms', $args );

			// Room Types Taxonomy
			$labels = array(
				'name'              => esc_html__( 'Room Types', 'designthemes-travel' ),
				'singular_name'     => esc_html__( 'Room Type', 'designthemes-travel' ),
				'search_items'      => esc_html__( 'Search Room Types', 'designthemes-travel' ),
				'all_items'         => esc_html__( 'All Room Types', 'designthemes-travel' ),
				'parent_item'       => esc_html__( 'Parent Room Type', 'designthemes-travel' ),
				'parent_item_colon' => esc_html__( 'Parent Room Type:', 'designthemes-travel' ),
				'edit_item'         => esc_html__( 'Edit Room Type', 'designthemes-travel' ),
				'update_item'       => esc_html__( 'Update Room Type', 'designthemes-travel' ),
				'add_new_item'      => esc_html__( 'Add New Room Type', 'designthemes-travel' ),
				'new_item_name'     => esc_html__( 'New Room Type Name', 'designthemes-travel' ),
				'menu_name'         => esc_html__( 'Room Types', 'designthemes-travel' ),
			);

			register_taxonomy ( 'dt_room_types', array (
				'dt_rooms'
			), array (
				'hierarchical' 		=> true,
				'labels' 			=> $labels,
				'show_admin_column' => true,
				'rewrite' 			=> array( 'slug' => $roomtypeslug ),
				'query_var' 		=> true
			) );

			// Room Capacities Taxonomy
			$labels = array(
				'name'              		 => esc_html__( 'Room Capacities', 'designthemes-travel' ),
				'singular_name'     		 => esc_html__( 'Room Capacity', 'designthemes-travel' ),
				'search_items'      		 => esc_html__( 'Search Room Capacities', 'designthemes-travel' ),
				'popular_items'     		 => esc_html__( 'Popular Room Capacities', 'designthemes-travel' ),
				'all_items'         		 => esc_html__( 'All Room Capacities', 'designthemes-travel' ),
				'parent_item'       		 => null,
				'parent_item_colon' 		 => null,
				'edit_item'         		 => esc_html__( 'Edit Room Capacity', 'designthemes-travel' ),
				'update_item'       		 => esc_html__( 'Update Room Capacity', 'designthemes-travel' ),
				'add_new_item'      		 => esc_html__( 'Add New Room Capacity', 'designthemes-travel' ),
				'new_item_name'     		 => esc_html__( 'New Room Capacity Name', 'designthemes-travel' ),
				'separate_items_with_commas' => esc_html__( 'Separate Capacities with commas', 'designthemes-travel' ),
				'add_or_remove_items'        => esc_html__( 'Add or remove Capacities', 'designthemes-travel' ),
				'choose_from_most_used'      => esc_html__( 'Choose from the most used Capacities', 'designthemes-travel' ),
				'not_found'                  => esc_html__( 'No Capacities found.', 'designthemes-travel' ),				
				'menu_name'        			 => esc_html__( 'Room Capacities', 'designthemes-travel' ),
			);

			register_taxonomy ( 'dt_room_capacities', array (
				'dt_rooms'
			), array (
				'hierarchical' 		=> false,
				'labels' 			=> $labels,
				'show_admin_column' => true,
				'meta_box_cb'		=> false,
				'show_in_quick_edit'=> false,
				'rewrite' 			=> array( 'slug' => $roomcapacityslug ),
				'query_var' 		=> true
			) );

			// Services Taxonomy
			$labels = array(
				'name'              		 => esc_html__( 'Services', 'designthemes-travel' ),
				'singular_name'     		 => esc_html__( 'Service', 'designthemes-travel' ),
				'search_items'      		 => esc_html__( 'Search Services', 'designthemes-travel' ),
				'popular_items'     		 => esc_html__( 'Popular Services', 'designthemes-travel' ),
				'all_items'         		 => esc_html__( 'All Services', 'designthemes-travel' ),
				'parent_item'       		 => null,
				'parent_item_colon' 		 => null,
				'edit_item'         		 => esc_html__( 'Edit Service', 'designthemes-travel' ),
				'update_item'       		 => esc_html__( 'Update Service', 'designthemes-travel' ),
				'add_new_item'      		 => esc_html__( 'Add New Service', 'designthemes-travel' ),
				'new_item_name'     		 => esc_html__( 'New Service Name', 'designthemes-travel' ),
				'separate_items_with_commas' => esc_html__( 'Separate Services with commas', 'designthemes-travel' ),
				'add_or_remove_items'        => esc_html__( 'Add or remove Services', 'designthemes-travel' ),
				'choose_from_most_used'      => esc_html__( 'Choose from the most used Services', 'designthemes-travel' ),
				'not_found'                  => esc_html__( 'No Services found.', 'designthemes-travel' ),				
				'menu_name'        			 => esc_html__( 'Additional Services', 'designthemes-travel' ),
			);

			register_taxonomy ( 'dt_services', array (
				'dt_rooms'
			), array (
				'hierarchical' 		=> false,
				'labels' 			=> $labels,
				'show_admin_column' => true,
				'meta_box_cb'		=> false,
				'show_in_quick_edit'=> false,
				'rewrite' 			=> array( 'slug' => $roomserviceslug ),
				'query_var' 		=> true
			) );

			// Amenities Taxonomy
			$labels = array(
				'name'              		 => esc_html__( 'Amenities', 'designthemes-travel' ),
				'singular_name'     		 => esc_html__( 'Amenity', 'designthemes-travel' ),
				'search_items'      		 => esc_html__( 'Search Amenities', 'designthemes-travel' ),
				'popular_items'     		 => esc_html__( 'Popular Amenities', 'designthemes-travel' ),
				'all_items'         		 => esc_html__( 'All Amenities', 'designthemes-travel' ),
				'parent_item'       		 => null,
				'parent_item_colon' 		 => null,
				'edit_item'         		 => esc_html__( 'Edit Amenity', 'designthemes-travel' ),
				'update_item'       		 => esc_html__( 'Update Amenity', 'designthemes-travel' ),
				'add_new_item'      		 => esc_html__( 'Add New Amenity', 'designthemes-travel' ),
				'new_item_name'     		 => esc_html__( 'New Amenity Name', 'designthemes-travel' ),
				'separate_items_with_commas' => esc_html__( 'Separate Amenities with commas', 'designthemes-travel' ),
				'add_or_remove_items'        => esc_html__( 'Add or remove Amenities', 'designthemes-travel' ),
				'choose_from_most_used'      => esc_html__( 'Choose from the most used Amenities', 'designthemes-travel' ),
				'not_found'                  => esc_html__( 'No Amenities found.', 'designthemes-travel' ),				
				'menu_name'        			 => esc_html__( 'Amenities', 'designthemes-travel' ),
			);

			register_taxonomy ( 'dt_amenities', array (
				'dt_rooms'
			), array (
				'hierarchical' 		=> false,
				'labels' 			=> $labels,
				'show_admin_column' => false,
				'show_in_quick_edit'=> true,
				'rewrite' 			=> array( 'slug' => $roomamenityslug ),
				'query_var' 		=> true
			) );
		}

		/**
		 * Rooms metabox options
		 */
		function dt_rooms_cs_metabox_options( $options ) {

			$symbol = dt_travel_get_currency_symbol();
			$roomid = isset($_REQUEST['post']) ? $_REQUEST['post'] : '';

			$options[]    = array(
			  'id'        => '_room_settings',
			  'title'     => esc_html__('Custom Room Options', 'designthemes-travel'),
			  'post_type' => 'dt_rooms',
			  'context'   => 'normal',
			  'priority'  => 'default',
			  'sections'  => array(

				array(
				  'name'  => 'general_section',
				  'title' => esc_html__('General Options', 'designthemes-travel'),
				  'icon'  => 'fa fa-cogs',

				  'fields' => array(

					array(
						'id'      => 'enable-sub-title',
						'type'    => 'switcher',
						'title'   => esc_html__('Show Breadcrumb', 'designthemes-travel' ),
						'default' => true
					),

					array(
						'id'	  => 'breadcrumb_position',
						'type'    => 'select',
						'title'   => esc_html__('Position', 'designthemes-travel' ),
						'options' => array(
							'header-top-absolute'    => esc_html__('Behind the Header','designthemes-travel'),
							'header-top-relative' 	   => esc_html__('Default','designthemes-travel'),
						),
						'default'    => 'header-top-relative',
						'dependency'  => array( 'enable-sub-title', '==', 'true' ),
					),

					array(
					  'id'    => 'breadcrumb_background',
					  'type'  => 'background',
					  'title' => esc_html__('Background', 'designthemes-travel'),
					  'desc'  => esc_html__('Choose background options for breadcrumb title section.', 'designthemes-travel'),
					  'dependency'   => array( 'enable-sub-title', '==', 'true' ),
					),

					array(
					  'id'      	 => 'layout',
					  'type'         => 'image_select',
					  'title'        => esc_html__('Layout', 'designthemes-travel'),
					  'options'      => array(
						'content-full-width'   => MAHARAJ_THEME_URI . '/cs-framework-override/images/without-sidebar.png',
						'with-left-sidebar'    => MAHARAJ_THEME_URI . '/cs-framework-override/images/left-sidebar.png',
						'with-right-sidebar'   => MAHARAJ_THEME_URI . '/cs-framework-override/images/right-sidebar.png',
					  ),
					  'default'      => 'content-full-width',
					  'attributes'   => array(
						'data-depend-id' => 'layout',
					  ),
					),

					array(
					  'id'  		 => 'show-standard-sidebar-left',
					  'type'  		 => 'switcher',
					  'title' 		 => esc_html__('Show Standard Left Sidebar', 'designthemes-travel'),
					  'dependency'   => array( 'layout', 'any', 'with-left-sidebar' ),
					),

					array(
					  'id'  		 => 'widget-area-left',
					  'type'  		 => 'select',
					  'title' 		 => esc_html__('Choose Widget Area - Left Sidebar', 'designthemes-travel'),
					  'class'		 => 'chosen',
					  'options'   	 => maharaj_custom_widgets(),
					  'attributes'   => array(
					  	'multiple'  	   => 'multiple',
						'data-placeholder' => esc_attr__('Select Widget Areas', 'designthemes-travel'),
					    'style' 		   => 'width: 400px;'
					  ),
					  'dependency'   => array( 'layout', 'any', 'with-left-sidebar' ),
					),

					array(
					  'id'  		 => 'show-standard-sidebar-right',
					  'type'  		 => 'switcher',
					  'title' 		 => esc_html__('Show Standard Right Sidebar', 'designthemes-travel'),
					  'dependency'   => array( 'layout', 'any', 'with-right-sidebar' ),
					),

					array(
					  'id'  		 => 'widget-area-right',
					  'type'  		 => 'select',
					  'title' 		 => esc_html__('Choose Widget Area - Right Sidebar', 'designthemes-travel'),
					  'class'		 => 'chosen',
					  'options'   	 => maharaj_custom_widgets(),
					  'attributes'   => array(
					  	'multiple'  	   => 'multiple',
						'data-placeholder' => esc_attr__('Select Widget Areas', 'designthemes-travel'),
					    'style' 		   => 'width: 400px;'
					  ),
					  'dependency'   => array( 'layout', 'any', 'with-right-sidebar' ),
					),

				  ), // end: fields
				), // end: a section

				array(
				  'name'  => 'gallery_section',
				  'title' => esc_html__('Gallery Options', 'designthemes-travel'),
				  'icon'  => 'fa fa-picture-o',
				  
				  'fields' => array(
				  
					array(
					  'id'          => 'room-gallery',
					  'type'        => 'gallery',
					  'title'       => esc_html__('Gallery Images', 'designthemes-travel'),
					  'desc'        => esc_html__('Simply add images to gallery items.', 'designthemes-travel'),
					  'add_title'   => esc_html__('Add Images', 'designthemes-travel'),
					  'edit_title'  => esc_html__('Edit Images', 'designthemes-travel'),
					  'clear_title' => esc_html__('Remove Images', 'designthemes-travel')
					),

				  ), // end: fields
				), // end: a section

				array(
				  'name'  => 'capacity_section',
				  'title' => esc_html__('Capacity Options', 'designthemes-travel'),
				  'icon'  => 'fa fa-car',

				  'fields' => array(

					array(
					  'id'      => 'room-qty',
					  'type'    => 'number',
					  'title'   => esc_html__('Quantity', 'designthemes-travel'),
					  'default' => 5,
					  'desc'    => esc_html__('The number of rooms.', 'designthemes-travel')
					),

					array(
					  'id'          => 'room-adults',
					  'type'        => 'select',
					  'title'       => esc_html__('Number of adults', 'designthemes-travel'),
					  'options'     => 'tags',
					  'class'       => 'chosen',
					  'query_args'  => array(
					  	'taxonomies'   => array( 'post_tag', 'dt_room_capacities' ),
						'order'        => 'ASC',
						'hide_empty'   => false
					  )
					),

					array(
					  'id'      => 'room-childs',
					  'type'    => 'number',
					  'title'   => esc_html__('Max children per room', 'designthemes-travel'),
					  'default' => 1
					),

					array(
					  'id'       => 'additional-info',
					  'type'     => 'wysiwyg',
					  'before'   => '<h4>'.esc_html__('Addition Information', 'designthemes-travel').'</h4>',
					  'settings' => array(
						'textarea_rows' => 5,
					  )
					),

				  ), // end: fields
				), // end: a section
				
				array(
				  'name'  => 'pricing_section',
				  'title' => esc_html__('Pricing Options', 'designthemes-travel'),
				  'icon'  => 'fa fa-dollar',

				  'fields' => array(

					array(
					  'id'        => 'regular_prices',
					  'type'      => 'fieldset',
					  'title'     => esc_html__('Regular Price', 'designthemes-travel'),
					  'fields'    => array(

						array(
						  'type'    => 'subheading',
						  'content' => esc_html__("Week Day's Pricing", 'designthemes-travel'),
						),

						array(
						  'id'      => 'price_day_1',
						  'type'    => 'text',
						  'title'   => esc_html__('Sunday', 'designthemes-travel'),
						  'default' => '0',
						  'before'  => $symbol,
						  'attributes'  => array(
						  	 'style'    => 'width: 100px; height: 30px;'
						  )
						),

						array(
						  'id'      => 'price_day_2',
						  'type'    => 'text',
						  'title'   => esc_html__('Monday', 'designthemes-travel'),
						  'before'  => $symbol,
  						  'default' => '0',
						  'attributes'  => array(
						  	 'style'    => 'width: 100px; height: 30px;'
						  )
						),

						array(
						  'id'      => 'price_day_3',
						  'type'    => 'text',
						  'title'   => esc_html__('Tuesday', 'designthemes-travel'),
						  'before'  => $symbol,
  						  'default' => '0',
						  'attributes'  => array(
						  	 'style'    => 'width: 100px; height: 30px;'
						  )
						),

						array(
						  'id'      => 'price_day_4',
						  'type'    => 'text',
						  'title'   => esc_html__('Wednesday', 'designthemes-travel'),
						  'before'  => $symbol,
  						  'default' => '0',
						  'attributes'  => array(
						  	 'style'    => 'width: 100px; height: 30px;'
						  )
						),

						array(
						  'id'      => 'price_day_5',
						  'type'    => 'text',
						  'title'   => esc_html__('Thursday', 'designthemes-travel'),
						  'before'  => $symbol,
  						  'default' => '0',
						  'attributes'  => array(
						  	 'style'    => 'width: 100px; height: 30px;'
						  )
						),

						array(
						  'id'      => 'price_day_6',
						  'type'    => 'text',
						  'title'   => esc_html__('Friday', 'designthemes-travel'),
						  'before'  => $symbol,
  						  'default' => '0',
						  'attributes'  => array(
						  	 'style'    => 'width: 100px; height: 30px;'
						  )
						),

						array(
						  'id'      => 'price_day_7',
						  'type'    => 'text',
						  'title'   => esc_html__('Saturday', 'designthemes-travel'),
						  'before'  => $symbol,
  						  'default' => '0',
						  'attributes'  => array(
						  	 'style'    => 'width: 100px; height: 30px;'
						  )
						),

					  ),
					),

					array(
					  'id'          => 'room-services',
					  'type'        => 'select',
					  'title'       => esc_html__('Addition Package', 'designthemes-travel'),
					  'options'     => 'tags',
					  'class'       => 'chosen',
					  'query_args'  => array(
					  	'taxonomies'   => array( 'post_tag', 'dt_services' ),
						'order'        => 'ASC',
						'hide_empty'   => false
					  ),
					  'attributes'  => array(
						'multiple'  => 'only-key',
						'style'     => 'width: 245px;'
					  ),
					  'info'        => 'Choose any services for this room.',
					),

				  ), // end: fields
				), // end: a section

				array(
				  'name'  => 'pricing_plan_section',
				  'title' => esc_html__('Pricing Plans', 'designthemes-travel'),
				  'icon'  => 'fa fa-calendar-check-o',

				  'fields' => array(

					array(
					  'id'              => 'pricing-range',
					  'type'            => 'group',
					  'title'           => esc_html__('Prices within Date Range', 'designthemes-travel'),
					  'button_title'    => esc_html__('Add New', 'designthemes-travel'),
					  'accordion_title' => esc_html__('Add New Plan', 'designthemes-travel'),
					  'fields'          => array(

						array(
						  'id'          => 'date-from',
						  'type'        => 'text',
						  'title'       => esc_html__('Date From', 'designthemes-travel'),
						  'attributes'  => array(
						  	 'style'    => 'width: 150px; height: 30px;'
						  ),
						  'class'		=> 'dt_datepicker'
						),

						array(
						  'id'          => 'date-to',
						  'type'        => 'text',
						  'title'       => esc_html__('Date To', 'designthemes-travel'),
						  'attributes'  => array(
						  	 'style'    => 'width: 150px; height: 30px;'
						  ),
						  'class'		=> 'dt_datepicker'
						),

						array(
						  'type'    => 'subheading',
						  'content' => esc_html__("Week Day's Pricing", 'designthemes-travel'),
						),

						array(
						  'id'      => 'price_day_1',
						  'type'    => 'text',
						  'title'   => esc_html__('Sunday', 'designthemes-travel'),
						  'before'  => $symbol,
						  'attributes'  => array(
						  	 'style'    => 'width: 100px; height: 30px;'
						  )
						),

						array(
						  'id'      => 'price_day_2',
						  'type'    => 'text',
						  'title'   => esc_html__('Monday', 'designthemes-travel'),
						  'before'  => $symbol,
						  'attributes'  => array(
						  	 'style'    => 'width: 100px; height: 30px;'
						  )
						),

						array(
						  'id'      => 'price_day_3',
						  'type'    => 'text',
						  'title'   => esc_html__('Tuesday', 'designthemes-travel'),
						  'before'  => $symbol,
						  'attributes'  => array(
						  	 'style'    => 'width: 100px; height: 30px;'
						  )
						),

						array(
						  'id'      => 'price_day_4',
						  'type'    => 'text',
						  'title'   => esc_html__('Wednesday', 'designthemes-travel'),
						  'before'  => $symbol,
						  'attributes'  => array(
						  	 'style'    => 'width: 100px; height: 30px;'
						  )
						),

						array(
						  'id'      => 'price_day_5',
						  'type'    => 'text',
						  'title'   => esc_html__('Thursday', 'designthemes-travel'),
						  'before'  => $symbol,
						  'attributes'  => array(
						  	 'style'    => 'width: 100px; height: 30px;'
						  )
						),

						array(
						  'id'      => 'price_day_6',
						  'type'    => 'text',
						  'title'   => esc_html__('Friday', 'designthemes-travel'),
						  'before'  => $symbol,
						  'attributes'  => array(
						  	 'style'    => 'width: 100px; height: 30px;'
						  )
						),

						array(
						  'id'      => 'price_day_7',
						  'type'    => 'text',
						  'title'   => esc_html__('Saturday', 'designthemes-travel'),
						  'before'  => $symbol,
						  'attributes'  => array(
						  	 'style'    => 'width: 100px; height: 30px;'
						  )
						),						

					  )
					),

						array(
						  'type'    => 'notice',
						  'class'	=> 'dt-fullcalendar',
						  'content' => '<h2 class="dt-fullcalendar-month">'.date('F, Y').'</h2>'
						  .'<div class="dt-fullcalendar-toolbar">
							  <div class="fc-right">
								<div class="fc-button-group">
								  <button type="button" class="fc-prev-button fc-button fc-state-default fc-corner-left" data-month="'.(date('m')-1).date('/d/Y').'" data-room="'.$roomid.'">
									  <span class="fc-icon fc-icon-left-single-arrow"></span>
								  </button>
								  <button type="button" class="fc-next-button fc-button fc-state-default fc-corner-right" data-month="'.(date('m')+1).date('/d/Y').'" data-room="'.$roomid.'">
									  <span class="fc-icon fc-icon-right-single-arrow"></span>
								  </button>
								</div>
							  </div>
							</div>'
						  .'<div id="dt-fullcalendar" data-events='.dt_travel_print_pricing_json( $roomid, date( 'Y-m-d' ) ).'></div>',
						),

				  ), // end: fields
				), // end: a section

			  ),
			);

			return $options;
		}

		/**
		 * Room taxonomy options
		 */
		function dt_rooms_cs_taxonomy_options( $options ) {

			$symbol = dt_travel_get_currency_symbol();

			$options[]   = array(
			  'id'       => '_room_capacity_options',
			  'taxonomy' => 'dt_room_capacities',
			  'fields'   => array(

				array(
				  'id'     => 'room_capacity',
				  'type'   => 'number',
				  'title'  => esc_html__('Capacity', 'designthemes-travel'),
	              'after'  => '<p class="description">'.esc_html__('The capacity is no.of persons occupy in a room. (ex: 5)', 'designthemes-travel').'</p>',
				),

			  ),
			);

			$options[]   = array(
			  'id'       => '_room_services',
			  'taxonomy' => 'dt_services',
			  'fields'   => array(

				array(
				  'id'     => 'service_price',
				  'type'   => 'text',
				  'title'  => esc_html__('Price', 'designthemes-travel'),
				  'before' => $symbol,
	              'after'  => '<p class="description">'.esc_html__('The price is cost of a service. (ex: 5)', 'designthemes-travel').'</p>'
				),

			  ),
			);

			$options[]   = array(
			  'id'       => '_room_amenities',
			  'taxonomy' => 'dt_amenities',
			  'fields'   => array(

				array(
				  'id'        => 'amenity_icon',
				  'type'      => 'image',
				  'title'     => esc_html__('Icon', 'designthemes-travel'),
				  'after'  	  => '<p class="description">'.esc_html__('Click button to insert amenity Icon.', 'designthemes-travel').'</p>',
				  'add_title' => esc_html__('Add Icon', 'designthemes-travel')
				),

			  ),
			);

			return $options;
		}

		/**
		 * Room capacities taxonomy columns
		 */
		function dt_rooms_custom_room_capacities_taxonomy_columns( $columns ) {
			
			unset( $columns['slug'], $columns['posts'] );

			$newcolumns = array (
				"dt_capacity" => '<a href="javascript:void(0)"><span>'.esc_html__('Capacity', 'designthemes-travel').'</span></a>',
			);

			$columns = array_merge ( $columns, $newcolumns );
			return $columns;
		}

		/**
		 * Room services taxonomy columns
		 */
		function dt_rooms_custom_services_taxonomy_columns( $columns ) {
			
			unset( $columns['slug'], $columns['posts'] );

			$newcolumns = array (
				"dt_service_price" => '<a href="javascript:void(0)"><span>'.esc_html__('Price', 'designthemes-travel').'</span></a>',
			);

			$columns = array_merge ( $columns, $newcolumns );
			return $columns;
		}

		/**
		 * Room capacities taxonomy columns content
		 */
		function dt_rooms_custom_room_capacities_taxonomy_columns_content( $content, $column_name, $term_id ) {

			$out = '';

			if ( 'dt_capacity' == $column_name ) {
				$meta = get_term_meta( $term_id, '_room_capacity_options', false );
				$content = !empty($meta[0]['room_capacity']) ? $meta[0]['room_capacity'] : '';

				$out .= !empty($content) ? "<span>{$content}</span>" : '';
			}
			$content = $out;

			return $content;
		}

		/**
		 * Room services taxonomy columns content
		 */
		function dt_rooms_custom_services_taxonomy_columns_content( $content, $column_name, $term_id ) {

			$out = '';

			if ( 'dt_service_price' == $column_name ) {
				$meta = get_term_meta( $term_id, '_room_services', false );
				$content = !empty($meta[0]['service_price']) ? $meta[0]['service_price'] : '';

				$symbol = dt_travel_get_currency_symbol();

				$out .= !empty($content) ? "<span>{$symbol}{$content}</span>" : '';
			}
			$content = $out;

			return $content;
		}

		/**
		 * Room framework options
		 */
		function dt_rooms_cs_framework_options( $options ) {

			$currencies = array();
			$currency_codes = dt_travel_get_currencies();
			foreach( $currency_codes as $code => $value ){
				$currencies[$code] = $value . ' ('. dt_travel_get_currency_symbol( $code ) .')';
			}

			$options[]      = array(
			  'name'        => 'rooms',
			  'title'       => esc_html__('Room Booking', 'designthemes-travel'),
			  'icon'        => 'fa fa-calendar',
			  'sections'	=> array(

				// -----------------------------------------
				// General Options
				// -----------------------------------------
				array(
				  'name'      => 'travel_general_options',
				  'title'     => esc_html__('General Options', 'designthemes-travel'),
				  'icon'      => 'fa fa-gear',

					'fields'  => array(

					  array(
						'type'    => 'subheading',
						'content' => esc_html__( "General Options", 'designthemes-travel' ),
					  ),

					  array(
						'id'           => 'booking-search-pageid',
						'type'         => 'select',
						'title'        => esc_html__('Search Page', 'designthemes-travel'),
						'options'      => 'pages',
						'class'        => 'chosen',
						'default_option' => esc_html__('Choose the page', 'designthemes-travel'),
						'info'       	 => esc_html__('Choose the page for booking search.', 'designthemes-travel')
					  ),

					  array(
						'id'           => 'booking-cart-pageid',
						'type'         => 'select',
						'title'        => esc_html__('Cart Page', 'designthemes-travel'),
						'options'      => 'pages',
						'class'        => 'chosen',
						'default_option' => esc_html__('Choose the page', 'designthemes-travel'),
						'info'       	 => esc_html__('Choose the page for booking cart.', 'designthemes-travel')
					  ),

					  array(
						'id'           => 'booking-checkout-pageid',
						'type'         => 'select',
						'title'        => esc_html__('Checkout Page', 'designthemes-travel'),
						'options'      => 'pages',
						'class'        => 'chosen',
						'default_option' => esc_html__('Choose the page', 'designthemes-travel'),
						'info'       	 => esc_html__('Choose the page for booking checkout.', 'designthemes-travel')
					  ),

					  array(
						'id'           => 'user-account-pageid',
						'type'         => 'select',
						'title'        => esc_html__('Account Page', 'designthemes-travel'),
						'options'      => 'pages',
						'class'        => 'chosen',
						'default_option' => esc_html__('Choose the page', 'designthemes-travel'),
						'info'       	 => esc_html__('Choose the page for user account.', 'designthemes-travel')
					  ),

					  array(
						'id'           => 'terms-pageid',
						'type'         => 'select',
						'title'        => esc_html__('Terms And Conditions Page', 'designthemes-travel'),
						'options'      => 'pages',
						'class'        => 'chosen',
						'default_option' => esc_html__('Choose the page', 'designthemes-travel'),
						'info'       	 => esc_html__('Choose the page for terms & conditions.', 'designthemes-travel')
					  ),

					  array(
						'id'           => 'book-currency',
						'type'         => 'select',
						'title'        => esc_html__('Currency', 'designthemes-travel'),
						'options'      => $currencies,
						'class'        => 'chosen',
						'default' 	   => 'USD'
					  ),

					  array(
						'id'           => 'currency-pos',
						'type'         => 'select',
						'title'        => esc_html__('Currency Position', 'designthemes-travel'),
						'options'      => array(
						  'left' 			 => esc_html__('Left ( $36.55 )', 'designthemes-travel'),
						  'right'      		 => esc_html__('Right ( 36.55$ )', 'designthemes-travel'),
						  'left-with-space'  => esc_html__('Left with space ( $ 36.55 )', 'designthemes-travel'),
						  'right-with-space' => esc_html__('Right with space ( 36.55 $ )', 'designthemes-travel'),
						),
						'class'        => 'chosen',
					  ),

					  array(
						'id'  		 => 'price-decimal',
						'type'  	 => 'number',
						'title' 	 => esc_html__('Number of decimal', 'designthemes-travel'),
						'after'		 => '<span class="cs-text-desc">&nbsp;'.esc_html__('No.of decimals in price', 'designthemes-travel').'</span>',
						'default' 	 => 1,
					  ),

					  array(
						'id'  		 => 'min-book-day',
						'type'  	 => 'number',
						'title' 	 => esc_html__('Minimum booking day', 'designthemes-travel'),
						'after'		 => '<span class="cs-text-desc">&nbsp;'.esc_html__('Atleast 1 day by default.', 'designthemes-travel').'</span>',
						'default' 	 => 1,
					  ),

					  array(
						'id'  		 => 'include-tax',
						'type'  	 => 'switcher',
						'title' 	 => esc_html__('Include Tax', 'designthemes-travel'),
						'info'		 => esc_html__('YES! to include tax price', 'designthemes-travel'),
						'default'	 => false,
					  ),

					  array(
						'id'  		 => 'tax-percentage',
						'type'  	 => 'number',
						'title' 	 => esc_html__('Tax Percentage', 'designthemes-travel'),
						'after'		 => '<span class="cs-text-desc">&nbsp;'.esc_html__('%', 'designthemes-travel').'</span>',
						'default' 	 => 10,
						'dependency'   => array( 'include-tax', '==', 'true' ),
					  ),

					  array(
						'id'  		 => 'enable-advance',
						'type'  	 => 'switcher',
						'title' 	 => esc_html__('Advance Payment', 'designthemes-travel'),
						'info'		 => esc_html__('YES! to enable advance payment', 'designthemes-travel'),
						'default'	 => false,
					  ),

					  array(
						'id'  		 => 'advance-percentage',
						'type'  	 => 'number',
						'title' 	 => esc_html__('Advance Percentage', 'designthemes-travel'),
						'after'		 => '<span class="cs-text-desc">&nbsp;'.esc_html__('%', 'designthemes-travel').'</span>',
						'default' 	 => 50,
						'dependency'   => array( 'enable-advance', '==', 'true' ),
					  ),

					),
				),

				// -----------------------------------------
				// Email Options
				// -----------------------------------------
				array(
				  'name'      => 'travel_email_options',
				  'title'     => esc_html__('Email Options', 'designthemes-travel'),
				  'icon'      => 'fa fa-envelope-o',

					'fields'      => array(

					  array(
						'type'    => 'subheading',
						'content' => esc_html__( 'Email Sender', 'designthemes-travel' ),
					  ),

					  array(
						'id'          => 'sender-name',
						'type'        => 'text',
						'title'       => esc_html__('From name', 'designthemes-travel'),
						'default'	  => get_bloginfo('title')
					  ),

					  array(
						'id'          => 'sender-email',
						'type'        => 'text',
						'title'       => esc_html__('From Email', 'designthemes-travel'),
						'default'	  => get_bloginfo('admin_email')
					  ),

					  array(
						'id'          => 'email-subject',
						'type'        => 'text',
						'title'       => esc_html__('Email subject', 'designthemes-travel'),
						'default'	  => esc_html__('Sub: Reservation', 'designthemes-travel')
					  ),

					  array(
						'type'    => 'subheading',
						'content' => esc_html__( 'New Order Email Options', 'designthemes-travel' ),
						'after'	  => '<p><span class="cs-text-desc">'.esc_html__('New order emails are sent to chosen recipient(s) when a order is received.', 'designthemes-travel' ).'</span></p>'
					  ),

					  array(
						'id'  		 => 'enable-new-order-email',
						'type'  	 => 'switcher',
						'title' 	 => esc_html__('Enable', 'designthemes-travel'),
						'default'	 => false,
					  ),

					  array(
						'id'         => 'new-order-email',
						'type'       => 'text',
						'title'      => esc_html__('Recipient(s)', 'designthemes-travel'),
						'after'		 => '<p class="cs-text">'.esc_html__('Enter recipients (comma separated) for this email. Defaults to ', 'designthemes-travel').get_bloginfo('admin_email').'.</p>',
						'attributes'    => array(
						  'placeholder' => get_bloginfo('admin_email'),
						),
					  ),

					  array(
						'id'         => 'new-order-subject',
						'type'       => 'text',
						'title'      => esc_html__('Subject', 'designthemes-travel'),
						'after'		 => '<p class="cs-text">'.esc_html__('Enter subject of new order email.', 'designthemes-travel').'</p>',
						'attributes'    => array(
						  'placeholder' => '[{site_title}] Reservation completed ({order_number}) - {order_date}',
						  'style'		=> 'width: 75%;'
						),
					  ),

					  array(
						'id'         => 'new-order-heading',
						'type'       => 'text',
						'title'      => esc_html__('Email Heading', 'designthemes-travel'),
						'after'		 => '<p class="cs-text">'.esc_html__('The main heading displays in the top of email. Default heading: New customer order.', 'designthemes-travel').'</p>',
						'attributes'    => array(
						  'placeholder' => esc_attr__('New Order Payment', 'designthemes-travel')
						),
					  ),

					  array(
						'id'         => 'new-order-heading-desc',
						'type'       => 'text',
						'title'      => esc_html__('Email Heading Description', 'designthemes-travel'),
						'attributes'    => array(
						  'placeholder' => esc_html__('The customer has completed the transaction', 'designthemes-travel')
						),
					  ),

					  array(
						'id'           => 'new-order-email-format',
						'type'         => 'select',
						'title'        => esc_html__('Email Format', 'designthemes-travel'),
						'options'      => array(
						  'plain' => esc_html__('Plain Text', 'designthemes-travel'),
						  'html'  => esc_html__('HTML', 'designthemes-travel')
						),
						'class'        => 'chosen',
					  ),

					  array(
						'type'    => 'subheading',
						'content' => esc_html__( 'Cancel Order Email Options', 'designthemes-travel' ),
						'after'	  => '<p><span class="cs-text-desc">'.esc_html__('Cancel order emails are sent to chosen recipient(s) when order has been marked cancelled.', 'designthemes-travel' ).'</span></p>'
					  ),

					  array(
						'id'  		 => 'enable-cancel-order-email',
						'type'  	 => 'switcher',
						'title' 	 => esc_html__('Enable', 'designthemes-travel'),
						'default'	 => false,
					  ),

					  array(
						'id'         => 'cancel-order-email',
						'type'       => 'text',
						'title'      => esc_html__('Recipient(s)', 'designthemes-travel'),
						'after'		 => '<p class="cs-text">'.esc_html__('Enter recipients (comma separated) for this email. Defaults to ', 'designthemes-travel').get_bloginfo('admin_email').'.</p>',
						'attributes'    => array(
						  'placeholder' => get_bloginfo('admin_email'),
						),
					  ),

					  array(
						'id'         => 'cancel-order-subject',
						'type'       => 'text',
						'title'      => esc_html__('Subject', 'designthemes-travel'),
						'after'		 => '<p class="cs-text">'.esc_html__('Enter subject of cancel order email.', 'designthemes-travel').'</p>',
						'attributes'    => array(
						  'placeholder' => '[{site_title}] Cancelled Reservation  ({order_number}) - {order_date}',
						  'style'		=> 'width: 75%;'
						),
					  ),

					  array(
						'id'         => 'cancel-order-heading',
						'type'       => 'text',
						'title'      => esc_html__('Email Heading', 'designthemes-travel'),
						'after'		 => '<p class="cs-text">'.esc_html__('The main heading displays in the top of email. Default heading: Cancelled order.', 'designthemes-travel').'</p>',
						'attributes'    => array(
						  'placeholder' => esc_attr__('Cancelled order', 'designthemes-travel')
						),
					  ),

					  array(
						'id'         => 'cancel-order-heading-desc',
						'type'       => 'text',
						'title'      => esc_html__('Email Heading Description', 'designthemes-travel'),
						'attributes'    => array(
						  'placeholder' => esc_html__('Order has been marked cancelled', 'designthemes-travel')
						),
					  ),

					  array(
						'id'           => 'cancel-order-email-format',
						'type'         => 'select',
						'title'        => esc_html__('Email Format', 'designthemes-travel'),
						'options'      => array(
						  'plain' => esc_html__('Plain Text', 'designthemes-travel'),
						  'html'  => esc_html__('HTML', 'designthemes-travel')
						),
						'class'        => 'chosen',
					  ),
					),
				),

				// -----------------------------------------
				// Payment Options
				// -----------------------------------------
				array(
				  'name'      => 'travel_payment_options',
				  'title'     => esc_html__('Payment Options', 'designthemes-travel'),
				  'icon'      => 'fa fa-paypal',

					'fields'      => array(

					  array(
						'type'    => 'subheading',
						'content' => esc_html__( 'Payment Options', 'designthemes-travel' ),
					  ),

					  array(
						'id'  		 => 'enable-booking',
						'type'  	 => 'switcher',
						'title' 	 => esc_html__('Guest Reservation', 'designthemes-travel'),
						'info'		 => esc_html__('Allows customers to checkout without creating an account.', 'designthemes-travel'),
						'default'	 => false,
					  ),

					  array(
						'id'  		 => 'offline-payment',
						'type'  	 => 'switcher',
						'title' 	 => esc_html__('Offline Payment', 'designthemes-travel'),
						'info'		 => esc_html__('Allows customers to made payments offline.', 'designthemes-travel'),
						'default'	 => false,
					  ),

					  array(
						'id'  		 => 'enable-paypal',
						'type'  	 => 'switcher',
						'title' 	 => esc_html__('PayPal', 'designthemes-travel'),
						'info'		 => esc_html__('Yes to enable PayPal live mode.', 'designthemes-travel'),
						'default'	 => true,
					  ),

					  array(
						'id'          => 'paypal-live-account',
						'type'        => 'text',
						'title'       => esc_html__('PayPal Email', 'designthemes-travel'),
						'desc'		  => esc_html__('Put the paypal business account email.', 'designthemes-travel'),
						'dependency'  => array( 'enable-paypal', '==', 'true' ),
					  ),

					  array(
						'id'  		 => 'enable-sandbox',
						'type'  	 => 'switcher',
						'title' 	 => esc_html__('PayPal Sandbox Mode', 'designthemes-travel'),
						'info'		 => esc_html__('Yes to enable PayPal sandbox mode.', 'designthemes-travel'),
						'default'	 => false,
					  ),

					  array(
						'id'          => 'paypal-sandbox-account',
						'type'        => 'text',
						'title'       => esc_html__('PayPal Sandbox Email', 'designthemes-travel'),
						'desc'		  => esc_html__('Put the paypal sandbox business account email.', 'designthemes-travel'),
						'dependency'  => array( 'enable-sandbox', '==', 'true' ),
					  ),

					),
				),

				// -----------------------------------------
				// Room Options
				// -----------------------------------------
				array(
				  'name'      => 'room_misc_options',
				  'title'     => esc_html__('Room Options', 'designthemes-travel'),
				  'icon'      => 'fa fa-home',

					'fields'      => array(

					  array(
						'type'    => 'subheading',
						'content' => esc_html__( 'Room Detail Options', 'designthemes-travel' ),
					  ),

					  array(
						'id'  		 => 'single-room-related',
						'type'  		 => 'switcher',
						'title' 		 => esc_html__('Show Related Rooms', 'designthemes-travel'),
						'label'		 => esc_html__('YES! to show related room items in single room.', 'designthemes-travel')
					  ),

					  array(
						'id'  		 => 'single-room-comments',
						'type'  		 => 'switcher',
						'title' 		 => esc_html__('Show Room Comment', 'designthemes-travel'),
						'label'		 => esc_html__('YES! to display comments in single rooms.', 'designthemes-travel'),
					  ),

					  array(
						'type'    => 'subheading',
						'content' => esc_html__( 'Room Archives Page Layout', 'designthemes-travel' ),
					  ),

					  array(
						'id'      	 => 'room-archives-page-layout',
						'type'         => 'image_select',
						'title'        => esc_html__('Page Layout', 'designthemes-travel'),
						'options'      => array(
						  'content-full-width'   => MAHARAJ_THEME_URI . '/cs-framework-override/images/without-sidebar.png',
						  'with-left-sidebar'    => MAHARAJ_THEME_URI . '/cs-framework-override/images/left-sidebar.png',
						  'with-right-sidebar'   => MAHARAJ_THEME_URI . '/cs-framework-override/images/right-sidebar.png',
						  'with-both-sidebar'    => MAHARAJ_THEME_URI . '/cs-framework-override/images/both-sidebar.png',
						),
						'default'      => 'content-full-width',
						'attributes'   => array(
						  'data-depend-id' => 'room-archives-page-layout',
						),
					  ),

					  array(
						'id'  		 => 'show-standard-left-sidebar-for-room-archives',
						'type'  		 => 'switcher',
						'title' 		 => esc_html__('Show Standard Left Sidebar', 'designthemes-travel'),
						'dependency'   => array( 'room-archives-page-layout', 'any', 'with-left-sidebar,with-both-sidebar' ),
					  ),

					  array(
						'id'  		 => 'show-standard-right-sidebar-for-room-archives',
						'type'  		 => 'switcher',
						'title' 		 => esc_html__('Show Standard Right Sidebar', 'designthemes-travel'),
						'dependency'   => array( 'room-archives-page-layout', 'any', 'with-right-sidebar,with-both-sidebar' ),
					  ),

					  array(
						'type'    => 'subheading',
						'content' => esc_html__( 'Room Archives Post Layout', 'designthemes-travel' ),
					  ),

					  array(
						'id'      	 => 'room-archives-post-layout',
						'type'         => 'image_select',
						'title'        => esc_html__('Post Layout', 'designthemes-travel'),
						'options'      => array(
						  'one-half-column'   => MAHARAJ_THEME_URI . '/cs-framework-override/images/one-half-column.png',
						  'one-third-column'  => MAHARAJ_THEME_URI . '/cs-framework-override/images/one-third-column.png',
						  'one-fourth-column' => MAHARAJ_THEME_URI . '/cs-framework-override/images/one-fourth-column.png',
						),
						'default'      => 'one-half-column',
					  ),

					  array(
						'id'           => 'room-archives-price-display',
						'type'         => 'select',
						'title'        => esc_html__('Price Display', 'designthemes-travel'),
						'options'      => array(
						  'min' => esc_html__('Min', 'designthemes-travel'),
						  'max'  => esc_html__('Max', 'designthemes-travel'),
						  'min_to_max'  => esc_html__('Min & Max', 'designthemes-travel'),
						),
						'class'        => 'chosen',
					  ),

					  array(
						'id'  		   => 'room-archives-rating',
						'type'  	   => 'switcher',
						'title' 	   => esc_html__('Show Rating', 'designthemes-travel')
					  ),

					  array(
						'type'    => 'subheading',
						'content' => esc_html__( 'Permalinks', 'designthemes-travel' ),
					  ),

					  array(
						'id'      => 'single-room-slug',
						'type'    => 'text',
						'title'   => esc_html__('Single Room Slug', 'designthemes-travel'),
						'after' 	=> '<p class="cs-text-info">'.esc_html__('Do not use characters not allowed in links. Use, eg. room-item ', 'designthemes-travel').'<br> <b>'.esc_html__('After made changes save permalinks.', 'designthemes-travel').'</b></p>',
					  ),

					  array(
						'id'      => 'room-types-slug',
						'type'    => 'text',
						'title'   => esc_html__('Room Type Slug', 'designthemes-travel'),
						'after' 	=> '<p class="cs-text-info">'.esc_html__('Do not use characters not allowed in links. Use, eg. room-type ', 'designthemes-travel').'<br> <b>'.esc_html__('After made changes save permalinks.', 'designthemes-travel').'</b></p>',
					  ),

					  array(
						'id'      => 'room-capacity-slug',
						'type'    => 'text',
						'title'   => esc_html__('Room Capacity Slug', 'designthemes-travel'),
						'after' 	=> '<p class="cs-text-info">'.esc_html__('Do not use characters not allowed in links. Use, eg. room-capacity ', 'designthemes-travel').'<br> <b>'.esc_html__('After made changes save permalinks.', 'designthemes-travel').'</b></p>',
					  ),

					  array(
						'id'      => 'room-service-slug',
						'type'    => 'text',
						'title'   => esc_html__('Room Service Slug', 'designthemes-travel'),
						'after' 	=> '<p class="cs-text-info">'.esc_html__('Do not use characters not allowed in links. Use, eg. room-service ', 'designthemes-travel').'<br> <b>'.esc_html__('After made changes save permalinks.', 'designthemes-travel').'</b></p>',
					  ),

					  array(
						'id'      => 'room-amenity-slug',
						'type'    => 'text',
						'title'   => esc_html__('Room Amenity Slug', 'designthemes-travel'),
						'after' 	=> '<p class="cs-text-info">'.esc_html__('Do not use characters not allowed in links. Use, eg. room-amenity ', 'designthemes-travel').'<br> <b>'.esc_html__('After made changes save permalinks.', 'designthemes-travel').'</b></p>',
					  ),

					),
				),

			  ),
			);

			return $options;
		}

		function dt_rooms_cs_framework_settings($settings){

			return $settings;
		}

		/**
		 *
		 * @param unknown $columns
		 * @return multitype:
		 */
		function dt_rooms_edit_columns($columns) {
			unset( $columns['taxonomy-dt_room_capacities'] );
			unset( $columns['taxonomy-dt_services'] );

			$newcolumns = array (
				"cb" => "<input type=\"checkbox\" />",
				"dt_room_thumb" => esc_html__("Image", 'designthemes-travel'),
				"title" => esc_html__("Title", 'designthemes-travel'),
				"author" => esc_html__("Author", 'designthemes-travel'),
				"dt_capacity" => esc_html__("Room Capacities", 'designthemes-travel'),
			);
			$columns = array_merge ( $newcolumns, $columns );
			return $columns;
		}

		/**
		 *
		 * @param unknown $columns
		 * @param unknown $id
		 */
		function dt_rooms_columns_display($columns, $id) {
			global $post;

			switch ($columns) {

				case "dt_room_thumb" :
				    $image = wp_get_attachment_image(get_post_thumbnail_id($id), array(75,75));
					if(!empty($image)):
					  	echo ($image);
				    else:
						$room_settings = get_post_meta ( $post->ID, '_room_settings', TRUE );
						$room_settings = is_array ( $room_settings ) ? $room_settings : array ();

						if( array_key_exists("room-gallery", $room_settings)) {
							$items = explode(',', $room_settings["room-gallery"]);
							echo wp_get_attachment_image( $items[0], array(75, 75) );
						}
					endif;
				break;

				case "dt_capacity" :
					$room_meta = get_post_meta( $id, '_room_settings', TRUE );
					$room_meta = is_array( $room_meta ) ? $room_meta : array();

					$room_adult = !empty($room_meta['room-adults']) ? $room_meta['room-adults'] : '';
					$term_meta = get_term_meta( $room_adult, '_room_capacity_options', false );
					$capacity = !empty($term_meta[0]['room_capacity']) ? $term_meta[0]['room_capacity'] : '';

					echo ($capacity).__(' Persons', 'designthemes-travel');
				break;
			}
		}
	}
}
?>