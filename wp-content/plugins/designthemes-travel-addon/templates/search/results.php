<?php
if ( !defined( 'ABSPATH' ) ) {
	exit();
}
?>
<div id="dt-room-search-results" class="dt-room-search-results"><?php
	// Getting search results...
	if( is_array( $results ) && count( $results ) >= 1 ):?>
        <ul class="dt-rooms-container"><?php
			$chkin_date = $_REQUEST['checkin_date'];
			$chkout_date = $_REQUEST['checkout_date'];

			foreach($results as $room_id => $item):

				$room_meta = get_post_meta( $room_id, '_room_settings', TRUE );
				$room_meta = is_array( $room_meta ) ? $room_meta : array(); ?>

	            <li class="dt-room">
                    <form name="dtfrm-select-room-item" class="dt-select-room-item">
                        <div class="dt-room-content"><?php
							if( has_post_thumbnail($room_id) ):
								echo '<div class="dt-room-thumb">';
									echo get_the_post_thumbnail( $room_id, 'medium' );
								echo '</div>';
							endif;?>
                            <div class="dt-room-info">
                                <h3 class="dt-room-name">
                                	<a href="<?php echo get_permalink($room_id);?>" title="<?php echo get_the_title($room_id);?>"><?php echo get_the_title($room_id);?></a>
                                </h3>
                                <div class="dt-room-meta">
                                    <div class="dt_room_capacity">
                                        <label><?php esc_html_e('Capacity:', 'designthemes-travel');?> </label>
                                        <span><?php echo $item['adults'];?></span>
                                    </div>
                                    <div class="dt_room_child">
                                        <label><?php esc_html_e('Max Child:', 'designthemes-travel');?> </label>
                                        <span><?php echo $item['childs'];?></span>
                                    </div>
                                    <div class="dt_room_quantity">
                                        <label><?php esc_html_e('Quantity:', 'designthemes-travel');?> </label>
                                        <span><?php echo dt_travel_qty_dropdown( $item['qty'] );?></span>
                                    </div>
                                    <div class="dt_room_price">
                                        <label><?php esc_html_e('Price:', 'designthemes-travel');?> </label>
                                        <span class="dt_room_item_price"><?php
											$price = dt_travel_get_price_between_dates($chkin_date, $chkout_date, $room_id);
                                        	echo dt_travel_get_formatted_price($price);?></span>
                                        <div class="dt_view_price">
                                        	<a href="#" class="dt-view-booking-room-details"><?php esc_html_e('(View price breakdown)', 'designthemes-travel');?></a>
                                            <div class="dt-booking-room-details">
                                            	<?php dt_travel_get_pricing_breakdown($chkin_date, $chkout_date, $room_id);?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="dt_addon_package_extra"><?php echo dt_travel_addon_services_checkboxes($room_id);?></div>
                                    <div class="dt_room_add_to_cart"><button class="dt_add_to_cart"><?php esc_html_e('Select this room', 'designthemes-travel');?></button></div>
                                </div>
                            </div>
                        </div>
                        <input type="hidden" name="check_in_date" value="<?php echo esc_attr($chkin_date);?>">
                        <input type="hidden" name="check_out_date" value="<?php echo esc_attr($chkout_date);?>">
                        <input type="hidden" name="room-id" value="<?php echo esc_attr($room_id);?>">
                    </form>
	            </li><?php
			endforeach;?>
        </ul><?php
    else:
		echo '<div class="dt-sc-error-box">'.esc_html__('There is no rooms were found, with your search criteria. Please try again with different dates', 'designthemes-travel').'</div>';
	endif;?>
</div>