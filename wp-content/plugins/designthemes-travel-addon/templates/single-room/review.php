<?php
if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directlysettings
}

$rating   = intval( get_comment_meta( $comment->comment_ID, 'rating', true ) );
?>
<li itemprop="review" itemscope itemtype="http://schema.org/Review" <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">

    <div id="comment-<?php comment_ID(); ?>" class="comment_container">

		<?php echo get_avatar( $comment, apply_filters( 'dt_room_review_gravatar_size', '60' ), '' ); ?>

        <div class="comment-text">

			<div class="comment-title-meta">
				<?php if ( $comment->comment_approved == '0' ) : ?>
    
                    <p class="meta"><em><?php _e( 'Your comment is awaiting approval', 'designthemes-travel' ); ?></em></p>
    
                <?php else : ?>
    
                    <p class="meta">
                        <strong itemprop="author"><?php comment_author(); ?></strong> &ndash;
                        <time itemprop="datePublished" datetime="<?php echo get_comment_date( 'c' ); ?>"><?php echo get_comment_date( 'd M Y' ); ?></time>
                    </p>
    
                <?php endif; ?>
    
                <?php if ( $rating && cs_get_option( 'room-archives-rating' ) ) : ?>
    
                    <div class="rating">
    
                        <div class="star-rating" title="<?php echo sprintf( __( 'Rated %0.2f out of 5', 'designthemes-travel' ), $rating ) ?>">
                            <span style="width:<?php echo ( $rating / 5 ) * 100; ?>%"></span>
                        </div>
    
                    </div>
    
                <?php endif; ?>
            </div>

            <div itemprop="description" class="description"><?php comment_text(); ?></div>
        </div>
    </div>
</li>