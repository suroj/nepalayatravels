<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php wp_head(); ?>
    <style>
        .new-header{
           padding: 15px 0px; 
        }
        .new-header .logo{
           margin-top: 10px; 
        }
        .new-header .address{
           padding-left: 40px;
           position: relative;
           margin-top: 15px;
        }
        .new-header .address:before{
           position: absolute;
           top: 15px;
           left:0px;
           font-family: FontAwesome;
           content: '\f041';
           font-size: 36px;
           color: #ccc;
       }
       .new-header .phone{
           padding-left: 40px;
           position: relative;
           margin-top: 15px;
        }
        .new-header .phone:before{
           position: absolute;
           top: 15px;
           left:0px;
           font-family: FontAwesome;
           content: '\f095';
           font-size: 36px;
           color: #ccc;
       }
       .new-header .new-nav{
           background: #2388D3;
           position: relative;
           top: 40px;
           z-index: 99999999;
           border-radius: 4px;
       }
       .new-header .new-nav ul{
           list-style: none;
           display: inline-block;
           width: 100%;
           margin: 0px;
       }
       .new-header .new-nav ul li{
           display: inline-block;
           position: relative;
           padding: 5px;
       }
       .new-header .new-nav ul li a{
           padding: 10px 15px;
           display: block;
           color: #FFFFFF;
           border-right: dashed 1px #FFFFFF;
       }
       .new-header .new-nav ul li ul{
           position: absolute;
           display: none;
       }
       .new-header .new-nav ul li:hover ul{
           display: inline-block;
           position: absolute;
           width: 280px;
           background: #333333;
           z-index:9999;
           padding: 0px;
       }
       .new-header .new-nav ul li:hover ul li{
           color: #ffffff;
           display: inline-block;
           width: 100%;
           border-bottom: 1px dashed #666;
       }
       .new-header .new-nav ul li:hover ul li a{
           color: #ffffff;
           padding: 5px 15px;
           border: 0px;
       }
       #header{
           display: none;
       }
       @media screen and (max-width: 600px) {
          .new-header .new-nav ul li{
              width: 100%;
          }
          .new-header .new-nav ul li a{
              border-right: 0px;
              border-bottom: dashed 1px #FFFFFF;
          }
          .new-header .new-nav ul li:last-child a{
              border-right: 0px;
              border-bottom: 0px;
          }
        }
    </style>
</head>

<body <?php body_class(); ?>>
    <?php
    
        /**
         * maharaj_hook_top hook.
         *      maharaj_hook_top - 10
         *      maharaj_page_loader - 20
         */
          // do_action( 'maharaj_hook_top' );
    ?>
    <div class="new-header">
        <div class="container">
            <div class="vc_col-sm-3">
                <img src="http://www.wpagedesign.co.uk/nepalaya/wp-content/uploads/2018/04/nepa_logo.png" class="img-responsive logo">
            </div>
            <div class="vc_col-sm-3">
                <div class="address">
                    177 Vine ST Unit 2, EV<br>
                    Boston, USA
                </div>
            </div>
            <div class="vc_col-sm-3">
                <div class="phone">
                    USA: +1 (617) 517-3072<br>
                    Viber: +977-1-9851102430
                </div>
            </div>
            <div class="vc_col-sm-3">
                <div class="phone">
                    Nepal Office<br>
                    +977-1-4414670, 4005270
                </div>
            </div>
        </div>
        <div class="container">
            <div class="new-nav">
                <div class="menu-container">
                <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
                </div>
            </div>
        </div>
    </div>
    <!-- **Wrapper** -->
    <div class="wrapper">
    
        <!-- ** Inner Wrapper ** -->
        <div class="inner-wrapper">    
