<?php
if ( !defined( 'ABSPATH' ) ) {
	exit();
}
?>
<h3><?php esc_html_e( 'Customer Details', 'designthemes-travel' ); ?></h3>
<div class="dt-customer clearfix">
	<?php dt_travel_get_template( 'checkout/customer-existing.php', array( 'customer' => $customer ) ); ?>
	<?php dt_travel_get_template( 'checkout/customer-new.php', array( 'customer' => $customer ) ); ?>
</div>
<div class="dt-sc-hr-invisible-small"></div>