<?php
if (! class_exists ( 'DTTravelAddonTemplates' )) {

	class DTTravelAddonTemplates {

		function __construct() {

			add_action( 'init', array(
				$this,
				'dt_travel_booking_received_endpoint'
			) );

			add_filter ( 'template_include', array (
				$this,
				'dt_travel_template_include'
			) );
			
			add_filter ( 'template_redirect', array (
				$this,
				'dt_travel_template_redirect'
			) );
		}

		function dt_travel_booking_received_endpoint() {

			add_rewrite_endpoint( 'thank-you', EP_PERMALINK | EP_PAGES );
			add_rewrite_tag( 'key', '([^&]+)' );

			flush_rewrite_rules();
		}

		function dt_travel_template_include($template) {

			$post_type = get_post_type();

			$file = '';
			$find = array();
			if ( $post_type !== 'dt_rooms' && false !== get_query_var( 'thank-you', false ) ) {
				return plugin_dir_path ( __FILE__ ) . '/checkout/thank-you.php';
			}

			if ( is_post_type_archive( 'dt_rooms' ) ) {
				$file = 'archive-room.php';
				$find[] = $file;
				$find[] = DTTRAVELADDON_PATH . '/' . $file;
			} else if ( is_room_taxonomy() ) {
				$term = get_queried_object();
				$taxonomy = $term->taxonomy;
				if ( strpos( $term->taxonomy, 'dt_room_' ) === 0 ) {
					$taxonomy = substr( $term->taxonomy, 3 );
				}

				if ( is_tax( 'dt_room_types' ) || is_tax( 'dt_room_capacities' ) ) {
					$file = 'taxonomy-' . $taxonomy . '.php';
				} else {
					$file = 'archive-room.php';
				}

				$find[] = 'taxonomy-' . $taxonomy . '-' . $term->slug . '.php';
				$find[] = DTTRAVELADDON_PATH . '/taxonomy-' . $taxonomy . '-' . $term->slug . '.php';
				$find[] = 'taxonomy-' . $term->taxonomy . '.php';
				$find[] = DTTRAVELADDON_PATH . '/taxonomy-' . $taxonomy . '.php';
				$find[] = $file;
			} else if ( is_singular('dt_rooms') ) {
				$file = 'single-room.php';
				$find[] = $file;
				$find[] = DTTRAVELADDON_PATH . '/' . $file;
			}

			if ( $file ) {
				$find[] = DTTRAVELADDON_PATH . '/' . $file;
				$dt_template = untrailingslashit( DTTRAVELADDON_PATH ) . '/templates/' . $file;
				$template = locate_template( array_unique( $find ) );

				if ( !$template && file_exists( $dt_template ) ) {
					$template = $dt_template;
				}
			}

			return $template;
		}

		function dt_travel_template_redirect() {

			if ( dt_travel_get_page_id( 'checkout' ) && is_page( dt_travel_get_page_id( 'checkout' ) ) && (!array_key_exists('dt_room_cart_items', $_SESSION) || empty( $_SESSION['dt_room_cart_items'] )) ) {
				wp_redirect( dt_travel_cart_url() );
				exit();
			}
		}
	}
} ?>