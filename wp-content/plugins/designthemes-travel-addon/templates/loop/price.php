<?php
/**
 * Loop Price
 *
 * @author        designthemes
 * @package       designthemes-travel-addon/templates
 * @version       1.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $post;
$price_display = cs_get_option( 'room-archives-price-display' );

$room_meta = get_post_meta($post->ID,'_room_settings',TRUE);
$room_meta = is_array($room_meta) ? $room_meta  : array();

$prices = isset( $room_meta['regular_prices'] ) ? $room_meta['regular_prices'] : array();
?>

<?php if ( $prices ): ?>
	<?php
	$min = min( $prices );
	$max = max( $prices );
	if( $min == '' )
		$min = 0;
	if( $max == '' )
		$max = 0;
	?>
    <div class="price">
		<?php if ( $price_display === 'max' ): ?>

            <span class="price_value price_max"><?php echo dt_travel_get_formatted_price( $max ) ?></span>

		<?php elseif ( $price_display === 'min_to_max' && $min !== $max ): ?>

            <span class="price_value price_min_to_max">
				<span class="price-max"><del><?php echo dt_travel_get_formatted_price( $max ) ?></del></span>
                <span class="price-min"><?php echo dt_travel_get_formatted_price( $min ) ?></span>
			</span>

		<?php else: ?>

            <span class="price_value price_min"><?php echo dt_travel_get_formatted_price( $min ) ?></span>

		<?php endif; ?>
        <span class="unit"><?php esc_html_e( 'per / night', 'designthemes-travel' ); ?></span>
    </div>
<?php endif; ?>